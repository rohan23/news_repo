<?php
$uid=Session::get('user_id');
if($uid=="")
{
	$user_id="0";
}
else{
	$user_id=$uid;
}

?>

				
					<div class="row">
						
						<?php
						if($local_data['status']['message']=="Successful")
						{
							if(array_key_exists('news', $local_data))
							{
						$k=count($local_data['news']); 
						for($j=0;$j<$k;$j++)
						{
							 $lo_news_id= $local_data['news'][$j]['id'];
							 $lo_category_id= $local_data['news'][$j]['category'];
							 $lo_news_title= $local_data['news'][$j]['news_title'];
							 $lo_user_id= $local_data['news'][$j]['user_id'];
							 $lo_image1= $local_data['news'][$j]['image1'];
							 $lo_likes= $local_data['news'][$j]['like'];
							 $lo_time= $local_data['news'][$j]['news_time'];
							 $lo_distance= $local_data['news'][$j]['news_distance'];
							 //$date_only=explode(" ",$lo_time);
							 //$date=$date_only[0];
							
							 $now1 = new DateTime();
							$ts11 = strtotime($lo_time);
							$ts22 = $now1->getTimestamp();
							$diff = $ts22 - $ts11;
							$hourdiff1 = round(( $now1->getTimestamp()-strtotime($lo_time))/3600, 1);
							$local_days=intval($hourdiff1/24);
							
							if($local_days=="0")
							{
								$l_days="Today";
							}
							else if($local_days=="1")
							{
								$l_days="Yesterday";
							}
							else 
							{
								$l_days=$local_days." days ago";	
							}
							
							 $lo_news = substr($lo_news_title,0,50);
							 
							 
						?>
							<div class="col-sm-4">
								<div class="post feature-post" id="clearfix">
									<div class="entry-header">
										<div class="entry-thumbnail">
											
											<a href="{!! route('news.route', ['news_id'=>$lo_news_id]) !!}"><img style="height:280px; width: 340px;" class="img-responsive" src="{{asset('admin/images/').'/'.$lo_image1}}" alt="" /></a>
										</div>
										
									</div>
									<div class="post-content">								
										<div class="entry-meta">
											<ul class="list-inline" style="font-size: 12px;">
												<i class="fa fa-location-arrow" aria-hidden="true"></i><?php echo $lo_distance; ?>km &nbsp;
												<li class="publish-date"><i class="fa fa-clock-o"></i><?php echo $l_days; ?></li>
												<!-- <li class="views"><i class="fa fa-eye"></i><?php echo $lo_likes; ?></li> -->
												<?php 
													if($lo_user_id==$user_id)
													{
													?>
													<li class="loves"><a href="{!! route('news.delete', ['news_id'=>$lo_news_id]) !!}"><i class="fa fa-trash"></i></a></li>
													<?php	
													}
												?>	
											</ul>
										</div>
										<h2 class="entry-title">
											<a href="{!! route('news.route', ['news_id'=>$lo_news_id]) !!}"><?php echo $lo_news; ?></a>
										</h2>
									</div>
								</div><!--/post--> 	
							</div>
							
							<?php }} } ?>
								
						</div>
							<div class="row" style="text-align:right; padding:40px;" id="hey" >
								<div class="col-md-6"></div>
							<div onclick="locational_news('<?php echo $index+1; ?>')" class="col-md-6" style="padding: 10px;text-align: center;background-color: #33739E;border-style: solid;border-color: #fff;">
								<a  style="color:#fff;">Load More &nbsp;<i class="fa fa-long-arrow-down" aria-hidden="true"></i></a></div>
						
			
		</div>
								
					
					
<?php ?>