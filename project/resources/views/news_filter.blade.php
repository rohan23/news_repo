@extends('layout.base', ['select' => 'about'])

  @section('body')
 <script type="text/javascript">
$(document).ready(function(){
    $('input[type="radio"]').click(function(){
        if($(this).attr("value")=="primary"){
        	$("#secondary").hide();
            $("#primary").show();
        }
        if($(this).attr("value")=="secondary"){
        	$("#primary").hide();
            $("#secondary").show();
            
        }
    });
});
</script>



<style>
	body
	{
		background-color:#fff;
	}
</style>
<?php

$days=Session::get('days');

$radius=Session::get('radius');

$radius=Session::get('primery_filter');
?>
	<div class="row" style="padding-top: 30px; margin:0px; text-align: center; font-size: 20px;">
	
	  	<big>Select Primary Filter</big> 
        &nbsp;<label><input type="radio" name="colorRadio" value="primary" checked="checked"> Radius </label>
        <label><input type="radio" name="colorRadio" value="secondary"> Time</label>

    </div>
    
<div id="primary">
	<form action="filters" method="post">
​<div class="container" id="s_radius">
	<div class="col-md-3"></div>
	<div class="col-md-6">
  <h2>Primary Sorting</h2>
  
  <input type="hidden" value="radius" name="primery_filter">
  <ul class="nav nav-tabs">
     <li class="active"><a data-toggle="tab" href="#home">Radius</a></li>
     <li><a >Time</a></li>
  </ul>
​ <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
      <div class="form-group">
			<label>Select Radius</label>
			<select class="form-control" name="radius" > 
				<option value="1000000.0"> No Limit</option>
				<option value="1.0">1.0 km</option>
				<option value="5.0">5.0 km</option>
				<option value="15.0">15.0 km</option>
				<option value="30.0">30.0 km</option>
				<option value="70.0"> 70.0 km</option>
				<option value="100.0">100.0 km</option>
				<option value="500.0"> 500.0 km</option>
				<option value="10000.0"> 10000.0 km</option>
			
			
			
			</select>
		</div>
      
    </div>
  <br>
<br>
<br>
  
  </div>
</div>
<div class="col-md-3"></div>
​  </div>


<div class="container" id="s_time" >
	<div class="col-md-3"></div>
	<div class="col-md-6">
<h2>Secondary Sorting</h2>
  <ul class="nav nav-tabs">
     <li><a>Radius</a></li>
     <li class="active"><a data-toggle="p_tab" href="#p_menu">Time</a></li>
  </ul>
​ <div class="tab-content">
    
    <div id="p_menu" class="tab-pane fade in active">
        <div class="form-group">
			<label>Select Time</label>
			<select class="form-control"  name="days"> 
				<option value="1000000.0"> No Limit</option>
				<option value="24.0">1 days</option>
				<option value="48.0">2 days</option>
				<option value="72.0">3 days</option>
				<option value="120.0">5 days</option>
				<option value="240.0">10 days</option>
				<option value="360.0">15 days</option>
				<option value="480.0">20 days</option>
			
			
			
			</select>
		</div>
      
    </div>
  
  </div>
</div>
</div>
<div class="col-md-3"></div>
	<div class="submit-button text-center" style="padding-bottom: 30px;">
		<button type="submit" class="btn btn-primary">Submit</button>
	</div>
</div>

</form>
<!--- SECONDRY Filter -->
<form action="filters" method="post">
<div id="secondary" style="display:none;">
	​<div class="container" id="s_radius">
	<div class="col-md-3"></div>
	<div class="col-md-6">
  <h2>Primary Sorting</h2>
   <input type="hidden" value="days" name="primery_filter">
  <ul class="nav nav-tabs">
  	<li><a>Radius</a></li>
     <li class="active"><a data-toggle="p_tab" href="#p_menu">Time</a></li>
    
  </ul>
​ <div class="tab-content">
	 <div id="p_menu" class="tab-pane fade in active">
      <div class="tab-content">
     <div class="form-group">
			<label>Select Time</label>
			<select class="form-control" name="days" > 
				<option value="1000000.0"> No Limit</option>
				<option value="24.0">1 days</option>
				<option value="48.0">2 days</option>
				<option value="72.0">3 days</option>
				<option value="120.0">5 days</option>
				<option value="240.0">10 days</option>
				<option value="360.0">15 days</option>
				<option value="480.0">20 days</option>
			
			
			
			</select>
		</div>
  
  </div>
      
    </div>
 
  <br>
<br>
<br>
  
  </div>
</div>
<div class="col-md-3"></div>
​  </div>


<div class="container" id="s_time" >
	<div class="col-md-3"></div>
	<div class="col-md-6">
<h2>Secondary Sorting</h2>
  <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#home">Radius</a></li>
     <li><a >Time</a></li>
  </ul>
​ <div class="tab-content">
     <div class="form-group">
			<label>Select Radius</label>
			<select class="form-control"  name="radius" > 
				<option value="1000000.0"> No Limit</option>
				<option value="1.0">1.0 km</option>
				<option value="5.0">5.0 km</option>
				<option value="15.0">15.0 km</option>
				<option value="30.0">30.0 km</option>
				<option value="70.0"> 70.0 km</option>
				<option value="100.0">100.0 km</option>
				<option value="500.0"> 500.0 km</option>
				<option value="10000.0"> 10000.0 km</option>
			
			
			
			</select>
		</div>
  
  </div>
</div>
</div>
	<div class="col-md-3"></div>
		<div class="submit-button text-center" style="padding-bottom: 30px;">
			<button type="submit" class="btn btn-primary">Submit</button>
		</div>
	</div>

</form>
@stop