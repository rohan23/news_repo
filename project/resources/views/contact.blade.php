@extends('layout.base', ['select' => 'contact'])

@section('body')
<!--Start Banner-->
   <div class="container">
			<div class="page-breadcrumbs">
				<h1 class="section-title title">Contact Us</h1>
			</div>
			<div class="contact-us contact-page-two">
				<style>
					.gm-style .place-card-large{
						display:none;
					}
				</style>
				<div class="map-section">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d8149950.465032703!2d105.12024501138558!3d4.127925841932924!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3034d3975f6730af%3A0x745969328211cd8!2sMalaysia!5e0!3m2!1sen!2smy!4v1478009861419" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
					<!--<div id="gmap"></div>-->
				</div>
				<div class="contact-info">	
					<h1 class="section-title title">Contact Information</h1>
					<ul class="list-inline">
						<li>
							<h2>Head Office</h2>
							<address>
								23-45A, Silictown <br>Great Country
								<p class="contact-mail"><strong>Email:</strong> <a href="#">hello@nearbynews.com</a></p>
								<p><strong>Call:</strong> +123 123 456 789</p>
							</address>
						</li>
						<li>
						   <h2>USA Office</h2>
							<address>
								245 North Street, <br>New York, NY
								<p class="contact-mail"><strong>Email:</strong> <a href="#">info@usa-nearbynews.com</a></p>
								<p><strong>Call:</strong> +123 123 456 789</p>
							</address>
						</li>
						<li>
						   <h2>UK Office</h2>
							<address>
								123, Pall Mall,<br> London England
								<p class="contact-mail"><strong>Email:</strong> <a href="#">info@uk-nearbynews.com</a></p>
								<p><strong>Call:</strong> +123 123 456 789</p>
							</address>
						</li>
					</ul>
				</div>
				<div class="message-box">
					<h1 class="section-title title">Drop Your Message</h1>
					<form id="comment-form" name="comment-form" method="post">
						<div class="row">
							<div class="col-sm-4">
								<div class="form-group">
									<label for="name">Name</label>
									<input type="text" name="name" class="form-control" required="required">
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									<label for="email">Email</label>
									<input type="email" name="email" class="form-control" required="required">
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									<label for="subject">Subject</label>
									<input type="subject" name="subject" class="form-control">
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<label for="comment" >Your Text</label>
									<textarea name="comment" id="comment" required="required" class="form-control" rows="5"></textarea>
								</div>
								<div class="text-right">
									<button type="button" class="btn btn-primary">Send </button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div><!-- contact-us -->
		</div><!--/.container-->
	</div><!--/#main-wrapper--> 
    <!--End Content-->
@stop
