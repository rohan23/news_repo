@extends('layout.base')

<script>
	function load_more(val1, val2, val3)
	{
		
		 jQuery.ajax({
		     url: '/loadmore',
		     type: 'POST',
		     data: {category_name: val1,user_id:val2,index:val3},
		     success: function (data) {
		 	 jQuery("#load_data").append(data);
		 	 $('#hey').remove();
		   //alert('success');
		     }

		 });
	}
</script>

@section('body')
<?php 
$uid=Session::get('user_id');
if($uid=="")
{
	$user_id="0";
}
else{
	$user_id=$uid;
}

$f_type=Session::get('primery_filter');
//echo "<pre>"; print_r($promotion); echo "</pre>";

function array_orderby()
{
    $args = func_get_args();
    $data = array_shift($args);
    foreach ($args as $n => $field) {
        if (is_string($field)) {
            $tmp = array();
            foreach ($data as $key => $row)
                $tmp[$key] = $row[$field];
            $args[$n] = $tmp;
            }
    }
    $args[] = &$data;
    call_user_func_array('array_multisort', $args);
    return array_pop($args);
}
if($f_type!="")
{
	if($f_type=="radius")
	{
		if(array_key_exists("locational_news", $get_list))
		{	
			$get_list['locational_news'] = array_orderby($get_list['locational_news'], 'news_distance', SORT_ASC);
		}
		if(array_key_exists("general_news", $get_list))
		{
			$get_list['general_news'] = array_orderby($get_list['general_news'], 'time_diff', SORT_ASC);
		}
		if(array_key_exists("news", $promotion))
		{
			$promotion['news'] = array_orderby($promotion['news'], 'news_distance', SORT_ASC);
		}
	}
	else
	{
		if(array_key_exists("locational_news", $get_list))
		{
			$get_list['locational_news'] = array_orderby($get_list['locational_news'], 'time_diff', SORT_ASC);
		}
		if(array_key_exists("general_news", $get_list))
		{
			$get_list['general_news'] = array_orderby($get_list['general_news'], 'time_diff', SORT_ASC);
		}
	}
}
/*
	echo "<pre>";
 print_r($get_list);
 echo "</pre>";*/

?>
<style>
	.post-content
	{
	 min-height:100px; 
	 max-height: 100px; 
	 height: 100px;
	}
.show_more_main {
margin: 15px 25px;
}
.show_more {
background-color: #f8f8f8;
background-image: -webkit-linear-gradient(top,#fcfcfc 0,#f8f8f8 100%);
background-image: linear-gradient(top,#fcfcfc 0,#f8f8f8 100%);
border: 1px solid;
border-color: #d3d3d3;
color: #333;
font-size: 12px;
outline: 0;
}
.show_more {
cursor: pointer;
display: block;
padding: 10px 0;
text-align: center;
font-weight:bold;
}


</style>
<p style="display: none;">{!! $ureg_id=Session::get('user_id') !!}</p>
<p style="display: none;">{!! $login_user_id=Session::get('user_id') !!}</p>	
<div class="container">
	<div class="section">
		<div class="row">
			<div class="col-sm-9" >
				<div class="page-breadcrumbs">
					<h1 class="section-title">{!! $cat_name!!}</h1>	
				</div>
				<div id="site-content" class="site-content">
				</div><!--/#site-content-->
				<div class="row">
					<div class="col-sm-12" >
						<div class="section">
							<div class="row" id="load_data">
								<?php
								if($get_list['status']['message']=="Successful")
								{
									
									$count=count($get_list['locational_news']);
									for($i=0;$i<$count;$i++)
									{
									 $lid= $get_list['locational_news'][$i]['id'];
									 $news_title= $get_list['locational_news'][$i]['news_title'];
									 $news_text= $get_list['locational_news'][$i]['news_text'];
									 
									 $like= $get_list['locational_news'][$i]['like'];
									 $comment= $get_list['locational_news'][$i]['comment'];
									 $image1= $get_list['locational_news'][$i]['image1'];
									 $report_user_id=$get_list['locational_news'][$i]['user_id'];
									 $news_time=$get_list['locational_news'][$i]['news_time'];
									 $news_distance=$get_list['locational_news'][$i]['news_distance'];
									 $news_title = substr($news_title,0,80);
									 
									   $now1 = new DateTime();
												$ts11 = strtotime($news_time);
												$ts22 = $now1->getTimestamp();
												$diff = $ts22 - $ts11;
												$hourdiff1 = round(( $now1->getTimestamp()-strtotime($news_time))/3600, 1);
												$category_days=round($hourdiff1/24);
												
												if($category_days=="0")
												{
													$days="Today";
												}
												else if($category_days=="1")
												{
													$days="Yesterday";
												}
												else 
												{
													$days=$category_days." days ago";	
												}
								?>
								
								<div class="col-sm-4" >
									<div class="post medium-post">
										<div class="entry-header">
											<div class="entry-thumbnail">
												<a href="{!! route('news.route', ['news_id'=>$lid]) !!}"><img class="center-cropped img-responsive" src="{{asset('admin/images/').'/'.$image1}}" alt="" style="width: 100%; height: 180px;" /></a>
											</div>
										</div>
										<div class="post-content">								
											<div class="entry-meta">
												<ul class="list-inline">
													<li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i> <?php echo $days; ?></a></li>
													<li class="loves"><i class="fa fa-location-arrow" aria-hidden="true"></i><?php echo $news_distance; ?>km</li>	
													<li class="views"><i class="fa fa-thumbs-o-up"></i><?php echo $like; ?></li>
													<li class="loves"><i class="fa fa fa-comment-o"></i><?php echo $comment; ?></li>
													<?php 
													if($user_id==$report_user_id)
													{
													?>
													<li class="loves"><a href="{!! route('news.delete', ['news_id'=>$lid]) !!}"><i class="fa fa-trash"></i></a></li>
													<?php	
													}
													?>
												</ul>
											</div>
											<h2 class="entry-title">
												<a href="{!! route('news.route', ['news_id'=>$lid]) !!}"><?php echo $news_title; ?> </a>
											</h2>
										</div>
									</div><!--/post-->
								</div>
							
								<?php 

									} 
									$count_general=count($get_list['general_news']);
									for($y=0;$y<$count_general;$y++)
									{
										 $id_general= $get_list['general_news'][$y]['id'];
										 $news_title_general= $get_list['general_news'][$y]['news_title'];
										 $news_text_general= $get_list['general_news'][$y]['news_text'];
										 
										 $like_general= $get_list['general_news'][$y]['like'];
										 $comment_general= $get_list['general_news'][$y]['comment'];
										 $image1_general= $get_list['general_news'][$y]['image1'];
										 $report_user_id_general=$get_list['general_news'][$y]['user_id'];
										 $general_country=$get_list['general_news'][$y]['country'];
										 $general_time=$get_list['general_news'][$y]['news_time'];
										
										 $now1 = new DateTime();
												$ts11 = strtotime($general_time);
												$ts22 = $now1->getTimestamp();
												$diff = $ts22 - $ts11;
												$hourdiff1 = round(( $now1->getTimestamp()-strtotime($general_time))/3600, 1);
												$general_days=round($hourdiff1/24);
												
									?>	
									<div class="col-sm-4">
									<div class="post medium-post">
										<div class="entry-header">
											<div class="entry-thumbnail">
												<a href="{!! route('news.route', ['news_id'=>$id_general]) !!}"><img class="center-cropped img-responsive" src="{{asset('admin/images/').'/'.$image1_general}}" alt="" style="min-width: 100%;max-height: 170px;" /></a>
											</div>
										</div>
										<div class="post-content">								
											<div class="entry-meta">
												<ul class="list-inline">
												<li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i> <?php echo $general_days; ?> days ago</a></li>
												<li class="publish-date"><i class="fa fa-globe" aria-hidden="true"></i><?php echo $general_country; ?></li>	
												<li class="views"><i class="fa fa-thumbs-o-up"></i><?php echo $like_general; ?></li>
												<li class="loves"><i class="fa fa fa-comment-o"></i><?php echo $comment_general; ?></li>
												<?php 
												if($login_user_id==$report_user_id_general)
												{
												?>
												<li class="loves"><a href="{!! route('news.delete', ['news_id'=>$id_general]) !!}"><i class="fa fa-trash"></i></a></li>
												<?php	
												}
												?>
												
											</ul>
											</div>
											<h2 class="entry-title">
												<a href="{!! route('news.route', ['news_id'=>$id_general]) !!}"><?php echo $news_title_general; ?> </a>
											</h2>
										</div>
									</div><!--/post-->
								</div>
																
								<?php
									}
									
									} 
									else
									{?> 
									<div class="post">
										<h2 class="entry-title" style="text-align: center;">
											News Not Available
										</h2>
									</div><!--/post--> 
									<?php }
								?>
							<div class="row" id="hey">
								<div class="col-md-12">
									<?php
											
												$search =  '!"#$%&/()=?*+\'-.,;:_' ;
												$search = str_split($search);
												if($login_user_id=="")
												{
													$user_reg_id=0;
													
												}
												else
												{
													$user_reg_id=$login_user_id;
													$in=0;	
												}
												
												$categories_name=str_replace($search, " or ", $cat_name)
											
												?>
								 <div class="show_more_main" id="show_more_main">
			        				<span  onclick="load_more('<?php echo $categories_name; ?>' , <?php echo $user_reg_id; ?> , <?php echo $index+1; ?>)" class="show_more" title="Load more posts">Show more</span>
			    				</div>
							</div>
						</div>	
						</div><!--/.section -->	
					
					</div>
				</div>
				
				</div>
			</div><!--/.col-sm-9 -->	
			
			<div class="col-sm-3">
				<div id="sitebar">
					
						
						<div class="widget">
								<h1 class="section-title title">Promotions</h1>
								<ul class="post-list">
									<?php
								if($promotion['status']['message']=="Successful")
								{
									
									$count=count($promotion['news']);
									for($i=0;$i<$count;$i++)
									{
									 $p_id= $promotion['news'][$i]['id'];
									 $title= $promotion['news'][$i]['title'];
									$image1= $promotion['news'][$i]['image1'];
									  $time= $promotion['news'][$i]['time'];
									  
									 $news_distance= $promotion['news'][$i]['news_distance'];
							
										
										
									 $news_title = substr($title,0,70);
									 
									 	$now1 = new DateTime();
										$ts11 = strtotime($time);
										$ts22 = $now1->getTimestamp();
										$diff = $ts22 - $ts11;
										$hourdiff11 = round(( $now1->getTimestamp()-strtotime($time))/3600, 1);
										//$days=ceil($hourdiff1/24);
										
										
											$adays=round($hourdiff11/24);
				
										if($adays=="0")
										{
											$pdays="Today";
										}
										else if($adays=="1")
										{
											$pdays="Yesterday";
										}
										else 
										{
											$pdays=$adays." days";	
										}
								?>
									<li>
										<div class="post small-post">
											<div class="entry-header">
												<div class="entry-thumbnail">
													<a href="{!! route('promotion.route', ['p_id'=>$p_id]) !!}"><img class="img-responsive" src="{{asset('admin/images/').'/'.$image1}}" alt="" /> </a>
												</div>
											</div>
											<div class="post-content">								
												<div class="video-catagory"><?php echo $pdays; ?></div>
												<div class="video-catagory"><?php echo $news_distance; ?>Km</div>
												<h2 class="entry-title">
													<a href="{!! route('promotion.route', ['p_id'=>$p_id]) !!}"><?php echo $news_title ?></a>
												</h2>
											</div>
										</div><!--/post--> 
									</li>
								
									<?php  } } ?>
								
								
									
									
								</ul>
							</div><!--/#widget-->
							
							
							
						
					</div><!--/#widget-->
				</div><!--/#sitebar-->
				
		
			</div>
		</div>				
	</div><!--/.section-->
</div><!--/.container-->

	@stop
