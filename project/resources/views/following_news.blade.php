@extends('layout.base')

@section('body')



<?php 
$uid=Session::get('user_id');
if($uid=="")
{
	$user_id="0";
}
else{
	$user_id=$uid;
}

$f_type=Session::get('primery_filter');


function array_orderby()
{
    $args = func_get_args();
    $data = array_shift($args);
    foreach ($args as $n => $field) {
        if (is_string($field)) {
            $tmp = array();
            foreach ($data as $key => $row)
                $tmp[$key] = $row[$field];
            $args[$n] = $tmp;
            }
    }
    $args[] = &$data;
    call_user_func_array('array_multisort', $args);
    return array_pop($args);
}
if($f_type!="")
{
if($f_type=="radius")
{
	if(array_key_exists("follower_news", $following_data))
	{
		$following_data['follower_news'] = array_orderby($following_data['follower_news'], 'distance', SORT_ASC);
	}
}
else
{
	if(array_key_exists("follower_news", $following_data))
	{
		$following_data['follower_news'] = array_orderby($following_data['follower_news'], 'distance', SORT_ASC);
	}
}
}
?>
<style>
	.post-content
	{
	 min-height:100px; 
	 max-height: 100px; 
	 height: 100px;
	}
</style>
<p style="display: none;">{!! $login_user_id=Session::get('user_id') !!}</p>	
<div class="container">
	<div class="section">
		<div class="row">
			<div class="col-sm-9">
				<div class="page-breadcrumbs">
					<h1 class="section-title">Following Author's News</h1>	
				</div>
				<div id="site-content" class="site-content">
				</div><!--/#site-content-->
				<div class="row">
					<div class="col-sm-12">
						<div class="section">
							<div class="row">
								<?php
								if($following_data['status']['message']=="Successful")
								{
									
									$count=count($following_data['follower_news']);
									for($i=0;$i<$count;$i++)
									{
									 $id= $following_data['follower_news'][$i]['id'];
									 $news_title= $following_data['follower_news'][$i]['news_title'];
									 $news_text= $following_data['follower_news'][$i]['news_text'];
									 
									 $like= $following_data['follower_news'][$i]['like'];
									 $comment= $following_data['follower_news'][$i]['comment'];
									 $image1= $following_data['follower_news'][$i]['image1'];
									 
									  $news_time= $following_data['follower_news'][$i]['news_time'];
									   $country= $following_data['follower_news'][$i]['country'];
									    $news_type= $following_data['follower_news'][$i]['news_type'];
										$news_distance= $following_data['follower_news'][$i]['distance'];
									 $news_title = substr($news_title,0,50);
									 
									 	$now1 = new DateTime();
										$ts11 = strtotime($news_time);
										$ts22 = $now1->getTimestamp();
										$diff = $ts22 - $ts11;
										$hourdiff1 = round(( $now1->getTimestamp()-strtotime($news_time))/3600, 1);
										//$days=ceil($hourdiff1/24);
										
										
											$adays=intval($hourdiff1/24);
				
										if($adays=="0")
										{
											$days="Today";
										}
										else if($adays=="1")
										{
											$days="Yesterday";
										}
										else 
										{
											$days=$adays." days";	
										}
								?>
								<div class="col-sm-4">
									<div class="post medium-post">
										<div class="entry-header">
											<div class="entry-thumbnail">
												<a href="{!! route('news.route', ['news_id'=>$id, 'user_id'=>$user_id]) !!}"><img class="center-cropped img-responsive" src="{{asset('admin/images/').'/'.$image1}}" alt="" style="width: 100%; height: 180px;" /></a>
											</div>
										</div>
										<div class="post-content">								
											<div class="entry-meta">
												<ul class="list-inline">
													<li class="publish-date"><i class="fa fa-clock-o"></i><?php echo $days; ?></li>
													<li>
														<?php
														if($news_type=="1")
														{ ?>
																<li class="publish-date" ><i class="fa fa-location-arrow" aria-hidden="true"></i><?php echo $news_distance; ?>km</li><br>	
														<?php }
														else 
														{ ?> 
															<li class="publish-date" "><i class="fa fa-globe" aria-hidden="true"></i><?php echo $country; ?></li><br>	
														<?php } ?>
													</li>
													
												<!-- 	<li class="views"><i class="fa fa-thumbs-o-up"></i><?php echo $like; ?></li>
													<li class="loves"><i class="fa fa fa-comment-o"></i><?php echo $comment; ?></li> -->
													
												
												</ul>
											</div>
											<h2 class="entry-title">
												<a href="{!! route('news.route', ['news_id'=>$id, 'user_id'=>$user_id]) !!}"><?php echo $news_title; ?> </a>
											</h2>
										</div>
									</div><!--/post-->
								</div>
								
							<?php } } ?>
								
									<p style="display: none;">{!! $ureg_id=Session::get('user_id') !!}</p>							
							
						</div><!--/.section -->	
					</div>
				</div>
				
				</div>
			</div><!--/.col-sm-9 -->	
			
			<div class="col-sm-3">
				<div id="sitebar">
					
						
						<div class="widget">
								<h1 class="section-title title">Promotions</h1>
								<ul class="post-list">
									<?php
								if($promotion['status']['message']=="Successful")
								{
									
									$count=count($promotion['news']);
									for($i=0;$i<$count;$i++)
									{
									 $p_id= $promotion['news'][$i]['id'];
									 $title= $promotion['news'][$i]['title'];
									$image1= $promotion['news'][$i]['image1'];
									  $time= $promotion['news'][$i]['time'];
									  
									 $news_distance= $promotion['news'][$i]['news_distance'];
									
										
										
									 $news_title = substr($title,0,70);
									 
									 	$now1 = new DateTime();
										$ts11 = strtotime($time);
										$ts22 = $now1->getTimestamp();
										$diff = $ts22 - $ts11;
										$hourdiff1 = round(( $now1->getTimestamp()-strtotime($time))/3600, 1);
										//$days=ceil($hourdiff1/24);
										
										
											$pdays=intval($hourdiff1/24);
				
										if($pdays=="0")
										{
											$p_days="Today";
										}
										else if($pdays=="1")
										{
											$p_days="Yesterday";
										}
										else 
										{
											$p_days=$pdays." days";	
										}
								?>
									<li>
										<div class="post small-post">
											<div class="entry-header">
												<div class="entry-thumbnail">
													<a href="{!! route('promotion.route', ['p_id'=>$p_id, 'user_id'=>$user_id]) !!}"><img class="img-responsive" src="{{asset('admin/images/').'/'.$image1}}" alt="" /> </a>
												</div>
											</div>
											<div class="post-content">								
												<div class="video-catagory"><?php echo $p_days; ?></div>
												<div class="video-catagory"><?php echo $news_distance; ?>Km</div>
												<h2 class="entry-title">
													<a href="{!! route('promotion.route', ['p_id'=>$p_id, 'user_id'=>$user_id]) !!}"><?php echo $news_title ?></a>
												</h2>
											</div>
										</div><!--/post--> 
									</li>
								
									<?php  } } ?>
								
								
									
									
								</ul>
							</div><!--/#widget-->
							
							
							
						
					</div><!--/#widget-->
				</div><!--/#sitebar-->
		</div>				
	</div><!--/.section-->
</div>
@stop

@section('post-script')
    <!-- Revolution Slider -->
   
@stop