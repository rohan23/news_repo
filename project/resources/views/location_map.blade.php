
@extends('layout.base', ['select' => 'about'])

@section('body')
 <p style="display:none;">Lat={!! $latitude= Session::get('user_lat'); !!}</p>
<p style="display:none;">long={!! $longitude= Session::get('user_lon'); !!}</p>
<input type="hidden" value="<?php echo $latitude; ?>"  id="main_lat"/>
<input type="hidden" value="<?php echo $longitude; ?>" id="main_lng"/>

<input type="hidden" value="<?php echo $latt; ?>"  id="main_lat1"/>
<input type="hidden" value="<?php echo $lngg; ?>" id="main_lng1"/>
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAXNYV2WAo_qX6kbNNGMCC_vFFSD1onJ5A&sensor=false"></script>
    <script type="text/javascript">
    var main_lat1=document.getElementById('main_lat').value;
	var main_lng1=document.getElementById('main_lng').value;
	var main_lat2=document.getElementById('main_lat1').value;
	var main_lng2=document.getElementById('main_lng1').value;
	var markers = jQuery.parseJSON('[{"lat": '+ main_lat1 +',"lng": '+ main_lng1 +'},{"lat": '+ main_lat2 +',"lng": '+ main_lng2 +'}]');
        window.onload = function () {
            var mapOptions = {
                center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
                zoom: 12,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var map = new google.maps.Map(document.getElementById("gmap"), mapOptions);
            var infoWindow = new google.maps.InfoWindow();
            var lat_lng = new Array();
            var latlngbounds = new google.maps.LatLngBounds();
            for (i = 0; i < markers.length; i++) {
                var data = markers[i]
                var myLatlng = new google.maps.LatLng(data.lat, data.lng);
                lat_lng.push(myLatlng);
                var marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map,
                    title: data.title
                });
                latlngbounds.extend(marker.position);
                (function (marker, data) {
                    google.maps.event.addListener(marker, "click", function (e) {
                        infoWindow.setContent(data.description);
                        infoWindow.open(map, marker);
                    });
                })(marker, data);
            }
            map.setCenter(latlngbounds.getCenter());
            map.fitBounds(latlngbounds);

            //***********ROUTING****************//

            //Intialize the Path Array
            var path = new google.maps.MVCArray();

            //Intialize the Direction Service
            var service = new google.maps.DirectionsService();

            //Set the Path Stroke Color
            var poly = new google.maps.Polyline({ map: map, strokeColor: '#4986E7' });

            //Loop and Draw Path Route between the Points on MAP
            for (var i = 0; i < lat_lng.length; i++) {
                if ((i + 1) < lat_lng.length) {
                    var src = lat_lng[i];
                    var des = lat_lng[i + 1];
                    path.push(src);
                    poly.setPath(path);
                    service.route({
                        origin: src,
                        destination: des,
                        travelMode: google.maps.DirectionsTravelMode.DRIVING
                    }, function (result, status) {
                        if (status == google.maps.DirectionsStatus.OK) {
                            for (var i = 0, len = result.routes[0].overview_path.length; i < len; i++) {
                                path.push(result.routes[0].overview_path[i]);
                            }
                        }
                    });
                }
            }
        }
    </script>
    <style>
hr 
{
    margin-top: 20px;
    margin-bottom: 20px;
    border: 0;
    border-top: 1px solid #eee;
}

div#gmap 
{
	width: 100%;
	height: 500px;
	border:double;
}

a[href^="http://maps.google.com/maps"]{display:none !important}
a[href^="https://maps.google.com/maps"]{display:none !important}

.gmnoprint a, .gmnoprint span, .gm-style-cc {
    display:none;
}
.gmnoprint div {
    background:none !important;
}

</style>
<div class="signup-page">
	<div class="container">
		<div class="row">
			<!-- user-login -->			
			<div class="col-md-9 col-sm-offset-1 col-sm-12">
					<h1 class="section-title title">Location Map</h1>
					<div class="col-md-12">
						<div class="widget">
							<!-- /Toolbar -->
							<div class="widget-content">
    							<div id="gmap"></div>
							</div>
						</div>
					</div>
				
			</div><!-- user-login -->			
		</div><!-- row -->	
	</div><!-- container -->
</div><!-- signup-page -->
    
    
@stop
