@extends('layout.base')

@section('body')
<?php 
$uid=Session::get('user_id');
if($uid=="")
{
	$user_id="0";
}
else{
	$user_id=$uid;
}


$f_type=Session::get('primery_filter');

function array_orderby()
{
    $args = func_get_args();
    $data = array_shift($args);
    foreach ($args as $n => $field) {
        if (is_string($field)) {
            $tmp = array();
            foreach ($data as $key => $row)
                $tmp[$key] = $row[$field];
            $args[$n] = $tmp;
            }
    }
    $args[] = &$data;
    call_user_func_array('array_multisort', $args);
    return array_pop($args);
}

if($f_type!="")
{
	if($f_type=="radius")
	{
		if(array_key_exists("locational_news", $latest_data))
		{	
			$latest_data['locational_news'] = array_orderby($latest_data['locational_news'], 'news_distance', SORT_ASC);
		}
		
	}
	else
	{
		if(array_key_exists("locational_news", $latest_data))
		{
			$latest_data['locational_news'] = array_orderby($latest_data['locational_news'], 'time_diff', SORT_ASC);
		}
	
	}
}


// echo "<pre>";
// print_r($latest_data);
// echo "</pre>";

?>
<style>
.post-content
	{
	 min-height:100px; 
	 max-height: 100px; 
	 height: 100px;
	}
	
	.center-cropped {
  object-fit: none; /* Do not scale the image */
  object-position: center; /* Center the image within the element */
  height: 200px;
  width: 100px;
}
</style>
<div class="container">
	<div class="page-breadcrumbs">
		<h1 class="section-title">Latest News</h1>	
	</div>
	<div class="section">
		<div class="row">
			<div class="col-sm-9">
				<div class="row">
					<div class="col-sm-12">
						<div class="section">
							
							<div class="row">
								<?php 
								if(array_key_exists("locational_news", $latest_data))
								{
									$count_news=count($latest_data['locational_news']);
									for($v=0; $v<$count_news; $v++)
									{
										 $c_news_id= $latest_data['locational_news'][$v]['id'];
										 $c_category_id= $latest_data['locational_news'][$v]['category'];
										 $c_news_title= $latest_data['locational_news'][$v]['news_title'];
										 $c_user_id= $latest_data['locational_news'][$v]['user_id'];
										 $c_image1= $latest_data['locational_news'][$v]['image1'];
										 $c_likes= $latest_data['locational_news'][$v]['like'];
										 $c_time= $latest_data['locational_news'][$v]['news_time'];
										 $c_comment= $latest_data['locational_news'][$v]['comment'];
										 $c_country= $latest_data['locational_news'][$v]['country'];
										 $c_news_time= $latest_data['locational_news'][$v]['news_time'];
										 $date_c=explode(" ",$c_time);
										 $date_c=$date_c[0];
										 $c_news = substr($c_news_title,0,50);
										 $c_news_distance= $latest_data['locational_news'][$v]['news_distance'];
											$now2 = new DateTime();
											$ts12 = strtotime($c_news_time);
											$ts23 = $now2->getTimestamp();
											$diff = $ts23 - $ts12;
											$hourdiff2 = round(( $now2->getTimestamp()-strtotime($c_news_time))/3600, 1);
											$category_days1=round($hourdiff2/24);
											if($category_days1==0)
											{
												$days1="Today";
											}
											else if($category_days1==1)
											{
												$days1="Yesterday";
											}
											else
											{
												$days1=$category_days1." days ago";
											}
										 
										 
									?>
								<div class="col-sm-4">
									<div class="post medium-post">
										<div class="entry-header">
											<div class="entry-thumbnail">
												<a href="{!! route('news.route', ['news_id'=>$c_news_id]) !!}"><img class="center-cropped img-responsive" src="{{asset('admin/images/').'/'.$c_image1}}" alt="" style="width: 100%; height: 180px;" /></a>
												
											</div>
										</div>
										<div class="post-content">								
											<div class="entry-meta">
												<ul class="list-inline">
													<li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i> <?php echo $days1; ?></a></li>
													<li class="loves"><i class="fa fa-location-arrow" aria-hidden="true"></i><?php echo $c_news_distance; ?>km</li>	
													<li class="views"><i class="fa fa-thumbs-o-up"></i><?php echo $c_likes; ?></li>
													<li class="loves"><i class="fa fa fa-comment-o"></i><?php echo $c_comment; ?></li>
												</ul>
											</div>
											<h2 class="entry-title">
												<a href="{!! route('news.route', ['news_id'=>$c_news_id]) !!}"><?php echo $c_news; ?></a>
											</h2>
										</div>
									</div><!--/post--> 
								</div>
									<?php
									}
								}
								
								
								?>
							</div>
							
							<div class="row">
								<?php 
								if(array_key_exists("general_news", $latest_data))
								{
									$count_news=count($latest_data['general_news']);
									for($w=0; $w<$count_news; $w++)
									{
										$g_news_id= $latest_data['general_news'][$w]['id'];
										$g_category_id= $latest_data['general_news'][$w]['category'];
										$g_news_title= $latest_data['general_news'][$w]['news_title'];
										$g_user_id= $latest_data['general_news'][$w]['user_id'];
										$g_image1= $latest_data['general_news'][$w]['image1'];
										$g_likes= $latest_data['general_news'][$w]['like'];
										$g_time= $latest_data['general_news'][$w]['news_time'];
										$g_comment= $latest_data['general_news'][$w]['comment'];
										$g_country= $latest_data['general_news'][$w]['country'];
										$date_c=explode(" ",$g_time);
										$date_c=$date_c[0];
										$g_news = substr($g_news_title,0,50);
										$g_news_time=$latest_data['general_news'][$w]['news_time'];
										 
										$now1 = new DateTime();
										$ts11 = strtotime($g_news_time);
										$ts22 = $now1->getTimestamp();
										$diff = $ts22 - $ts11;
										$hourdiff1 = round(( $now1->getTimestamp()-strtotime($g_news_time))/3600, 1);
										$category_days=round($hourdiff1/24);
										if($category_days==0)
										{
											$days="Today";
										}
										else if($category_days==1)
										{
											$days="Yesterday";
										}
										else
										{
											$days=$category_days." days ago";
										}
									?>
								<div class="col-sm-4">
									<div class="post medium-post">
										<div class="entry-header">
											<div class="entry-thumbnail">
												<a href="{!! route('news.route', ['news_id'=>$c_news_id, 'user_id'=>$user_id]) !!}"><img class="center-cropped img-responsive" src="{{asset('admin/images/').'/'.$g_image1}}" alt="" style="width: 100%; height: 180px;" /></a>
												
											</div>
										</div>
										<div class="post-content">								
											<div class="entry-meta">
												<ul class="list-inline">
													<li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i> <?php echo $days; ?></a></li>
													<li class="publish-date"><i class="fa fa-globe" aria-hidden="true"></i><?php echo $g_country; ?></li>	
													<li class="views"><i class="fa fa-thumbs-o-up"></i><?php echo $g_likes; ?></li>
													<li class="loves"><i class="fa fa fa-comment-o"></i><?php echo $g_comment; ?></li>
												</ul>
											</div>
											<h2 class="entry-title">
												<a href="{!! route('news.route', ['news_id'=>$c_news_id, 'user_id'=>$user_id]) !!}"><?php echo $c_news; ?></a>
											</h2>
										</div>
									</div><!--/post--> 
								</div>
									<?php
									}
								}
								
								
								?>
								
								
							</div>
							
							
						</div><!--/.section -->	
					</div>
				</div>
			</div><!--/.col-sm-9 -->	
			
			<div class="col-sm-3">
				<div id="sitebar">
					<div class="widget follow-us">
						<h1 class="section-title title">Follow Us</h1>
						<ul class="list-inline social-icons">
							<li><a href="#"><i class="fa fa-facebook"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
							<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
							<li><a href="#"><i class="fa fa-youtube"></i></a></li>
						</ul>
					</div><!--/#widget-->
				</div><!--/#sitebar-->
			</div>
		</div>				
	</div><!--/.section-->
</div><!--/.container-->


		
@stop