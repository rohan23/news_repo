@extends('layout.base')

@section('body')



<?php 
/* echo "<pre>"; 
 print_r($promotion);
 echo "</pre>";
 * 
 */
$uid=Session::get('user_id');
if($uid=="")
{
	$user_id="0";
}
else{
	$user_id=$uid;
}


$index=$index;


$f_type=Session::get('primery_filter');

function array_orderby()
{
    $args = func_get_args();
    $data = array_shift($args);
    foreach ($args as $n => $field) {
        if (is_string($field)) {
            $tmp = array();
            foreach ($data as $key => $row)
                $tmp[$key] = $row[$field];
            $args[$n] = $tmp;
            }
    }
    $args[] = &$data;
    call_user_func_array('array_multisort', $args);
    return array_pop($args);
}
if($f_type!="")
{
if($f_type=="radius")
{
	if(array_key_exists("news", $promotion))
	{
		$promotion['news'] = array_orderby($promotion['news'], 'news_distance', SORT_ASC);		
	}
}
else
{
if(array_key_exists("news", $promotion))
{
	$promotion['news'] = array_orderby($promotion['news'], 'news_distance', SORT_ASC);	
}
}
}
?>
<style>
	.post-content
	{
	 min-height:100px; 
	 max-height: 150px; 
	 height: 100px;
	}
</style>
<p style="display: none;">{!! $login_user_id=Session::get('user_id') !!}</p>	
<div class="container">
	<div class="section">
		<div class="row">
			
			<div class="col-sm-12">
				<div class="page-breadcrumbs">
					<h1 class="section-title">Promotions</h1>	
				</div>
				<div id="site-content" class="site-content">
				</div><!--/#site-content-->
				<div class="row">
					<!-- <div class="col-sm-1"></div> -->
					<div class="col-sm-12">
						<div class="section">
										<div class="row">
								<?php
								if($promotion['status']['message']=="Successful")
								{
									
									$count=count($promotion['news']);
									for($i=0;$i<$count;$i++)
									{
									 $p_id= $promotion['news'][$i]['id'];
									 $title= $promotion['news'][$i]['title'];
									 $text= $promotion['news'][$i]['text'];
									 
									 $email= $promotion['news'][$i]['email'];
									 $user_id= $promotion['news'][$i]['user_id'];
									 $phone= $promotion['news'][$i]['phone'];
									 
									  $time= $promotion['news'][$i]['time'];
									  $like= $promotion['news'][$i]['like'];
									  $comment= $promotion['news'][$i]['comment'];
									  $country= $promotion['news'][$i]['country'];
										
									 $image1= $promotion['news'][$i]['image1'];
									 $news_distance= $promotion['news'][$i]['news_distance'];
									
										
										
									 $news_title = substr($title,0,150);
									 
									 	$now1 = new DateTime();
										$ts11 = strtotime($time);
										$ts22 = $now1->getTimestamp();
										$diff = $ts22 - $ts11;
										$hourdiff1 = round(( $now1->getTimestamp()-strtotime($time))/3600, 1);
										//$days=ceil($hourdiff1/24);
										
										
											$adays=intval($hourdiff1/24);
				
										if($adays=="0")
										{
											$days="Today";
										}
										else if($adays=="1")
										{
											$days="Yesterday";
										}
										else 
										{
											$days=$adays." days";	
										}
								?>
								<div class="col-sm-4">
									<div class="post medium-post">
										<div class="entry-header">
											<div class="entry-thumbnail">
												<a href="{!! route('promotion.route', ['p_id'=>$p_id]) !!}"><img class="center-cropped img-responsive" src="{{asset('admin/images/').'/'.$image1}}" alt="" style="width: 100%; height: 240px;" /></a>
											</div>
										</div>
										<div class="post-content">								
											<div class="entry-meta" >
												<ul class="list-inline" style="font-size:14px;">
													<li class="publish-date"><i class="fa fa-clock-o"></i><?php echo $days; ?></li>
													<li>
														<li class="publish-date" ><i class="fa fa-location-arrow" aria-hidden="true"></i><?php echo $news_distance; ?>km</li>
														<li class="publish-date"><i class="fa fa-globe" aria-hidden="true"></i><?php echo $country; ?></li>
													</li>
													<li class="views"><i class="fa fa-thumbs-o-up"></i><?php echo $like; ?></li>
													<li class="loves"><i class="fa fa fa-comment-o"></i><?php echo $comment; ?></li>
												</ul>
											</div>
											<h2 class="entry-title">
												<a href="{!! route('promotion.route', ['p_id'=>$p_id]) !!}"><?php echo $news_title; ?> </a>
											</h2>
										</div>
									</div><!--/post-->
								</div>
								
							<?php } } ?>
								
									<p style="display: none;">{!! $ureg_id=Session::get('user_id') !!}</p>							
							
						</div><!--/.section -->	
					</div>
					</div>
				</div>
				
				</div>
			</div><!--/.col-sm-9 -->	
			
			</div>
		</div>				
	</div><!--/.section-->
</div>
@stop

@section('post-script')
    <!-- Revolution Slider -->
   
@stop