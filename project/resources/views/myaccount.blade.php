@extends('layout.base')

@section('body')
<?php 
$uid=Session::get('user_id');

// echo "<pre>";
// print_r($profile_data['locational_news']);
 // echo "<pre>";
?>

<div class="container">
    <div class="row profile">
		@include('account_base')
		<div class="col-md-9">
            <div class="profile-content">
			 		<div class="row">
							<div class="col-sm-12">
								<div class="section">
									<div class="row">
										<?php 
										if($profile_data['status']['message']=="Successful")
										{
											
											if (array_key_exists('general_news', $profile_data)) {
												
    
											$c= count($profile_data['general_news']);

										for($i=0;$i<$c;$i++)
										{
											 $news_id= $profile_data['general_news'][$i]['id'];
											 $name= $profile_data['general_news'][$i]['name'];
											 $news_title= $profile_data['general_news'][$i]['news_title'];
											 $news_text= $profile_data['general_news'][$i]['news_text'];
											  $time= $profile_data['general_news'][$i]['time'];
											 $like= $profile_data['general_news'][$i]['like'];
											 $comment= $profile_data['general_news'][$i]['comment'];
											 $image1= $profile_data['general_news'][$i]['image1'];
											  $news_time= $profile_data['general_news'][$i]['news_time'];
											 $report_user_id=$profile_data['general_news'][$i]['user_id'];
											 $news_distance=ceil($profile_data['general_news'][$i]['distance']);
											 
											 $news_title = substr($news_title,0,50);
											 //$date_only=explode(" ",$time);
											 //$date=$date_only[0];
											 
											    $now1 = new DateTime();
												$ts11 = strtotime($news_time);
												$ts22 = $now1->getTimestamp();
												$diff = $ts22 - $ts11;
												$hourdiff1 = round(( $now1->getTimestamp()-strtotime($news_time))/3600, 1);
												$general_time=intval($hourdiff1/24);
												if($general_time=="0")
												{
													$days="Today";
												}
												else if($general_time=="1")
												{
													$days="Yesterday";
												}
												else 
												{
													$days=$general_time." days ago";	
												}
										?>
										<div class="col-sm-4">
											<div class="post medium-post">
												<div class="entry-header">
													<div class="entry-thumbnail">
														<a href="{!! route('news.route', ['news_id'=>$news_id, 'user_id'=>$uid]) !!}"><img class="img-responsive" style="width:240px; height:260px;" src="{{asset('admin/images/').'/'.$image1}}" alt="" /></a>
													</div>
												</div>
												<div class="post-content">								
													<div class="entry-meta">
														<ul class="list-inline">
													
														<li class="publish-date" style="padding-bottom: 10px; font-size:16px;"><i class="fa fa-location-arrow" aria-hidden="true"></i><?php echo $news_distance; ?>km</li><br>	
														<li class="publish-date"><i class="fa fa-clock-o"></i><?php echo $days; ?></li>
															<li class="loves"><i class="fa fa-thumbs-o-up"></i><?php echo $like; ?></li>
															<li class="views"><i class="fa fa-comments" aria-hidden="true"></i><?php echo $comment; ?></li>
																	<?php 
													if($report_user_id==$uid)
													{
													?>
													<li class="loves"><a href="{!! route('news.delete', ['news_id'=>$news_id]) !!}"><i class="fa fa-trash"></i></a></li>
													<?php	
													}
													?>
														</ul>
													</div>
													<h2 class="entry-title">
														<p><a href="{!! route('news.route', ['news_id'=>$news_id, 'user_id'=>$uid]) !!}"><?php echo $news_title; ?></a></p>
														
													</h2>
												</div>
											</div><!--/post--> 
											
										</div>
									<?php } } 
									else{
										?>
										<h1>Not Any Post</h1> 
										<?php
									} 
									 ?>
								
										<?php 
										
											
											if (array_key_exists('locational_news', $profile_data)) {
												
    
											$c= count($profile_data['locational_news']);

										for($i=0;$i<$c;$i++)
										{
											 $gnews_id= $profile_data['locational_news'][$i]['id'];
											 $gname= $profile_data['locational_news'][$i]['name'];
											 $gnews_title= $profile_data['locational_news'][$i]['news_title'];
											 $gnews_text= $profile_data['locational_news'][$i]['news_text'];
											  $gtime= $profile_data['locational_news'][$i]['time'];
											 $glike= $profile_data['locational_news'][$i]['like'];
											 $gcomment= $profile_data['locational_news'][$i]['comment'];
											 $gimage1= $profile_data['locational_news'][$i]['image1'];
											  $gnews_time= $profile_data['locational_news'][$i]['news_time'];
											 $greport_user_id=$profile_data['locational_news'][$i]['user_id'];
											 $gcountry=$profile_data['locational_news'][$i]['country'];
											 $gnews_distance=ceil($profile_data['locational_news'][$i]['distance']);
											 $gnews = substr($gnews_title,0,50);
											 //$gdate_only=explode(" ",$gtime);
											 //$gdate=$gdate_only[0];
											 
											 
											 
											    $now1 = new DateTime();
												$ts11 = strtotime($gnews_time);
												$ts22 = $now1->getTimestamp();
												$diff = $ts22 - $ts11;
												$hourdiff1 = round(( $now1->getTimestamp()-strtotime($gnews_time))/3600, 1);
												$locational_time=intval($hourdiff1/24);
												
												if($locational_time=="0")
												{
													$gdays="Today";
												}
												else if($locational_time=="1")
												{
													$gdays="Yesterday";
												}
												else 
												{
													$gdays=$locational_time." days ago";	
												}
										?>
										<div class="col-sm-4">
											<div class="post medium-post">
												<div class="entry-header">
													<div class="entry-thumbnail">
														<a href="{!! route('news.route', ['news_id'=>$gnews_id, 'user_id'=>$uid]) !!}"><img class="img-responsive" style="width:240px; height:260px;" src="{{asset('admin/images/').'/'.$gimage1}}" alt="" /></a>
													</div>
												</div>
												<div class="post-content">								
													<div class="entry-meta">
														<ul class="list-inline">
													
														<li class="publish-date" style="padding-bottom: 10px; font-size:16px;"><i class="fa fa-globe" aria-hidden="true"></i><?php echo $gcountry; ?></li><br>	
														<li class="publish-date"><i class="fa fa-clock-o"></i><?php echo $gdays; ?></li>
															<li class="loves"><i class="fa fa-heart-o"></i><?php echo $glike; ?></li>
															<li class="views"><i class="fa fa-comments" aria-hidden="true"></i><?php echo $gcomment; ?></li>
															<?php 
													if($greport_user_id==$uid)
													{
													?>
													<li class="loves"><a href="{!! route('news.delete', ['news_id'=>$gnews_id]) !!}"><i class="fa fa-trash"></i></a></li>
													<?php	
													}
													?>
														</ul>
													</div>
													<h2 class="entry-title">
														<p><a href="{!! route('news.route', ['news_id'=>$gnews_id, 'user_id'=>$uid]) !!}"><?php echo $gnews; ?></a></p>
														
													</h2>
												</div>
											</div><!--/post--> 
											
										</div>
									<?php } } }
else{
	?>
	<h1>Not Any Post</h1> 
	<?php
} 
 ?>
									</div>
									
								</div><!--/.section -->	
						
							</div>
						</div>
            </div>
		</div>
	</div>
</div>

		
@stop