@extends('layout.base')

@section('body')

<!-- signup-page -->
		<div class="signup-page">
			<div class="container">
				<div class="row">
					<!-- user-login -->			
					<div class="col-sm-6 col-sm-offset-3">
						<div class="ragister-account account-login">		
							<h1 class="section-title title">Login</h1>
							<!--<div class="login-options text-center">
								<a href="#" class="facebook-login"><i class="fa fa-facebook"></i> Login with Facebook</a>
								<a href="#" class="twitter-login"><i class="fa fa-twitter"></i> Login with Twitter</a>
								
							</div>
							<div class="devider text-center">Or</div>
							
							-->
							
							<form id="registation-form" name="registation-form" method="post" action="loginuser">
								{!! csrf_field() !!}
								
								<div class="form-group" id="top-login-username">
									<label>Select Role</label>
									<select class="form-control" name="role_type"  required="required">
										<option value="user">User</option>
											<option value="admin">Admin</option>
										
									</select>
								</div>
								<div class="form-group" id="top-login-username">
									<label>Select Country</label>
									<select class="form-control" id="country_code1" onchange="doit1();" required="required">
										<option value=""> Select Country </option>
										@foreach($country as $country_detail)
											<option value="{!! $country_detail->phonecode; !!}">{!! $country_detail->country; !!}</option>
										@endforeach
									</select>
								</div>
								<div class="form-group">
									<div class="col-md-3">
										<label>Code</label>
										<input type="text" readonly="readonly"  name="phone_code" id="pcode1" placeholder="+60" class="form-control" required="required">	
									</div>
									<div class="col-md-9">
										<label>Phone No.</label>
										<input type="text" onkeypress="validate(event)" name="phone" placeholder="Please enter phone no." class="form-control" required="required">
										<p id="error1" style="color:#f14950; font-size: 15px;">*NOTE: Accepts only numeric values  </p>
									</div>
									<!--<input type="email" name="email" class="form-control" required="required">-->
									
								</div>
								<div class="form-group">
									<label>Password</label>
									<input type="password" name="password" class="form-control" required="required">
								</div>
								<!-- checkbox -->
								<div class="checkbox">
									<!--<label class="pull-left"><input type="checkbox" name="signing" id="signing"> Keep Me Login </label>
									<a href="#" class="pull-right ">Forgot Password </a> -->
								</div><!-- checkbox -->	
								<div class="submit-button text-center">
									<button type="submit" class="btn btn-primary">Account Login</button>
								</div>
							</form>	
							<div class="new-user text-center">
								<!--<a style="color:#F6433E;" href="{{url('forgot_password')}}">Forgot Password</a>-->
								<p>Don't have an account ? <a style="color:#F6433E;" href="{{url('signup')}}">Register Now</a> </p>
							</div>
							
						</div>
					</div><!-- user-login -->			
				</div><!-- row -->	
			</div><!-- container -->
		</div><!-- signup-page -->
<script>
	function doit1()
	{
		var val2 = document.getElementById("country_code1").value;
		document.getElementById('pcode1').value = val2;
	}
</script>

<script type="text/javascript">
  function validate(evt) {
  var theEvent = evt || window.event;
  var key = theEvent.keyCode || theEvent.which;
  key = String.fromCharCode( key );
  var regex = /[0-9]|\./;
  if( !regex.test(key) ) {
    theEvent.returnValue = false;
    if(theEvent.preventDefault) theEvent.preventDefault();
  }
}
    </script>

@stop