@extends('layout.base')

@section('body')
<!-- signup-page -->
		<div class="signup-page">
			<div class="container">
				<div class="row">
					<!-- user-login -->			
					<div class="col-sm-6 col-sm-offset-3">
						<div class="ragister-account account-login">		
							<h1 class="section-title title">Verify User</h1>
							<!--<div class="login-options text-center">
								<a href="#" class="facebook-login"><i class="fa fa-facebook"></i> Login with Facebook</a>
								<a href="#" class="twitter-login"><i class="fa fa-twitter"></i> Login with Twitter</a>
								
							</div>
							<div class="devider text-center">Or</div>
							
							-->
							
							<form id="registation-form" name="registation-form" method="post" action="verify_user">
								{!! csrf_field() !!}
								<input type="hidden" name="user_phoneno" value="<?php echo $user_phonenumber; ?>" class="form-control" required="required">
								<div class="form-group">
									<label>Enter Your OTP:</label>
									<input type="text" name="user_otp" class="form-control" required="required">
								</div>
								<div class="submit-button text-center">
									<button type="submit" class="btn btn-primary">Submit</button>
								</div>
							</form>	
						</div>
					</div><!-- user-login -->			
				</div><!-- row -->	
			</div><!-- container -->
		</div><!-- signup-page -->
		<br><br><br><br><br><br><br>
@stop