<h2 class="entry-title">
	<span style="display:none;" id="exist_lang"><?php echo $translation['news'][0]['language']; ?></span>
	<span style="display:none; white-space: pre-wrap;" id="n_title"><?php echo $translation['news'][0]['header']; ?></span>
	<span style="display:none; white-space: pre-wrap;" id="n_text"><?php echo $translation['news'][0]['text']; ?></span>
<?php echo $translation['news'][0]['header']; ?>
</h2>
<div class="entry-content">
	<p style="white-space: pre-wrap;"><?php echo $translation['news'][0]['text']; ?></p>
</div>