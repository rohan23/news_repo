@extends('layout.base')

@section('body')
<p style="display: none;">Lat={!! $latitude= Session::get('user_lat'); !!}</p>
<p style="display: none;">long={!! $longitude= Session::get('user_lon'); !!}</p>

<input type="hidden" value="<?php echo $latitude; ?>"  id="main_lat"/>
<input type="hidden" value="<?php echo $longitude; ?>" id="main_lng"/>
<script type="text/javascript">


</script>


<!-- Google Map CSS and JS -->
<!--
<script>
function set_location()
{	
	var lat=document.getElementById('lat').value;
	var lng=document.getElementById('lon').value;
	$.ajax({
		 type:'POST',
		 url:'/selected_location',
		 data:'latt='+lat +'&lngg='+lng,
		 success: function(data) 
			{
				location.reload();
			}
	  });
}

</script>
-->
<!-- Javascript -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAXNYV2WAo_qX6kbNNGMCC_vFFSD1onJ5A&libraries=places&callback=initAutocomplete" async defer></script>
<script>
  // This example adds a search box to a map, using the Google Place Autocomplete
  // feature. People can enter geographical searches. The search box will return a
  // pick list containing a mix of places and predicted search terms.

  // This example requires the Places library. Include the libraries=places
  // parameter when you first load the API. For example:
  // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">
function initGeolocation()
{
	if( navigator.geolocation )
	{
	   // Call getCurrentPosition with success and failure callbacks
	   navigator.geolocation.getCurrentPosition( success, fail );
	}
	else
	{
	   alert("Sorry, your browser does not support geolocation services.");
	}
	
}

function success(position)
{	
	
	document.getElementById('lon').value = position.coords.longitude;
	document.getElementById('lat').value = position.coords.latitude
	var lat=position.coords.latitude;
	var lng=position.coords.longitude;
	$.ajax({
		 type:'POST',
		 url:'/current_location',
		 data:'latt='+lat +'&lngg='+lng,
		 success: function(data) 
			{
				location.reload();
			}
	  });

}

function fail()
{
	var lat=3.1502860618571256;
	var lng=101.61503791809082;
	$.ajax({
		 type:'POST',
		 url:'/current_location',
		 data:'latt='+lat +'&lngg='+lng,
		 success: function(data)
			{
				location.reload();
			}
	  });

}
  function initAutocomplete() {
  	var main_lat=document.getElementById('main_lat').value;
	var main_lng=document.getElementById('main_lng').value;
  	var uluru = jQuery.parseJSON('{"lat": '+main_lat+', "lng": '+main_lng+'}');
  	var map = new google.maps.Map(document.getElementById('gmap'), {
          zoom: 12,
          center: uluru
        });



    // Create the search box and link it to the UI element.
    var input = document.getElementById('pac-input');
    var searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    
    google.maps.event.addListener(map, "click", function(event) {
            // get lat/lon of click
            var clickLat = event.latLng.lat();
            var clickLon = event.latLng.lng();
            document.getElementById('lon').value = clickLat;
     document.getElementById('lat').value = clickLon;
         
                
				for(i=0; i<markers.length; i++){
			        markers[i].setMap(null);
			    }
				markers = [];
	            markers.push(new google.maps.Marker({
              		map: map,
              		position: new google.maps.LatLng(clickLat,clickLon)
              		
            	}));
            	
				$.ajax({
					 type:'POST',
					 url:'/selected_location',
					 data:'latt='+clickLon +'&lngg='+clickLat,
					 success: function(data) 
						{
							//location.reload();
						}
				  });
	});

        // Bias the SearchBox results towards current map's viewport.
    map.addListener('bounds_changed', function() {
      searchBox.setBounds(map.getBounds());
    });

    var markers = [];
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener('places_changed', function() {
      var places = searchBox.getPlaces();

      if (places.length == 0) {
        return;
      }

      // Clear out the old markers.
      markers.forEach(function(marker) {
        marker.setMap(null);
      });
      markers = [];

      // For each place, get the icon, name and location.
      var bounds = new google.maps.LatLngBounds();
      places.forEach(function(place) {
        if (!place.geometry) {
          console.log("Returned place contains no geometry");
          return;
        }
        var icon = {
          url: place.icon,
          size: new google.maps.Size(71, 71),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(17, 34),
          scaledSize: new google.maps.Size(25, 25)
        };
        //alert(place.geometry.location);

        // Create a marker for each place.
        markers.push(new google.maps.Marker({
          map: map,
          icon: icon,
          title: place.name,
          position: place.geometry.location
        }));

        if (place.geometry.viewport) {
          // Only geocodes have viewport.
          bounds.union(place.geometry.viewport);
        } else {
          bounds.extend(place.geometry.location);
        }
      });
      map.fitBounds(bounds);
    });
    markers.push(new google.maps.Marker({
          position: uluru,
          map: map
        }));

  }

</script>
<!-- Javascript -->
<!-- Map CSS -->
<style>
hr 
{
    margin-top: 20px;
    margin-bottom: 20px;
    border: 0;
    border-top: 1px solid #eee;
}

div#gmap 
{
	width: 100%;
	height: 500px;
	border:double;
}
#c
{
	text-align:left;
}
.controls 
{
	margin-top: 10px;
	border: 1px solid transparent;
	border-radius: 2px 0 0 2px;
	box-sizing: border-box;
	-moz-box-sizing: border-box;
	height: 32px;
	outline: none;
	box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
}
#pac-input 
{
	background-color: #fff;
	font-family: Roboto;
	font-size: 15px;
	font-weight: 300;
	margin-left: 12px;
	padding: 0 11px 0 13px;
	text-overflow: ellipsis;
	width: 300px;
}
#pac-input:focus 
{
	border-color: #4d90fe;
}
.pac-container 
{
	font-family: Roboto;
}
#type-selector 
{
	color: #fff;
	background-color: #4d90fe;
	padding: 5px 11px 0px 11px;
}
#type-selector label 
{
	font-family: Roboto;
	font-size: 13px;
	font-weight: 300;
}
#target 
{
	width: 345px;
}
.ms-search{
	display:none;
}

a[href^="http://maps.google.com/maps"]{display:none !important}
a[href^="https://maps.google.com/maps"]{display:none !important}

.gmnoprint a, .gmnoprint span, .gm-style-cc {
    display:none;
}
.gmnoprint div {
    background:none !important;
}

</style>
<!-- Map CSS -->
<!-- Google Map CSS and JS -->


<!--Add News Page -->
		<div class="signup-page">
			<div class="container">
				<div class="row">
					<!-- user-login -->			
					<div class="col-md-9 col-sm-offset-1 col-sm-12">
						<div class="ragister-account">		
							<h1 class="section-title title">Set User Location</h1>
								<div class="col-md-12">
									<div class="widget">
										<!-- /Toolbar -->
										<div class="widget-content">
											<input id="pac-input" class="controls" type="text" placeholder="Search Box">
			    							<div id="gmap"></div>
										</div>
									</div>
									<input type="hidden" id='lat' class="form-control" name="latt" required="required" placeholder="Select latitude from map">
									<input type="hidden" id='lon' name="lngg" class="form-control" required="required" placeholder="Select longitude from map">
								</div>
								<div class="submit-button text-center">
									<!--<button type="button" class="btn btn-primary" onclick="set_location();">Share Seleted Location</button>-->
									<button type="button" class="btn btn-primary" onclick="initGeolocation();">My Current Location</button>
								</div>
						</div>
					</div><!-- user-login -->			
				</div><!-- row -->	
			</div><!-- container -->
		</div><!-- signup-page -->

<!-- Multi Select JS -->
@stop
