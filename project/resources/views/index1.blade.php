@extends('layout.base')

@section('body')



<?php 
$uid=Session::get('user_id');
if($uid=="")
{
	$user_id="0";
}
else{
	$user_id=$uid;
}
?>
<div id="main-wrapper" class="homepage">
<div class="container">
			<div class="section">
				<div class="row">
					<div class="site-content col-md-9">
						<div class="row">
							<div class="col-sm-12">
								<div id="home-slider">
									@foreach($result1 as $res1)
									<div class="post feature-post">
										<div class="entry-header">
											<div class="entry-thumbnail">
												<!--<img class="img-responsive" src="images/post/slider/1.jpg" alt="" />-->
												<a href="{!! route('news.route', ['news_id'=>$res1->id, 'user_id'=>$user_id]) !!}"><img class="img-responsive" src="{{asset('admin/images/').'/'.$res1->image1}}" style="min-width: 100%; max-height: 350px;"  height="225px" width="270px" alt=""/></a>
											</div>
											<!--<div class="catagory world"><a href="#">World</a></div>-->
										</div>
										<div class="post-content">								
											<div class="entry-meta">
												<ul class="list-inline">
													<!--<li class="publish-date"><i class="fa fa-clock-o"></i><a href="#"> Nov 1, 2015 </a></li>
													<li class="views"><i class="fa fa-eye"></i><a href="#">15k</a></li>-->
													<li class="loves"><i class="fa fa-thumbs-o-up"></i>{!! $res1->like !!}</li>
													<li class="comments"><i class="fa fa-comment-o"></i>{!! $res1->comment !!}</li>
												</ul>
											</div>
											<h2 class="entry-title">
												<a href="{!! route('news.route', ['news_id'=>$res1->id, 'user_id'=>$user_id]) !!}">{!! $res1->news_title !!}</a>
											</h2>
										</div>
									</div><!--/post--> 
									@endforeach
								</div>
							</div>
							<!--<div class="col-sm-4">
								<div class="post feature-post">
									<div class="entry-header">
										<div class="entry-thumbnail">
											<img class="img-responsive" src="images/post/slider/2.jpg" alt="" />
										</div>
										<div class="catagory health"><span><a href="#">Health</a></span></div>
									</div>
									<div class="post-content">								
										<div class="entry-meta">
											<ul class="list-inline">
												<li class="publish-date"><i class="fa fa-clock-o"></i><a href="#"> Nov 7, 2015 </a></li>
												<li class="views"><i class="fa fa-eye"></i><a href="#">15k</a></li>
												<li class="loves"><i class="fa fa-heart-o"></i><a href="#">278</a></li>
											</ul>
										</div>
										<h2 class="entry-title">
											<a href="news-details.html">Manhunt intensifies for fugitive in Kentucky, Tennessee</a>
										</h2>
									</div>
								</div><!--/post--> 
							<!--</div>-->
						</div>
						<div class="row">
							<div class="col-sm-4">
								<div class="post feature-post">
									<div class="entry-header">
										<div class="entry-thumbnail">
											<img class="img-responsive" src="images/post/slider/3.jpg" alt="" />
										</div>
										<div class="catagory technology"><span><a href="#">Technology</a></span></div>
									</div>
									<div class="post-content">								
										<div class="entry-meta">
											<ul class="list-inline">
												<li class="publish-date"><i class="fa fa-clock-o"></i> Nov 2, 2015 </li>
												<li class="views"><i class="fa fa-thumbs-o-up"></i>15k</li>
												<li class="loves"><i class="fa fa-heart-o"></i>278</li>
											</ul>
										</div>
										<h2 class="entry-title">
											<a href="#">Japan in four gorgeous Pokémon-themed colors</a>
										</h2>
									</div>
								</div><!--/post--> 	
							</div>
							<div class="col-sm-4">
								<div class="post feature-post">
									<div class="entry-header">
										<div class="entry-thumbnail">
											<img class="img-responsive" src="images/post/slider/5.jpg" alt="" />
										</div>
										<div class="catagory entertainment"><a href="#">Entertainment</a></div>
									</div>
									<div class="post-content">								
										<div class="entry-meta">
											<ul class="list-inline">
												<li class="publish-date"><i class="fa fa-clock-o"></i> Nov 3, 2015 </li>
												<li class="views"><i class="fa fa-eye"></i>15k</li>
												<li class="loves"><i class="fa fa-heart-o"></i>278</li>
											</ul>
										</div>
										<h2 class="entry-title">
											<a href="#">We finally found out how BB-8 really works</a>
										</h2>
									</div>
								</div><!--/post--> 
							</div>
							<div class="col-sm-4">
								<div class="post feature-post">
									<div class="entry-header">
										<div class="entry-thumbnail">
											<img class="img-responsive" src="images/post/slider/6.jpg" alt="" />
										</div>
										<div class="catagory politics"><span>Politics</span></div>
									</div>
									<div class="post-content">								
										<div class="entry-meta">
											<ul class="list-inline">
												<!--<li class="publish-date"><i class="fa fa-clock-o"></i> Nov 5, 2015 </li>-->
												<li class="views"><i class="fa fa-eye"></i>15k</li>
												<li class="loves"><i class="fa fa-heart-o"></i>278</li>
											</ul>
										</div>
										<h2 class="entry-title">
											<a href="#">And the most streamed Beatles song on Spotify is..</a>
										</h2>
									</div>
								</div><!--/post--> 
							</div>
						</div>
					</div><!--/#content--> 
					
					<div class="col-md-3 visible-md visible-lg">
						<div class="add featured-add">
							<a href="#"><img class="img-responsive" src="images/post/add/add1.jpg" alt="" /></a>
						</div>
					</div><!--/#add--> 
				</div>
			</div><!--/.section--> 
			
			<div class="section add inner-add">
				<a href="#"><img class="img-responsive" src="images/post/add/add2.jpg" alt="" /></a>
			</div><!--/.section-->
			
			<div class="section">				
				<div class="latest-news-wrapper">
					<h1 class="section-title">Latest News</h1>	
					<div id="latest-news">
						
						@foreach($result as $res)
						<div class="post medium-post">
							<div class="entry-header">
								<div class="entry-thumbnail">
									<a href="{!! route('news.route', ['news_id'=>$res->id, 'user_id'=>$user_id]) !!}"><img class="img-responsive" src="{{asset('admin/images/').'/'.$res->image1}}"  height="225px" width="270px" style="min-width:100%;max-height: 150px;" alt=""/></a>
									
								</div>
								<!--<div class="catagory politics"><span><a href="#">Politics</a></span></div>-->
							</div>
							<div class="post-content">								
								<div class="entry-meta">
									<ul class="list-inline">
										<!--<li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i> Nov 5, 2015 </a></li>-->
										<li class="views"><i class="fa fa-thumbs-o-up"></i>{!! $res->comment !!}</li>
										<li class="loves"><i class="fa fa-comment-o"></i>{!! $res->like !!}</li>
									</ul>
								</div>
								<h2 class="entry-title">
									<a href="{!! route('news.route', ['news_id'=>$res->id, 'user_id'=>$user_id]) !!}">{!! $res->news_title !!}</a>
									<!--<a href="#">{!! $res->news_title !!}</a>-->
								</h2>
							</div>
						</div><!--/post--> 
						@endforeach
					</div>
				</div><!--/.latest-news-wrapper-->
			</div><!--/.section-->
		</div><!--/.container-->
		 <!--<div id="twitter-feed">
			<div class="container text-center">
				<div class="row">
					<div class="col-sm-8 col-sm-offset-2">
						<div class="twitter-feeds">
							<div class="twitter-feed">
								<img class="img-responsive" src="images/others/twitter.png" alt="" />
								<h2>#Newspress</h2>
								<p>Confusing <a href="#">#design</a> terms: what's the difference between quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. <a href="#">#UX, #UI and #IA? http://buff.ly/1KdjpEi  </a> </p>
							</div>
							<div class="twitter-feed">
								<img class="img-responsive" src="images/others/twitter.png" alt="" />
								<h2>#ThemeRegion</h2>
								<p>Confusing <a href="#">#design</a> terms: what's the difference between quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. <a href="#">#UX, #UI and #IA? http://buff.ly/1KdjpEi  </a> </p>
							</div>
							<div class="twitter-feed">
								<img class="img-responsive" src="images/others/twitter.png" alt="" />
								<h2>#Doors</h2>
								<p>Confusing <a href="#">#design</a> terms: what's the difference between quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. <a href="#">#UX, #UI and #IA? http://buff.ly/1KdjpEi  </a> </p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><!--/#twitter-feed-->
</div>

<!--<div class="subscribe-me text-center">
	<h1>Don’t Miss The Hottest News</h1>
	<h2>Subscribe our Newsletter</h2>
	<a href="#close" class="sb-close-btn"><img class="<img-responsive></img-responsive>" src="images/others/close-button.png" alt="" /></a>
	<form action="#" method="post" id="popup-subscribe-form" name="subscribe-form">			
		<div class="input-group">
			<span class="input-group-addon"><img src="images/others/icon-message.png" alt="" /></span>
			<input type="text" placeholder="Enter your email" name="email">
			<button type="submit" name="subscribe">Go</button>
		</div>
	</form>
</div> --> <!--/.subscribe-me--> 
@stop

@section('post-script')
    <!-- Revolution Slider -->
    <script type="text/javascript">
        jQuery('.tp-banner').show().revolution(
        {
            dottedOverlay:"none",
            delay:16000,
            startwidth:1170,
            startheight:700,
            hideThumbs:200,

            thumbWidth:100,
            thumbHeight:50,
            thumbAmount:5,

            navigationType:"nexttobullets",
            navigationArrows:"solo",
            navigationStyle:"preview",

            touchenabled:"on",
            onHoverStop:"on",

            swipe_velocity: 0.7,
            swipe_min_touches: 1,
            swipe_max_touches: 1,
            drag_block_vertical: false,

            parallax:"mouse",
            parallaxBgFreeze:"on",
            parallaxLevels:[7,4,3,2,5,4,3,2,1,0],

            keyboardNavigation:"off",

            navigationHAlign:"center",
            navigationVAlign:"bottom",
            navigationHOffset:0,
            navigationVOffset:20,

            soloArrowLeftHalign:"left",
            soloArrowLeftValign:"center",
            soloArrowLeftHOffset:20,
            soloArrowLeftVOffset:0,

            soloArrowRightHalign:"right",
            soloArrowRightValign:"center",
            soloArrowRightHOffset:20,
            soloArrowRightVOffset:0,

            shadow:0,
            fullWidth:"on",
            fullScreen:"off",

            spinner:"spinner4",

            stopLoop:"off",
            stopAfterLoops:-1,
            stopAtSlide:-1,

            shuffle:"off",

            autoHeight:"off",
            forceFullWidth:"off",



            hideThumbsOnMobile:"off",
            hideNavDelayOnMobile:1500,
            hideBulletsOnMobile:"off",
            hideArrowsOnMobile:"off",
            hideThumbsUnderResolution:0,

            hideSliderAtLimit:0,
            hideCaptionAtLimit:0,
            hideAllCaptionAtLilmit:0,
            startWithSlide:0,
            videoJsPath:"rs-plugin/videojs/",
            fullScreenOffsetContainer: ""
        });
    </script>
@stop