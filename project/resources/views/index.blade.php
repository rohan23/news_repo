@extends('layout.base')
<!-- Load More locational News -->
<script>
	function locational_news(val1)
	{
		  jQuery.ajax({
		     url: '/index_locational',
		     type: 'POST',
		     data: {index: val1},
		     success: function (data) {
		 	 jQuery("#locational").append(data);
		 	 $('#hey').remove();
		    //alert('success');
		    }
		 });
	}
</script>
<!-- Load More locational News -->
<!-- Load More Country News -->
<script>
	function country_news(val1)
	{
		jQuery.ajax({
			url: '/index_country',
			type: 'POST',
			data: {index: val1},
			success: function (data) {
				jQuery("#country_news").append(data);
				$('#hey1').remove();
			}
		});
	}
</script>
<!-- Load More Country News -->
@section('body')
 
<p style="display:none;">Lat={!! $latitude= Session::get('user_lat'); !!}</p>
<p style="display:none;">long={!! $longitude= Session::get('user_lon'); !!}</p>
<?php  

if($latitude==""  && $longitude=="")
{
?>	
<script type="text/javascript">
function success()
{	
	var lat=3.1502860618571256;
	var lng=101.61503791809082;
	var ver="success";
	$.ajax({
		 type:'POST',
		 url:'/ajaxurl',
		 data:'latt='+lat +'&lngg='+lng +'&verify='+ver,
		 success: function(data) 
			{
				location.reload();
			}
	  });
}
window.onload = success();
</script>

<?php
}	

?>
<input type="hidden" id="lat" />
<input type="hidden" id="lon"  />
<?php 
$uid=Session::get('user_id');
if($uid=="")
{
	$user_id="0";
}
else{
	$user_id=$uid;
}

//echo "<pre>";print_r($latest_data);echo "</pre>";

$f_type=Session::get('primery_filter');

function array_orderby()
{
    $args = func_get_args();
    $data = array_shift($args);
    foreach ($args as $n => $field) {
        if (is_string($field)) {
            $tmp = array();
            foreach ($data as $key => $row)
                $tmp[$key] = $row[$field];
            $args[$n] = $tmp;
            }
    }
    $args[] = &$data;
    call_user_func_array('array_multisort', $args);
    return array_pop($args);
}
if($f_type!="")
{
	if($f_type=="radius")
	{
		if(array_key_exists("locational_news", $latest_data))
		{	
			$latest_data['locational_news'] = array_orderby($latest_data['locational_news'], 'news_distance', SORT_ASC);
		}
		if(array_key_exists("general_news", $latest_data))
		{
			$latest_data['general_news'] = array_orderby($latest_data['general_news'], 'time_diff', SORT_ASC);
		}
		if(array_key_exists("news", $local_data))
		{
			$local_data['news'] = array_orderby($local_data['news'], 'news_distance', SORT_ASC);	
		}
		if(array_key_exists("news", $country_data))
		{
			$country_data['news'] = array_orderby($country_data['news'], 'time_diff', SORT_ASC);	
		}
	}
	else
	{
		if(array_key_exists("locational_news", $latest_data))
		{
			$latest_data['locational_news'] = array_orderby($latest_data['locational_news'], 'time_diff', SORT_ASC);
		}
		if(array_key_exists("general_news", $latest_data))
		{
			$latest_data['general_news'] = array_orderby($latest_data['general_news'], 'time_diff', SORT_ASC);
		}
	
		if(array_key_exists("news", $local_data))
		{
			$local_data['news'] = array_orderby($local_data['news'], 'time_diff', SORT_ASC);
		}
		if(array_key_exists("news", $country_data))
		{	
			$country_data['news'] = array_orderby($country_data['news'], 'time_diff', SORT_ASC);
		}
	}
}
/*
echo "<pre>";
print_r($country_data);
echo "</pre>";
*/
?>

</style>
<body>
		<div class="container">
			<div id="main-slider">
				
				<?php
				if($latest_data['status']['message']=="Successful")
				{
					if(array_key_exists('locational_news', $latest_data))
					{
				$c=count($latest_data['locational_news']); 
				for($i=0;$i<$c;$i++)
				{
					 $lnews_id= $latest_data['locational_news'][$i]['id'];
					 $lcategory_id= $latest_data['locational_news'][$i]['category'];
					 $lnews_title= $latest_data['locational_news'][$i]['news_title'];
					 $luser_id= $latest_data['locational_news'][$i]['user_id'];
					 $limage1= $latest_data['locational_news'][$i]['image1'];
					 $ldistance= $latest_data['locational_news'][$i]['news_distance'];
					  $lnews_time= $latest_data['locational_news'][$i]['news_time'];
					 
					  $now1 = new DateTime();
							$ts11 = strtotime($lnews_time);
							$ts22 = $now1->getTimestamp();
							$diff = $ts22 - $ts11;
							$hourdiff1 = round(( $now1->getTimestamp()-strtotime($lnews_time))/3600, 1);
							$locational_days=intval($hourdiff1/24);
							
							if($locational_days=="0")
							{
								$ldays="Today";
							}
							else if($locational_days=="1")
							{
								$ldays="Yesterday";
							}
							else 
							{
								$ldays=$locational_days." days ago";	
							}
							
					 $l_news = substr($lnews_title,0,70);
					
				?>
				<div class="post feature-post" style="background-image:url({{asset('admin/images/').'/'.$limage1}});  background-size:cover;">
					
					<div class="post-content">
						
						<div >
							<i class="fa fa-location-arrow" aria-hidden="true"></i><?php echo $ldistance; ?>km	
							<i class="fa fa-clock-o"></i><?php echo $ldays; ?>
							<?php 
													if($luser_id==$user_id)
													{
													?>
													<a style="float:right;" href="{!! route('news.delete', ['news_id'=>$lnews_id]) !!}"><i class="fa fa-trash"></i></a>
													<?php	
													}
												?>	
							</div>
							<h3 class="entry-title">
							<a href="{!! route('news.route', ['news_id'=>$lnews_id]) !!}"><?php echo $l_news; ?></a>
							
								
							</h3>
							
						
					</div>
				</div><!--/post-->
				<?php } } }?>
			
					
				<?php
				if($latest_data['status']['message']=="Successful")
				{
					if(array_key_exists('general_news', $latest_data))
					{
				$a=count($latest_data['general_news']); 
				for($b=0;$b<$a;$b++)
				{
					 $gnews_id= $latest_data['general_news'][$b]['id'];
					 $gcategory_id= $latest_data['general_news'][$b]['category'];
					 $gnews_title= $latest_data['general_news'][$b]['news_title'];
					 $guser_id= $latest_data['general_news'][$b]['user_id'];
					 $gimage1= $latest_data['general_news'][$b]['image1'];
					 $gdistance= $latest_data['general_news'][$b]['news_distance'];
					 $gcountry= $latest_data['general_news'][$b]['country'];
					 $gnews_time= $latest_data['general_news'][$b]['news_time'];
					 
					  $now1 = new DateTime();
							$ts11 = strtotime($gnews_time);
							$ts22 = $now1->getTimestamp();
							$diff = $ts22 - $ts11;
							$hourdiff1 = round(( $now1->getTimestamp()-strtotime($gnews_time))/3600, 1);
							$general_days=intval($hourdiff1/24);
					 		
							if($general_days=="0")
							{
								$gdays="Today";
							}
							else if($general_days=="1")
							{
								$gdays="Yesterday";
							}
							else 
							{
								$gdays=$general_days." days ago";	
							}
							
					$gnews = substr($gnews_title,0,70);
				?>
				<div class="post feature-post" style="background-image:url({{asset('admin/images/').'/'.$gimage1}});  background-size:cover;">
					
					<div class="post-content">
				
						<div >
							
							<i class="fa fa-globe" aria-hidden="true"></i></i><?php echo $gcountry; ?>
							<i class="fa fa-clock-o"></i><?php echo $gdays; ?>
							<?php 
													if($guser_id==$user_id)
													{
													?>
													<a style="float: right;" href="{!! route('news.delete', ['news_id'=>$gnews_id]) !!}"><i style="coor:#FFFFFF;" class="fa fa-trash"></i></a>
													<?php	
													}
												?>	
						</div>
						<h4 class="entry-title">
							<a href="{!! route('news.route', ['news_id'=>$gnews_id]) !!}"><?php echo $gnews; ?></a>
								
						</h3>
					</div>
				</div><!--/post-->
				
				<?php } } } ?>
			</div><!-- #main-slider -->
		</div>

	
		<div class="container">		
			
			
			
		<div class="row" style="padding-top:10px;">
			<div class="col-sm-9" id="locational">
				<h1 class="section-title title">Locational News</h1>
					<div class="row">
						
						<?php
						if($local_data['status']['message']=="Successful")
						{
							if(array_key_exists('news', $local_data))
							{
						$k=count($local_data['news']); 
						for($j=0;$j<$k;$j++)
						{
							 $lo_news_id= $local_data['news'][$j]['id'];
							 $lo_category_id= $local_data['news'][$j]['category'];
							 $lo_news_title= $local_data['news'][$j]['news_title'];
							 $lo_user_id= $local_data['news'][$j]['user_id'];
							 $lo_image1= $local_data['news'][$j]['image1'];
							 $lo_likes= $local_data['news'][$j]['like'];
							 $lo_time= $local_data['news'][$j]['news_time'];
							 $lo_distance= $local_data['news'][$j]['news_distance'];
							 //$date_only=explode(" ",$lo_time);
							 //$date=$date_only[0];
							
							 $now1 = new DateTime();
							$ts11 = strtotime($lo_time);
							$ts22 = $now1->getTimestamp();
							$diff = $ts22 - $ts11;
							$hourdiff1 = round(( $now1->getTimestamp()-strtotime($lo_time))/3600, 1);
							$local_days=intval($hourdiff1/24);
							
							if($local_days=="0")
							{
								$l_days="Today";
							}
							else if($local_days=="1")
							{
								$l_days="Yesterday";
							}
							else 
							{
								$l_days=$local_days." days ago";	
							}
							
							 $lo_news = substr($lo_news_title,0,50);
							 
							 
						?>
							<div class="col-sm-4">
								<div class="post feature-post" id="clearfix">
									<div class="entry-header">
										<div class="entry-thumbnail">
											
											<a href="{!! route('news.route', ['news_id'=>$lo_news_id]) !!}"><img style="height:280px; width: 340px;" class="img-responsive" src="{{asset('admin/images/').'/'.$lo_image1}}" alt="" /></a>
										</div>
										
									</div>
									<div class="post-content">								
										<div class="entry-meta">
											<ul class="list-inline" style="font-size: 12px;">
												<i class="fa fa-location-arrow" aria-hidden="true"></i><?php echo $lo_distance; ?>km &nbsp;
												<li class="publish-date"><i class="fa fa-clock-o"></i><?php echo $l_days; ?></li>
												<!-- <li class="views"><i class="fa fa-eye"></i><?php echo $lo_likes; ?></li> -->
												<?php 
													if($lo_user_id==$user_id)
													{
													?>
													<li class="loves"><a href="{!! route('news.delete', ['news_id'=>$lo_news_id]) !!}"><i class="fa fa-trash"></i></a></li>
													<?php	
													}
												?>	
											</ul>
										</div>
										<h2 class="entry-title">
											<a href="{!! route('news.route', ['news_id'=>$lo_news_id]) !!}"><?php echo $lo_news; ?></a>
										</h2>
									</div>
								</div><!--/post--> 	
							</div>
							
							<?php }} } ?>
								
						</div>
							<div class="row" style="text-align:right; padding:40px;" id="hey" >
								<div class="col-md-6"></div>
							<div onclick="locational_news('<?php echo $index+1; ?>')" class="col-md-6" style="padding: 10px;text-align: center;background-color: #33739E;border-style: solid;border-color: #fff;">
								<a  style="color:#fff;">Load More &nbsp;<i class="fa fa-long-arrow-down" aria-hidden="true"></i></a></div>
						
			
		</div>
								
						</div>
					
	
				
						<div class="col-sm-3" style="padding: 10px;" >
							<div class="widget" id="country_news">
								<h1 class="section-title title">Country News</h1>
							<?php
						if($country_data['status']['message']=="Successful")
						{
							if(array_key_exists('news', $country_data))
							{
								$u=count($country_data['news']); 
								for($v=0;$v<$u;$v++)
								{
									 $c_news_id= $country_data['news'][$v]['id'];
									 $c_category_id= $country_data['news'][$v]['category'];
									 $c_news_title= $country_data['news'][$v]['news_title'];
									 $c_user_id= $country_data['news'][$v]['user_id'];
									 $c_image1= $country_data['news'][$v]['image1'];
									 $c_likes= $country_data['news'][$v]['like'];
									 $c_time= $country_data['news'][$v]['news_time'];
									
									 $date_c=explode(" ",$c_time);
									 $date_c=$date_c[0];
									 
									 $c_news = substr($c_news_title,0,70);
								?>
									
										<ul class="post-list">
											<li>
												<div class="post small-post">
													<div class="entry-header">
														<div class="entry-thumbnail">
															<a href="{!! route('news.route', ['news_id'=>$c_news_id]) !!}"><img class="img-responsive" src="{{asset('admin/images/').'/'.$c_image1}}" alt="" /></a>
														</div>
													</div>
													<div class="post-content">								
														
														<h2 class="entry-title" style="font-size:16px;">
															<a href="{!! route('news.route', ['news_id'=>$c_news_id]) !!}"><?php echo $c_news; ?></a>
														</h2>
													</div>
												</div>
											</li>
											
										</ul>
								<?php } } } ?>
							</div>
									<div class="row" style="text-align:right; padding:40px;" id="hey1" >
							
							<div onclick="country_news('<?php echo $index+1; ?>')" class="col-md-12" style="padding: 10px;text-align: center;background-color: #33739E;border-style: solid;border-color: #fff;">
								<a  style="color:#fff;">Load More &nbsp;<i class="fa fa-long-arrow-down" aria-hidden="true"></i></a></div>
						
			
		</div>
						</div>
		</div>
		
		
		</div><!--/.container--> 
		
		

	

</body>
</html>
@stop

@section('post-script')
 
@stop