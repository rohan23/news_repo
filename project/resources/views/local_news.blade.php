@extends('layout.base')

@section('body')
<?php 
$uid=Session::get('user_id');
if($uid=="")
{
	$user_id="0";
}
else{
	$user_id=$uid;
}



$f_type=Session::get('primery_filter');

function array_orderby()
{
    $args = func_get_args();
    $data = array_shift($args);
    foreach ($args as $n => $field) {
        if (is_string($field)) {
            $tmp = array();
            foreach ($data as $key => $row)
                $tmp[$key] = $row[$field];
            $args[$n] = $tmp;
            }
    }
    $args[] = &$data;
    call_user_func_array('array_multisort', $args);
    return array_pop($args);
}

if($f_type!="")
{
	if($f_type=="radius")
	{
		if(array_key_exists("news", $latest_data))
		{	
			$latest_data['news'] = array_orderby($latest_data['news'], 'news_distance', SORT_ASC);
		}
		
	}
	else
	{
		if(array_key_exists("news", $latest_data))
		{
			$latest_data['news'] = array_orderby($latest_data['news'], 'time_diff', SORT_ASC);
		}
	
	}
}

// echo "<pre>";
 // print_r($latest_data);
// echo "</pre>";

?>
<style>
.post-content
	{
	 min-height:100px; 
	 max-height: 100px; 
	 height: 100px;
	}
	
	.center-cropped {
  object-fit: none; /* Do not scale the image */
  object-position: center; /* Center the image within the element */
  height: 200px;
  width: 100px;
}
</style>
<div class="container">
	<div class="page-breadcrumbs">
		<h1 class="section-title">Local News</h1>	
	</div>
	<div class="section">
		<div class="row">
			<div class="col-sm-9">
				<div class="row">
					<div class="col-sm-12">
						<div class="section">
							<div class="row">
								<?php 
								if(array_key_exists("news", $latest_data))
								{
									$count_news=count($latest_data['news']);
									for($v=0; $v<$count_news; $v++)
									{
										 $c_news_id= $latest_data['news'][$v]['id'];
										 $c_category_id= $latest_data['news'][$v]['category'];
										 $c_news_title= $latest_data['news'][$v]['news_title'];
										 $c_user_id= $latest_data['news'][$v]['user_id'];
										 $c_image1= $latest_data['news'][$v]['image1'];
										 $c_likes= $latest_data['news'][$v]['like'];
										 $c_time= $latest_data['news'][$v]['news_time'];
										 $c_comment= $latest_data['news'][$v]['comment'];
										 $c_country= $latest_data['news'][$v]['country'];
										 $c_news_time=$latest_data['news'][$v]['news_time'];
										 $c_news_distance=$latest_data['news'][$v]['news_distance'];
										 $date_c=explode(" ",$c_time);
										 $date_c=$date_c[0];
										 $c_news = substr($c_news_title,0,50);
										 
										 $now2 = new DateTime();
											$ts12 = strtotime($c_news_time);
											$ts23 = $now2->getTimestamp();
											$diff = $ts23 - $ts12;
											$hourdiff2 = round(( $now2->getTimestamp()-strtotime($c_news_time))/3600, 1);
											$category_days1=round($hourdiff2/24);
											if($category_days1==0)
											{
												$days1="Today";
											}
											else if($category_days1==1)
											{
												$days1="Yesterday";
											}
											else
											{
												$days1=$category_days1." days ago";
											}
										 
									?>
								<div class="col-sm-4">
									<div class="post medium-post">
										<div class="entry-header">
											<div class="entry-thumbnail">
												<a href="{!! route('news.route', ['news_id'=>$c_news_id]) !!}"><img class="center-cropped img-responsive" src="{{asset('admin/images/').'/'.$c_image1}}" alt="" style="width: 100%; height: 180px;" /></a>
												
											</div>
										</div>
										<div class="post-content">								
											<div class="entry-meta">
												<ul class="list-inline">
													<li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i> <?php echo $days1; ?></a></li>
													<li class="loves"><i class="fa fa-location-arrow" aria-hidden="true"></i><?php echo $c_news_distance; ?>km</li>	
													<li class="views"><i class="fa fa-thumbs-o-up"></i><?php echo $c_likes; ?></li>
													<li class="loves"><i class="fa fa fa-comment-o"></i><?php echo $c_comment; ?></li>
												</ul>
											</div>
											<h2 class="entry-title">
												<a href="{!! route('news.route', ['news_id'=>$c_news_id]) !!}"><?php echo $c_news; ?></a>
											</h2>
										</div>
									</div><!--/post--> 
								</div>
									<?php
									}
								}
								
								
								?>
								
								
							</div>
						</div><!--/.section -->	
					</div>
				</div>
			</div><!--/.col-sm-9 -->	
			
			<div class="col-sm-3">
				<div id="sitebar">
					<div class="widget follow-us">
						<h1 class="section-title title">Follow Us</h1>
						<ul class="list-inline social-icons">
							<li><a href="#"><i class="fa fa-facebook"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
							<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
							<li><a href="#"><i class="fa fa-youtube"></i></a></li>
						</ul>
					</div><!--/#widget-->
				</div><!--/#sitebar-->
			</div>
		</div>				
	</div><!--/.section-->
</div><!--/.container-->


		
@stop