@extends('layout.base')

@section('body')
<?php 
	$uid=Session::get('user_id');
if($uid=="")
{
	$user_id="0";
}
else{
	$user_id=$uid;

}

$role=Session::get('role');

$country=Session::get('country');

 count($categories['data']);


?>
<!-- Google Fonts -->
	
	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">

	<!-- Styles -->
	<link href="jQuery.filer-1.3.0/css/jquery.filer.css" rel="stylesheet">

	<!-- Jvascript -->
	<script src="http://code.jquery.com/jquery-3.1.0.min.js" crossorigin="anonymous"></script>
	<script src="jQuery.filer-1.3.0/js/jquery.filer.min.js" type="text/javascript"></script>
	<script src="jQuery.filer-1.3.0/examples/default/js/custom.js" type="text/javascript"></script>
	 <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAXNYV2WAo_qX6kbNNGMCC_vFFSD1onJ5A&libraries=places&callback=initAutocomplete"
         async defer></script>
	<script>
      // This example adds a search box to a map, using the Google Place Autocomplete
      // feature. People can enter geographical searches. The search box will return a
      // pick list containing a mix of places and predicted search terms.

      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      function initAutocomplete() {
        var map = new google.maps.Map(document.getElementById('gmap'), {
          center: {lat: 4.2105, lng: 101.9758},
          zoom: 7,
          mapTypeId: 'roadmap'
        });

        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        
        google.maps.event.addListener(map, "click", function(event) {
                // get lat/lon of click
                var clickLat = event.latLng.lat();
                var clickLon = event.latLng.lng();
                document.getElementById('lon').value = clickLat;
         document.getElementById('lat').value = clickLon;
         
                
				for(i=0; i<markers.length; i++){
			        markers[i].setMap(null);
			    }
				markers = [];
	            markers.push(new google.maps.Marker({
              		map: map,
              		position: new google.maps.LatLng(clickLat,clickLon)
              		
            	}));
});

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }

          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location
            }));

            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });
      }

    </script>
	<style>
        

        hr {
            margin-top: 20px;
            margin-bottom: 20px;
            border: 0;
            border-top: 1px solid #eee;
        }
        
         div#gmap {
        width: 100%;
        height: 500px;
        border:double;
        
        
 }
  #c
 {
 	text-align:left;
 }
 .controls {
        margin-top: 10px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 300px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }

      .pac-container {
        font-family: Roboto;
      }

      #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
      }

      #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }
      #target {
        width: 345px;
      }
 
    </style>
    
    <!-- Multi Select CSS -->
    <link href="http://nearbynews.co/multiselct-js-css/jquery.multiselect.css" rel="stylesheet" type="text/css">
   	<!-- Multi Select CSS --> 
<!--Add News Page -->
		<div class="signup-page">
			<div class="container">
				<div class="row">
					<!-- user-login -->			
					<div class="col-md-9 col-sm-offset-1 col-sm-12">
						<div class="ragister-account">		
							<h1 class="section-title title">Post Your News</h1>
							
							<form id="registation-form"  method="post" action="news_insert" enctype="multipart/form-data">
								
								
								{!! csrf_field() !!}
								
								<input type="hidden" name="country" class="form-control" value="{!! $longitude= Session::get('country'); !!}" >
								<input type="hidden" name="user_id" class="form-control" value="{!! $longitude= Session::get('user_id'); !!}" >
								<input type="hidden" name="role" class="form-control" value="{!! $longitude= Session::get('role'); !!}" >
								<div class="form-group">
									<label>Select Category</label>
									<select id="langOpt" multiple size="4" name="category[]" required="required" style="width:100%;"> 
										<option value="">Select Category</option>
										<?php
										$c=count($categories['data']);
										for($b=0;$b<$c;$b++)
										{
											$c_id=$categories['data'][$b]['id'];
											$c_name=$categories['data'][$b]['category'];
						
						 
										?>
											<option value="<?php echo $c_id; ?>"><?php echo $c_name; ?></option>
										<?php } ?>
										
										
									</select>
								</div>
								<div class="form-group">
									<label>Select Language</label>
									<select class="form-control" name="language" required="required"> 
										<option value="">Select Language</option>
										<?php
										$f=count($languages['data']);
										for($a=0;$a<$f;$a++)
										{
											$l_id=$languages['data'][$a]['id'];
											$l_name=$languages['data'][$a]['language'];
						
						 
										?>
											<option value="<?php echo $l_id; ?>"><?php echo $l_name; ?></option>
										<?php } ?>
										
										
									</select>
								</div>
								<div class="form-group">
									<label>News Title</label>
									<input type="text" name="title" class="form-control" required="required" placeholder="News Title">
								</div>
								
								<div class="form-group">
									<label>News Text</label>
									<textarea rows="3" style="width: 100%;" name="text"  required="required" >News Description</textarea>
								</div><br />
								
								<!-- <div class="form-group">
									<label>Your image</label>
									<input type="file" multiple="mulltiple" style="padding: 0px;" name="image1" accept="image/*" class="form-control">
								</div> -->
							  <div id="content">
							
								    <input type="file" name="images[]" id="filer_input" class="form-control"  multiple="multiple" required="required">
								
								<!-- end of Example 1 -->
						
						   </div>
						   
						  					
							<div class="col-md-12">
						<div class="widget">

								
								<!-- /Toolbar -->
							
							<div class="widget-content">
								 <input id="pac-input" class="controls" type="text" placeholder="Search Box">
    							<div id="gmap"></div>
							</div>
						</div>
						<br>
						<?php 
						if($role=="user")
						{?>
							 <div class="form-group">
						   		<div class="col-md-6">
									<div class="col-md-4"><label>LATITUDE</label></div>
									<div class="col-md-8"><input type="text" id='lat' class="form-control" name="latitude" required="required" placeholder="Select latitude from map"></div>
								</div>
								
						    <div class="col-md-6">
									<div class="col-md-4"><label>LONGITUDE</label></div>
									<div class="col-md-8"><input type="text" id='lon' name="longitude" class="form-control" required="required" placeholder="Select longitude from map"></div>
								</div>
								</div>	
						<?php 
						}
						else 
						{ ?>
							 <div class="form-group">
						   		<div class="col-md-6">
									<div class="col-md-4"><label>LATITUDE</label></div>
									<div class="col-md-8"><input type="text" id='lat' class="form-control" name="latitude" value="0.0" placeholder="Select latitude from map"></div>
								</div>
								
						    <div class="col-md-6">
									<div class="col-md-4"><label>LONGITUDE</label></div>
									<div class="col-md-8"><input type="text" id='lon' name="longitude" class="form-control" value="0.0"  placeholder="Select longitude from map"></div>
								</div>
								</div>	
						<?php } ?>
							
					</div>
								<div class="submit-button text-center">
									<button type="submit" class="btn btn-primary">Submit</button>
								</div>
							</form>	
						</div>
					</div><!-- user-login -->			
				</div><!-- row -->	
			</div><!-- container -->
		</div><!-- signup-page -->
<!-- Multi Select JS -->
<script src="http://nearbynews.co/multiselct-js-css/jquery.min.js"></script>
<script src="http://nearbynews.co/multiselct-js-css/jquery.multiselect.js"></script>
<script>
$('#langOpt').multiselect({
    columns: 1,
    placeholder: 'Select Languages'
});

$('#langOpt2').multiselect({
    columns: 1,
    placeholder: 'Select Languages',
    search: true
});

$('#langOpt3').multiselect({
    columns: 1,
    placeholder: 'Select Languages',
    search: true,
    selectAll: true
});

$('#langOptgroup').multiselect({
    columns: 4,
    placeholder: 'Select Languages',
    search: true,
    selectAll: true
});
</script>
<!-- Multi Select JS -->



@stop














