
<?php 
$uid=Session::get('user_id');

$user_id=$profile_data['user_profile']['id'];
$profile_name=$profile_data['user_profile']['name'];
$profile_image=$profile_data['user_profile']['image'];
$profile_id=$profile_data['user_profile']['id'];
$follower=$profile_data['user_profile']['follow'];
$profile_type=$profile_data['user_profile']['role_type'];
$country=$profile_data['user_profile']['country'];
$user_time=$profile_data['user_profile']['time'];
$news_likes=$profile_data['news likes'];
$is_follow=$profile_data['is_follow'];


	$now1 = new DateTime();
	$ts11 = strtotime($user_time);
	$ts22 = $now1->getTimestamp();
	$diff = $ts22 - $ts11;
	$hourdiff1 = round(( $now1->getTimestamp()-strtotime($user_time))/3600, 1);
	$profile_days=intval($hourdiff1/24);
	if($profile_days=="0")
	{
		$days="Today";
	}
	else 
	{
		$days=$profile_days." days";	
	}

?>

<style>
	body {
  background: #F1F3FA;
}

/* Profile container */
.profile {
  margin: 20px 0;
}

/* Profile sidebar */
.profile-sidebar {
  padding: 20px 0 10px 0;
  background: #fff;
}

.profile-userpic img {
  float: none;
  margin: 0 auto;
  width: 50%;
  height: 50%;
  -webkit-border-radius: 50% !important;
  -moz-border-radius: 50% !important;
  border-radius: 50% !important;
}

.profile-usertitle {
  text-align: center;
  margin-top: 20px;
}

.profile-usertitle-name {
  color: #5a7391;
  font-size: 16px;
  font-weight: 600;
  margin-bottom: 7px;
}

.profile-usertitle-job {
  text-transform: uppercase;
  color: #5b9bd1;
  font-size: 12px;
  font-weight: 600;
  margin-bottom: 15px;
}

.profile-userbuttons {
  text-align: center;
  margin-top: 10px;
}

.profile-userbuttons .btn {
  text-transform: uppercase;
  font-size: 11px;
  font-weight: 600;
  padding: 6px 15px;
  margin-right: 5px;
}

.profile-userbuttons .btn:last-child {
  margin-right: 0px;
}
    
.profile-usermenu {
  margin-top: 30px;
}

.profile-usermenu ul li {
  border-bottom: 1px solid #f0f4f7;
}

.profile-usermenu ul li:last-child {
  border-bottom: none;
}

.profile-usermenu ul li a {
  color: #93a3b5;
  font-size: 14px;
  font-weight: 400;
}

.profile-usermenu ul li a i {
  margin-right: 8px;
  font-size: 14px;
}

.profile-usermenu ul li a:hover {
  background-color: #fafcfd;
  color: #5b9bd1;
}

.profile-usermenu ul li.active {
  border-bottom: none;
}

.profile-usermenu ul li.active a {
  color: #5b9bd1;
  background-color: #f6f9fb;
  border-left: 2px solid #5b9bd1;
  margin-left: -2px;
}

/* Profile Content */
.profile-content {
  padding: 20px;
  background: #fff;
  min-height: 460px;
}
.follow
{
	background-color:#2D93FF;
	padding:5px 10px 5px 10px;
	color:#ffffff;
	text-align: center;
	font-weight: 300px;
}
.follow:hover
{
	color:#fff;
}
.unfollow
{
	background-color:#ff3f3e;
	padding:5px 10px 5px 10px;
	color:#ffffff;
	text-align: center;
	font-weight: 300px;
}
.unfollow:hover
{
	color:#fff;
}
.edit{
    cursor: pointer;
}
</style>

    
		<div class="col-md-3">
			<div class="profile-sidebar">
				<!-- SIDEBAR USERPIC -->
				<div class="profile-userpic">
					<?php if($profile_image!="")
					{?>
						
					<img style="max-height:500px; width:60%;" class="center-cropped img-responsive" src="{{asset('admin/images/').'/'.$profile_image}}" alt="" />
				    <?php } 
				    else
				    {?>
				    	<img style="max-height:500px; width:60%;" class="center-cropped img-responsive" src="{{asset('images/user.png')}}" alt="" />
				    <?php } ?>
				</div>
				<?php
				if($user_id==$uid)
				{?>
					<div style="float:right; margin-right: 5px;"><a class="edit"  data-toggle="modal" data-target="#pro"><i style="color:#303030;" class="fa fa-pencil" aria-hidden="true"></i></a></div>
				<?php } ?>
				
				
				
					<!-- Modal For Edit Data-->
						<div class="modal fade" id="pro" role="dialog">
						<div class="modal-dialog">
						<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title" style="text-align: center;">Edit Your Data </h4>
								</div>
								<form action="{{url('profile_edit')}}" method="post" enctype="multipart/form-data">
									{!! csrf_field() !!}															       
									<div class="modal-body">
										<input type="hidden" name="id" value="<?php echo $user_id; ?>">
										<div class="row" style="padding-bottom: 10px;">
											<div class="col-md-2"><label>Image</label></div>
											<div class="col-md-10"><input type="file" name="image" class="form-control" ></div>
	 									</div>
	 									
										<div class="row" style="padding-bottom: 10px;">
											<div class="col-md-2"><label>Name</label></div>
											<div class="col-md-10"><input type="text" name="name" value="<?php echo $profile_name; ?>" class="form-control" ></div>
	 									</div>
										
										
										<br>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
											<button type="submit" class="btn btn-primary">Submit</button>
										</div>
								  	</div>
							  	</form>
							</div>
						</div>
					</div>
					<!-- End edit data Modal -->
				
				<!-- END SIDEBAR USERPIC -->
				<!-- SIDEBAR USER TITLE -->
				<div class="profile-usertitle">
					<div class="profile-usertitle-name">
						<?php  echo strtoupper($profile_name); ?>
					</div>
					<div class="profile-usertitle-job">
						<?php echo $profile_type; ?>
					</div>
				</div>
				<!-- END SIDEBAR USER TITLE -->
				<!-- SIDEBAR BUTTONS -->
				<div class="profile-userbuttons">
					<a class="btn btn-success btn-sm"><?php echo $follower; ?>&nbsp; Followers</a>
					<a class="btn btn-danger btn-sm"><?php echo $news_likes; ?>&nbsp;Likes</a>
				</div>
				<br>
				<p style="text-align: center;">User Since: <?php echo $days;?></p>
				<?php
				
				if($user_id!=$uid)
				{
				if($is_follow=="true")
				{?>
					<div class="text-center"><a href="{!! route('follow', ['u_id'=>$user_id , 'f_id'=>$uid]) !!}" class="unfollow">UNFOLLOW <?php echo strtoupper($profile_name); ?></a></div>
				<?php }
				else
				{
					if($uid!="")
					{?>
						<div class="text-center"><a href="{!! route('follow', ['u_id'=>$user_id , 'f_id'=>$uid]) !!}" class="follow">FOLLOW <?php echo strtoupper($profile_name); ?></a></div>
					<?php }
					else
					{?>
						<div class="text-center"><a href="{{url('login')}}" class="follow">FOLLOW <?php echo strtoupper($profile_name); ?></a></div>
					
				<?php }  } }?>
				
				
				
				
				
				<!-- END SIDEBAR BUTTONS -->
				<!-- SIDEBAR MENU -->
				<div class="profile-usermenu">
					<ul class="nav">
						<li class="active">
							<a href="{!! route('myaccount', ['user_id'=>$user_id , 'c_user'=>$uid]) !!}">
							<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
							Posts</a>
						</li>
						<li>
							<a href="{!! route('mycomments', ['user_id'=>$user_id]) !!}">
							<i class="fa fa-comments" aria-hidden="true"></i>
							Comments </a>
						</li>
						<!-- <li>
							<a href="#" target="_blank">
							<i class="fa fa-user" aria-hidden="true"></i>
							Edit Profile</a>
						</li> -->
						<li>
							<a href="{{url('logout')}}">
							<i class="fa fa-power-off" aria-hidden="true"></i>
							Logout</a>
						</li>
					</ul>
				</div>
				<!-- END MENU -->
			</div>
		</div>

	


