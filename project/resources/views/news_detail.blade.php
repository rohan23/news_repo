@extends('layout.base')

@section('body')
    

<script>
	function trans_value(val1, val2)
	{
		var val3='"'+document.getElementById("n_title").innerText+'"';
		var val4='"'+document.getElementById("n_text").innerText+'"';
		var val5=document.getElementById("exist_lang").innerText;
		//alert('translate_code=' + val1 + 'news_id=' + val2 + 'news_title=' + val3 + 'news_text=' + val4 + 'existing_code=' + val5);
		jQuery.ajax({
		    url: '/translate',
		    type: 'POST',
		    data: {translate_code: val1,news_id:val2,news_title:val3,news_text:val4,existing_lang:val5},
		    success: function (data) {
		 	jQuery("#translated_news").html(data);
		   //alert('success');
		    }
		});
		
	}
</script>
	<!-- Css For Image Zooming -->
<style>
.center-cropped {
  object-fit: none; /* Do not scale the image */
  object-position: center; /* Center the image within the element */
  height: 200px;
  width: 100%;
}
</style>
		
<?php 
//GET Array key name
$abc=array_keys($get_comm);

$c=Session::get('user_id');

if($c!="")
{
	$cuser=$c;
}
else
{
	$cuser="0";
}
//echo "<pre>"; print_r($getnews); echo "</pre>";

$category_name=$getnews['news']['0']['category_name'];


?>


<div class="container">
	<div class="section photo-gallery">
		<div class="col-sm-9">
			<h1 class="section-title title">News Detail</h1>	
			
			<div id="photo-gallery" class="row carousel slide carousel-fade post" data-ride="carousel">
			<?php 
			if (array_key_exists('news', $getnews)) 
			{
				$news_id=$getnews['news']['0']['id'];
				$user_news_id=$getnews['news']['0']['user_id'];
				$news_title=$getnews['news']['0']['news_title'];
				$news_language=$getnews['news']['0']['language'];
				$news_text=$getnews['news']['0']['news_text'];
				$news_time=$getnews['news']['0']['news_time'];
				$news_like=$getnews['news']['0']['like'];
				$news_type=$getnews['news']['0']['news_type'];
				$news_comment=$getnews['news']['0']['comment'];
				$news_country=$getnews['news']['0']['country'];
				$news_image1=$getnews['news']['0']['image1'];
				$news_image2=$getnews['news']['0']['image2'];
				$news_image3=$getnews['news']['0']['image3'];
				$news_image4=$getnews['news']['0']['image4'];
				$news_image5=$getnews['news']['0']['image5'];
				$news_image6=$getnews['news']['0']['image6'];
				$news_image7=$getnews['news']['0']['image7'];
				$news_image8=$getnews['news']['0']['image8'];
				$news_image9=$getnews['news']['0']['image9'];
				$user_name=$getnews['news']['0']['name'];
				$user_image=$getnews['news']['0']['image'];
				$language_name=$getnews['news']['0']['language_name'];
				
				$news_distance=$getnews['news']['0']['news_distance'];
				$user_like=$getnews['like']['Liked'];
				$user_flag=$getnews['flag']['Flagged'];
				
				$now1 = new DateTime();
				$ts11 = strtotime($news_time);
				$ts22 = $now1->getTimestamp();
				$diff = $ts22 - $ts11;
				$hourdiff1 = round(( $now1->getTimestamp()-strtotime($news_time))/3600, 1);
				$adays=intval($hourdiff1/24);
				
				if($adays=="0")
				{
					$days="Today";
				}
				if($adays=="1")
				{
					$days="Yesterday";
				}
				else 
				{
					$days=$adays." days";	
				}
				
			?>						
			<div class="carousel-inner">
				<div style="padding:20px;">
				</i>Categories:
				<?php
				
				//echo $category_name;
				
				$pp = explode(",",$category_name);
				for($t=0;$t < count($pp); $t++)
				{?>
					<?php echo $pp[$t];
				}
				?>
			</div>
				<?php 
				if($news_image1!="")
				{
				?>
				<div class="item active">
					<a href="{{asset('admin/images/').'/'.$news_image1}}" class="image-link"><img style="max-height:425px; width:100%;" class="center-cropped img-responsive" src="{{asset('admin/images/').'/'.$news_image1}}" alt="" /></a>
					
				</div>
				<?php
				}
				if($news_image2!="")
				{?>
				<div class="item">
					<a href="{{asset('admin/images/').'/'.$news_image2}}" class="image-link"><img style="max-height:425px; width:100%;" class="center-cropped img-responsive" src="{{asset('admin/images/').'/'.$news_image2}}" alt="" /></a>
				</div>
				<?php
				}
				if($news_image3!="")
				{?>
				<div class="item">
					<a href="{{asset('admin/images/').'/'.$news_image3}}" class="image-link"><img id="myImg" style="max-height:425px; width:100%;" class="center-cropped img-responsive" src="{{asset('admin/images/').'/'.$news_image3}}" alt="" /></a>
				</div>
				<?php
				}
				if($news_image4!="")
				{?>
				<div class="item">
					<a href="{{asset('admin/images/').'/'.$news_image4}}" class="image-link"><img id="myImg" style="max-height:425px; width:100%;" class="center-cropped img-responsive" src="{{asset('admin/images/').'/'.$news_image4}}" alt="" /></a>
				</div>
				<?php
				}
				if($news_image5!="")
				{?>
				<div class="item">
					<a href="{{asset('admin/images/').'/'.$news_image5}}" class="image-link"><img id="myImg" style="max-height:425px; width:100%;" class="center-cropped img-responsive" src="{{asset('admin/images/').'/'.$news_image5}}" alt="" /></a>
				</div>
				<?php
				}
				if($news_image6!="")
				{?>
				<div class="item">
					<a href="{{asset('admin/images/').'/'.$news_image6}}" class="image-link"><img id="myImg" style="max-height:425px; width:100%;" class="center-cropped img-responsive" src="{{asset('admin/images/').'/'.$news_image6}}" alt="" /></a>
				</div>
				<?php
				}
				if($news_image7!="")
				{?>
				<div class="item">
					<a href="{{asset('admin/images/').'/'.$news_image7}}" class="image-link"><img id="myImg" style="max-height:425px; width:100%;" class="center-cropped img-responsive" src="{{asset('admin/images/').'/'.$news_image7}}" alt="" /></a>
				</div>
				<?php
				}
				if($news_image8!="")
				{?>
				<div class="item">
					<a href="{{asset('admin/images/').'/'.$news_image8}}" class="image-link"><img id="myImg" style="max-height:425px; width:100%;" class="center-cropped img-responsive" src="{{asset('admin/images/').'/'.$news_image8}}" alt="" /></a>
				</div>
				<?php
				}
				if($news_image9!="")
				{?>
				<div class="item">
					<a href="{{asset('admin/images/').'/'.$news_image9}}" class="image-link"><img id="myImg" style="max-height:425px; width:100%;" class="center-cropped img-responsive" src="{{asset('admin/images/').'/'.$news_image9}}" alt="" /></a>
				</div>
			<?php } ?>
			</div><!--/carousel-inner-->
			<br><br>
			<div>
				
				<div style="float:right;">
				
					<div class="col-md-12">
					<div class="form-group">
						  <select class="form-control" id="translate" onChange="trans_value(this.value, <?php echo $news_id; ?>);">
						  	
				   	<?php
				   	 	$count1=count($languages['data']);
				   	 	for($f=0;$f<$count1;$f++)
						{
							if($language_name==$languages['data'][$f]['language']) 
							{
							?>
							<option selected value="<?php echo $languages['data'][$f]['language_code'] ?>"><?php echo $languages['data'][$f]['language']  ?></option>
							<?php
							}
							else{
								?>
							<option value="<?php echo $languages['data'][$f]['language_code'] ?>"><?php echo $languages['data'][$f]['language']  ?></option>	
								<?php
							}
							?>
							
					<?php 
						} 
			   		?>
						    </select>
						    
						    
					
				    
						</div>
						</div>
				</div>
			<ol class="gallery-indicators carousel-indicators">
				<?php
				if($news_image1!="")
				{?>
				<li data-target="#photo-gallery" data-slide-to="0" class="active">
					<img style="max-height:38px; width:38px;" class="img-responsive" src="{{asset('admin/images/').'/'.$news_image1}}" alt="" />
				</li>
				<?php
				}
				if($news_image2!="")
				{?>
				<li data-target="#photo-gallery" data-slide-to="1">
					<img style="max-height:38px; width:38px;" class="img-responsive" src="{{asset('admin/images/').'/'.$news_image2}}" alt="" />
				</li>
				<?php
				}
				if($news_image3!="")
				{?>
				<li data-target="#photo-gallery" data-slide-to="2">
					<img style="max-height:38px; width:38px;" class="img-responsive" src="{{asset('admin/images/').'/'.$news_image3}}" alt="" />
				</li>
				<?php
				}
				if($news_image4!="")
				{?>
				<li data-target="#photo-gallery" data-slide-to="3">
					<img style="max-height:38px; width:38px;" class="img-responsive" src="{{asset('admin/images/').'/'.$news_image4}}" alt="" />
				</li>
				<?php
				}
				if($news_image5!="")
				{?>
				<li data-target="#photo-gallery" data-slide-to="4">
					<img style="max-height:38px; width:38px;" class="img-responsive" src="{{asset('admin/images/').'/'.$news_image5}}" alt="" />
				</li>
				<?php
				}
				if($news_image6!="")
				{?>
				<li data-target="#photo-gallery" data-slide-to="5">
					<img style="max-height:38px; width:38px;" class="img-responsive" src="{{asset('admin/images/').'/'.$news_image6}}" alt="" />
				</li>
				<?php
				}
				if($news_image7!="")
				{?>
				<li data-target="#photo-gallery" data-slide-to="6">
					<img style="max-height:38px; width:38px;" class="img-responsive" src="{{asset('admin/images/').'/'.$news_image7}}" alt="" />
				</li>
				<?php
				}
				if($news_image8!="")
				{?>
				<li data-target="#photo-gallery" data-slide-to="7">
					<img style="max-height:38px; width:38px;" class="img-responsive" src="{{asset('admin/images/').'/'.$news_image8}}" alt="" />
				</li> 
				<?php
				}
				if($news_image9!="")
				{?>
				<li data-target="#photo-gallery" data-slide-to="8">
					<img style="max-height:38px; width:38px;" class="img-responsive" src="{{asset('admin/images/').'/'.$news_image9}}" alt="" />
				</li>
				<?php } ?>
			</ol><!--/gallery-indicators-->
			
			</div>
			<div class="post-content">								
				<div class="entry-meta">
					<ul class="list-inline">
						<li class="posted-by"><i class="fa fa-user"></i><a href="{!! route('myaccount', ['user_id'=>$user_news_id , 'c_user'=>$cuser]) !!}"> by {!! $user_name !!}</a></li>
						@if(Session::get('username')=="")
							<li class="loves"><a href="{{url('login')}}"><i class="fa fa-thumbs-o-up"></i>{!! $news_like !!}</a></li>
							<li class="loves"><a href="{{url('login')}}"><i class="fa fa-comment-o"></i>{!! $news_comment !!}</a></li>
						@else
							@if($user_like=='no')
							<li><a href="{!! route('likeunlike.route', ['news_id'=>$news_id, 'user_id'=>Session::get('user_id')]) !!}"><i class="fa fa-thumbs-o-up"></i>{!! $news_like !!}</a></li>
							<li class="comments"><i class="fa fa-comment-o"></i>{!! $news_comment !!}</li>
						@elseif($user_like=='yes')
							<li><a href="{!! route('likeunlike.route', ['news_id'=>$news_id, 'user_id'=>Session::get('user_id')]) !!}"><i class="fa fa-thumbs-up"></i>{!! $news_like !!}</a></li>
							<li class="comments"><i class="fa fa-comment-o"></i>{!! $news_comment !!}</li>
							@endif
						@endif
						
						@if($news_type=='0')
						
							
						<li class="publish-date" ><a href="#">	<i class="fa fa-globe" aria-hidden="true"></i><?php echo $news_country; ?></a></li> 	
						
						@else
						
							
						 <li class="publish-date" ><a href="{!! route('map.route', ['news_id'=>$news_id]) !!}"><i class="fa fa-location-arrow" aria-hidden="true"></i><?php echo $news_distance; ?>km</a></li> 	
							
					@endif
					
					<li class="publish-date"><i class="fa fa-clock-o"></i><?php echo $days; ?></li>
					
					
					@if(Session::get('username')!="" AND $user_flag=="no")
							<li style="float: right;"><a data-toggle="modal" data-target="#myModal"><img class="img-responsive" height="20px" width="20px" src="{{asset('images/flag.png')}}" alt="" /></a></li>
						@endif
					</ul>
					<!-- News Flag Modal -->
					<div class="modal fade" id="myModal" role="dialog">
						<div class="modal-dialog">
						<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title" style="text-align: center;">Report Here</h4>
								</div>
								<form action="{{url('report_news')}}" method="post">
									{!! csrf_field() !!}															       
									<div class="modal-body">
										<p style="font-size:16px; text-align: center;">Please enter reason below to report news </p>
										<div class="row">
											<input type="hidden" name="n_id" value="<?php echo $news_id; ?>">
											<input type="hidden" name="u_id" value="{!! Session::get('user_id') !!}">
											<div class="col-md-1"></div>
											<div class="col-md-10"><textarea class="form-control" name="message" placeholder="Enter Reason Here...."></textarea></div>
										</div>
										<br>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
											<button type="submit" class="btn btn-primary">Submit</button>
										</div>
								  	</div>
							  	</form>
							</div>
						</div>
					</div>
					<div id="translated_news">
						<span style="display:none; white-space: pre-wrap;" id="n_title"><?php echo $news_title; ?></span>
						<span style="display:none; white-space: pre-wrap;" id="n_text"><?php echo $news_text; ?></span>
						<span style="display:none;" id="exist_lang"><?php echo $language_name; ?></span>
					<h2 class="entry-title">
					{!! $news_title !!}
					</h2>
					<div class="entry-content">
						<p style="white-space: pre-wrap;">{!! $news_text !!}</p>
					</div>
					</div>
					<?php } ?>
				</div>
			</div>
		</div><!--/photo-gallery--> 
		
		<div class="row">
			<div class="col-sm-12">								
				<div class="comments-wrapper">
						
					<?php 
					if($abc['0']=="news")
					{
						$count_comments=count($get_comm['news']);
						if($count_comments>0)
						{
						?>
						<h1 class="section-title title">Comments</h1>
						<?php
						}
						?>
						
						<?php 
						for($i=0; $i<$count_comments; $i++)
						{
							$comm_flag= $get_comm['news'][$i]['comment_flag'];
							if($comm_flag==0)
							{
							?>
								
								<ul class="media-list">
								<li class="media">
									<div class="media-left">
										<p><?php $user_comment_image= $get_comm['news'][$i]['image']; ?></p>
										<span style="display: none;">{!! $path=public_path(); !!}</span>
										<?php 
										if(file_exists($path."/admin/images/".$user_comment_image) && $user_comment_image!="")
										{
										?>
											<img class="media-object" src="{{asset('admin/images/').'/'.$user_comment_image}}" alt="">
										<?php	
										}
										else
										{
										?>
											<img class="media-object" src="{{asset('images/user.png')}}" alt="">
										<?php
										}
										?>
									</div>
									<div class="media-body">
										<h2 style="color:#000;"><?php echo $get_comm['news'][$i]['name']; ?></h2>
										<p ><?php echo $get_comm['news'][$i]['comment']; ?></p>
										<p><?php $comm_id= $get_comm['news'][$i]['id']; ?></p>
										<p><?php $comm_user_id= $get_comm['news'][$i]['user_id']; ?></p>
										<p style="display: none;">{!! $session_uid= Session::get('user_id'); !!}</p>
										<ul class="list-inline">
											<?php 
											if($session_uid==$comm_user_id)
											{
											?>
												<li style=" float:right;"><a href="{!! route('comment_delete.route', ['news_id'=>$news_id, 'comment_id'=>$comm_id]) !!}"><i  style="color:#080808;" class="fa fa-trash"></i></a></li>
											<?php
											}
											?>
											<?php 
											if($session_uid!=$comm_user_id && $session_uid!="")
											{
											?>
												<li><a data-toggle="modal" data-target="#comment_flag_Modal<?php echo $comm_id; ?>"><img class="img-responsive" height="20px" width="20px" src="{{asset('images/flag.png')}}" alt="" /></a></a></li>
												<!-- Comment Flag Modal -->
												<div class="modal fade" id="comment_flag_Modal<?php echo $comm_id; ?>" role="dialog">
												<div class="modal-dialog">
												<!-- Modal content-->
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal">&times;</button>
														<h4 class="modal-title" style="text-align: center;">Report Here On Comment</h4>
													</div>
													<form action="{{url('flag_comment')}}" method="post">
														{!! csrf_field() !!}															       
														<div class="modal-body">
															<p style="font-size:16px; text-align: center;">Please enter reason below to report this Comment </p>
															<div class="row">
																<input type="text" name="comment_id" value="<?php echo $comm_id; ?>">
																<input type="text" name="u_id" value="{!! Session::get('user_id') !!}">
																<div class="col-md-1"></div>
																<div class="col-md-10"><textarea class="form-control" name="message" placeholder="Enter Reason Here...."></textarea></div>
															</div>
															<br>
															<div class="modal-footer">
																<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
																<button type="submit" class="btn btn-primary">Submit</button>
															</div>
														</div>
													</form>
												</div>
												</div>
												</div>
											<?php
											}
											?>
											<?php
											if($session_uid!="")
											{
												$comm_like= $get_comm['news'][$i]['comment_liked'];
												if($comm_like==0)
												{
												?>
													<li style=" float:right;"><a href="{!! route('comment_like.route', ['user_id'=>$session_uid, 'comment_id'=>$comm_id]) !!}"><i style="color:#080808; float:right;" class="fa fa-thumbs-o-up"></i></a></li>
												<?php	
												}
												else
												{
												?>
													<li style=" float:right;"><a href="{!! route('comment_like.route', ['user_id'=>$session_uid, 'comment_id'=>$comm_id]) !!}"><i style="color:#080808;" class="fa fa-thumbs-up"></i></i></a></li>
												<?php	
												}
											}
											?>
										</ul>
									</div>
								</li>
								</ul>
							<?php
								} 
							}
						}
						?>
						@if(Session::get('username')!="")
						<div class="comments-box">
							<h1 class="section-title title">Leave a Comment</h1>
							<form id="comment-form" name="comment-form" method="post"  action="{{url('newscomments')}}">
								{!! csrf_field() !!}
								<input type="hidden" value="<?php echo $news_id; ?>" name="news_id" />
								<input type="hidden" value="{!! Session::get('user_id'); !!}" name="user_id" />
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<label for="comment" >Your Text</label>
											<textarea name="user_comment" id="comment" required="required" class="form-control" rows="5"></textarea>
										</div>
										<div class="text-center">
											<button type="submit" class="btn btn-primary pull-right">Send </button>
										</div>
									</div>
								</div>
							</form>
						</div>	
						@endif
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-3">
			<div id="sitebar">
				<div class="widget follow-us" style="padding:0px;">
					<h1 class="section-title title">Follow Us</h1>
					<ul class="list-inline social-icons">
						<li><a href="#"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
						<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
						<li><a href="#"><i class="fa fa-youtube"></i></a></li>
					</ul>
				</div><!--/#widget-->
			</div><!--/#sitebar-->
		</div>
	
	</div><!--/.section-gallary-->
</div><!--/.container-->

@stop
