@section('script')
   	<!--/#scripts--> 
   	
    <script type="text/javascript" src="{{ URL::asset('js/jquery.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/jquery.magnific-popup.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/owl.carousel.min.js') }}"></script> 
	<script type="text/javascript" src="{{ URL::asset('js/moment.min.js') }}"></script> 
	<script type="text/javascript" src="{{ URL::asset('js/jquery.simpleWeather.min.js') }}"></script> 
	<script type="text/javascript" src="{{ URL::asset('js/jquery.sticky-kit.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/jquery.easy-ticker.min.js') }}"></script> 
	<script type="text/javascript" src="{{ URL::asset('js/jquery.subscribe-better.min.js') }}"></script> 
    <script type="text/javascript" src="{{ URL::asset('js/main.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/switcher.js') }}"></script>
   
    <!--<script src="http://code.jquery.com/jquery-1.9.1.js"></script>-->

@show