<?php 
$search =  '!"#$%&/()=?*+\'-.,;:_' ;
$search = str_split($search);
?>

<p style="display: none;">{!! $ureg_id=Session::get('user_id') !!}</p>

<?php 
if($ureg_id=="")
{
	$user_reg_id=0;
}
else
{
	$user_reg_id=$ureg_id;	
}

//$catt=array_values($catt);
/*unset($catt['data'][0]);
unset($catt['data'][1]);
unset($catt['data'][2]);
*/
$catt=array_values($catt['data']);
?>
	<header id="navigation">
		
			<div class="navbar" role="banner">
				<div class="container">
					<a class="secondary-logo" href="">
						 
						<img class="img-responsive" src="images/presets/preset1/newsapplogo.png" alt="logo"> 
					</a>
				</div>
				<div class="topbar">
					<div class="container">
						<div id="topbar" class="navbar-header">		
							<a class="navbar-brand" href="{{url('/')}}" style=" color: red; font-weight: 600; font-size: 22px; padding-top: 0px; font-family: -webkit-pictograph;">
							<img class="img-responsive" src="{{asset('images/presets/preset1/newsapplogo.png')}}" alt="" />	
								<!-- <img class="main-logo img-responsive" src="images/presets/preset1/logo.png" alt="logo"> -->
							</a>
							<div id="topbar-right">
								<!-- <div class="dropdown language-dropdown">						
									<a data-toggle="dropdown" href="#"><span class="change-text">En</span> <i class="fa fa-angle-down"></i></a>
									<ul class="dropdown-menu language-change">
										<li><a href="#">EN</a></li>
										<li><a href="#">FR</a></li>
										<li><a href="#">GR</a></li>
										<li><a href="#">ES</a></li>
									</ul>								
								</div> -->
								<div id="date-time"></div>
								<!--<div id="weather"></div>-->
							</div>
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div> 
					</div> 
				</div> 
				<div id="menubar" class="container">	
					<nav id="mainmenu" class="navbar-left collapse navbar-collapse"> 
						<ul class="nav navbar-nav">
							<li class="sports"><a  href="{{url('/')}}">Home</a></li>
							<li class="environment dropdown mega-dropdown"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">News Category</a>
								<div class="dropdown-menu mega-menu">
									<div class="container">
										<div class="row">
											<?php
												$len=count($catt);
												$parts=ceil($len/5);
												$cc=0;
												for($x=0;$x<$parts;$x++)
												{
													$firsthalf = array_slice($catt, $cc, 5);
													$cc=$cc+5;
												?>
											<div class="col-sm-2">
												<!--<h2>Category</h2>-->
												<ul>
													<?php
													for($y=0; $y<count($firsthalf);$y++)
													{
														$catt_name=$firsthalf[$y]['category'];
														$catt_id=$firsthalf[$y]['id'];
														
														if($catt_name=="Local")
														{
														?>
														<li><a href="{!! route('local_category', ['local_category'=>str_replace($search, " or ", $catt_name)]) !!}"><?php echo $catt_name; ?></a></li>
														<?php
														}
														else if($catt_name=="Latest")
														{
														?>
														<li><a href="{!! route('latest_category', ['latest_category'=>str_replace($search, " or ", $catt_name)]) !!}"><?php echo $catt_name; ?></a></li>
														<?php
														}
														else if($catt_name=="Country")
														{
														?>
														<li><a href="{!! route('country_category', ['country_category'=>str_replace($search, " or ", $catt_name)]) !!}"><?php echo $catt_name; ?></a></li>
														<?php
														}
														else{
															?>
															<li><a href="{!! route('category_name', ['category_name'=>str_replace($search, " or ", $catt_name)]) !!}"><?php echo $catt_name; ?></a></li>
															<?php
														}
													}
													?>
													
												</ul>
											</div>		
													
											<?php		
												}
											?>
											
											
										</div>
									</div>
								</div>
							</li>
							<li><a href="{!! route('promotions') !!}">Promotions</a></li>
							<?php
							if($ureg_id!="")
							{?>
								<li class="sports"><a href="{{url('news')}}">Post News Here</a></li>
							<?php }
							else							
							{
								
							} ?>
							
							<!-- <li class="sports"><a href="{{url('set_location')}}">Set Location</a></li> -->
							<?php
							if($ureg_id!="")
							{?>
								
								<li><a href="{!! route('following_news', ['user_id'=>$user_reg_id]) !!}">Followers News</a></li>
							<?php } ?>
								
								
								
								<li class="business dropdown"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Settings</a>
								<ul class="dropdown-menu">
									<li><a href="{{ url('filter') }}">Filters</a></li>
									<li class="sports"><a href="{{url('set_location')}}">Set Location</a></li>
								</ul>
							</li>
							
							<!--<li><a data-toggle="modal" data-target="#myModal">Setting</a></li>-->
							<!-- <li class="sports"><a href="{{url('/')}}">Promotions</a></li> -->
							<!--<li class="sports"><a href="{{url('about-us')}}">About Us</a></li>-->
							
							<!--<li class="sports"><a href="{{url('contact')}}">Contact Us</a></li>-->
							
						</ul> 					
					</nav>
					<div class="searchNlogin">
						<ul>
							<!--<li class="search-icon"><i class="fa fa-search"></i></li>-->
							
							@if(Session::get('username')=="")
							<li class="dropdown user-panel"><a href="{{url('login')}}" title="User Login" ><i class="fa fa-user">&nbsp;&nbsp;Login</i></a>
								
							</li>
						    @else
							    <span style="display: none;">{!! $path=public_path(); !!}</span>
							    <span style="display: none;">{!! $image_name=Session::get('userimage'); !!}</span>
						    	<li class="sports"><a href="{!! route('myaccount', ['user_id'=>$user_reg_id ,'c_user'=>$user_reg_id]) !!}">{!! ucfirst(Session::get('username')); !!}</a></i></li>
						    	<?php 
							    if(file_exists($path."/admin/images/".$image_name) && $image_name!="")
								{
								?>
									<li class="sports"><a href="{!! route('myaccount', ['user_id'=>$user_reg_id, 'c_user'=>$user_reg_id]) !!}"><img class="img-responsive" style="height: 30px; width: 30px; border-radius: 100%" src="{{asset('admin/images/').'/'.ucfirst(Session::get('userimage'))}}"  height="225px" width="270px" alt="Image"/></a></i></li>
								<?php	
								}
								else
								{
								?>
									<li class="sports"><a href="{!! route('myaccount', ['user_id'=>$user_reg_id , 'c_user'=>$user_reg_id ]) !!}"><img class="img-responsive" style="height: 30px; width: 30px; border-radius: 100%" src="{{asset('images/user.png')}}"  height="225px" width="270px" alt="Image"/></a></i></li>
								<?php
								}
							    ?>
						        
						        <li class="sports"><a href="{{url('logout')}}">Logout</a></li>
						    @endif
							
							
							
						</ul>
						<div class="search">
							<form role="form">
								<input type="text" class="search-form" autocomplete="off" placeholder="Type & Press Enter">
							</form>
						</div> <!--/.search--> 
					</div><!-- searchNlogin -->
				</div>
			</div>
		</header><!--/#navigation-->
 <!-- Setting Model -->
 
 <style type="text/css">
.box{
   
    display: none;
   }
</style>
<script type="text/javascript">
$(document).ready(function(){
    $('input[type="radio"]').click(function(){
        if($(this).attr("value")=="red"){
            $(".box").not(".red").hide();
            $(".red").show();
        }
        if($(this).attr("value")=="green"){
            $(".box").not(".green").hide();
            $(".green").show();
        }
    });
});
</script>	
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Setting</h4>
        </div>
        <div class="modal-body">
        
	  <div>
	  	Select Primary
        <label><input type="radio" name="colorRadio" value="red"> Radius </label>
        <label><input type="radio" name="colorRadio" value="green"> Distance</label>
    </div>
    <div class="red box">
    	Radius Primary
    </div>
    <div class="green box">
    	Distance Primary
    </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
		
	<!-- Setting Model -->	
		
		
		
		
		<!-- Flash Message -->
		<div class="flash-message" id="remove">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
		  @if(Session::has('alert-' . $msg))
		
		  <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
		  @endif
		@endforeach
		</div> <!-- end .flash-message -->
		