@section('stylesheet')

<link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ URL::asset('css/font-awesome.min.css') }}" rel="stylesheet">
<link href="{{ URL::asset('css/magnific-popup.css') }}" rel="stylesheet">
<link href="{{ URL::asset('css/owl.carousel.css') }}" rel="stylesheet">
<link href="{{ URL::asset('css/subscribe-better.css') }}" rel="stylesheet">
<link href="{{ URL::asset('css/main.css') }}" rel="stylesheet">
<link href="{{ URL::asset('css/responsive.css') }}" rel="stylesheet">
<link id="preset" rel="stylesheet" type="text/css" href="{{ URL::asset('css/presets/preset1.css') }}">

	<!--Google Fonts-->
	<link href='https://fonts.googleapis.com/css?family=Signika+Negative:400,300,600,700' rel='stylesheet' type='text/css'>
	
    <!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
    <![endif]-->
    
    
    <link rel="shortcut icon" href="{{ URL::asset('images/ico/favico.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ URL::asset('images/ico/apple-touch-icon-144-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ URL::asset('images/ico/apple-touch-icon-114-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="" href="{{ URL::asset('images/ico/apple-touch-icon-72-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" href="{{ URL::asset('images/ico/apple-touch-icon-57-precomposed.png') }}">
@show