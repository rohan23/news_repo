<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<!--title-->
    <title>NEARBY NEWS </title>
    @include('layout.stylesheet')
</head>
<body>

@yield('pre-body')
@include('layout.header')
@include('layout.script')
@yield('body')
@include('layout.footer')
<script type="text/javascript">
$(function() {
setTimeout(function() { $(".flash-message").fadeOut(1500); }, 3000)
});
</script>
</body>
</html>