@extends('layout.base')

@section('body')
    

<script>
	function trans_value(val1, val2)
	{
		var val3='"'+document.getElementById("n_title").innerText+'"';
		var val4='"'+document.getElementById("n_text").innerText+'"';
		var val5=document.getElementById("exist_lang").innerText;
		//alert('translate_code=' + val1 + 'promotion_id=' + val2 + 'promotion_title=' + val3 + 'promotion_text=' + val4 + 'existing_code=' + val5);
		jQuery.ajax({
		    url: '/translate',
		    type: 'POST',
		    data: {translate_code: val1,promotion_id:val2,promotion_title:val3,promotion_text:val4,existing_lang:val5},
		    success: function (data) {
		 	jQuery("#translated_news").html(data);
		   //alert('success');
		    }
		});
		
	}
</script>
	<!-- Css For Image Zooming -->
<style>
.center-cropped {
  object-fit: none; /* Do not scale the image */
  object-position: center; /* Center the image within the element */
  height: 200px;
  width: 100%;
}
</style>
		
<?php 
//GET Array key name
$abc=array_keys($comments);

$c=Session::get('user_id');

if($c!="")
{
	$cuser=$c;
}
else
{
	$cuser="0";
}
//echo "<pre>"; print_r($languages); echo "</pre>";




?>


<div class="container">
	<div class="section photo-gallery">
		<div class="col-sm-9">
			<h1 class="section-title title">Promotion Detail</h1>	
			<div id="photo-gallery" class="row carousel slide carousel-fade post" data-ride="carousel">
			<?php 
			if (array_key_exists('news', $promotions)) 
			{
				$promotion_id=$promotions['news']['0']['id'];
				$user_promotion_id=$promotions['news']['0']['user_id'];
				$promotion_title=$promotions['news']['0']['title'];
				$promotion_text=$promotions['news']['0']['text'];
				$promotion_time=$promotions['news']['0']['time'];
				$promotion_like=$promotions['news']['0']['like'];
				$promotion_comment=$promotions['news']['0']['comment'];
				$promotion_country=$promotions['news']['0']['country'];
				$promotion_image1=$promotions['news']['0']['image1'];
				$promotion_image2=$promotions['news']['0']['image2'];
				$promotion_image3=$promotions['news']['0']['image3'];
				$promotion_image4=$promotions['news']['0']['image4'];
				$promotion_image5=$promotions['news']['0']['image5'];
				$promotion_image6=$promotions['news']['0']['image6'];
				$promotion_image7=$promotions['news']['0']['image7'];
				$promotion_image8=$promotions['news']['0']['image8'];
				$promotion_image9=$promotions['news']['0']['image9'];
				
				$promotion_distance=$promotions['news']['0']['news_distance'];
				$user_like=$promotions['like']['Liked'];
				$user_flag=$promotions['flag']['Flagged'];
				
				$now1 = new DateTime();
				$ts11 = strtotime($promotion_time);
				$ts22 = $now1->getTimestamp();
				$diff = $ts22 - $ts11;
				$hourdiff1 = round(( $now1->getTimestamp()-strtotime($promotion_time))/3600, 1);
				$adays=intval($hourdiff1/24);
				
				if($adays=="0")
				{
					$days="Today";
				}
				else 
				{
					$days=$adays." days";	
				}
				
			?>						
			<div class="carousel-inner">
				
				<?php 
				if($promotion_image1!="")
				{
				?>
				<div class="item active">
					<a href="{{asset('admin/images/').'/'.$promotion_image1}}" class="image-link"><img style="max-height:425px; width:100%;" class="center-cropped img-responsive" src="{{asset('admin/images/').'/'.$promotion_image1}}" alt="" /></a>
					
				</div>
				<?php
				}
				if($promotion_image2!="")
				{?>
				<div class="item">
					<a href="{{asset('admin/images/').'/'.$promotion_image2}}" class="image-link"><img style="max-height:425px; width:100%;" class="center-cropped img-responsive" src="{{asset('admin/images/').'/'.$promotion_image2}}" alt="" /></a>
				</div>
				<?php
				}
				if($promotion_image3!="")
				{?>
				<div class="item">
					<a href="{{asset('admin/images/').'/'.$promotion_image3}}" class="image-link"><img id="myImg" style="max-height:425px; width:100%;" class="center-cropped img-responsive" src="{{asset('admin/images/').'/'.$promotion_image3}}" alt="" /></a>
				</div>
				<?php
				}
				if($promotion_image4!="")
				{?>
				<div class="item">
					<a href="{{asset('admin/images/').'/'.$promotion_image4}}" class="image-link"><img id="myImg" style="max-height:425px; width:100%;" class="center-cropped img-responsive" src="{{asset('admin/images/').'/'.$promotion_image4}}" alt="" /></a>
				</div>
				<?php
				}
				if($promotion_image5!="")
				{?>
				<div class="item">
					<a href="{{asset('admin/images/').'/'.$promotion_image5}}" class="image-link"><img id="myImg" style="max-height:425px; width:100%;" class="center-cropped img-responsive" src="{{asset('admin/images/').'/'.$promotion_image5}}" alt="" /></a>
				</div>
				<?php
				}
				if($promotion_image6!="")
				{?>
				<div class="item">
					<a href="{{asset('admin/images/').'/'.$promotion_image6}}" class="image-link"><img id="myImg" style="max-height:425px; width:100%;" class="center-cropped img-responsive" src="{{asset('admin/images/').'/'.$promotion_image6}}" alt="" /></a>
				</div>
				<?php
				}
				if($promotion_image7!="")
				{?>
				<div class="item">
					<a href="{{asset('admin/images/').'/'.$promotion_image7}}" class="image-link"><img id="myImg" style="max-height:425px; width:100%;" class="center-cropped img-responsive" src="{{asset('admin/images/').'/'.$promotion_image7}}" alt="" /></a>
				</div>
				<?php
				}
				if($promotion_image8!="")
				{?>
				<div class="item">
					<a href="{{asset('admin/images/').'/'.$promotion_image8}}" class="image-link"><img id="myImg" style="max-height:425px; width:100%;" class="center-cropped img-responsive" src="{{asset('admin/images/').'/'.$promotion_image8}}" alt="" /></a>
				</div>
				<?php
				}
				if($promotion_image9!="")
				{?>
				<div class="item">
					<a href="{{asset('admin/images/').'/'.$promotion_image9}}" class="image-link"><img id="myImg" style="max-height:425px; width:100%;" class="center-cropped img-responsive" src="{{asset('admin/images/').'/'.$promotion_image9}}" alt="" /></a>
				</div>
			<?php } ?>
			</div><!--/carousel-inner-->
			<br><br>
			<div>
	
			<ol class="gallery-indicators carousel-indicators">
				<?php
				if($promotion_image1!="")
				{?>
				<li data-target="#photo-gallery" data-slide-to="0" class="active">
					<img style="max-height:38px; width:38px;" class="img-responsive" src="{{asset('admin/images/').'/'.$promotion_image1}}" alt="" />
				</li>
				<?php
				}
				if($promotion_image2!="")
				{?>
				<li data-target="#photo-gallery" data-slide-to="1">
					<img style="max-height:38px; width:38px;" class="img-responsive" src="{{asset('admin/images/').'/'.$promotion_image2}}" alt="" />
				</li>
				<?php
				}
				if($promotion_image3!="")
				{?>
				<li data-target="#photo-gallery" data-slide-to="3">
					<img style="max-height:38px; width:38px;" class="img-responsive" src="{{asset('admin/images/').'/'.$promotion_image3}}" alt="" />
				</li>
				<?php
				}
				if($promotion_image4!="")
				{?>
				<li data-target="#photo-gallery" data-slide-to="4">
					<img style="max-height:38px; width:38px;" class="img-responsive" src="{{asset('admin/images/').'/'.$promotion_image4}}" alt="" />
				</li>
				<?php
				}
				if($promotion_image5!="")
				{?>
				<li data-target="#photo-gallery" data-slide-to="5">
					<img style="max-height:38px; width:38px;" class="img-responsive" src="{{asset('admin/images/').'/'.$promotion_image5}}" alt="" />
				</li>
				<?php
				}
				if($promotion_image6!="")
				{?>
				<li data-target="#photo-gallery" data-slide-to="6">
					<img style="max-height:38px; width:38px;" class="img-responsive" src="{{asset('admin/images/').'/'.$promotion_image6}}" alt="" />
				</li>
				<?php
				}
				if($promotion_image7!="")
				{?>
				<li data-target="#photo-gallery" data-slide-to="7">
					<img style="max-height:38px; width:38px;" class="img-responsive" src="{{asset('admin/images/').'/'.$promotion_image7}}" alt="" />
				</li>
				<?php
				}
				if($promotion_image8!="")
				{?>
				<li data-target="#photo-gallery" data-slide-to="8">
					<img style="max-height:38px; width:38px;" class="img-responsive" src="{{asset('admin/images/').'/'.$promotion_image8}}" alt="" />
				</li> 
				<?php
				}
				if($promotion_image9!="")
				{?>
				<li data-target="#photo-gallery" data-slide-to="9">
					<img style="max-height:38px; width:38px;" class="img-responsive" src="{{asset('admin/images/').'/'.$promotion_image9}}" alt="" />
				</li>
				<?php } ?>
			</ol><!--/gallery-indicators-->
			
			</div>
			<div class="post-content">								
				<div class="entry-meta">
					<ul class="list-inline">
						@if(Session::get('username')=="")
							<li class="loves"><a href="{{url('login')}}"><i class="fa fa-thumbs-o-up"></i>{!! $promotion_like !!}</a></li>
							<li class="loves"><a href="{{url('login')}}"><i class="fa fa-comment-o"></i>{!! $promotion_comment !!}</a></li>
						@else
							@if($user_like=='no')
							<li><a href="{!! route('likedislike.route', ['p_id'=>$promotion_id, 'user_id'=>Session::get('user_id')]) !!}"><i class="fa fa-thumbs-o-up"></i>{!! $promotion_like !!}</a></li>
							<li class="comments"><i class="fa fa-comment-o"></i>{!! $promotion_comment !!}</li>
						@elseif($user_like=='yes')
							<li><a href="{!! route('likedislike.route', ['p_id'=>$promotion_id, 'user_id'=>Session::get('user_id')]) !!}"><i class="fa fa-thumbs-up"></i>{!! $promotion_like !!}</a></li>
							<li class="comments"><i class="fa fa-comment-o"></i>{!! $promotion_comment !!}</li>
							@endif
						@endif
					
						
							
						<li class="publish-date" ><a href="#">	<i class="fa fa-globe" aria-hidden="true"></i><?php echo $promotion_country; ?></a></li> 	
						
						
						
							
						 <li class="publish-date" ><a href="#"><i class="fa fa-location-arrow" aria-hidden="true"></i><?php echo $promotion_distance; ?>km</a></li> 	
							
					
					
					<li class="publish-date"><i class="fa fa-clock-o"></i><?php echo $days; ?></li>
					
					
					@if(Session::get('username')!="" AND $user_flag=="no")
							<li style="float: right;"><a data-toggle="modal" data-target="#myModal"><img class="img-responsive" height="20px" width="20px" src="{{asset('images/flag.png')}}" alt="" /></a></li>
						@endif
					</ul>
					<!-- News Flag Modal -->
					<div class="modal fade" id="myModal" role="dialog">
						<div class="modal-dialog">
						<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title" style="text-align: center;">Report Here</h4>
								</div>
								<form action="{{url('report_promotion')}}" method="post">
									{!! csrf_field() !!}															       
									<div class="modal-body">
										<p style="font-size:16px; text-align: center;">Please enter reason below to report news </p>
										<div class="row">
											<input type="hidden" name="p_id" value="<?php echo $promotion_id; ?>">
											<input type="hidden" name="u_id" value="{!! Session::get('user_id') !!}">
											<div class="col-md-1"></div>
											<div class="col-md-10"><textarea class="form-control" name="message" placeholder="Enter Reason Here...."></textarea></div>
										</div>
										<br>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
											<button type="submit" class="btn btn-primary">Submit</button>
										</div>
								  	</div>
							  	</form>
							</div>
						</div>
					</div>
					
					<h2 class="entry-title">
					{!! $promotion_title !!}
					</h2>
					<div class="entry-content">
						<p style="white-space: pre-wrap;">{!! $promotion_text !!}</p>
					</div>
					
					<?php } ?>
				</div>
			</div>
		</div><!--/photo-gallery--> 
		
		<div class="row" >
			<div class="col-sm-12">								
				<div class="comments-wrapper">
						
					<?php 
					if($abc['0']=="news")
					{
						$count_comments=count($comments['news']);
						if($count_comments>0)
						{
						?>
						<h1 class="section-title title">Comments</h1>
						<?php
						}
						?>
						
						<?php 
						for($i=0; $i<$count_comments; $i++)
						{
							
							?>
								
								<ul class="media-list">
								<li class="media" style="padding:10px;">
									<div class="media-left">
										<p><?php $user_comment_image= $comments['news'][$i]['image']; ?></p>
										<span style="display: none;">{!! $path=public_path(); !!}</span>
										<?php 
										if(file_exists($path."/admin/images/".$user_comment_image) && $user_comment_image!="")
										{
										?>
											<img class="media-object" src="{{asset('admin/images/').'/'.$user_comment_image}}" alt="">
										<?php	
										}
										else
										{
										?>
											<img class="media-object" src="{{asset('images/user.png')}}" alt="">
										<?php
										}
										?>
									</div>
									<div class="media-body">
										<h2 style="color:#000;"><?php echo $comments['news'][$i]['name']; ?></h2>
										<p ><?php echo $comments['news'][$i]['comment']; ?></p>
										<p><?php $comm_id= $comments['news'][$i]['id']; ?></p>
										<p><?php $comm_user_id= $comments['news'][$i]['user_id']; ?></p>
										<p style="display: none;">{!! $session_uid= Session::get('user_id'); !!}</p>
										<ul class="list-inline">
											<?php 
											if($session_uid==$comm_user_id)
											{
											?>
												<li style=" float:right;"><a href="{!! route('delete_comment.route', ['p_id'=>$promotion_id, 'c_id'=>$comm_id]) !!}"><i  style="color:#080808;" class="fa fa-trash"></i></a></li>
											<?php
											}
											?>
											<?php 
											if($session_uid!=$comm_user_id && $session_uid!="")
											{
											?>
												<li><a data-toggle="modal" data-target="#comment_flag_Modal<?php echo $comm_id; ?>"><img class="img-responsive" height="20px" width="20px" src="{{asset('images/flag.png')}}" alt="" /></a></a></li>
												<!-- Comment Flag Modal -->
												<div class="modal fade" id="comment_flag_Modal<?php echo $comm_id; ?>" role="dialog">
												<div class="modal-dialog">
												<!-- Modal content-->
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal">&times;</button>
														<h4 class="modal-title" style="text-align: center;">Report Here On Comment</h4>
													</div>
													<form action="{{url('flag_comment')}}" method="post">
														{!! csrf_field() !!}															       
														<div class="modal-body">
															<p style="font-size:16px; text-align: center;">Please enter reason below to report this Comment </p>
															<div class="row">
																<input type="text" name="comment_id" value="<?php echo $comm_id; ?>">
																<input type="text" name="u_id" value="{!! Session::get('user_id') !!}">
																<div class="col-md-1"></div>
																<div class="col-md-10"><textarea class="form-control" name="message" placeholder="Enter Reason Here...."></textarea></div>
															</div>
															<br>
															<div class="modal-footer">
																<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
																<button type="submit" class="btn btn-primary">Submit</button>
															</div>
														</div>
													</form>
												</div>
												</div>
												</div>
											<?php
											}
											?>
											<?php
											if($session_uid!="")
											{
												$comm_like= $comments['news'][$i]['comment_liked'];
												if($comm_like==0)
												{
												?>
													<li style=" float:right;"><a href="{!! route('like_comment.route', ['user_id'=>Session::get('user_id'), 'comment_id'=>$comm_id]) !!}"><i style="color:#080808; float:right;" class="fa fa-thumbs-o-up"></i></a></li>
												<?php	
												}
												else
												{
												?>
													<li style=" float:right;"><a href="{!! route('like_comment.route', ['user_id'=>Session::get('user_id'), 'comment_id'=>$comm_id]) !!}"><i style="color:#080808;" class="fa fa-thumbs-up"></i></i></a></li>
												<?php	
												}
											}
											?>
										</ul>
									</div>
								</li>
								</ul>
							<?php
								} 
							
							}
						?>
						@if(Session::get('username')!="")
						<div class="comments-box">
							<h1 class="section-title title">Leave a Comment</h1>
							<form id="comment-form" name="comment-form" method="post"  action="{{url('promotioncomments')}}">
								{!! csrf_field() !!}
								<input type="hidden" value="<?php echo $promotion_id; ?>" name="promotion_id" />
								<input type="hidden" value="{!! Session::get('user_id'); !!}" name="user_id" />
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<label for="comment" >Your Text</label>
											<textarea name="comment" id="comment" required="required" class="form-control" rows="3"></textarea>
										</div>
										<div class="text-center">
											<button type="submit" class="btn btn-primary pull-right">Send </button>
										</div>
									</div>
								</div>
							</form>
						</div>	
						@endif
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-3">
			<div id="sitebar">
				<div class="widget follow-us" style="padding:0px;">
					<h1 class="section-title title">Follow Us</h1>
					<ul class="list-inline social-icons">
						<li><a href="#"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
						<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
						<li><a href="#"><i class="fa fa-youtube"></i></a></li>
					</ul>
				</div><!--/#widget-->
			</div><!--/#sitebar-->
		</div>
	
	</div><!--/.section-gallary-->
</div><!--/.container-->

@stop
