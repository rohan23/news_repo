<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>NearBy News</title>
<link rel="stylesheet" href="{{ URL::asset('landing1/css/bootstrap.min.css') }}">
<link href="{{ URL::asset('landing1/css/custom.css') }}" href="" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('landing1/css/font-awesome.min.css') }}" href="" rel="stylesheet" type="text/css" />
<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">


<style>
ul.play-store{ display:block;margin-top:15px;}
ul.play-store li{ float:left; margin-right:20px;}
ul.play-store li img{ width:200px; transition:1s; opacity:.9;}
ul.play-store li img:hover{ filter: grayscale(100%); opacity:1;}
</style>
</head>
<body>
	
<div class="container-fluid banner">
  <div class="container banner-text">
  	<div class="flash-message" id="remove">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
		  @if(Session::has('alert-' . $msg))
		
		  <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
		  @endif
		@endforeach
		</div>
  	<div class="col-md-6 col-sm-6 hidden-xs">
	<?php 
  		$image1= $landing_data['landing1'][0]['header_image'];
		$image2= $landing_data['landing1'][0]['image']; 
		$heading11=$landing_data['landing1'][0]['heading'];
  		?>
    	<img src="{{asset('landing1/img/').'/'.$image1}}" class="img-responsive" />
  	</div>
    <div class="col-md-6 col-sm-6">
   <div class="text-center"><center><img class="img-responsive" src="{{asset('landing1/img/').'/'.$image2}}" /></center></div> 
    <h1 class=""><?php echo $heading11; ?></h1>
    <h4 class="line-height"><?php echo $landing_data['landing1'][0]['description']; ?></h4>
     <h3 class="">We’re available on <b>App Store</b> and <b>Play Store</b>. Download now to start! </h3>
    
    <div class="clearfix"></div>
    <form action="{{url('save_email')}}" method="post">
     <input type="email" required="required" name="email" class="form-control subscribe bold pull-left margin-top-email" placeholder="Enter your Email Address" /> <button type="submit" class="btn btn-lg btn-primary bold pull-left margin-top-email">Get Notify </button> </form>
    <ul class="play-store">
	<!--<li>
	<a href="#"><img class="img-responsive" src="{{asset('landing1/img/app-store.png')}}" /></a>
	</li>
	<li>
    <a href="#"><img class="img-responsive" src="{{asset('landing1/img/google-store.png')}}" /></a>
	</li>
	</ul>
	<div class="clearfix"></div>
	<h4 style="font-weight:700;"><br>We get you the most relevant news!</h4>
    
    </div> -->
  </div>
</div>

 


<section >
  <div class="container-fluid with-bg bg-light-gray text-white">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 text-center" style="padding-bottom: 30px;">
          <h2 class="regular">How It Works</h2>
         <!--  <h4 class="col-md-8 col-md-offset-2 regular line-height"><?php echo $landing_data['landing2'][0]['content']; ?></h4> -->
        </div>
        <div style="padding: 60px 10px;">
        <div class="col-xs-12 col-md-4 col-sm-4 text-center">
          <h3 class="section-heading bold"><small style="color:#fff; font-size:16px;">Step : 1 </small> Open the App</h3>
          <h4 class="line-height">Download the app for your smart phone and open it.</h4>
        </div>
  		 <div class="col-xs-12 col-md-4 col-sm-4 text-center">
          <h3 class="section-heading bold"><small style="color:#fff; font-size:16px;">Step : 2 </small> Choose categories</h3>
          <h4 class="line-height">Choose various categories you want you see news from.</h4>
        </div>
         <div class="col-xs-12 col-md-4 col-sm-4 text-center">
          <h3 class="section-heading bold"><small style="color:#fff; font-size:16px;">Step : 3 </small> Choose Location</h3>
          <h4 class="line-height">Choose the locations from which you want to read the news.</h4>
        </div>
        </div>
        <div style="padding: 60px 10px;">
         <div class="col-xs-12 col-md-4 col-sm-4 text-center">
          <h3 class="section-heading bold"><small style="color:#fff; font-size:16px;">Step : 4 </small>Select Language</h3>
          <h4 class="line-height">Select any language you want to read news in.</h4>
        </div>
         <div class="col-xs-12 col-md-4 col-sm-4 text-center">
          <h3 class="section-heading bold"><small style="color:#fff; font-size:16px;">Step : 5 </small> Start Reading </h3>
          <h4 class="line-height">Now you have personalized you app according to you. Start reading!</h4>
        </div>
         <div class="col-xs-12 col-md-4 col-sm-4 text-center">
          <h3 class="section-heading bold"><small style="color:#fff; font-size:16px;">Step : 6 </small> Contribute to news</h3>
          <h4 class="line-height">Login to the app to post your own news or upload images relevant to the existing news.</h4>
        </div>
		</div>
		<!-- <div class="col-xs-12 col-md-4 col-sm-4 text-center">
          <div class="section-icon section-3"></div>
          <h3 class="section-heading bold"><?php echo $landing_data['landing2'][0]['sub_heading3']; ?></h3>
          <h4 class="line-height"><?php echo $landing_data['landing2'][0]['sub_content3']; ?></h4>
        </div>
		<div class="col-xs-12 col-md-4 col-sm-4 text-center">
          <div class="section-icon section-3"></div>
          <h3 class="section-heading bold"><?php echo $landing_data['landing2'][0]['sub_heading3']; ?></h3>
          <h4 class="line-height"><?php echo $landing_data['landing2'][0]['sub_content3']; ?></h4>
    </div> -->
      </div>
    </div>
  </div>
</section>
<?php 
	$image3= $landing_data['landing3'][0]['image'];
	 
?>

<section>
  <div class="container-fluid with-bg">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-md-6 col-sm-6 section-container">
          <h2 class="bold"><?php echo $landing_data['landing3'][0]['heading']; ?></h2>
          <h3><?php echo $landing_data['landing3'][0]['content']; ?></h3>
          <br>
          
          <div class="row f-text">
          	<div class="col-xs-2">
            	<i class="fa fa-refresh text-primary light" ></i>
            </div>
            <div class="col-xs-8">
            	<h3 class="bold"><?php echo $landing_data['landing3'][0]['sub_heading1']; ?></h3>
                <h4><?php echo $landing_data['landing3'][0]['sub_content1']; ?></h4>            
            </div>         
          
          </div>
          
          <div class="row f-text">
          	<div class="col-xs-2">
            	<i class="fa fa-clock-o text-primary light" ></i>
            </div>
            <div class="col-xs-8">
            	<h3 class="bold"><?php echo $landing_data['landing3'][0]['sub_heading2']; ?></h3>
                <h4><?php echo $landing_data['landing3'][0]['sub_content2']; ?></h4>            
            </div>         
          
          </div>
          <!--div class="show_more_main" id="show_more_main">
			<span id="" class="show_more" title="Load more posts">Show more</span>
			<span class="loding" style="display: none;"><span class="loding_txt">Loading....</span></span>
		  </div-->
          
        </div>
         
        <div class="col-xs-12 col-md-6 col-sm-6"><img src="{{asset('landing1/img/').'/'.$image3}}" class="img-responsive" /> </div>
      </div>
    </div>
  </div>
</section>


<?php $image4= $landing_data['landing4'][0]['image']; ?>
<section>
  <div class="container-fluid with-bg video-bg" style="background-image:url({{asset('landing1/img/').'/'.$image4}});">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 text-center text-white">
          <h2 class="regular"><?php echo $landing_data['landing4'][0]['title']; ?></h2>
          <h4 class="col-md-8 col-md-offset-2 regular line-height" style="border-bottom:#fff thin solid; padding-bottom:25px"><?php echo $landing_data['landing4'][0]['heading']; ?></h4>
        </div>
        
        <div class="text-center play-btn">
        	<i class="fa fa-play-circle"></i>        
        </div>
        
      </div>
    </div>
  </div>
</section>

 


 


<footer>  
  <div class="container-fluid">
    <div class="container">
      <div class="row">
        <div class="text-center">
        	<a href="#" target="_blank" class="icon-social facebook"><i class="fa fa-facebook-f"></i></a>
            <a href="#" class="icon-social facebook"><i class="fa fa-linkedin"></i></a>
            <a href="#" class="icon-social facebook radius" target="_blank"><i class="fa fa-twitter"></i></a>
            <a href="#" class="icon-social facebook radius" target="_blank"><i class="fa fa-google-plus"></i></a>
            <a href="#" class="icon-social facebook radius" target="_blank"><i class="fa fa-youtube"></i></a>
        </div>
        <div class="col-xs-12">
          <p class="text-center bold line-height">&copy; Copyright 2016 News Nearby.<br>All Rights Reserved - Terms & Policy</p>
        </div>
      </div>
    </div>
  </div>
</footer>



<script src="{{ URL::asset('landing1/js/jquery.min.js') }}" type="text/javascript"></script> 
<script src="{{ URL::asset('landing1/js/bootstrap.min.js') }}" type="text/javascript"></script>
 
<script type="text/javascript">
$(function() {
setTimeout(function() { $("#remove").fadeOut(1500); }, 3000)
});
</script>
</body>
</html>
