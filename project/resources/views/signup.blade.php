@extends('layout.base')

@section('body')
<!-- signup-page -->
		<div class="signup-page">
			<div class="container">
				<div class="row">
					<!-- user-login -->			
					<div class="col-sm-6 col-sm-offset-3">
						<div class="ragister-account">		
							<h1 class="section-title title">Create an Account</h1>
							
							<form id="registation-form"  method="post" action="signup" enctype="multipart/form-data">
								{!! csrf_field() !!}
								<div class="form-group">
									<label>Name</label>
									<input type="text" name="name" class="form-control" required="required" placeholder="Enter your name">
								</div>
								<div class="form-group">
									<label>Select Country</label>
									<select class="form-control" id="country_code" name="country_name" onchange="doit11();" required="required"> 
										<option value=""> Select Country </option>
										@foreach($country as $country_detail)
										<option value="{!! $country_detail->phonecode; !!}">{!! $country_detail->country; !!}</option>
										@endforeach
									</select>
								</div>
								<div class="form-group">
									<div class="col-md-3">
										<label>Code</label>
										<input type="text" readonly="readonly"  name="phone_code" id="phonecode" placeholder="Code" class="form-control" required="required">
									</div>
									<div class="col-md-9">
										<label>Phone No.</label>
										<input type="text" id="text1" onkeypress="validate(event)" name="phone" placeholder="Please enter phone no. with country code " class="form-control" required="required">
										<p id="error" style="color:#ff3333; font-size: 15px;">*NOTE: Accepts only numeric values  </p>
									</div>
									
								</div>
								<div class="form-group">
									<label>Password</label>
									<input type="password" name="password" id="txtNewPassword" class="form-control" required="required" placeholder="Enter your password">
 								</div>
								<div class="form-group">
									<label>Confirm Password</label>
									<input type="password" name="password-again" class="form-control" required="required" id="txtConfirmPassword" onChange="checkPasswordMatch();" placeholder="Again enter your password">
									<span class="registrationFormAlert" id="divCheckPasswordMatch1"></span>
									<span class="registrationFormAlert" id="divCheckPasswordMatch2"></span>
								</div>
								<p id="messageshow1" style="display: none;">Passwords do not match!</p>
								<p id="messageshow2" style="display: none;">Passwords Match</p>
								<div class="form-group">
									<label>Your image</label>
									<input type="file"  name="image" required="required" class="form-control" accept="image/*" class="form-control">
								</div>
								<!-- checkbox -->
								<!--<div class="checkbox">
								<label class="pull-left" for="signing"><input type="checkbox" name="signing" id="signing"> I agree to our Terms and Conditions </label> 
								</div> --><!-- checkbox -->	
								<div class="submit-button text-center">
									<button type="submit" class="btn btn-primary">Register</button>
								</div>
							</form>	
						</div>
					</div><!-- user-login -->			
				</div><!-- row -->	
			</div><!-- container -->
		</div><!-- signup-page -->

<script>
	function doit11()
	{
		var val2 = document.getElementById("country_code").value;
		document.getElementById('phonecode').value = val2;
	}
</script>
<script type="text/javascript">
  function validate(evt) {
  var theEvent = evt || window.event;
  var key = theEvent.keyCode || theEvent.which;
  key = String.fromCharCode( key );
  var regex = /[0-9]|\./;
  if( !regex.test(key) ) {
    theEvent.returnValue = false;
    if(theEvent.preventDefault) theEvent.preventDefault();
  }
}
</script>
<script>
	function checkPasswordMatch() {
	var password=document.getElementById("txtNewPassword").value;
	var confirmPassword = document.getElementById("txtConfirmPassword").value;
	
	if (password != confirmPassword)
	{
		document.getElementById('txtNewPassword').value = "";
		document.getElementById('txtConfirmPassword').value = "";
		document.getElementById('messageshow2').style.display = 'none';
		document.getElementById('messageshow1').style.display = 'inline';
		document.getElementById('messageshow1').style.color = 'red';
	 }
	else
	{
		document.getElementById('messageshow1').style.display = 'none';
		document.getElementById('messageshow2').style.display = 'inline';
		document.getElementById('messageshow2').style.color = 'green';
	}
}
</script>
@stop