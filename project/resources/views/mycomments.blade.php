@extends('layout.base')

@section('body')
<?php 
$uid=Session::get('user_id');

 // echo "<pre>";
 // print_r($comment);
 // echo "<pre>";

?>

<div class="container">
    <div class="row profile">
		@include('account_base')
		<div class="col-md-9">
			<div class="comments-wrapper" style="margin-top:0px;">
			<h1 class="section-title title">Comments</h1>
			<?php 
			if($comment['status']['message']=="Successfull")
			{
			  $c = count($comment['comments']);

			for($i=0;$i<$c;$i++)
			{
					
				$comments= $comment['comments'][$i]['comment'];

				$likes= $comment['comments'][$i]['likes'];
				 $comment_likes= $comment['comments'][$i]['comment_liked'];
				 $title= $comment['comments'][$i]['title'];
				 $image1= $comment['comments'][$i]['image1'];
				 
				
			?>
			<ul class="media-list">
				<li class="media">
					<div class="media-left">
						<a href="#"><img class="media-object" src="{{asset('admin/images/').'/'.$image1}}" alt=""></a>
					</div>
					<div class="media-body">
						<h2><?php echo $title; ?></h2>
						<!-- <h3 class="date"><a href="#">15 December 2015</a></h3> -->
						<p><?php echo $comments; ?></p>
						<div class="loves" style="float:left;"><i class="fa fa-heart-o"></i>&nbsp;&nbsp;<?php echo $likes; ?></div>
					</div>
				</li>
				
			</ul>
			<?php } } 
			else
			{?>
				NO COMMENTS YET
			<?php }?>
		</div>

		</div>
	</div>
</div>

		
@stop