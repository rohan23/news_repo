Array
(
    [data] => Array
        (
            [0] => Array
                (
                    [cat_id] => 1
                    [category] => Local
                    [id] => fb91252f-647f-4a47-8a06-28506d2968c1
                    [order_by] => 1
                    [compulsory] => yes
                )

            [1] => Array
                (
                    [cat_id] => 3
                    [category] => Latest
                    [id] => ca4db095-b45c-419e-b5f0-227344a7965b
                    [order_by] => 2
                    [compulsory] => yes
                )

            [2] => Array
                (
                    [cat_id] => 2
                    [category] => Country
                    [id] => f9d5b7af-0914-45b5-8471-21d0a68c38b0
                    [order_by] => 3
                    [compulsory] => yes
                )

            [3] => Array
                (
                    [cat_id] => 4
                    [category] => Lifestyle
                    [id] => 867c39f5-c847-4021-b89b-194b81a342e8
                    [order_by] => 4
                    [compulsory] => no
                )

            [4] => Array
                (
                    [cat_id] => 5
                    [category] => Food
                    [id] => 5a9d87c4-0d9e-468c-8f48-15346b0b42ce
                    [order_by] => 5
                    [compulsory] => no
                )

            [5] => Array
                (
                    [cat_id] => 6
                    [category] => Pets
                    [id] => 295bd302-ff1a-47da-8fd1-24976fff2e30
                    [order_by] => 6
                    [compulsory] => no
                )

            [6] => Array
                (
                    [cat_id] => 7
                    [category] => Buy/Sell
                    [id] => f9143172-3500-43d9-a585-1220ac5c7718
                    [order_by] => 7
                    [compulsory] => no
                )

            [7] => Array
                (
                    [cat_id] => 8
                    [category] => Infrastructure
                    [id] => 9e514018-9144-49ee-ad5a-25292511610e
                    [order_by] => 8
                    [compulsory] => no
                )

            [8] => Array
                (
                    [cat_id] => 9
                    [category] => Education
                    [id] => 21e80a42-015d-43db-970d-0457559fbe12
                    [order_by] => 9
                    [compulsory] => no
                )

            [9] => Array
                (
                    [cat_id] => 10
                    [category] => Sports
                    [id] => cd4ee3e0-639e-470e-9656-1c071e1408da
                    [order_by] => 10
                    [compulsory] => no
                )

            [10] => Array
                (
                    [cat_id] => 11
                    [category] => Fashion
                    [id] => 3db89526-f995-41fe-a218-1337b3f662af
                    [order_by] => 11
                    [compulsory] => no
                )

            [11] => Array
                (
                    [cat_id] => 12
                    [category] => Property
                    [id] => 77123917-4d52-4113-be18-286ec32c4bdd
                    [order_by] => 12
                    [compulsory] => no
                )

            [12] => Array
                (
                    [cat_id] => 13
                    [category] => Finance
                    [id] => 5c7805cb-8b2b-433d-844f-2ae8d4d8c37c
                    [order_by] => 13
                    [compulsory] => no
                )

            [13] => Array
                (
                    [cat_id] => 14
                    [category] => Entertainment
                    [id] => 9da14079-98c7-4b6f-abb8-1d8cc2aad134
                    [order_by] => 14
                    [compulsory] => no
                )

            [14] => Array
                (
                    [cat_id] => 15
                    [category] => Social
                    [id] => 21ed430f-e8f6-4015-b48b-13619d00f3e7
                    [order_by] => 15
                    [compulsory] => no
                )

            [15] => Array
                (
                    [cat_id] => 16
                    [category] => View
                    [id] => 1f10e7c3-90dc-457c-a9b4-04065ce84a4a
                    [order_by] => 16
                    [compulsory] => no
                )

            [16] => Array
                (
                    [cat_id] => 17
                    [category] => Health
                    [id] => eefefc6c-197b-4357-b678-232cc841d5a4
                    [order_by] => 17
                    [compulsory] => no
                )

            [17] => Array
                (
                    [cat_id] => 18
                    [category] => Legal
                    [id] => 6acd7c73-1167-475e-884b-197faf1d3508
                    [order_by] => 18
                    [compulsory] => no
                )

            [18] => Array
                (
                    [cat_id] => 19
                    [category] => Car
                    [id] => 8f25e5cd-2213-46cb-83be-16597830f50f
                    [order_by] => 19
                    [compulsory] => no
                )

            [19] => Array
                (
                    [cat_id] => 20
                    [category] => Ask
                    [id] => afc701e2-f696-450c-bf58-0d66bf95f7d6
                    [order_by] => 20
                    [compulsory] => no
                )

            [20] => Array
                (
                    [cat_id] => 21
                    [category] => Technology
                    [id] => a8bc3b6f-6185-4726-9c0e-04136d6a58f2
                    [order_by] => 21
                    [compulsory] => no
                )

        )

    [status] => Array
        (
            [message] => Successful
        )

)