<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<!--title-->
    <title>NEARBY NEWS </title>



<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/font-awesome.min.css" rel="stylesheet">
<link href="../css/magnific-popup.css" rel="stylesheet">
<link href="../css/owl.carousel.css" rel="stylesheet">
<link href="../css/subscribe-better.css" rel="stylesheet">
<link href="../css/main.css" rel="stylesheet">
<link href="../css/responsive.css" rel="stylesheet">
<link id="preset" rel="stylesheet" type="text/css" href="../css/presets/preset1.css">

	<!--Google Fonts-->
	<link href='https://fonts.googleapis.com/css?family=Signika+Negative:400,300,600,700' rel='stylesheet' type='text/css'>
	
    <!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
    <![endif]-->
    
    
    <link rel="shortcut icon" href="../images/ico/favico.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="" href="../images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../images/ico/apple-touch-icon-57-precomposed.png">
<script>
	function trans_value(val1, val2)
	{
		var val3='"'+document.getElementById("n_title").innerText+'"';
		var val4='"'+document.getElementById("n_text").innerText+'"';
		var val5=document.getElementById("exist_lang").innerText;
		//alert('translate_code=' + val1 + 'promotion_id=' + val2 + 'promotion_title=' + val3 + 'promotion_text=' + val4 + 'existing_code=' + val5);
		jQuery.ajax({
		    url: '/translate',
		    type: 'POST',
		    data: {translate_code: val1,promotion_id:val2,promotion_title:val3,promotion_text:val4,existing_lang:val5},
		    success: function (data) {
		 	jQuery("#translated_news").html(data);
		   //alert('success');
		    }
		});
		
	}
</script>
	<!-- Css For Image Zooming -->
<style>
.center-cropped {
  object-fit: none; /* Do not scale the image */
  object-position: center; /* Center the image within the element */
  height: 200px;
  width: 100%;
}
</style>
	<?php
	include('main_class.php');
	$db = new Database();
	$db->connect();
	$page=$db->escapeString($_GET['page']);
	$val=$db->escapeString($_GET['val']);
	$id1=$db->escapeString($_GET['id']);
	?>	

<div class="container">
	
	
		<div class="section photo-gallery" style="padding-top: 50px; float:right;">
		
	
	
		<div class="col-sm-12">
			<h1 class="section-title title">Promotion Preview</h1>	
			<div id="photo-gallery" class="row carousel slide carousel-fade post" data-ride="carousel">
			<?php 

									
									
									$db->select('promotion_news','*',NULL,"id='$id1'",NULL);
									$res=$db->getResult();
									$count=count($res);
									for($i=0;$i<$count; $i++) 
									{
    									$id=$res[$i]['id'];
										$category=$res[$i]['category'];
										$title=$res[$i]['title'];
										$text=$res[$i]['text'];
										$email=$res[$i]['email'];
									 	$phone=$res[$i]['phone'];
										$radius=$res[$i]['radius'];
										$price=$res[$i]['price'];
										$start_date=$res[$i]['start_date'];
										$end_date=$res[$i]['end_date'];
										$lat=$res[$i]['lat'];
										$log=$res[$i]['log'];
										$adays=$res[$i]['time'];
											$like=$res[$i]['like'];
										$comment=$res[$i]['comment'];
										$status=$res[$i]['status'];
										
											// if($adays=="0")
												// {
													// $days="Today";
												// }
												// else 
												// {
													// $days=$adays." days";	
												// }
// 				
				
										$db->select('promotion_images','*',NULL,"p_id='$id1'",NULL);
										$res1=$db->getResult();
										$count1=count($res1);
										for($p=0;$p<$count1; $p++) 
										{
											$image1=$res1[$p]['image1'];
											
											$image2=$res1[$p]['image2'];
											
											$image3=$res1[$p]['image3'];
											
											$image4=$res1[$p]['image4'];
											
											$image5=$res1[$p]['image5'];
											
											$image6=$res1[$p]['image6'];
										
											$image7=$res1[$p]['image7'];
											
											$image8=$res1[$p]['image8'];
											
											$image9=$res1[$p]['image9'];
											
										}
									?>
			
				
			<div class="carousel-inner">
				
		
					
						<?php 
				if($image1!="")
				{
				?>
				<div class="item active">
					<a href="images/<?php echo $image1; ?>" class="image-link"><img style="max-height:425px; width:100%;" class="center-cropped img-responsive" src="images/<?php echo $image1; ?>" alt="" /></a>
					
				</div>
				<?php
				}
				if($image2!="")
				{?>
				<div class="item">
					<a href="images/<?php echo $image2; ?>"  class="image-link"><img style="max-height:425px; width:100%;" class="center-cropped img-responsive" src="images/<?php echo $image2; ?>" alt="" /></a>
				</div>
				<?php
				}
				if($image3!="")
				{?>
				<div class="item">
					<a href="images/<?php echo $image3; ?>" class="image-link"><img id="myImg" style="max-height:425px; width:100%;" class="center-cropped img-responsive" src="images/<?php echo $image3; ?>" alt="" /></a>
				</div>
				<?php
				}
				if($image4!="")
				{?>
				<div class="item">
					<a href="images/<?php echo $image4; ?>" class="image-link"><img id="myImg" style="max-height:425px; width:100%;" class="center-cropped img-responsive" src="images/<?php echo $image4; ?>" alt="" /></a>
				</div>
				<?php
				}
				if($image5!="")
				{?>
				<div class="item">
					<a href="images/<?php echo $image5; ?>" class="image-link"><img id="myImg" style="max-height:425px; width:100%;" class="center-cropped img-responsive" src="images/<?php echo $image5; ?>" alt="" /></a>
				</div>
				<?php
				}
				if($image6!="")
				{?>
				<div class="item">
					<a href="images/<?php echo $image6; ?>" class="image-link"><img id="myImg" style="max-height:425px; width:100%;" class="center-cropped img-responsive" src="images/<?php echo $image6; ?>" alt="" /></a>
				</div>
				<?php
				}
				if($image7!="")
				{?>
				<div class="item">
					<a href="images/<?php echo $image7; ?>" class="image-link"><img id="myImg" style="max-height:425px; width:100%;" class="center-cropped img-responsive" src="images/<?php echo $image7; ?>" alt="" /></a>
				</div>
				<?php
				}
				if($image8!="")
				{?>
				<div class="item">
					<a href="images/<?php echo $image8; ?>" class="image-link"><img id="myImg" style="max-height:425px; width:100%;" class="center-cropped img-responsive" src="images/<?php echo $image8; ?>" alt="" /></a>
				</div>
				<?php
				}
				if($image9!="")
				{?>
				<div class="item">
					<a href="images/<?php echo $image9; ?>" class="image-link"><img id="myImg" style="max-height:425px; width:100%;" class="center-cropped img-responsive" src="images/<?php echo $image9; ?>" alt="" /></a>
				</div>
			<?php } ?>
			</div><!--/carousel-inner-->
			<br><br>
			<div>
	
			<ol class="gallery-indicators carousel-indicators">
				<div style="float:right;">
					<span style="width:300px; padding:7px 50px; background-color:#000000;"><a style="color:#fff;" href="promotion_view?page=<?php echo $page; ?>&val=<?php echo $val; ?>">BACK</a></span> 
					<br>
					<br>
					<br>
						<span style="width:300px; padding:7px 50px; background-color:#000000;"><a style="color:#fff;" href="promotion_approve.php?page=<?php echo $page; ?>&val=<?php echo $val; ?>&id=<?php echo $id1; ?>">
							<?php if($status=="approve") 
							{
								echo "Approved";
							}
							else
							{
								echo "Pending";
							}
							?>
						</a></span> 
					</div>
				<?php
				if($image1!="")
				{?>
				<li data-target="#photo-gallery" data-slide-to="0" class="active">
					<img style="max-height:38px; width:38px;" class="img-responsive" src="images/<?php echo $image1; ?>" alt="" />
				</li>
				<?php
				}
				if($image2!="")
				{?>
				<li data-target="#photo-gallery" data-slide-to="1">
					<img style="max-height:38px; width:38px;" class="img-responsive" src="images/<?php echo $image2; ?>" alt="" />
				</li>
				<?php
				}
				if($image3!="")
				{?>
				<li data-target="#photo-gallery" data-slide-to="3">
					<img style="max-height:38px; width:38px;" class="img-responsive" src="images/<?php echo $image3; ?>" alt="" />
				</li>
				<?php
				}
				if($image4!="")
				{?>
				<li data-target="#photo-gallery" data-slide-to="4">
					<img style="max-height:38px; width:38px;" class="img-responsive" src="images/<?php echo $image4; ?>" alt="" />
				</li>
				<?php
				}
				if($image5!="")
				{?>
				<li data-target="#photo-gallery" data-slide-to="5">
					<img style="max-height:38px; width:38px;" class="img-responsive" src="images/<?php echo $image5; ?>" alt="" />
				</li>
				<?php
				}
				if($image6!="")
				{?>
				<li data-target="#photo-gallery" data-slide-to="6">
					<img style="max-height:38px; width:38px;" class="img-responsive" src="images/<?php echo $image6; ?>" alt="" />
				</li>
				<?php
				}
				if($image7!="")
				{?>
				<li data-target="#photo-gallery" data-slide-to="7">
					<img style="max-height:38px; width:38px;" class="img-responsive" src="images/<?php echo $image7; ?>" alt="" />
				</li>
				<?php
				}
				if($image8!="")
				{?>
				<li data-target="#photo-gallery" data-slide-to="8">
					<img style="max-height:38px; width:38px;" class="img-responsive" src="images/<?php echo $image8; ?>" alt="" />
				</li> 
				<?php
				}
				if($image9!="")
				{?>
				<li data-target="#photo-gallery" data-slide-to="9">
					<img style="max-height:38px; width:38px;" class="img-responsive" src="images/<?php echo $image9; ?>" alt="" />
				</li>
				<?php } ?>
			</ol><!--/gallery-indicators-->
			
			</div>
			<div class="post-content">								
				<div class="entry-meta">
					<ul class="list-inline">
					
							<li class="loves"><a ><i class="fa fa-thumbs-o-up"></i><?php echo $like; ?></a></li>
							<li class="loves"><a ><i class="fa fa-comment-o"></i><?php echo $comment; ?></a></li>
					
							
					
					
					<!-- <li class="publish-date"><i class="fa fa-clock-o"></i><?php echo $days; ?></li> -->
					
					
					
					</ul>
				
					
					<h2 class="entry-title">
					<?php echo $title; ?>
					</h2>
					<div class="entry-content">
						<p style="white-space: pre-wrap;"><?php echo $text; ?></p>
					</div>
					
					<?php } ?>
				</div>
			</div>
		</div><!--/photo-gallery--> 
		
	
		</div>
		<!--<div class="col-sm-3">
			<div id="sitebar">
				<div class="widget follow-us" style="padding:0px;">
					<h1 class="section-title title">Follow Us</h1>
					<ul class="list-inline social-icons">
						<li><a href="#"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
						<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
						<li><a href="#"><i class="fa fa-youtube"></i></a></li>
					</ul>
				</div>
			</div>
		</div> -->
	
	</div><!--/.section-gallary-->
</div><!--/.container-->


 <script type="text/javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/jquery.magnific-popup.min.js"></script>
	<script type="text/javascript" src="../js/owl.carousel.min.js"></script> 
	<script type="text/javascript" src="../js/moment.min.js"></script> 
	<script type="text/javascript" src="../js/jquery.simpleWeather.min.js"></script> 
	<script type="text/javascript" src="../js/jquery.sticky-kit.min.js"></script>
	<script type="text/javascript" src="../js/jquery.easy-ticker.min.js"></script> 
	<script type="text/javascript" src="../js/jquery.subscribe-better.min.js"></script> 
    <script type="text/javascript" src="../js/main.js"></script>
    <script type="text/javascript" src="../js/switcher.js"></script>
