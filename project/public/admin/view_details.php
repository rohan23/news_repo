<?php 
include('conn.php');
$sql=mysqli_query($conn, "select country from signup order by country asc");
$row=mysqli_fetch_array($sql);
$old_country=$row['country'];
$query=mysqli_query($conn, "select * from signup order by country asc");
$num=mysqli_num_rows($query);
$count=0;
$i=0; 
$arr=array();
while($row1=mysqli_fetch_array($query))
{
	$new_country=$row1['country'];
	if($old_country==$new_country && $num!=$i)
	{
		$count=$count+1;
		if(($num-1)==$i)
		{
			$arr['name'][]=$new_country;
			$arr['count'][]=$count;
		}
	}
	else
	{
		$arr['name'][]=$old_country;
		$arr['count'][]=$count;
		$count=1;
		$old_country=$new_country;
	}
	$i++;
}

$c=count($arr['name']);

$array=array();
for($a=0; $a<$c; $a++)
{
	$country=$arr['name'][$a];
	$country_count=$arr['count'][$a];
	$geocode_stats = file_get_contents("http://maps.googleapis.com/maps/api/geocode/json?address=$country&sensor=false");
	$output_deals = json_decode($geocode_stats);
	$latLng = $output_deals->results[0]->geometry->location;
	$lat = $latLng->lat;
	$lng = $latLng->lng;
	$array[$a]['title']="$country_count";
	$array[$a]['lat']="$lat";
	$array[$a]['lng']="$lng";
	$array[$a]['description']=$country."(".$country_count.")";
}
 $str=json_encode($array);
?>
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAXNYV2WAo_qX6kbNNGMCC_vFFSD1onJ5A&sensor=false" type="text/javascript"></script>
<script type="text/javascript">// <![CDATA[
var markers = <?php echo $str; ?>;
window.onload = function () {
var mapOptions = {
center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
zoom: 10,
mapTypeId: google.maps.MapTypeId.ROADMAP
};
var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
var infoWindow = new google.maps.InfoWindow();
var lat_lng = new Array();
var latlngbounds = new google.maps.LatLngBounds();
for (i = 0; i < markers.length; i++) {
var data = markers[i]
var myLatlng = new google.maps.LatLng(data.lat, data.lng);
lat_lng.push(myLatlng);
var marker = new google.maps.Marker({
position: myLatlng,
map: map,
label: data.title,
title: data.title
});
latlngbounds.extend(marker.position);
(function (marker, data) {
google.maps.event.addListener(marker, "click", function (e) {
infoWindow.setContent(data.description);
infoWindow.open(map, marker);
});
})(marker, data);
}
map.setCenter(latlngbounds.getCenter());
map.fitBounds(latlngbounds);

}

</script>
<div id="dvMap" style="width: 100%; height: 500px;"></div>