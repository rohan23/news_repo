<?php 
// Session start and value of session assigning to the variables 
session_start();
$u=$_SESSION['username'];
if($u=="")
{
	// check either user is login or not
	header('location:index');	
}
else
{
	include('main_class.php');
	$db = new Database();
	$db->connect();
	$country_code=$db->escapeString($_POST['country_code']);
	$country=$db->escapeString($_POST['country']);
	$db->insert('country',array('country'=>$country,'phonecode'=>$country_code));  // Table name, column names and respective values
	$res = $db->getResult();
	$count=count($res);
	$log="A New Country Added By $u ";
	//date_default_timezone_set("Asia/Kolkata"); 
	$date = date('Y-m-d H:i:s');
	if($count>0)
	{
		header('location:country');
		$db->insert('admin_logs',array('user'=>$u,'log'=>$log,'time'=>$date));  // Table name, column names and respective values
	}
	else 
	{
		 echo ("<SCRIPT LANGUAGE='JavaScript'>
				window.alert('Password Does not match')
				window.location.href='country';
				</SCRIPT>");
	}
}	
?>