<?php 
// Session start and value of session assigning to the variables 
session_start();
$u=$_SESSION['username'];
if($u=="")
{
	// check either user is login or not
	header('location:index');	
}
else
{
	include('main_class.php');
	 $db = new Database();
	 $db->connect();
	 $category=$db->escapeString($_POST['category']);
	 $language=$db->escapeString($_POST['language']);
	 $news_title=$db->escapeString($_POST['news_title']);
	 $news_text=$db->escapeString($_POST['news_text']);
	 $author=$db->escapeString($_POST['author']);
	 $lat=$db->escapeString($_POST['lat']);
	 $long=$db->escapeString($_POST['long']);
	 //date_default_timezone_set("Asia/Kolkata"); 
	 $date = date('Y-m-d H:i:s');
	$db->insert('news',array('id'=>NULL,'category'=>$category,'language'=>$language,'news_title'=>$news_title,'news_text'=>$news_text,'news_author'=>$author,'news_time'=>$date,'lat'=>$lat,'log'=>$long));  // Table name, column names and respective values
	$res = $db->getResult();
	$count=count($res);
	if($count>0)
	{
		header('location:news');
	}
	else 
	{
		 echo ("<SCRIPT LANGUAGE='JavaScript'>
				window.alert('Password Does not match')
				window.location.href='news';
				</SCRIPT>");
	}
}	
?>