<?php 
// Session start and value of session assigning to the variables 
session_start();
$u=$_SESSION['username'];

$_SESSION['user_agent'] = $_SERVER['HTTP_USER_AGENT']; # Save The User Agent
	$_SESSION['ip_address'] = $_SERVER['REMOTE_ADDR']; # Save The IP Address
		if( $_SESSION['user_agent'] === $_SERVER['HTTP_USER_AGENT'])
		{
		    # The User's Browser Hasn't Changed Since The Last Login
		    if( $_SESSION['ip_address'] === $_SERVER['REMOTE_ADDR'] ){
		    	$u=$_SESSION['username'];
if($u=="")
{
	// check either user is login or not
	header('location:index');	
}
else
{
	
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<title>Near By News App</title>
	<!--=== CSS ===-->

	<!-- Bootstrap -->
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

	<!-- jQuery UI -->
	<!--<link href="plugins/jquery-ui/jquery-ui-1.10.2.custom.css" rel="stylesheet" type="text/css" />-->
	<!--[if lt IE 9]>
		<link rel="stylesheet" type="text/css" href="plugins/jquery-ui/jquery.ui.1.10.2.ie.css"/>
	<![endif]-->

	<!-- Theme -->
	<link href="assets/css/main.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/plugins.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/icons.css" rel="stylesheet" type="text/css" />

	<link rel="stylesheet" href="assets/css/fontawesome/font-awesome.min.css">
	<!--[if IE 7]>
		<link rel="stylesheet" href="assets/css/fontawesome/font-awesome-ie7.min.css">
	<![endif]-->

	<!--[if IE 8]>
		<link href="assets/css/ie8.css" rel="stylesheet" type="text/css" />
	<![endif]-->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>

	<!--=== JavaScript ===-->

	<script type="text/javascript" src="assets/js/libs/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>

	<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/libs/lodash.compat.min.js"></script>

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
		<script src="assets/js/libs/html5shiv.js"></script>
	<![endif]-->

	<!-- Smartphone Touch Events -->
	<script type="text/javascript" src="plugins/touchpunch/jquery.ui.touch-punch.min.js"></script>
	<script type="text/javascript" src="plugins/event.swipe/jquery.event.move.js"></script>
	<script type="text/javascript" src="plugins/event.swipe/jquery.event.swipe.js"></script>

	<!-- General -->
	<script type="text/javascript" src="assets/js/libs/breakpoints.js"></script>
	<script type="text/javascript" src="plugins/respond/respond.min.js"></script> <!-- Polyfill for min/max-width CSS3 Media Queries (only for IE8) -->
	<script type="text/javascript" src="plugins/cookie/jquery.cookie.min.js"></script>
	<script type="text/javascript" src="plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script type="text/javascript" src="plugins/slimscroll/jquery.slimscroll.horizontal.min.js"></script>

	<!-- Page specific plugins -->
	<!-- Charts -->
	<script type="text/javascript" src="plugins/sparkline/jquery.sparkline.min.js"></script>

	<script type="text/javascript" src="plugins/daterangepicker/moment.min.js"></script>
	<script type="text/javascript" src="plugins/daterangepicker/daterangepicker.js"></script>
	<script type="text/javascript" src="plugins/blockui/jquery.blockUI.min.js"></script>

	<!-- Forms -->
	<script type="text/javascript" src="plugins/uniform/jquery.uniform.min.js"></script> <!-- Styled radio and checkboxes -->
	<script type="text/javascript" src="plugins/select2/select2.min.js"></script> <!-- Styled select boxes -->

	<!-- DataTables -->
	<script type="text/javascript" src="plugins/datatables/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="plugins/datatables/tabletools/TableTools.min.js"></script> <!-- optional -->
	<script type="text/javascript" src="plugins/datatables/colvis/ColVis.min.js"></script> <!-- optional -->
	<script type="text/javascript" src="plugins/datatables/DT_bootstrap.js"></script>

	<!-- App -->
	<script type="text/javascript" src="assets/js/app.js"></script>
	<script type="text/javascript" src="assets/js/plugins.js"></script>
	<script type="text/javascript" src="assets/js/plugins.form-components.js"></script>
	<script type="text/javascript" src="plugins/bootstrap-multiselect/bootstrap-multiselect.min.js"></script>

	<script>
	$(document).ready(function(){
		"use strict";

		App.init(); // Init layout and core plugins
		Plugins.init(); // Init all plugins
		FormComponents.init(); // Init all form-specific plugins
	});
	</script>

	<!-- Demo JS -->
	<script type="text/javascript" src="assets/js/custom.js"></script>

</head>
<body>

	<!--top header page here-->
	<?php include('top_header.php');?>
	<!--end top header page here-->

	<div id="container">
		<div id="sidebar" class="sidebar-fixed">
		<!--side menu content here-->
		<?php include('side_menu.php');?>
		<!-- end side menu content here-->
			<div id="divider" class="resizeable"></div>
		</div>
		<!-- /Sidebar -->

		<div id="content">
			<div class="container">
				<div class="crumbs">
					<ul id="breadcrumbs" class="breadcrumb">
						<li>
							<i class="icon-home"></i>
							<a href="dashboard">Dashboard</a>
						</li>
						<li class="current">
							<a href="#" title="">EDIT NEWS</a>
						</li>
					</ul>
				</div>
				<div class="row" style="padding-top: 60px;">
					<div class="col-md-1"></div>
					<div class="col-md-10">
						<div class="widget box">
							<div class="widget-header">
								<h3 style="text-align: center; font-weight: 900;"> Edit News</h3>
							</div>
							<div class="widget-content">
									<?php
									include('main_class.php');
									$db = new Database();
									$db->connect();
									$id1=$db->escapeString($_GET['id']);
									$db->select('news','*',NULL,"id='$id1'",NULL);
									$res=$db->getResult();
									$count=count($res);
									for($i=0;$i<$count; $i++) 
									{
    									$id=$res[$i]['id'];
										$category=$res[$i]['category'];
									 	$language=$res[$i]['language'];
									 	$user_id=$res[$i]['user_id'];
										$news_title=$res[$i]['news_title'];
										$news_text=$res[$i]['news_text'];
									 	$news_time=$res[$i]['news_time'];
										$lat=$res[$i]['lat'];
										$log=$res[$i]['log'];
										$status=$res[$i]['status'];
										$like=$res[$i]['like'];
										$comment=$res[$i]['comment'];
									
									
									
										
									?>
								<form class="form-horizontal row-border"  action="news_editquery.php" method="post" enctype="multipart/form-data">
									<!--<div class="form-group">
										<label class="col-md-2 control-label">Multiselect:</label>
										<div class="col-md-10">
											<select multiple="multiple" class="multiselect">
												<option value="cheese" selected>Cheese</option>
												<option value="tomatoes" selected>Tomatoes</option>
												<option value="mozarella" selected>Mozzarella</option>
												<option value="mushrooms">Mushrooms</option>
												<option value="pepperoni">Pepperoni</option>
												<option value="onions">Onions</option>
											</select>
										</div>
									</div> -->
									<div class="form-group">
									
											<input type="hidden" name="cat" value="<?php echo $category; ?>">
									
											<input type="hidden" name="id" value="<?php echo $id; ?>" >
											<input type="hidden" name="lan" value="<?php echo $language; ?>" >
										
											<label class="col-md-2 control-label">News Category<span class="required"></span></label>
										<div class="col-md-10">
											<select multiple="multiple" class="multiselect" name="category[]" class="form-control required has-success">
												<?php
											$db->select('category','*',NULL,NULL,NULL);
											$res1=$db->getResult();
											$count1=count($res1);
											for($a=0;$a<$count1;$a++) 
											{
    										// output data of each row
												$category_name1=$res1[$a]['category'];
												$category_id1=$res1[$a]['id'];
												$tags=$tags.$category_id1.',';
											?>
													<!--<option value="<?php echo $id1; ?>"><?php echo $category; ?></option>-->
												<?php }
											$tags1=rtrim($tags, ',');
											$stringA = explode(",", $tags1);
											$stringB = explode(",", $category);
											
											$Difference_1 = array_diff($stringA, $stringB); // Check string A Against String B
											$Difference_2 = array_diff($stringB, $stringA); // Check String B against String A
											$Difference = array_merge($Difference_1, $Difference_2); // Merge the two difference arrays together 
											$Difference = implode(',', $Difference); // Convert to a string
											?>
												
												
												
												
											<?php
											$db->select('category','*',NULL,NULL,NULL);
											$res2=$db->getResult();
											$count2=count($res2);
											for($x=0;$x<$count2;$x++) 
											{
    										// output data of each row
												$category_name=$res2[$x]['category'];
												$category_id=$res2[$x]['id'];
												
												$tags=$tags.$category_id.',';
												$ca = explode(',',$category);
												$ca_count=count($ca);
												for($y=0; $y<$ca_count; $y++)
												{
													if($category_id==$ca[$y])
													{
													?>
														<option value="<?php echo $category_id; ?>" selected><?php echo $category_name; ?></option>
													<?php
													break;
													}
												}
											}
												
												$ca1 = explode(',',$Difference);
												$ca1_count=count($ca1);
												for($b=0; $b<$ca1_count; $b++)
												{
													$cat_id=$ca1[$b];
													$db->select('category','*',NULL,"id='$cat_id'",NULL);
													$res3=$db->getResult();
													$count3=count($res3);
													for($z=0; $z<$count3; $z++)
													{
														$category_name2=$res3[$z]['category'];
															$category_id2=$res3[$z]['id'];
													
													?>
														<option value="<?php echo $category_id2; ?>"><?php echo $category_name2; ?></option>
													<?php
													}
													
												}
											?>
										
											</select>
										</div>
										
										
									</div>
									<div class="form-group">
										<label class="col-md-2 control-label">News Language<span class="required"></span></label>
										<div class="col-md-10">
											<select name="language" class="form-control required has-success">
											<option value="">Select language</option>
											<?php
											
											$db->select('language','*',NULL,NULL,NULL);
											$res=$db->getResult();
											$count=count($res);
											for($i=0;$i<$count; $i++) 
											{
    										// output data of each row
												$language=$res[$i]['language'];
												$id2=$res[$i]['id'];
												
											?>
												<option value="<?php echo $id2; ?>"> <?php echo $language; ?> </option>
												<?php } ?>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-2 control-label">News Title</label>
										<div class="col-md-10"><input type="text" name="news_title" value="<?php echo $news_title; ?>" class="form-control"></div>
									</div>
									<div class="form-group">
										<label class="col-md-2 control-label">News Text</label>
										<div class="col-md-10"><input type="text" name="news_text" value="<?php echo $news_text; ?>" class="form-control"></div>
									</div>
									<!--<div class="form-group">
										<label class="col-md-2 control-label">Latitude</label>
										<div class="col-md-10"><input type="text" readonly="readonly" value="<?php echo $lat; ?>" class="form-control"></div>
									</div> -->
									<!--<div class="form-group">
										<label class="col-md-2 control-label">Longitude</label>
										<div class="col-md-10"><input type="text" readonly="readonly" value="<?php echo $log; ?>" class="form-control"></div>
									</div> -->
									<div class="form-group">
										<label class="col-md-2 control-label">Image1</label>
										<div class="col-md-10"><input type="file" name="image1" class="form-control"></div>
									</div>
									<div class="form-group">
										<label class="col-md-2 control-label">Image2</label>
										<div class="col-md-10"><input type="file" name="image2" class="form-control"></div>
									</div>
									<div class="form-group">
										<label class="col-md-2 control-label">Image3</label>
										<div class="col-md-10"><input type="file" name="image3" class="form-control"></div>
									</div>
									<div class="form-group">
										<label class="col-md-2 control-label">Image4</label>
										<div class="col-md-10"><input type="file" name="image4" class="form-control"></div>
									</div>
									<div class="form-group">
										<label class="col-md-2 control-label">Image5</label>
										<div class="col-md-10"><input type="file" name="image5" class="form-control"></div>
									</div>
									<div class="form-group">
										<label class="col-md-2 control-label">Image6</label>
										<div class="col-md-10"><input type="file" name="image6" class="form-control"></div>
									</div>
									<div class="form-group">
										<label class="col-md-2 control-label">Image7</label>
										<div class="col-md-10"><input type="file" name="image7" class="form-control"></div>
									</div>
									<div class="form-group">
										<label class="col-md-2 control-label">Image8</label>
										<div class="col-md-10"><input type="file" name="image8" class="form-control"></div>
									</div>
									<div class="form-group">
										<label class="col-md-2 control-label">Image9</label>
										<div class="col-md-10"><input type="file" name="image9" class="form-control"></div>
									</div>
									<div class="form-actions">
										<input type="submit" value="Submit" class="btn btn-primary pull-right">
									</div>
								</form>
								<?php } ?>
							</div>
						</div>
						<!-- /Validation Example 1 -->
					</div>
					<div class="col-md-1"></div>
				</div> 
			
			</div> <!-- /.row -->

				<!-- /Page Content -->
		</div>
			<!-- /.container -->

	</div>


</body>
</html>
<?php } }
    else{

        # The User's IP Address Has Changed.
        # This Might Be A Session Hijacking Attempt

        # Destroy The Session
        $_SESSION = null;
        session_destroy(); # Destroy The Session
        session_unset(); # Unset The Session
        header('location: index');
		exit;

    }
}
else
{
    # The User's Browser Has Changed.
    # This Might Be A Session Hijacking Attempt

    # Destroy The Session
    $_SESSION = null;
    session_destroy(); # Destroy The Session
    session_unset(); # Unset The Session
            header('location: index');
		exit;
    
}?>