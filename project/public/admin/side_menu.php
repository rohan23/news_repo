<?php 
// Session start and value of session assigning to the variables 
session_start();
$u=$_SESSION['username'];
$r=$_SESSION['role'];
$c=$_SESSION['category'];
$s=$_SESSION['super'];
?>
			<div id="sidebar-content">

				<!-- Search Input -->
				<!-- <form class="sidebar-search">
					<div class="input-box">
						<button type="submit" class="submit">
							<i class="icon-search"></i>
						</button>
						<span>
							<input type="text" placeholder="Search...">
						</span>
					</div>
				</form> -->

				<!-- Search Results -->
				<!-- <div class="sidebar-search-results">

					<i class="icon-remove close"></i>
					<!-- Documents 
					<div class="title">
						Documents
					</div>
					<ul class="notifications">
						<li>
							<a href="javascript:void(0);">
								<div class="col-left">
									<span class="label label-info"><i class="icon-file-text"></i></span>
								</div>
								<div class="col-right with-margin">
									<span class="message"><strong>John Doe</strong> received $1.527,32</span>
									<span class="time">finances.xls</span>
								</div>
							</a>
						</li>
						<li>
							<a href="javascript:void(0);">
								<div class="col-left">
									<span class="label label-success"><i class="icon-file-text"></i></span>
								</div>
								<div class="col-right with-margin">
									<span class="message">My name is <strong>John Doe</strong> ...</span>
									<span class="time">briefing.docx</span>
								</div>
							</a>
						</li>
					</ul>
					
					<div class="title">
						Persons
					</div>
					<ul class="notifications">
						<li>
							<a href="javascript:void(0);">
								<div class="col-left">
									<span class="label label-danger"><i class="icon-female"></i></span>
								</div>
								<div class="col-right with-margin">
									<span class="message">Jane <strong>Doe</strong></span>
									<span class="time">21 years old</span>
								</div>
							</a>
						</li>
					</ul>
				</div>  --><!-- /.sidebar-search-results -->

				<!--=== Navigation ===-->
				<ul id="nav">
					<li class="current">
						<a href="dashboard">
							<i class="icon-dashboard"></i>
							Dashboard
						</a>
					</li>
					<?php if($s=="yes")
							{?>
					<li>
						<a href="javascript:void(0);">
							<i class="icon-user"></i>
							Landing Page
						</a>
						<ul class="sub-menu">
							
							<li>
								<a href="landing_page1">
								<i class="icon-angle-right"></i>
									Landing Page - 1
								</a>
							</li>
							<li>
								<a href="landing_page2">
								<i class="icon-angle-right"></i>
									Landing Page - 2
								</a>
							</li>
							<li>
								<a href="landing_page3">
								<i class="icon-angle-right"></i>
									Landing Page - 3
								</a>
							</li>
							<li>
								<a href="landing_page4">
								<i class="icon-angle-right"></i>
									Landing Page - 4
								</a>
							</li>
												
						</ul>
					</li>
					<?php } ?>
					
						
					<?php if($r=="superadmin")
					{ ?>
					<li>
						<a href="javascript:void(0);">
							<i class="icon-user"></i>
							Admin
						</a>
						<ul class="sub-menu">
							
							<li>
								<a href="admin">
								<i class="icon-angle-right"></i>
									Add New Admin
								</a>
							</li>
							<li>
								<a href="admins">
								<i class="icon-angle-right"></i>
									Edit Super Admins
								</a>
							</li>
							<li>
								<a href="subadmins">
								<i class="icon-angle-right"></i>
									Edit Admins
								</a>
							</li>
							<?php if($s=="yes")
							{?>
							<li>
								<a href="superadmin">
								<i class="icon-angle-right"></i>
									Edit Main Super Admin Credentials
								</a>
							</li>
							<?php } ?>							
						</ul>
					</li>
						
					<?php } 
					 if($r=="superadmin")
					{ ?>
					<li>
						<a href="javascript:void(0);">
							<i class="icon-fixed-width"></i>
							Users
						</a>
						<ul class="sub-menu">
							
							<li>
								<a href="users">
								<i class="icon-angle-right"></i>
									View User
								</a>
							</li>
							
						</ul>
					</li>
					<?php }
					if($r=="superadmin")
					{ ?>
					<li>
						<a href="javascript:void(0);">
							<i class="icon-fixed-width"></i>
							News Category
						</a>
						<ul class="sub-menu">
							<li>
								<a href="category">
								<i class="icon-angle-right"></i>
									Edit News Category
								</a>
							</li>
						</ul>
					</li>
					
					
					<!-- <li>
						<a href="javascript:void(0);">
							<i class="icon-fixed-width"></i>
							Analyised Data
						</a>
						<ul class="sub-menu">
							
							<li>
								<a href="device_analyse">
								<i class="icon-angle-right"></i>
									View By device
								</a>
							</li>
							<li>
								<a href="#">
								<i class="icon-angle-right"></i>
									View User
								</a>
							</li>
							
						</ul>
					</li> -->
					<?php } 
					 if($r=="superadmin")
					{ ?>
						
					<li>
						<a href="javascript:void(0);">
							<i class="icon-th"></i>
							News Language
						</a>
						<ul class="sub-menu">
							<li>
								<a href="language">
								<i class="icon-angle-right"></i>
									Add New Language
								</a>
							</li>
							
						</ul>
					</li>
					<?php } 
					 if($r=="superadmin")
					{ ?>
						
					<li>
						<a href="javascript:void(0);">
							<i class="icon-fixed-width"></i>
							Country
						</a>
						<ul class="sub-menu">
							<li>
								<a href="country">
								<i class="icon-angle-right"></i>
									View Country
								</a>
							</li>
							
						</ul>
					</li>
					<?php } ?>
					<li>
						<a href="javascript:void(0);">
								<i class="icon-fixed-width"></i>
							Promotion Category
						</a>
						<ul class="sub-menu">
							<li>
								<a href="promotion_category">
								<i class="icon-angle-right"></i>
									Add Promotion Category
								</a>
							</li>
						
						</ul>
					</li>
					<li>
						<a href="javascript:void(0);">
								<i class="icon-fixed-width"></i>
							Promotion News
						</a>
						<ul class="sub-menu">
							<li>
								<a href="promotion">
								<i class="icon-angle-right"></i>
									Add Promotion News
								</a>
							</li>
							<li>
								<a href="promotion_view">
								<i class="icon-angle-right"></i>
									View Promotion News
								</a>
							</li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0);">
								<i class="icon-fixed-width"></i>
							Promotion Price
						</a>
						<ul class="sub-menu">
							<li>
								<a href="add_plan">
								<i class="icon-angle-right"></i>
									Add Promotion Price
								</a>
							</li>
							<!-- <li>
								<a href="view_plan">
								<i class="icon-angle-right"></i>
									View Promotion Price
								</a>
							</li> -->
						</ul>
					</li>
						<li>
						<a href="javascript:void(0);">
							<i class="icon-folder-open-alt"></i>
							News
						</a>
						<ul class="sub-menu">
							<li>
								<a href="news">
								<i class="icon-angle-right"></i>
									View News
								</a>
							</li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0);">
							<i class="icon-calendar-empty"></i>
							News Comments
						</a>
						<ul class="sub-menu">
							
							<li>
								<a href="news_comment">
								<i class="icon-angle-right"></i>
									News Comments
								</a>
							</li>
							
						</ul>
					</li>
					<li>
						<a href="javascript:void(0);">
							<i class="icon-fixed-width"></i>
							Reported News
						</a>
						<ul class="sub-menu">
							<li>
								<a href="report_news">
								<i class="icon-angle-right"></i>
									View Reports of news
								</a>
							</li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0);">
							<i class="icon-comments"></i>
							Reported Comments
						</a>
						<ul class="sub-menu">
							<li>
								<a href="report_comments">
								<i class="icon-angle-right"></i>
									View Reports of Comments
								</a>
							</li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0);">
							<i class="icon-folder-open-alt"></i>
							Feedback
						</a>
						<ul class="sub-menu">
							<li>
								<a href="feedback">
								<i class="icon-angle-right"></i>
									View Feedback
								</a>
							</li>
						</ul>
					</li>
					<?php  
					 if($r=="superadmin")
					{ ?>
					<li>
						<a href="javascript:void(0);">
							<i class="icon-align-right"></i>
							Sub Admin Activity
						</a>
						<ul class="sub-menu">
							<li>
								<a href="logs">
								<i class="icon-angle-right"></i>
									View Logs
								</a>
							</li>
							
						</ul>
					</li>
					<?php } ?>
						<li>
						<a href="javascript:void(0);">
							<i class="icon-question-sign"></i>
							FAQ
						</a>
						<ul class="sub-menu">
							<li>
								<a href="faq">
								<i class="icon-angle-right"></i>
									FAQ
								</a>
							</li>
							
						</ul>
					</li>
					<li>
						<a href="javascript:void(0);">
							<i class="icon-question-sign"></i>
							Terms & Conditions
						</a>
						<ul class="sub-menu">
							<li>
								<a href="term">
								<i class="icon-angle-right"></i>
									Terms & Conditions
								</a>
							</li>
							
						</ul>
					</li>
						<!-- <li>
						<a href="javascript:void(0);">
							<i class="icon-desktop"></i>
							Images
						</a>
						<!-- <ul class="sub-menu">
							<li>
								<a href="images.php">
								<i class="icon-angle-right"></i>
									View News Images
								</a>
							</li>
							
						</ul>
 					</li> -->
					<!--<li>
						<a href="javascript:void(0);">
							<i class="icon-desktop"></i>
							Form
						</a>
						<ul class="sub-menu">
							<li>
								<a href="demo_form">
								<i class="icon-angle-right"></i>
								Form
								</a>
							</li>
							
						</ul>
					</li>
					
					<li>
						<a href="javascript:void(0);">
							<i class="icon-desktop"></i>
							Table
						</a>
						<ul class="sub-menu">
							<li>
								<a href="table">
								<i class="icon-angle-right"></i>
								View Data
								</a>
							</li>
							
						</ul>
					</li> -->
					<!--<li>
						<a href="javascript:void(0);">
							<i class="icon-edit"></i>
							Form Elements
						</a>
						<ul class="sub-menu">
							<li>
								<a href="form_components.html">
								<i class="icon-angle-right"></i>
								Form Components
								</a>
							</li>
							<li>
								<a href="form_layouts.html">
								<i class="icon-angle-right"></i>
								Form Layouts
								</a>
							</li>
							<li>
								<a href="form_validation.html">
								<i class="icon-angle-right"></i>
								Form Validation
								</a>
							</li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0);">
							<i class="icon-table"></i>
							Tables
						</a>
						<ul class="sub-menu">
							<li>
								<a href="tables_static.html">
								<i class="icon-angle-right"></i>
								Static Tables
								</a>
							</li>
							<li>
								<a href="tables_dynamic.html">
								<i class="icon-angle-right"></i>
								Dynamic Tables (DataTables)
								</a>
							</li>
							<li>
								<a href="tables_responsive.html">
								<i class="icon-angle-right"></i>
								Responsive Tables
								</a>
							</li>
						</ul>
					</li>
					<li>
						<a href="charts.html">
							<i class="icon-bar-chart"></i>
							Charts &amp; Statistics
						</a>
					</li>
					<li>
						<a href="javascript:void(0);">
							<i class="icon-folder-open-alt"></i>
							Pages
						</a>
						<ul class="sub-menu">
							<li>
								<a href="login.html">
								<i class="icon-angle-right"></i>
								Login
								</a>
							</li>
							<li>
								<a href="pages_user_profile.html">
								<i class="icon-angle-right"></i>
								User Profile
								</a>
							</li>
							<li>
								<a href="pages_calendar.html">
								<i class="icon-angle-right"></i>
								Calendar
								</a>
							</li>
							<li>
								<a href="pages_invoice.html">
								<i class="icon-angle-right"></i>
								Invoice
								</a>
							</li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0);">
							<i class="icon-list-ol"></i>
							4 Level Menu
						</a>
						<ul class="sub-menu">
							<li class="open-default">
								<a href="javascript:void(0);">
									<i class="icon-cogs"></i>
									Item 1
									<span class="arrow"></span>
								</a>
								<ul class="sub-menu">
									<li class="open-default">
										<a href="javascript:void(0);">
											<i class="icon-user"></i>
											Sample Link 1
											<span class="arrow"></span>
										</a>
										<ul class="sub-menu">
											<li class="current"><a href="javascript:void(0);"><i class="icon-remove"></i> Sample Link 1</a></li>
											<li><a href="javascript:void(0);"><i class="icon-pencil"></i> Sample Link 1</a></li>
											<li><a href="javascript:void(0);"><i class="icon-edit"></i> Sample Link 1</a></li>
										</ul>
									</li>
									<li><a href="javascript:void(0);"><i class="icon-user"></i>  Sample Link 1</a></li>
									<li><a href="javascript:void(0);"><i class="icon-external-link"></i>  Sample Link 2</a></li>
									<li><a href="javascript:void(0);"><i class="icon-bell"></i>  Sample Link 3</a></li>
								</ul>
							</li>
							<li>
								<a href="javascript:void(0);">
									<i class="icon-globe"></i>
									Item 2
									<span class="arrow"></span>
								</a>
								<ul class="sub-menu">
									<li><a href="javascript:void(0);"><i class="icon-user"></i>  Sample Link 1</a></li>
									<li><a href="javascript:void(0);"><i class="icon-external-link"></i>  Sample Link 1</a></li>
									<li><a href="javascript:void(0);"><i class="icon-bell"></i>  Sample Link 1</a></li>
								</ul>
							</li>
							<li>
								<a href="javascript:void(0);">
									<i class="icon-folder-open"></i>
									Item 3
								</a>
							</li>
						</ul>
					</li>
				</ul>
				
				<!-- /Navigation -->
			<!--	<div class="sidebar-title">
					<span>Notifications</span>
				</div>
				<ul class="notifications demo-slide-in"> <!-- .demo-slide-in is just for demonstration purposes. You can remove this. -->
			<!--		<li style="display: none;"> <!-- style-attr is here only for fading in this notification after a specific time. Remove this. -->
		<!--				<div class="col-left">
							<span class="label label-danger"><i class="icon-warning-sign"></i></span>
						</div>
						<div class="col-right with-margin">
							<span class="message">Server <strong>#512</strong> crashed.</span>
							<span class="time">few seconds ago</span>
						</div>
					</li>
		<!--			<li style="display: none;"> <!-- style-attr is here only for fading in this notification after a specific time. Remove this. -->
		<!--				<div class="col-left">
							<span class="label label-info"><i class="icon-envelope"></i></span>
						</div>
						<div class="col-right with-margin">
							<span class="message"><strong>John</strong> sent you a message</span>
							<span class="time">few second ago</span>
						</div>
					</li>
					<li>
						<div class="col-left">
							<span class="label label-success"><i class="icon-plus"></i></span>
						</div>
						<div class="col-right with-margin">
							<span class="message"><strong>Emma</strong>'s account was created</span>
							<span class="time">4 hours ago</span>
						</div>
					</li>-->
				</ul>

				<!-- <div class="sidebar-widget align-center">
					<div class="btn-group" data-toggle="buttons" id="theme-switcher">
						<label class="btn active">
							<input type="radio" name="theme-switcher" data-theme="bright"><i class="icon-sun"></i> Bright
						</label>
						<label class="btn">
							<input type="radio" name="theme-switcher" data-theme="dark"><i class="icon-moon"></i> Dark
						</label>
					</div>
				</div> -->

			</div>