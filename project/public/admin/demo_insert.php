<?php 
// Session start and value of session assigning to the variables 
session_start();
$u=$_SESSION['username'];
$r=$_SESSION['role'];
$c=$_SESSION['category'];
$country1=$_SESSION['country'];
$_SESSION['user_agent'] = $_SERVER['HTTP_USER_AGENT']; # Save The User Agent
	$_SESSION['ip_address'] = $_SERVER['REMOTE_ADDR']; # Save The IP Address
		if( $_SESSION['user_agent'] === $_SERVER['HTTP_USER_AGENT'])
		{
		    # The User's Browser Hasn't Changed Since The Last Login
		    if( $_SESSION['ip_address'] === $_SERVER['REMOTE_ADDR'] ){
		    	$u=$_SESSION['username'];
				$r=$_SESSION['role'];
				$c=$_SESSION['category'];
				$country1=$_SESSION['country'];
if($u=="")
{
	// check either user is login or not
	header('location:index');	
}
else
{


?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<title>Near By News App</title>
	<!--=== CSS ===-->

	<!-- Bootstrap -->
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

	<!-- jQuery UI -->
	<!--<link href="plugins/jquery-ui/jquery-ui-1.10.2.custom.css" rel="stylesheet" type="text/css" />-->
	<!--[if lt IE 9]>
		<link rel="stylesheet" type="text/css" href="plugins/jquery-ui/jquery.ui.1.10.2.ie.css"/>
	<![endif]-->

	<!-- Theme -->
	<link href="assets/css/main.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/plugins.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/icons.css" rel="stylesheet" type="text/css" />

	<link rel="stylesheet" href="assets/css/fontawesome/font-awesome.min.css">
	<!--[if IE 7]>
		<link rel="stylesheet" href="assets/css/fontawesome/font-awesome-ie7.min.css">
	<![endif]-->

	<!--[if IE 8]>
		<link href="assets/css/ie8.css" rel="stylesheet" type="text/css" />
	<![endif]-->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>

	<!--=== JavaScript ===-->

	<script type="text/javascript" src="assets/js/libs/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>

	<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/libs/lodash.compat.min.js"></script>

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
		<script src="assets/js/libs/html5shiv.js"></script>
	<![endif]-->

	<!-- Smartphone Touch Events -->
	<script type="text/javascript" src="plugins/touchpunch/jquery.ui.touch-punch.min.js"></script>
	<script type="text/javascript" src="plugins/event.swipe/jquery.event.move.js"></script>
	<script type="text/javascript" src="plugins/event.swipe/jquery.event.swipe.js"></script>

	<!-- General -->
	<script type="text/javascript" src="assets/js/libs/breakpoints.js"></script>
	<script type="text/javascript" src="plugins/respond/respond.min.js"></script> <!-- Polyfill for min/max-width CSS3 Media Queries (only for IE8) -->
	<script type="text/javascript" src="plugins/cookie/jquery.cookie.min.js"></script>
	<script type="text/javascript" src="plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script type="text/javascript" src="plugins/slimscroll/jquery.slimscroll.horizontal.min.js"></script>

	<!-- Page specific plugins -->
	<!-- Charts -->
	<script type="text/javascript" src="plugins/sparkline/jquery.sparkline.min.js"></script>

	<script type="text/javascript" src="plugins/daterangepicker/moment.min.js"></script>
	<script type="text/javascript" src="plugins/daterangepicker/daterangepicker.js"></script>
	<script type="text/javascript" src="plugins/blockui/jquery.blockUI.min.js"></script>

	<!-- Forms -->
	<script type="text/javascript" src="plugins/uniform/jquery.uniform.min.js"></script> <!-- Styled radio and checkboxes -->
	<script type="text/javascript" src="plugins/select2/select2.min.js"></script> <!-- Styled select boxes -->

	<!-- DataTables -->
	<script type="text/javascript" src="plugins/datatables/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="plugins/datatables/tabletools/TableTools.min.js"></script> <!-- optional -->
	<script type="text/javascript" src="plugins/datatables/colvis/ColVis.min.js"></script> <!-- optional -->
	<script type="text/javascript" src="plugins/datatables/DT_bootstrap.js"></script>

	<!-- App -->
	<script type="text/javascript" src="assets/js/app.js"></script>
	<script type="text/javascript" src="assets/js/plugins.js"></script>
	<script type="text/javascript" src="assets/js/plugins.form-components.js"></script>

	<script>
	$(document).ready(function(){
		"use strict";

		App.init(); // Init layout and core plugins
		Plugins.init(); // Init all plugins
		FormComponents.init(); // Init all form-specific plugins
	});
	</script>
	<script>
function approve(val1){

$.ajax({
 type: "POST",
 url: "approved.php",
 data:'id=' + val1 , 
 success: function(data)
 {
  $("#disapproved_list"+val1).html(data);
 }
 });
}
</script>


	<!-- Demo JS -->
	<script type="text/javascript" src="assets/js/custom.js"></script>

</head>
<body>

	<!--top header page here-->
	<?php include('top_header.php');?>
	<!--end top header page here-->

	<div id="container">
		<div id="sidebar" class="sidebar-fixed">
		<!--side menu content here-->
		<?php include('side_menu.php');?>
		<!-- end side menu content here-->
			<div id="divider" class="resizeable"></div>
		</div>
		<!-- /Sidebar -->

		<div id="content">
			<div class="container">
				<div class="crumbs">
					<ul id="breadcrumbs" class="breadcrumb">
						<li>
							<i class="icon-home"></i>
							<a href="dashboard">Dashboard</a>
						</li>
						<li class="current">
							<a href="#" title="">NEWS</a>
						</li>
					</ul>
				</div>
			<?php
	
			include('main_class.php');
			$db = new Database();
			$db->connect();
			$page=$db->escapeString($_GET['page']);
			$val=$db->escapeString($_GET['val']);
			if($val=="")
			{
				$val="10";
			}
			
			if($page!="")
			{
				$index=$page;
			}
			else
			{
				$index=1;
			}
			$p=$index-1;
			
			$skip=$p*$val;
			?>
			<div class="row" style="padding-top: 50px;">
		
										 
					<div class="col-md-12">
						<div class="widget box">
							<div class="widget-header">
								
								<div class="toolbar no-padding">
									<div class="btn-group">
										<span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span>
									</div>
								</div>
							</div>
							
							<div class="row" style="padding:0px 20px 0px 20px;;">
								<div class="col-md-6" style="padding:10px;">
									<div class="form-group">
									  <div class="input-icon">
									  	
										<div class="col-md-5">
												<select class="form-control" onchange="javascript:location.href = this.value;" style="padding:5px;"> 
												<option value="">RECORDS PER PAGE</option>
											
												 <option value="?page=1&val=5">5</option>
												  <option value="?page=1&val=10">10</option>
												  <option value="?page=1&val=25">25</option>
												  <option value="?page=1&val=50">50</option>
												  <option value="?page=1&val=100">100</option>
												  <option value="?page=1&val=200">200</option>
												  <option value="?page=1&val=all">ALL</option>
												
											</select>
										</div>
									  </div>
									</div>
								</div>
								<div class="col-md-6" style="padding:15px;text-align:right;">
									<div class="form-group">
										<div class="col-md-6"></div>
										<div class="col-md-6"><input type="text" placeholder="Search..." id="search_field" class="form-control"></div>
									</div>
									
								</div>
							</div>
							
							
							<div class="widget-content">
								<div style="overflow-x: auto">
								<table class="table table-striped table-bordered table-hover table-checkable" style="width:100%;" id="myTable">
									<thead>
										<tr class="myHead">
											<th>Id</th>
											<th>Category</th>
											<th>Language</th>
											<th>User Id</th>
											<th>News Title</th>
											<th>News Text</th>
											<th>News Time</th>
											<th>Country</th>
											<th>Current Status</th>
											<th>Like</th>
											<th>Comment</th>
											<th>Report</th>
											<th>View Images</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										
										<?php 
											include('conn.php');
											
											
											if($r=="superadmin")
											{
												if($val!="all")
												{
													$db->select('news','*',NULL,NULL,"id DESC",$val,$skip);
												}
												else
												{
													$db->select('news','*',NULL,NULL,"id DESC");
												}
												
											}
											else
											{
												if($val!="all")
												{
														if($c!="all")
														{
														$db->select('news','*',NULL," country='$country1' AND category LIKE '$c'","id DESC",$val,$skip);
														
														}
														else 
														{
															$db->select('news','*',NULL,"country='$country1'","id DESC",$skip,$val);
															
														}
												}
												else
												{
													if($c!="all")
														{
														$db->select('news','*',NULL," country='$country1' AND category LIKE '$c'","id DESC");
												
														}
														else 
														{
															$db->select('news','*',NULL,"country='$country1'","id DESC");
															
														}
												}
												
												
												
											}
												$res=$db->getResult();
												$count=count($res);
											
									
											for($i=0;$i<$count; $i++) 
											{
    										// output data of each row
    										 	$id=$res[$i]['id'];
       											$category=$res[$i]['category'];
												
												$cat=explode(",", $category);
												for($x=0; $x<count($cat); $x++)
												{
													$cc= $cc. "'".$cat[$x]."',";
												}
												$cc1= rtrim($cc, ',');
												$db->select('category','*',NULL,"id IN ($cc1)","id desc");
												$res23=$db->getResult();
												
												for($y=0; $y<count($res23); $y++)
												{
													$s= $s.$res23[$y]['category'].",";
												}
												
												$s1=rtrim($s, ',');
												$s3=ltrim($s1, 'no');
       										 	$language=$res[$i]['language'];
       										 	$user_id=$res[$i]['user_id'];
												$news_title=$res[$i]['news_title'];
												$news_text=$res[$i]['news_text'];
       										 	$news_time=$res[$i]['news_time'];
												$country2=$res[$i]['country'];
												$lat=$res[$i]['lat'];
												$log=$res[$i]['log'];
												$status=$res[$i]['status'];
												$like=$res[$i]['like'];
												$comment=$res[$i]['comment'];
												
												$db->select('flag','*',NULL,"news_id='$id'",NULL);
												$res1=$db->getResult();
												$count1=count($res1);
												$flag=$count1;
										?>
										<tr>
											<td><?php echo $id; ?></td>
											<td><?php echo $cate = substr($s3, 0, 20); ?><a href="news_description.php?id=<?php echo $id; ?>">read more...</a></td>
											<td><?php echo $language; ?></td>
											<td><?php echo $user_id; ?></td>
											<td><?php echo $news_title; ?> </td>

											<?php $string = $news_text;
											if (strlen($string) > 25) 
											{
											
											?>
											<td><?php echo $trimstring = substr($string, 0, 25); ?> <a href="news_description.php?id=<?php echo $id; ?>">read more...</a></td>
											<?php 
											}
											 else 
											 {?>
												<td><?php echo $news_text; ?></td>
											 <?php } ?>
											
											
											<td><?php echo $news_time; ?></td>
											<td><?php echo $country2; ?> </td>
											<!--<td><?php echo $log; ?> </td>-->
											
											<?php
											if($status==pending)
											{?>
												
												<td>
								   <div id="disapproved_list<?php echo $id;?>">	
								    	<a onclick="approve(<?php echo $id; ?>)" ><span class="label label-danger">Offline</span></a><br></a><br>
								
								<a data-toggle="modal" href="#reject<?php echo $id; ?>" > <span class="label label-warning">Reject</span><br></a>
												</div>
												</td>
											
											<?php }
											else
											{ ?>
												<td>
														<div id="disapproved_list<?php echo $id; ?>"> <a data-toggle="modal" href="#disapprove<?php echo $id; ?>" > <span class="label label-success">Online</span><br></a></div>
													
												</td>	
											<?php } ?>
						
											<td><?php echo $like; ?> </td>
											<td><?php echo $comment; ?> </td>
											<td><?php echo $flag; ?> </td>
											<td><a href="images.php?id=<?php echo $id; ?>" class="bs-tooltip" title="images" data-original-title="images"><i class="icon-fixed-width"></i></a></td>
											<td class="align-center">
												<span class="btn-group">
													<a href="news_edit.php?id=<?php echo $id; ?>" class="bs-tooltip" title="" data-original-title="Edit"><i class="icon-pencil"></i></a>
													<a data-toggle="modal" href="#myModal<?php echo $id; ?>"class="bs-tooltip" title="Delete" class="btn btn-xs"><i class="icon-trash"></i></a>
												</span>
											</td>
										</tr>
											<!-- model for Reject news -->
										<div class="modal fade" id="reject<?php echo $id; ?>">
											<div class="modal-dialog">
												<div class="modal-content">
													<form>
														<div class="row">
															<h3 style="text-align:center;">Enter Your Reason For Rejecting the news</h3>
														</div>
														
														<!-- disapprove ajex -->
											<script>
											function reject(dis){
											var val4=$("#reject_id"+ dis).val();
											$.ajax({
											 type: "POST",
											 url: "reject.php",
											 data:'id=' + dis + '&message=' + val4, 
											 success: function(data)
											 {
											  $("#disapproved_list"+dis).html(data);
											   $("#reject"+dis).modal('hide');

											 
											 }
											});
											
											}
											</script>
											<!-- stop -->	
														<!-- <div class="row" style="padding:20px;">
															<div class="col-md-3"><label>Id</label></div>
															<div class="col-md-8"><input type="text" value="<?php echo $id; ?>" name="d_iddd<?php echo $id; ?>" class="form-control"></div>
														</div>  -->
														<div class="row" style="padding:7px;">
															<div class="col-md-3"><label>Enter Reason Here</label></div>
															<div class="col-md-8"><input type="text" name="r_message<?php echo $id; ?>"  class="form-control" id="reject_id<?php echo $id; ?>"></div>
														</div>
														
													<div class="modal-footer">
														<!-- <button type="submit"  class="btn btn-primary" >Yes</button> -->
														<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
														<a class="btn btn-primary" onclick="reject(<?php echo $id; ?>)">Submit</a>
													</div>
													</form>

												</div><!-- /.modal-content -->
											</div><!-- /.modal-dialog -->
										</div><!-- /.modal -->
										<!-- model for disapprove news -->
										<div class="modal fade" id="disapprove<?php echo $id; ?>">
											<div class="modal-dialog">
												<div class="modal-content">
													<form>
														<div class="row">
															<h3 style="text-align:center;">Enter Your Reason For Disapprove the news</h3>
														</div>
														
														<!-- disapprove ajex -->
											<script>
											function dis_approve(dis){
											var val4=$("#message_id"+ dis).val();
											
											$.ajax({
											 type: "POST",
											 url: "disapproved.php",
											 data:'id=' + dis + '&message=' + val4, 
											 success: function(data)
											 {
											  $("#disapproved_list"+dis).html(data);
											   $("#disapprove"+dis).modal('hide');

											 
											 }
											});
											
											}
											</script>
											<!-- stop -->	
														<!-- <div class="row" style="padding:20px;">
															<div class="col-md-3"><label>Id</label></div>
															<div class="col-md-8"><input type="text" value="<?php echo $id; ?>" name="d_iddd<?php echo $id; ?>" class="form-control"></div>
														</div>  -->
														<div class="row" style="padding:7px;">
															<div class="col-md-3"><label>Enter Reason Here</label></div>
															<div class="col-md-8"><input type="text" name="d_message<?php echo $id; ?>"  class="form-control" id="message_id<?php echo $id; ?>"></div>
														</div>
														
													<div class="modal-footer">
														<!-- <button type="submit"  class="btn btn-primary" >Yes</button> -->
														<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
														<a class="btn btn-primary" onclick="dis_approve(<?php echo $id; ?>)">Submit</a>
													</div>
													</form>

												</div><!-- /.modal-content -->
											</div><!-- /.modal-dialog -->
										</div><!-- /.modal -->
										<!-- Modal dialog -->
											<div class="modal fade" id="myModal<?php echo $id; ?>">
												<div class="modal-dialog">
													<div class="modal-content">
														<div class="modal-header">
															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
															<h4 class="modal-title">Delete Confirmation</h4>
														</div>
														<div class="modal-body">
															Are You Sure You Want To Delete The Records
														</div>
														<div class="modal-footer">
															<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
															<a href="news_delete.php?id=<?php echo $id; ?>" class="btn btn-primary" >Yes</a
														</div>
													</div><!-- /.modal-content -->
												</div><!-- /.modal-dialog -->
											</div><!-- /.modal -->
										
											

									<?php 
										unset($s);
										unset($cc);
										} 
									?>
									</tbody>
								</table>
								<?php
											
								if($val!="all")
								{
								 
									
								?>
								
						<div style="float:right">
								<?php
								if($r=="superadmin")
								{
									$db->select('news','*',NULL,NULL,"id DESC");
								}
								else
								{
									if($c!="all")
										{
										$db->select('news','*',NULL," country='$country1' AND category LIKE '$c'","id DESC");
									
										}
										else 
										{
											$db->select('news','*',NULL,"country='$country1'","id DESC");
											
										}
								}
								$res1=$db->getResult();
								$count1=count($res1);
								$total_pages = ceil($count1 / $val); ?>
								<ul class="pagination">
									
								<?php
								if($val<=$count1)
								{
								 if($page>1)
								{
									echo "<li><a href='?page=".($page-1)."&val=".($val)."'>PREVIOUS</a></li> "; // Goto 1st page  
								} 
								
								 for ($i=1; $i<=$total_pages; $i++) 
								 
								{
									 if($page==$i)
									{
										echo "<li "."class="."current"."><a href='?page=".$i."&val=".($val)."'>".$i."</a></li>"; 
									}
									 else
									 {
									 	 echo "<li><a href='?page=".$i."&val=".($val)."'>".$i."</a></li>"; 
									 }
								    
								};
								
								if($page!=$total_pages)
								{
									echo "<li><a href='?page=".($page+1)."&val=".($val)."'>NEXT</a></li> "; // Goto Last page  
								}
								}
								?>
								</ul>
						</div>
							<?php }  ?>
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</div> <!-- /.row -->

				<!-- /Page Content -->
		</div>
			<!-- /.container -->

	</div>


</body>
<script>
	$('#search_field').on('keyup', function() {
  var value = $(this).val();
  var patt = new RegExp(value, "i");

  $('#myTable').find('tr').each(function() {
    if (!($(this).find('td').text().search(patt) >= 0)) {
      $(this).not('.myHead').hide();
    }
    if (($(this).find('td').text().search(patt) >= 0)) {
      $(this).show();
    }
  });

});
</script>
</html>
<?php } }
    else{

        # The User's IP Address Has Changed.
        # This Might Be A Session Hijacking Attempt

        # Destroy The Session
        $_SESSION = null;
        session_destroy(); # Destroy The Session
        session_unset(); # Unset The Session
        header('location: index');
		exit;

    }
}
else
{
    # The User's Browser Has Changed.
    # This Might Be A Session Hijacking Attempt

    # Destroy The Session
    $_SESSION = null;
    session_destroy(); # Destroy The Session
    session_unset(); # Unset The Session
            header('location: index');
		exit;
    
}?>