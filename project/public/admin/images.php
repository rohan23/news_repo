<?php 
// Session start and value of session assigning to the variables 
session_start();
$u=$_SESSION['username'];
$_SESSION['user_agent'] = $_SERVER['HTTP_USER_AGENT']; # Save The User Agent
	$_SESSION['ip_address'] = $_SERVER['REMOTE_ADDR']; # Save The IP Address
		if( $_SESSION['user_agent'] === $_SERVER['HTTP_USER_AGENT'])
		{
		    # The User's Browser Hasn't Changed Since The Last Login
		    if( $_SESSION['ip_address'] === $_SERVER['REMOTE_ADDR'] ){
		    	$u=$_SESSION['username'];
if($u=="")
{
	// check either user is login or not
	header('location:login');	
}
else
{
	
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<title>Near By News App</title>

	<!--=== CSS ===-->

	<!-- Bootstrap -->
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

	<!-- jQuery UI -->
	<!--<link href="plugins/jquery-ui/jquery-ui-1.10.2.custom.css" rel="stylesheet" type="text/css" />-->
	<!--[if lt IE 9]>
		<link rel="stylesheet" type="text/css" href="plugins/jquery-ui/jquery.ui.1.10.2.ie.css"/>
	<![endif]-->

	<!-- Theme -->
	<link href="assets/css/main.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/plugins.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/icons.css" rel="stylesheet" type="text/css" />

	<link rel="stylesheet" href="assets/css/fontawesome/font-awesome.min.css">
	<!--[if IE 7]>
		<link rel="stylesheet" href="assets/css/fontawesome/font-awesome-ie7.min.css">
	<![endif]-->

	<!--[if IE 8]>
		<link href="assets/css/ie8.css" rel="stylesheet" type="text/css" />
	<![endif]-->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>

	<!--=== JavaScript ===-->

	<script type="text/javascript" src="assets/js/libs/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>

	<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/libs/lodash.compat.min.js"></script>

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
		<script src="assets/js/libs/html5shiv.js"></script>
	<![endif]-->

	<!-- Smartphone Touch Events -->
	<script type="text/javascript" src="plugins/touchpunch/jquery.ui.touch-punch.min.js"></script>
	<script type="text/javascript" src="plugins/event.swipe/jquery.event.move.js"></script>
	<script type="text/javascript" src="plugins/event.swipe/jquery.event.swipe.js"></script>

	<!-- General -->
	<script type="text/javascript" src="assets/js/libs/breakpoints.js"></script>
	<script type="text/javascript" src="plugins/respond/respond.min.js"></script> <!-- Polyfill for min/max-width CSS3 Media Queries (only for IE8) -->
	<script type="text/javascript" src="plugins/cookie/jquery.cookie.min.js"></script>
	<script type="text/javascript" src="plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script type="text/javascript" src="plugins/slimscroll/jquery.slimscroll.horizontal.min.js"></script>

	<!-- Page specific plugins -->
	<!-- Charts -->
	<script type="text/javascript" src="plugins/sparkline/jquery.sparkline.min.js"></script>

	<script type="text/javascript" src="plugins/daterangepicker/moment.min.js"></script>
	<script type="text/javascript" src="plugins/daterangepicker/daterangepicker.js"></script>
	<script type="text/javascript" src="plugins/blockui/jquery.blockUI.min.js"></script>

	<!-- Forms -->
	<script type="text/javascript" src="plugins/uniform/jquery.uniform.min.js"></script> <!-- Styled radio and checkboxes -->
	<script type="text/javascript" src="plugins/select2/select2.min.js"></script> <!-- Styled select boxes -->

	<!-- DataTables -->
	<script type="text/javascript" src="plugins/datatables/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="plugins/datatables/tabletools/TableTools.min.js"></script> <!-- optional -->
	<script type="text/javascript" src="plugins/datatables/colvis/ColVis.min.js"></script> <!-- optional -->
	<script type="text/javascript" src="plugins/datatables/DT_bootstrap.js"></script>

	<!-- App -->
	<script type="text/javascript" src="assets/js/app.js"></script>
	<script type="text/javascript" src="assets/js/plugins.js"></script>
	<script type="text/javascript" src="assets/js/plugins.form-components.js"></script>

	<script>
	$(document).ready(function(){
		"use strict";

		App.init(); // Init layout and core plugins
		Plugins.init(); // Init all plugins
		FormComponents.init(); // Init all form-specific plugins
	});
	</script>

	<!-- Demo JS -->
	<script type="text/javascript" src="assets/js/custom.js"></script>

</head>

<body>

	<!--top header page here-->
	<?php include('top_header.php');?>
	<!--end top header page here-->

	<div id="container">
		<div id="sidebar" class="sidebar-fixed">
		<!--side menu content here-->
		<?php include('side_menu.php');?>
		<!-- end side menu content here-->
			<div id="divider" class="resizeable"></div>
		</div>
		<!-- /Sidebar -->

		<div id="content">
			<div class="container">
				<div class="crumbs">
					<ul id="breadcrumbs" class="breadcrumb">
						<li>
							<i class="icon-home"></i>
							<a href="dashboard">Dashboard</a>
						</li>
						<li class="current">
							<a href="#" title="">News Images</a>
						</li>
					</ul>
				</div>
				
				<div class="row">
					<div class="col-md-12" style="padding-top: 60px;">
						<div class="widget box">
							<div class="widget-header">
								
								<div class="toolbar no-padding">
									<div class="btn-group">
										<span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span>
									</div>
								</div>
							</div>
							<div class="widget-content">
								<table class="table table-striped table-bordered table-hover table-checkable datatable">
									<thead>
										<tr>
											<th>Id</th>
											<th>News Id</th>
											<th>User Id</th>
											<th>Image1</th>
											<th>Image2</th>
											<th>Image3</th>
											<th>Image4</th>
											<th>Image5</th>
											<th>Image6</th>
											<th>Image7</th>
											<th>Image8</th>
											<th>Image9</th>
										</tr>
									</thead>
									<tbody>
										<?php 
											$id=$_GET['id'];
											include('main_class.php');
											$db = new Database();
											$db->connect();
											$db->select('images','*',NULL,"news_id='$id'",NULL);
											$res=$db->getResult();
											$count=count($res);
											for($i=0;$i<$count; $i++) 
											{
    										// output data of each row
    										 	$id=$res[$i]['id'];
       											$news_id=$res[$i]['news_id'];
												$u_id=$res[$i]['u_id'];
       										 	$image1=$res[$i]['image1'];
												$image2=$res[$i]['image2'];
												$image3=$res[$i]['image3'];
												$image4=$res[$i]['image4'];
												$image5=$res[$i]['image5'];
												$image6=$res[$i]['image6'];
												$image7=$res[$i]['image7'];
												$image8=$res[$i]['image8'];
												$image9=$res[$i]['image9'];
										?>
										<tr>
											<td><?php echo $id; ?></td>
											<td><?php echo $news_id; ?></td>
											<td><?php echo $u_id; ?></td>
											<td><img  style="height:50px; width: 80px;" src="images/<?php echo $image1; ?>"></td>
											<td><img  style="height:50px; width: 80px;" src="images/<?php echo $image2; ?>"></td>
											<td><img  style="height:50px; width: 80px;" src="images/<?php echo $image3; ?>"></td>
											<td><img  style="height:50px; width: 80px;" src="images/<?php echo $image4; ?>"></td>
											<td><img  style="height:50px; width: 80px;" src="images/<?php echo $image5; ?>"></td>
											<td><img  style="height:50px; width: 80px;" src="images/<?php echo $image6; ?>"></td>
											<td><img  style="height:50px; width: 80px;" src="images/<?php echo $image7; ?>"></td>
											<td><img  style="height:50px; width: 80px;" src="images/<?php echo $image8; ?>"></td>
											<td><img  style="height:50px; width: 80px;" src="images/<?php echo $image9; ?>"></td>
											
											<!-- <td class="align-center">
												<span class="btn-group">
													<a data-toggle="modal" href="#myModal1<?php echo $id; ?>"class="bs-tooltip" title="Delete" class="btn btn-xs"><i class="icon-trash"></i></a>
												</span>
											</td> -->
										</tr>
										<!-- Modal dialog -->
											<!-- <div class="modal fade" id="myModal1<?php echo $id; ?>">
												<div class="modal-dialog">
													<div class="modal-content">
														<div class="modal-header">
															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
															<h4 class="modal-title">Delete Confirmation</h4>
														</div>
														<div class="modal-body">
															Are You Sure You Want To Delete The Records
														</div>
														<div class="modal-footer">
															<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
															<a href="image_delete.php?id=<?php echo $id; ?>" class="btn btn-primary" >Yes</a
														</div>
													</div><!-- /.modal-content
												</div><!-- /.modal-dialog
											</div><!-- /.modal -->

									<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				
				
				
			</div> <!-- /.row -->

				<!-- /Page Content -->
		</div>
			<!-- /.container -->

	</div>


</body>
</html>
<?php } }
    else{

        # The User's IP Address Has Changed.
        # This Might Be A Session Hijacking Attempt

        # Destroy The Session
        $_SESSION = null;
        session_destroy(); # Destroy The Session
        session_unset(); # Unset The Session
        header('location: index');
		exit;

    }
}
else
{
    # The User's Browser Has Changed.
    # This Might Be A Session Hijacking Attempt

    # Destroy The Session
    $_SESSION = null;
    session_destroy(); # Destroy The Session
    session_unset(); # Unset The Session
            header('location: index');
		exit;
    
}?>