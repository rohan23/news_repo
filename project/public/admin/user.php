<?php 
// Session start and value of session assigning to the variables 
session_start();
$u=$_SESSION['username'];
$_SESSION['user_agent'] = $_SERVER['HTTP_USER_AGENT']; # Save The User Agent
	$_SESSION['ip_address'] = $_SERVER['REMOTE_ADDR']; # Save The IP Address
		if( $_SESSION['user_agent'] === $_SERVER['HTTP_USER_AGENT'])
		{
		    # The User's Browser Hasn't Changed Since The Last Login
		    if( $_SESSION['ip_address'] === $_SERVER['REMOTE_ADDR'] ){
		    	$u=$_SESSION['username'];
if($u=="")
{
	// check either user is login or not
	header('location:index');	
}
else
{
	
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<title>Dynamic Tables (DataTables) | Melon - Flat &amp; Responsive Admin Template</title>

	<!--=== CSS ===-->

	<!-- Bootstrap -->
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

	<!-- jQuery UI -->
	<!--<link href="plugins/jquery-ui/jquery-ui-1.10.2.custom.css" rel="stylesheet" type="text/css" />-->
	<!--[if lt IE 9]>
		<link rel="stylesheet" type="text/css" href="plugins/jquery-ui/jquery.ui.1.10.2.ie.css"/>
	<![endif]-->

	<!-- Theme -->
	<link href="assets/css/main.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/plugins.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/icons.css" rel="stylesheet" type="text/css" />

	<link rel="stylesheet" href="assets/css/fontawesome/font-awesome.min.css">
	<!--[if IE 7]>
		<link rel="stylesheet" href="assets/css/fontawesome/font-awesome-ie7.min.css">
	<![endif]-->

	<!--[if IE 8]>
		<link href="assets/css/ie8.css" rel="stylesheet" type="text/css" />
	<![endif]-->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>

	<!--=== JavaScript ===-->

	<script type="text/javascript" src="assets/js/libs/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>

	<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/libs/lodash.compat.min.js"></script>

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
		<script src="assets/js/libs/html5shiv.js"></script>
	<![endif]-->

	<!-- Smartphone Touch Events -->
	<script type="text/javascript" src="plugins/touchpunch/jquery.ui.touch-punch.min.js"></script>
	<script type="text/javascript" src="plugins/event.swipe/jquery.event.move.js"></script>
	<script type="text/javascript" src="plugins/event.swipe/jquery.event.swipe.js"></script>

	<!-- General -->
	<script type="text/javascript" src="assets/js/libs/breakpoints.js"></script>
	<script type="text/javascript" src="plugins/respond/respond.min.js"></script> <!-- Polyfill for min/max-width CSS3 Media Queries (only for IE8) -->
	<script type="text/javascript" src="plugins/cookie/jquery.cookie.min.js"></script>
	<script type="text/javascript" src="plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script type="text/javascript" src="plugins/slimscroll/jquery.slimscroll.horizontal.min.js"></script>

	<!-- Page specific plugins -->
	<!-- Charts -->
	<script type="text/javascript" src="plugins/sparkline/jquery.sparkline.min.js"></script>

	<script type="text/javascript" src="plugins/daterangepicker/moment.min.js"></script>
	<script type="text/javascript" src="plugins/daterangepicker/daterangepicker.js"></script>
	<script type="text/javascript" src="plugins/blockui/jquery.blockUI.min.js"></script>

	<!-- Forms -->
	<script type="text/javascript" src="plugins/uniform/jquery.uniform.min.js"></script> <!-- Styled radio and checkboxes -->
	<script type="text/javascript" src="plugins/select2/select2.min.js"></script> <!-- Styled select boxes -->

	<!-- DataTables -->
	<script type="text/javascript" src="plugins/datatables/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="plugins/datatables/tabletools/TableTools.min.js"></script> <!-- optional -->
	<script type="text/javascript" src="plugins/datatables/colvis/ColVis.min.js"></script> <!-- optional -->
	<script type="text/javascript" src="plugins/datatables/DT_bootstrap.js"></script>

	<!-- App -->
	<script type="text/javascript" src="assets/js/app.js"></script>
	<script type="text/javascript" src="assets/js/plugins.js"></script>
	<script type="text/javascript" src="assets/js/plugins.form-components.js"></script>

	<script>
	$(document).ready(function(){
		"use strict";

		App.init(); // Init layout and core plugins
		Plugins.init(); // Init all plugins
		FormComponents.init(); // Init all form-specific plugins
	});
	</script>

	<!-- Demo JS -->
	<script type="text/javascript" src="assets/js/custom.js"></script>

</head>

<body>

	<!--top header page here-->
	<?php include('top_header.php');?>
	<!--end top header page here-->

	<div id="container">
		<div id="sidebar" class="sidebar-fixed">
		<!--side menu content here-->
		<?php include('side_menu.php');?>
		<!-- end side menu content here-->
			<div id="divider" class="resizeable"></div>
		</div>
		<!-- /Sidebar -->

		<div id="content">
			<div class="container">
				<div class="row">
					<div class="col-md-1"></div>
					<div class="col-md-10">
						<div class="widget box">
							<div class="widget-header">
								<h3 style="text-align: center; font-weight: 900;"> Enter Your Details</h3>
							</div>
							<div class="widget-content">
								<form class="form-horizontal row-border"  action="user_insert" method="post">
									<div class="form-group">
										<label class="col-md-2 control-label">First Name:</label>
										<div class="col-md-10"><input type="text" name="fname" class="form-control"></div>
									</div>
									<div class="form-group">
										<label class="col-md-2 control-label">Last Name:</label>
										<div class="col-md-10"><input type="text" name="lname" class="form-control"></div>
									</div>
									<div class="form-group">
										<label class="col-md-2 control-label">User Name:</label>
										<div class="col-md-10"><input type="text" name="user" class="form-control"></div>
									</div>
									<div class="form-actions">
										<input type="submit" value="Submit" class="btn btn-primary pull-right">
									</div>
								</form>
							</div>
						</div>
						<!-- /Validation Example 1 -->
					</div>
					<div class="col-md-1"></div>
				</div>
			</div> <!-- /.row -->

				<!-- /Page Content -->
		</div>
			<!-- /.container -->

	</div>


</body>
</html>
<?php } }
    else{

        # The User's IP Address Has Changed.
        # This Might Be A Session Hijacking Attempt

        # Destroy The Session
        $_SESSION = null;
        session_destroy(); # Destroy The Session
        session_unset(); # Unset The Session
        header('location: index');
		exit;

    }
}
else
{
    # The User's Browser Has Changed.
    # This Might Be A Session Hijacking Attempt

    # Destroy The Session
    $_SESSION = null;
    session_destroy(); # Destroy The Session
    session_unset(); # Unset The Session
            header('location: index');
		exit;
    
}?>