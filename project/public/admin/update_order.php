<?php 
 
// Session start and value of session assigning to the variables 
session_start();
$u=$_SESSION['username'];

if($u=="")
{
	// check either user is login or not
	header('location:index');	
}
else
{
include('main_class.php');
	$db = new Database();
	$db->connect();
	
	$id=$db->escapeString($_POST['id']);
	$order=$db->escapeString($_POST['order']);
	$category=$db->escapeString($_POST['category']);
	$compulsory=$db->escapeString($_POST['compulsory']);
	if($order=="" AND $compulsory=="")
	{
		$db->update('category',array('category'=>$category),"id='$id'"); 
	}
	else
	{
		$db->update('category',array('category'=>$category,'order_by'=>$order,'compulsory'=>$compulsory),"id='$id'");
	}
	$res = $db->getResult();
	$count=count($res);
	$log="$u Reordered News Category Of id=$id";
	//date_default_timezone_set("Asia/Kolkata"); 
	$date = date('Y-m-d H:i:s');
	if($count>0)
	{
		header('location:category');
		$db->insert('admin_logs',array('user'=>$u,'log'=>$log,'time'=>$date));  // Table name, column names and respective 		values
	}
	else 
	{
		 echo ("<SCRIPT LANGUAGE='JavaScript'>
				window.alert('Data Not Updated')
				window.location.href='category';
				</SCRIPT>");
	}
}
?>