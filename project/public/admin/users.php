<?php 
// Session start and value of session assigning to the variables 
session_start();
$u=$_SESSION['username'];
$_SESSION['user_agent'] = $_SERVER['HTTP_USER_AGENT']; # Save The User Agent
	$_SESSION['ip_address'] = $_SERVER['REMOTE_ADDR']; # Save The IP Address
		if( $_SESSION['user_agent'] === $_SERVER['HTTP_USER_AGENT'])
		{
		    # The User's Browser Hasn't Changed Since The Last Login
		    if( $_SESSION['ip_address'] === $_SERVER['REMOTE_ADDR'] ){
		    	$u=$_SESSION['username'];
if($u=="")
{
	// check either user is login or not
	header('location:index');	
}
else
{
	
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<title>Near By News App</title>

	<!--=== CSS ===-->

	<!-- Bootstrap -->
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

	<!-- jQuery UI -->
	<!--<link href="plugins/jquery-ui/jquery-ui-1.10.2.custom.css" rel="stylesheet" type="text/css" />-->
	<!--[if lt IE 9]>
		<link rel="stylesheet" type="text/css" href="plugins/jquery-ui/jquery.ui.1.10.2.ie.css"/>
	<![endif]-->

	<!-- Theme -->
	<link href="assets/css/main.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/plugins.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/icons.css" rel="stylesheet" type="text/css" />

	<link rel="stylesheet" href="assets/css/fontawesome/font-awesome.min.css">
	<!--[if IE 7]>
		<link rel="stylesheet" href="assets/css/fontawesome/font-awesome-ie7.min.css">
	<![endif]-->

	<!--[if IE 8]>
		<link href="assets/css/ie8.css" rel="stylesheet" type="text/css" />
	<![endif]-->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>

	<!--=== JavaScript ===-->

	<script type="text/javascript" src="assets/js/libs/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>

	<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/libs/lodash.compat.min.js"></script>

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
		<script src="assets/js/libs/html5shiv.js"></script>
	<![endif]-->

	<!-- Smartphone Touch Events -->
	<script type="text/javascript" src="plugins/touchpunch/jquery.ui.touch-punch.min.js"></script>
	<script type="text/javascript" src="plugins/event.swipe/jquery.event.move.js"></script>
	<script type="text/javascript" src="plugins/event.swipe/jquery.event.swipe.js"></script>

	<!-- General -->
	<script type="text/javascript" src="assets/js/libs/breakpoints.js"></script>
	<script type="text/javascript" src="plugins/respond/respond.min.js"></script> <!-- Polyfill for min/max-width CSS3 Media Queries (only for IE8) -->
	<script type="text/javascript" src="plugins/cookie/jquery.cookie.min.js"></script>
	<script type="text/javascript" src="plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script type="text/javascript" src="plugins/slimscroll/jquery.slimscroll.horizontal.min.js"></script>

	<!-- Page specific plugins -->
	<!-- Charts -->
	<script type="text/javascript" src="plugins/sparkline/jquery.sparkline.min.js"></script>

	<script type="text/javascript" src="plugins/daterangepicker/moment.min.js"></script>
	<script type="text/javascript" src="plugins/daterangepicker/daterangepicker.js"></script>
	<script type="text/javascript" src="plugins/blockui/jquery.blockUI.min.js"></script>

	<!-- Forms -->
	<script type="text/javascript" src="plugins/uniform/jquery.uniform.min.js"></script> <!-- Styled radio and checkboxes -->
	<script type="text/javascript" src="plugins/select2/select2.min.js"></script> <!-- Styled select boxes -->

	<!-- DataTables -->
	<script type="text/javascript" src="plugins/datatables/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="plugins/datatables/tabletools/TableTools.min.js"></script> <!-- optional -->
	<script type="text/javascript" src="plugins/datatables/colvis/ColVis.min.js"></script> <!-- optional -->
	<script type="text/javascript" src="plugins/datatables/DT_bootstrap.js"></script>

	<!-- App -->
	<script type="text/javascript" src="assets/js/app.js"></script>
	<script type="text/javascript" src="assets/js/plugins.js"></script>
	<script type="text/javascript" src="assets/js/plugins.form-components.js"></script>

	<script>
	$(document).ready(function(){
		"use strict";

		App.init(); // Init layout and core plugins
		Plugins.init(); // Init all plugins
		FormComponents.init(); // Init all form-specific plugins
	});
	</script>

	<?php 
	function str_replace_first($from, $to, $subject)
	{
	    $from = '/'.preg_quote($from, '/').'/';
	
	    return preg_replace($from, $to, $subject, 1);
	}
	?>
	<!-- Demo JS -->
	<script type="text/javascript" src="assets/js/custom.js"></script>

	

<style>
	.table-controls
	{
		line-height:250%;
	}
	a{cursor: pointer; }
	ul.pagination {
    display: inline-block;
    padding: 0;
    margin: 0;
}

ul.pagination li {display: inline;}

ul.pagination li a {
    color: black;
    float: left;
    padding: 5px 10px;
    text-decoration: none;
    transition: background-color .3s;
    border: 1px solid #ddd;
}

ul.pagination li a.active {
    background-color: #4CAF50;
    color: white;
    border: 1px solid #4CAF50;
}

ul.pagination li a:hover:not(.active) {background-color: #668EB0;}


</style>
</head>

<body>

	<!--top header page here-->
	<?php include('top_header.php');?>
	<!--end top header page here-->

	<div id="container">
		<div id="sidebar" class="sidebar-fixed">
		<!--side menu content here-->
		<?php include('side_menu.php');?>
		<!-- end side menu content here-->
			<div id="divider" class="resizeable"></div>
		</div>
		<!-- /Sidebar -->

		<div id="content">
			<div class="container">
				<div class="crumbs">
					<ul id="breadcrumbs" class="breadcrumb">
						<li>
							<i class="icon-home"></i>
							<a href="dashboard">Dashboard</a>
						</li>
						<li class="current">
							<a href="#" title="">USERS</a>
						</li>
					</ul>
				</div>
				<?php
	
			include('main_class.php');
			$db = new Database();
			$db->connect();
			$page=$db->escapeString($_GET['page']);
			$val=$db->escapeString($_GET['val']);
			if($val=="")
			{
				$val="10";
			}
			
			if($page!="")
			{
				$index=$page;
			}
			else
			{
				$index=1;
			}
			$p=$index-1;
			
			$skip=$p*$val;
			?>
				<div class="row" style="padding-top: 50px;">
					<h2 style="color:#444; text-align:center; padding: 0px;">Our User's</h2>
					<div class="col-md-12">
						<div class="widget box">
							<div class="widget-header">
							
								<div class="toolbar no-padding">
									
									<div class="btn-group">
										
										<span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span>
									</div>
								</div>
								
							</div>
							<div class="row" style="padding:0px 20px 0px 20px;;">
								<div class="col-md-6" style="padding:10px;">
									<div class="form-group">
									  <div class="input-icon">
									  	
										<div class="col-md-5">
												<select class="form-control" onchange="javascript:location.href = this.value;" style="padding:5px;"> 
												<option value="">RECORDS PER PAGE</option>
											
												 <option value="?page=1&val=5">5</option>
												  <option value="?page=1&val=10">10</option>
												  <option value="?page=1&val=25">25</option>
												  <option value="?page=1&val=50">50</option>
												  <option value="?page=1&val=100">100</option>
												  <option value="?page=1&val=200">200</option>
												  <option value="?page=1&val=all">ALL</option>
												
											</select>
										</div>
									  </div>
									</div>
								</div>
								<div class="col-md-6" style="padding:15px;text-align:right;">
									<div class="form-group">
										<div class="col-md-6"></div>
										<div class="col-md-6"><input type="text" placeholder="Search..." id="search_field" class="form-control"></div>
									</div>
									
								</div>
							</div>
							<div style="overflow-x: auto">
							<div class="widget-content">
								<table class="table table-striped table-bordered table-hover table-checkable" style="width:100%;" id="myTable">
									<thead>
										<tr class="myHead">
											
											<th>Id</th>
											<th>Verify</th>
											<th>Name</th>
											<th>Country</th>
											<th class="hidden-xs">Phone No.</th>
											<th>Image</th>
											<th>Direct Post</th>
											<th>Block</th>
											<th>About</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<?php 
											if($val!="all")
											{
												$db->select('signup','*',NULL,NULL,"id DESC",$val,$skip);
											}
											else
											{
												$db->select('signup','*',NULL,NULL,"id DESC");
											}
											
											$res=$db->getResult();
											$count=count($res);
											for($i=0;$i<$count; $i++) 
											{
    										// output data of each row
    										 	$id=$res[$i]['id'];
       											$name=$res[$i]['name'];
       										 	$country=$res[$i]['country'];
												$phone=$res[$i]['phone'];
												$image=$res[$i]['image'];
												$block1=$res[$i]['comment_block'];
												$block2=$res[$i]['news_block'];
												$post=$res[$i]['post_direct'];
												$verify=$res[$i]['verify'];
										?>
										<tr>
											<td><?php echo $id; ?></td>
											<td style="text-align: center;">
												<?php if($verify!="No")
												{?>
													Verified
												<?php }
												else
												{ ?>
													Not Verified </td>
												<?php } ?>
												
											<td><?php echo $name; ?></td>
											<td><?php echo $country; ?></td>
											<td class="hidden-xs"><?php echo $phone; ?> </td>
											<?php 
											if($image!="")
											{?>
											<td><img style="height: 100px; width: 100px;" src="images/<?php echo $image; ?>" ></td>
											<?php
											}
											else
											{?>
												<td>No Image</td>	
											<?php } ?>
											<td class="align-center">
												<ul class="table-controls">
													<?php if($post=="yes")
													{?>

													<li><a onclick="approve(<?php echo $id; ?>)"><div id="direct_post<?php echo $id; ?>"><span class="label label-success">Direct</span></div></a> </li>
													<?php }
													else
													{?>
													<li><a onclick="approve(<?php echo $id; ?>)"><div id="direct_post<?php echo $id; ?>"><span class="label label-warning">Indirect</span></div></a> </li>
											<?php } ?>
														<!--Allow user to direct posting of news --> 
													<script>
												function approve(val1){
											
												$.ajax({
												 type: "POST",
												 url: "direct.php",
												 data:'id=' + val1, 
												 success: function(data)
												 {
												  $("#direct_post" + val1).html(data);
												 }
												 });
												}
												</script>
												</ul>
											</td>
									
											<td class="align-center">
												<ul class="table-controls">
													<?php if($block2=="active")
													{?>
														<div id="block_user<?php echo $id; ?>"> <a data-toggle="modal" href="#reject<?php echo $id; ?>" ><span class="label label-success">Active</span><br></a></div>
														<!-- <li><a onclick="news_blocks(<?php echo $id; ?>)"><div id="news_posting<?php echo $id; ?>"><span class="label label-warning">Block </span></div> </a> </li> -->
													
													<?php }
													else
													{?>
														<div id="unblock_user<?php echo $id; ?>"><li><a onclick="news_unblock(<?php echo $id; ?>)"><span class="label label-warning">Block</span></a> </li></div>
													<?php } ?>
												
												</ul>
											</td>
											<script>
												function news_unblock(dis){
												
												$.ajax({
												 type: "POST",
												 url: "news_unblock.php",
												 data:'id=' + dis,
												 success: function(data)
												 {
												  $("#unblock_user"+dis).html(data);
												  
												 }
												 });
												}
												</script>
											
											<td class="align-center">
												<ul class="table-controls">
													<li><a href="user_post?id=<?php echo $id;?>"><span class="label label-warning">Posts</span></a> </li><br>
													<li><a href="user_followers?id=<?php echo $id;?>"><span class="label label-info">Followers +</span></a> </li><br>
													<li><a href="user_list_follow?id=<?php echo $id;?>"><span class="label label-success">List Of Following</span></a> </li>
												</ul>
											</td>
											<td class="align-center">
												<span class="btn-group">
													<a data-toggle="modal" href="#update<?php echo $id; ?>"class="bs-tooltip" title="Delete" class="btn btn-xs"><i style="font-size: 20px;" class="icon-pencil"></i></a>
													<a data-toggle="modal" href="#myModal1<?php echo $id; ?>"class="bs-tooltip" title="Delete" class="btn btn-xs"><i style="font-size: 20px;" class="icon-trash"></i></a>
													
												</span>
											</td>
										</tr>
											<div class="modal fade" id="update<?php echo $id; ?>">
											<div class="modal-dialog">
												<div class="modal-content">
													<form action="update_no.php" method="post">
														<div class="row">
															<h3 style="text-align:center;">Edit User Phone No. </h3>
														</div>
													<?php
													
													$db->select('country','*',NULL,"country='$country'",NULL);
													
													
													
													$con=$db->getResult();
													$cn=count($con);
													for($p=0;$p<$cn; $p++) 
													{
		    										// output data of each row
		    											if($country=="Malaysia")
														{
															$code=$con[$p]['phonecode'];
															$replace_phone=str_replace_first($code, '', $phone);
															$trimmed_phone_no= ltrim($replace_phone,"0");
														}
														else
														{
															$code=$con[$p]['phonecode'];	
															$replace_phone=str_replace_first($code, '', $phone);
															$trimmed_phone_no=$replace_phone;
														}
														
													?>
											
													<div class="row" style="padding-top:20px;">
														<div class="col-md-3" style="text-align:center;"><label>Update Phone no.</label></div>
														<div class="col-md-8"><input type="text" required="required" value="<?php echo $trimmed_phone_no; ?>"  name="phone" class="form-control" ></div>
													</div>
													
													<div class="row" style="padding-top:20px;">
														<div class="col-md-3" style="text-align:center;"><label>Verify User</label></div>
														<div class="col-md-8">
															<select name="verify" class="form-control" >
																<option value="">Verify User</option>
																<option value="Yes">Yes</option>
																<option value="No">No</option>
															</select>
														</div>
													</div>
													
														<input type="hidden" value="<?php echo $id; ?>"  name="id">
														<input type="hidden" value="<?php echo $code; ?>"  name="code">
														<input type="hidden" value="<?php echo $index; ?>"  name="page">
														<input type="hidden" value="<?php echo $val; ?>"  name="val">
														
													<div class="modal-footer">
														<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
														<button type="submit" class="btn btn-primary">Submit</a>
													</div>
												<?php } ?>
													</form>

												</div><!-- /.modal-content -->
											</div><!-- /.modal-dialog -->
										</div><!-- /.modal -->
										
											<div class="modal fade" id="reject<?php echo $id; ?>">
											<div class="modal-dialog">
												<div class="modal-content">
													<form>
														<div class="row">
															<h3 style="text-align:center;">Select No. of days to block the user </h3>
														</div>
										
											
											<script>
												function news_blocks(dis){
												var val4=$("#reject_id"+ dis).val();
												$.ajax({
												 type: "POST",
												 url: "news_block.php",
												 data:'id=' + dis + '&days=' + val4, 
												 success: function(data)
												 {
												  $("#block_user"+dis).html(data);
											      $("#reject"+dis).modal('hide');
											      location.reload();
												 }
												 });
												}
												</script>
											<!-- stop -->	
											
													<div class="row" style="padding-top:20px;">
														<div class="col-md-3" style="text-align:center;"><label>Enter Here</label></div>
														<div class="col-md-8"><input type="text" required="required" placeholder="Enter Days Here(only numeric)" name="r_message<?php echo $id; ?>" class="form-control" id="reject_id<?php echo $id; ?>"></div>
													</div>
														
													<div class="modal-footer">
														<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
														<a class="btn btn-primary" onclick="news_blocks(<?php echo $id; ?>)">Submit</a>
													</div>
													</form>

												</div><!-- /.modal-content -->
											</div><!-- /.modal-dialog -->
										</div><!-- /.modal -->
										<!-- Modal dialog -->
											<div class="modal fade" id="myModal1<?php echo $id; ?>">
												<div class="modal-dialog">
													<div class="modal-content">
														<div class="modal-header">
															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
															<h4 class="modal-title">Delete Confirmation</h4>
														</div>
														<div class="modal-body">
															Are You Sure You Want To Delete The Records
														</div>
														<form action="user_delete.php" method="post">
															<input type="hidden" value="<?php echo $id; ?>"  name="id">

															<input type="hidden" value="<?php echo $index; ?>"  name="page">
															<input type="hidden" value="<?php echo $val; ?>"  name="val">
														
														<div class="modal-footer">
															<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
															<button type="submit" class="btn btn-primary" >Yes</button>
														</div>
														</form>
													</div><!-- /.modal-content -->
												</div><!-- /.modal-dialog -->
											</div><!-- /.modal -->

									<?php } ?>
									</tbody>
								</table>
							 <?php
											
								if($val!="all")
								{
								 
									
								?>
								
						<div style="float:right">
								<?php
								$db->select('signup','*',NULL,NULL,NULL);
								$res1=$db->getResult();
								$count1=count($res1);
								$total_pages = ceil($count1 / $val); ?>
								<ul class="pagination">
								<?php 
								if($val<=$count1)
								{
								if($page>1)
								{
									echo "<li><a href='?page=".($page-1)."&val=".($val)."'>PREVIOUS</a></li> "; // Goto 1st page  
								} 
								
								 for ($i=$index; $i<=($index+9); $i++) 
								 
								{
									if($i<=$total_pages)
									{
									
									 	 echo "<li><a href='?page=".$i."&val=".($val)."'>".$i."</a></li>"; 
									
									}
								};
								
								if($page!=$total_pages)
								{
									echo "<li><a href='?page=".($page+1)."&val=".($val)."'>NEXT</a></li> "; // Goto Last page  
								} 
								}
								?>
								</ul>
						</div>
							<?php }  ?>
							</div>
							</div>
						</div>
					</div>
				</div>
				
			</div> <!-- /.row -->

				<!-- /Page Content -->
		</div>
			<!-- /.container -->

	</div>


</body>
<script>
	$('#search_field').on('keyup', function() {
  var value = $(this).val();
  var patt = new RegExp(value, "i");

  $('#myTable').find('tr').each(function() {
    if (!($(this).find('td').text().search(patt) >= 0)) {
      $(this).not('.myHead').hide();
    }
    if (($(this).find('td').text().search(patt) >= 0)) {
      $(this).show();
    }
  });

});
</script>

</html>
<?php 




} }
    else{

        # The User's IP Address Has Changed.
        # This Might Be A Session Hijacking Attempt

        # Destroy The Session
        $_SESSION = null;
        session_destroy(); # Destroy The Session
        session_unset(); # Unset The Session
        header('location: index');
		exit;

    }
}
else
{
    # The User's Browser Has Changed.
    # This Might Be A Session Hijacking Attempt

    # Destroy The Session
    $_SESSION = null;
    session_destroy(); # Destroy The Session
    session_unset(); # Unset The Session
            header('location: index');
		exit;
    
}?>