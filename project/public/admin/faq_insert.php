<?php 
// Session start and value of session assigning to the variables 
session_start();
$u=$_SESSION['username'];
if($u=="")
{
	// check either user is login or not
	header('location:index');	
}
else
{
	include('main_class.php');
	$db = new Database();
	$db->connect();
	$question=$db->escapeString($_POST['question']);
	$answer=$db->escapeString($_POST['answer']);
	$db->insert('faq',array('question'=>$question,'answer'=>$answer,));  // Table name, column names and respective values
	$res = $db->getResult();
	$count=count($res);
	$log="  FAQ Inserted By $u ";
	
	$date = date('Y-m-d H:i:s');
	if($count>0)
	{
		$db->insert('admin_logs',array('user'=>$u,'log'=>$log,'time'=>$date));  // Table name, column names and respective values
	}
	else 
	{
		 echo ("<SCRIPT LANGUAGE='JavaScript'>
				window.alert('Data not Inserted')
				window.location.href='language';
				</SCRIPT>");
	}
}	
?>