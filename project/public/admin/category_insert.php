<?php 
// Session start and value of session assigning to the variables 
session_start();
$u=$_SESSION['username'];
if($u=="")
{
	// check either user is login or not
	header('location:index');	
}
else
{
	include('main_class.php');
	$db = new Database();
	$db->connect();
	
	
function generate_uuid() {
    return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
        mt_rand( 0, 0xffff ),
        mt_rand( 0, 0x0C2f ) | 0x4000,
        mt_rand( 0, 0x3fff ) | 0x8000,
        mt_rand( 0, 0x2Aff ), mt_rand( 0, 0xffD3 ), mt_rand( 0, 0xff4B )
    );

}

 	$cat_id = generate_uuid();
	
	$category=$db->escapeString($_POST['category']);
	$db->select('category','order_by',NULL,NULL,NULL);
	$res1=$db->getResult();
	$count1=count($res1);
	if($count1>0)
	{
		for($i=0;$i<$count1; $i++) 
		{
	    	$c_id[]=$res1[$i]['order_by'];
			
		}
		$m=max($c_id);
	}
	else
	{
		$c_id=0;
	}
	$db->insert('category',array('category'=>$category,'id'=>$cat_id,'order_by'=>$m+1));  // Table name, column names and respective values
	$res = $db->getResult();
	$count=count($res);
	$log="Category Insrted By $u ";
	//date_default_timezone_set("Asia/Kolkata"); 
	$date = date('Y-m-d H:i:s');
	if($count>0)
	{
		$db->insert('admin_logs',array('user'=>$u,'log'=>$log,'time'=>$date));  // Table name, column names and respective 		values
		header('location:category');
	}
	else 
	{
		 echo ("<SCRIPT LANGUAGE='JavaScript'>
				window.alert('Category Not Inserted')
				window.location.href='category';
				</SCRIPT>");
	} 
}	
?>