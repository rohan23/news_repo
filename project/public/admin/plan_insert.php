<?php 
// Session start and value of session assigning to the variables 
session_start();
$u=$_SESSION['username'];
if($u=="")
{
	// check either user is login or not
	header('location:index');	
}
else
{
	include('main_class.php');
	$db = new Database();
	$db->connect();
	$radius=$db->escapeString($_POST['radius']);
	$price=$db->escapeString($_POST['price']);
	
	$db->insert('promotion_price',array('radius'=>$radius,'price'=>$price));  // Table name, column names and respective values
	$res = $db->getResult();
	$count=count($res);
	$log="Promotion Price Add By $u ";
	//date_default_timezone_set("Asia/Kolkata"); 
	$date = date('Y-m-d H:i:s');
	if($count>0)
	{
		$db->insert('admin_logs',array('user'=>$u,'log'=>$log,'time'=>$date));  // Table name, column names and respective 		values
		header('location:add_plan');
	}
	else 
	{
		 echo ("<SCRIPT LANGUAGE='JavaScript'>
				window.alert('Password Does not match')
				window.location.href='add_plan';
				</SCRIPT>");
	}
}	
?>