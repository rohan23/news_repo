<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<title>Near By News App</title>

		<!-- Bootstrap -->
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />


	<!-- Theme -->
	<link href="assets/css/main.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/plugins.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/icons.css" rel="stylesheet" type="text/css" />

	<link rel="stylesheet" href="assets/css/fontawesome/font-awesome.min.css">

	
	<!--=== JavaScript ===-->

	<script type="text/javascript" src="assets/js/libs/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>

	<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/libs/lodash.compat.min.js"></script>



	<!-- Smartphone Touch Events -->
	<script type="text/javascript" src="plugins/touchpunch/jquery.ui.touch-punch.min.js"></script>
	<script type="text/javascript" src="plugins/event.swipe/jquery.event.move.js"></script>
	<script type="text/javascript" src="plugins/event.swipe/jquery.event.swipe.js"></script>

	<!-- General -->
	<script type="text/javascript" src="assets/js/libs/breakpoints.js"></script>
	<script type="text/javascript" src="plugins/respond/respond.min.js"></script> <!-- Polyfill for min/max-width CSS3 Media Queries (only for IE8) -->
	<script type="text/javascript" src="plugins/cookie/jquery.cookie.min.js"></script>
	<script type="text/javascript" src="plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script type="text/javascript" src="plugins/slimscroll/jquery.slimscroll.horizontal.min.js"></script>

	<!-- Page specific plugins -->
	<!-- Charts -->
	<script type="text/javascript" src="plugins/sparkline/jquery.sparkline.min.js"></script>

	<script type="text/javascript" src="plugins/daterangepicker/moment.min.js"></script>
	<script type="text/javascript" src="plugins/daterangepicker/daterangepicker.js"></script>
	<script type="text/javascript" src="plugins/blockui/jquery.blockUI.min.js"></script>

	<!-- Forms -->
	<script type="text/javascript" src="plugins/uniform/jquery.uniform.min.js"></script> <!-- Styled radio and checkboxes -->
	<script type="text/javascript" src="plugins/select2/select2.min.js"></script> <!-- Styled select boxes -->

	<!-- DataTables -->
	<script type="text/javascript" src="plugins/datatables/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="plugins/datatables/tabletools/TableTools.min.js"></script> <!-- optional -->
	<script type="text/javascript" src="plugins/datatables/colvis/ColVis.min.js"></script> <!-- optional -->
	<script type="text/javascript" src="plugins/datatables/DT_bootstrap.js"></script>

	<!-- App -->
	<script type="text/javascript" src="assets/js/app.js"></script>
	<script type="text/javascript" src="assets/js/plugins.js"></script>
	<script type="text/javascript" src="assets/js/plugins.form-components.js"></script>
	<script type="text/javascript" src="plugins/bootstrap-multiselect/bootstrap-multiselect.min.js"></script>
	
	 <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAXNYV2WAo_qX6kbNNGMCC_vFFSD1onJ5A&libraries=places&callback=initAutocomplete"
         async defer></script>
	<!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAXNYV2WAo_qX6kbNNGMCC_vFFSD1onJ5A&callback=initMap"></script>-->
	<!--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC6v5-2uaq_wusHDktM9ILcqIrlPtnZgEk&sensor=false"></script>-->
	<script>
	$(document).ready(function(){
		"use strict";

		App.init(); // Init layout and core plugins
		Plugins.init(); // Init all plugins
		FormComponents.init(); // Init all form-specific plugins
	});
	</script>
	
	
	<script>
      // This example adds a search box to a map, using the Google Place Autocomplete
      // feature. People can enter geographical searches. The search box will return a
      // pick list containing a mix of places and predicted search terms.

      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      function initAutocomplete() {
        var map = new google.maps.Map(document.getElementById('gmap'), {
          center: {lat: 4.2105, lng: 101.9758},
          zoom: 7,
          mapTypeId: 'roadmap'
        });

        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        
        google.maps.event.addListener(map, "click", function(event) {
                // get lat/lon of click
                var clickLat = event.latLng.lat();
                var clickLon = event.latLng.lng();
                document.getElementById('lat').value = clickLat;
         document.getElementById('lon').value = clickLon;
         
                
				for(i=0; i<markers.length; i++){
			        markers[i].setMap(null);
			    }
				markers = [];
	            markers.push(new google.maps.Marker({
              		map: map,
              		position: new google.maps.LatLng(clickLat,clickLon)
              		
            	}));
});

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }

          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location
            }));

            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });
      }

    </script>
 
     <style>
 div#gmap {
        width: 100%;
        height: 500px;
        border:double;
        
        
 }
 #c
 {
 	text-align:left;
 }
 .controls {
        margin-top: 10px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 300px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }

      .pac-container {
        font-family: Roboto;
      }

      #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
      }

      #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }
      #target {
        width: 345px;
      }
 
 </style>
 
 <script>
	function getprice(){
	var val2 = document.getElementById("radius").value;
	$.ajax({
	 type: "POST",
	 url: "radius.php",
	 data:'radius=' + val2, 
	 success: function(data)
	 {
	  $("#price").html(data);
	 }
	});
	
	}
 </script>
	
	
	<script>
	function getdate(){
	var v = $("input[name='sdate']").val();
	$.ajax({
	 type: "POST",
	 url: "enddate.php",
	 data:'sdate=' + v, 
	 success: function(data)
	 {
	  $("#edate").html(data);
	 }
	});
	
	}
 </script>

	
	<!-- Demo JS -->
		<script type="text/javascript" src="assets/js/custom.js"></script>
		<script type="text/javascript" src="assets/js/demo/ui_general.js"></script>
	<script type="text/javascript" src="assets/js/demo/google_maps.js"></script>
</head>

<body onLoad="initGeolocation();">
	
	<!--top header page here-->
	<?php include('top_header.php');?>
	<!--end top header page here-->
	
	<div id="container">
		<div id="sidebar" class="sidebar-fixed">
		<!--side menu content here-->
		<?php include('side_menu.php');?>
		<!-- end side menu content here-->
			<div id="divider" class="resizeable"></div>
		</div>

		<div id="content">
			<div class="container">
				<div class="crumbs">
					<ul id="breadcrumbs" class="breadcrumb">
						<li>
							<i class="icon-home"></i>
							<a href="dashboard">Dashboard</a>
						</li>
						<li class="current">
							<a href="#" title="">PROMOTION NEWS</a>
						</li>
					</ul>
				</div>
				<!-- Breadcrumbs line -->
			
				<!-- /Breadcrumbs line -->

				

				<!--=== Page Content ===-->
				<div class="row" style="padding-top: 50px;">
					<!--=== Basic ===-->
						<div class="row" style="padding-top: 60px;">
					<div class="col-md-1"></div>
					<div class="col-md-10">
						<div class="widget box">
							<div class="widget-header">
								<h3 style="text-align: center; font-weight: 900;"> Enter Promotions </h3>
							</div>
							<div class="widget-content">
								<form class="form-horizontal row-border"  action="promotion_insert.php" method="post" enctype="multipart/form-data">
									
									<div class="form-group">
										<label class="col-md-3 control-label" id="c">Category<span class="required"></span></label>
										<div class="col-md-9">
											<select  name="category" class="form-control required has-success">
											<option value="">Select Category</option>
											<?php
											include('main_class.php');
											$db = new Database();
											$db->connect();
											$db->select('promotion_category','*',NULL,NULL,"id DESC");
											$res=$db->getResult();
											$count=count($res);
											for($i=0;$i<$count; $i++) 
											{
    										// output data of each row
												$category=$res[$i]['category'];
												$id=$res[$i]['id'];
											
											?>
												
												<option value="<?php echo $id; ?>"> <?php echo $category; ?> </option>
												<?php } ?>
											</select>
										</div>
									</div>
								
									<div class="form-group">
										<label class="col-md-3 control-label" id="c">Promotion Title</label>
										<div class="col-md-9"><input type="text" name="title" placeholder="Enter Promotional Title" class="form-control"></div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label" id="c">Promotion Description</label>
										<div class="col-md-9"><textarea name="text" rows="5" placeholder="Enter Promotional Text" class="form-control"></textarea></div>
									</div>
									
									<div class="form-group">
										<label class="col-md-3 control-label" id="c">Radius<span class="required"></span></label>
										<div class="col-md-9">
											<select  name="radius" id="radius" onchange="getprice()" class="form-control required has-success">
											<option value="">Select Radius</option>
											<?php
											
											$db->select('promotion_price','*',NULL,NULL,"radius ASC");
											$res1=$db->getResult();
											$count1=count($res1);
											for($j=0;$j<$count1; $j++) 
											{
    										// output data of each row
												$radius=$res1[$j]['radius'];
											
											?>
												
												<option value="<?php echo $radius; ?>"> <?php echo $radius; ?> </option>
												<?php } ?>
											</select>
										</div>
									</div>
									
									<div id="price">
									<div class="form-group">
										<label class="col-md-3 control-label" id="c">Price</label>
										<div class="col-md-9"><input type="text" name="price" class="form-control"></div>
									</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label" id="c">Email</label>
										<div class="col-md-9"><input type="text" name="email" placeholder="Enter Email Here" class="form-control"></div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label" id="c">Phone No</label>
										<div class="col-md-9"><input type="text" name="phone" placeholder="Enter Phone No Here" class="form-control"></div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label" id="c">Commencement date</label>
										<div class="col-md-9"><input type="text" name="sdate" placeholder="Enter Start Date Date" onchange="getdate()" class="form-control datepicker"></div>
									</div>
									<div id="edate">
										<div class="form-group">
											<label class="col-md-3 control-label" id="c">End date</label>
											<div class="col-md-9"><input type="text" name="edate" placeholder="Enter End Date" class="form-control datepicker"></div>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label" id="c">Image1</label>
										<div class="col-md-9"><input type="file" name="image1" class="form-control" required="required"></div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label" id="c">Image2</label>
										<div class="col-md-9"><input type="file" name="image2" class="form-control"></div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label" id="c">Image3</label>
										<div class="col-md-9"><input type="file" name="image3" class="form-control"></div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label" id="c">Image4</label>
										<div class="col-md-9"><input type="file" name="image4" class="form-control"></div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label" id="c">Image5</label>
										<div class="col-md-9"><input type="file" name="image5" class="form-control"></div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label" id="c">Image6</label>
										<div class="col-md-9"><input type="file" name="image6" class="form-control"></div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label" id="c">Image7</label>
										<div class="col-md-9"><input type="file" name="image7" class="form-control"></div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label" id="c">Image8</label>
										<div class="col-md-9"><input type="file" name="image8" class="form-control"></div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label" id="c">Image9</label>
										<div class="col-md-9"><input type="file" name="image9" class="form-control"></div>
									</div>
									<input type="hidden" readonly="readonly" id='lat' name="lat" placeholder="Select Your Latitude From Map" class="form-control">
									<input type="hidden" id='lon' name="log" readonly="readonly" placeholder="Select Your Longitude From Map" class="form-control">
									
							
				</div>
					<div class="col-md-12">
						<div class="widget">

								
								<!-- /Toolbar -->
							
							<div class="widget-content">
								 <input id="pac-input" class="controls" type="text" placeholder="Search Box">
    							<div id="gmap"></div>
							</div>
						</div>
					</div>
				<!--	<input type="hidden" id='lat' name="lat">
					<input type="hidden" id='lon' name="log"> -->
					
									<div class="form-actions">
										<input type="submit" value="Submit" class="btn btn-primary pull-right">
									</div>
					</form>
							</div>
						</div>
					
					</div>
				</div>


				
				<!-- /Page Content -->
			</div>
			<!-- /.container -->

		</div>
	</div>

</body>
</html>