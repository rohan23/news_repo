
<!DOCTYPE html>

<script>
$("#search").on("keyup", function() {
    var value = $(this).val();

    $("table tr").each(function(index) {
        if (index !== 0) {

            $row = $(this);

            var id = $row.find("td:all").text();

            if (id.indexOf(value) !== 0) {
                $row.hide();
            }
            else {
                $row.show();
            }
        }
    });
});
	</script>
<body>
	<?php
	
			include('main_class.php');
			$db = new Database();
			$db->connect();
			$page=$db->escapeString($_GET['page']);
			$val=$db->escapeString($_GET['val']);
			if($val=="")
			{
				$val="10";
			}
			
			if($page!="")
			{
				$index=$page;
			}
			else
			{
				$index=1;
			}
			$p=$index-1;
			
			$skip=$p*$val;
			?>
							
							
								<select onchange="javascript:location.href = this.value;">
									  <option value="">NO OF RECORDS PER PAGE</option>
									  <option value="?page=1&val=5">5</option>
									  <option value="?page=1&val=10">10</option>
									  <option value="?page=1&val=25">25</option>
									  <option value="?page=1&val=50">50</option>
									  <option value="?page=1&val=100">100</option>
									  <option value="?page=1&val=200">200</option>
									  <option value="?page=1&val=all">ALL</option>
								</select>
								<input type="text" placeholder="Search..." id="search_field">
								<!-- <input type="text" id="search" placeholder="live search"></input> -->
								<!-- <input type="text" onkeyup="filter()" id="game"> -->
								<table border="1px solid #000;" style="width:100%;" id="myTable">
									<thead>
										<tr style="font-size:19px;" class="myHead">
											<!-- <th>Id</th> -->
											<th>Categories</th>
											<th>Order</th>
											<th>Is Compulsory</th>
											
										</tr>
									</thead>
									<tbody>
										<?php 
											
											if($val!="all")
											{
												$db->select('category','*',NULL,NULL,NULL,$val,$skip);
											}
											else
											{
												$db->select('category','*',NULL,NULL,NULL);
											}
											$res=$db->getResult();
											$count=count($res);
											for($i=0;$i<$count; $i++) 
											{
    										// output data of each row
    										 	$id=$res[$i]['id'];
       											$category=$res[$i]['category'];
												$order=$res[$i]['order_by'];
												$cat_id=$res[$i]['cat_id'];
												$compulsory=$res[$i]['compulsory'];
       										 	
										?>
										<tr>
											<!-- <td><?php echo $id; ?></td> -->
											<td><?php echo $category; ?></td>
											
											<td><?php echo $order; ?></td>
											<?php
											if($compulsory=="yes")
											{?>
											<td>Compulsory</td>
											<?php 
											}
											else 
											{?>
												<td>Not Compulsory</td>
											<?php } ?>
											
											
											
										</tr>
									
										
											
										
									<?php } ?>
									</tbody>
								</table>
								<?php
								if($val!="all")
								{ ?>
								
				<div style="float:right">
								<?php
								$db->select('category','*',NULL,NULL,NULL);
								$res1=$db->getResult();
								$count1=count($res1);
								$total_pages = ceil($count1 / $val);
								
								if($page>1)
								{
									echo "<a href='pagination.php?page=".($page-1)."&val=".($val)."'>PREVIOUS</a> "; // Goto 1st page  
								}
								
								
								for ($i=1; $i<$total_pages; $i++) 
								{ 
								     echo "<a href='pagination.php?page=".$i."&val=".($val)."'>".$i."</a> "; 
								}; 
								if($page!=$total_pages)
								{
									echo "<a href='pagination.php?page=".($page+1)."&val=".($val)."'>NEXT</a> "; // Goto Last page  
								}
								?>
								
				</div>
				<?php } ?>
			

</body>
<script src="//code.jquery.com/jquery.min.js"></script>
<script>
	$('#search_field').on('keyup', function() {
  var value = $(this).val();
  var patt = new RegExp(value, "i");

  $('#myTable').find('tr').each(function() {
    if (!($(this).find('td').text().search(patt) >= 0)) {
      $(this).not('.myHead').hide();
    }
    if (($(this).find('td').text().search(patt) >= 0)) {
      $(this).show();
    }
  });

});
</script>
</html>
