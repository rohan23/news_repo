<?php 
// Session start and value of session assigning to the variables 
session_start();
$u=$_SESSION['username'];
$_SESSION['user_agent'] = $_SERVER['HTTP_USER_AGENT']; # Save The User Agent
	$_SESSION['ip_address'] = $_SERVER['REMOTE_ADDR']; # Save The IP Address
		if( $_SESSION['user_agent'] === $_SERVER['HTTP_USER_AGENT'])
		{
		    # The User's Browser Hasn't Changed Since The Last Login
		    if( $_SESSION['ip_address'] === $_SERVER['REMOTE_ADDR'] ){
		    	$u=$_SESSION['username'];
if($u=="")
{
	// check either user is login or not
	header('location:index');	
}
else
{
	
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<title>Near By News App</title>

	<!--=== CSS ===-->

	<!-- Bootstrap -->
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

	<!-- jQuery UI -->
	<!--<link href="plugins/jquery-ui/jquery-ui-1.10.2.custom.css" rel="stylesheet" type="text/css" />-->
	<!--[if lt IE 9]>
		<link rel="stylesheet" type="text/css" href="plugins/jquery-ui/jquery.ui.1.10.2.ie.css"/>
	<![endif]-->

	<!-- Theme -->
	<link href="assets/css/main.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/plugins.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/icons.css" rel="stylesheet" type="text/css" />

	<link rel="stylesheet" href="assets/css/fontawesome/font-awesome.min.css">
	<!--[if IE 7]>
		<link rel="stylesheet" href="assets/css/fontawesome/font-awesome-ie7.min.css">
	<![endif]-->

	<!--[if IE 8]>
		<link href="assets/css/ie8.css" rel="stylesheet" type="text/css" />
	<![endif]-->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>

	<!--=== JavaScript ===-->

	<script type="text/javascript" src="assets/js/libs/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>

	<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/libs/lodash.compat.min.js"></script>

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
		<script src="assets/js/libs/html5shiv.js"></script>
	<![endif]-->

	<!-- Smartphone Touch Events -->
	<script type="text/javascript" src="plugins/touchpunch/jquery.ui.touch-punch.min.js"></script>
	<script type="text/javascript" src="plugins/event.swipe/jquery.event.move.js"></script>
	<script type="text/javascript" src="plugins/event.swipe/jquery.event.swipe.js"></script>

	<!-- General -->
	<script type="text/javascript" src="assets/js/libs/breakpoints.js"></script>
	<script type="text/javascript" src="plugins/respond/respond.min.js"></script> <!-- Polyfill for min/max-width CSS3 Media Queries (only for IE8) -->
	<script type="text/javascript" src="plugins/cookie/jquery.cookie.min.js"></script>
	<script type="text/javascript" src="plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script type="text/javascript" src="plugins/slimscroll/jquery.slimscroll.horizontal.min.js"></script>

	<!-- Page specific plugins -->
	<!-- Charts -->
	<script type="text/javascript" src="plugins/sparkline/jquery.sparkline.min.js"></script>

	<script type="text/javascript" src="plugins/daterangepicker/moment.min.js"></script>
	<script type="text/javascript" src="plugins/daterangepicker/daterangepicker.js"></script>
	<script type="text/javascript" src="plugins/blockui/jquery.blockUI.min.js"></script>

	<!-- Forms -->
	<script type="text/javascript" src="plugins/uniform/jquery.uniform.min.js"></script> <!-- Styled radio and checkboxes -->
	<script type="text/javascript" src="plugins/select2/select2.min.js"></script> <!-- Styled select boxes -->

	<!-- DataTables -->
	<script type="text/javascript" src="plugins/datatables/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="plugins/datatables/tabletools/TableTools.min.js"></script> <!-- optional -->
	<script type="text/javascript" src="plugins/datatables/colvis/ColVis.min.js"></script> <!-- optional -->
	<script type="text/javascript" src="plugins/datatables/DT_bootstrap.js"></script>

	<!-- App -->
	<script type="text/javascript" src="assets/js/app.js"></script>
	<script type="text/javascript" src="assets/js/plugins.js"></script>
	<script type="text/javascript" src="assets/js/plugins.form-components.js"></script>

	<script>
	$(document).ready(function(){
		"use strict";

		App.init(); // Init layout and core plugins
		Plugins.init(); // Init all plugins
		FormComponents.init(); // Init all form-specific plugins
	});
	</script>


	<!-- Demo JS -->
	<script type="text/javascript" src="assets/js/custom.js"></script>

	

<style>
	.table-controls
	{
		line-height:250%;
	}
	a{cursor: pointer; }
	ul.pagination {
    display: inline-block;
    padding: 0;
    margin: 0;
}

ul.pagination li {display: inline;}

ul.pagination li a {
    color: black;
    float: left;
    padding: 5px 10px;
    text-decoration: none;
    transition: background-color .3s;
    border: 1px solid #ddd;
}

ul.pagination li a.active {
    background-color: #4CAF50;
    color: white;
    border: 1px solid #4CAF50;
}

ul.pagination li a:hover:not(.active) {background-color: #668EB0;}


</style>
</head>

<body>

	<!--top header page here-->
	<?php include('top_header.php');?>
	<!--end top header page here-->

	<div id="container">
		<div id="sidebar" class="sidebar-fixed">
		<!--side menu content here-->
		<?php include('side_menu.php');?>
		<!-- end side menu content here-->
			<div id="divider" class="resizeable"></div>
		</div>
		<!-- /Sidebar -->

		<div id="content">
			<div class="container">
				<div class="crumbs">
					<ul id="breadcrumbs" class="breadcrumb">
						<li>
							<i class="icon-home"></i>
							<a href="dashboard">Dashboard</a>
						</li>
						<li class="current">
							<a href="#" title="">ADMINS</a>
						</li>
					</ul>
				</div>
				<?php
	
			include('main_class.php');
			$db = new Database();
			$db->connect();
			$page=$db->escapeString($_GET['page']);
			$val=$db->escapeString($_GET['val']);
			if($val=="")
			{
				$val="10";
			}
			
			if($page!="")
			{
				$index=$page;
			}
			else
			{
				$index=1;
			}
			$p=$index-1;
			
			$skip=$p*$val;
			?>
				<div class="row" style="padding-top: 50px;">
					<h2 style="color:#444; text-align:center; padding: 0px;">OUR SUPER ADMINS</h2>
					<div class="col-md-12">
						<div class="widget box">
							<div class="widget-header">
							
								<div class="toolbar no-padding">
									
									<div class="btn-group">
										
										<span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span>
									</div>
								</div>
								
							</div>
							<div class="row" style="padding:0px 20px 0px 20px;;">
								<div class="col-md-6" style="padding:10px;">
									<div class="form-group">
									  <div class="input-icon">
									  	
										<div class="col-md-5">
												<select class="form-control" onchange="javascript:location.href = this.value;" style="padding:5px;"> 
												<option value="">RECORDS PER PAGE</option>
											
												 <option value="?page=1&val=5">5</option>
												  <option value="?page=1&val=10">10</option>
												  <option value="?page=1&val=25">25</option>
												  <option value="?page=1&val=50">50</option>
												  <option value="?page=1&val=100">100</option>
												  <option value="?page=1&val=200">200</option>
												  <option value="?page=1&val=all">ALL</option>
												
											</select>
										</div>
									  </div>
									</div>
								</div>
								<div class="col-md-6" style="padding:15px;text-align:right;">
									<div class="form-group">
										<div class="col-md-6"></div>
										<div class="col-md-6"><input type="text" placeholder="Search..." id="search_field" class="form-control"></div>
									</div>
									
								</div>
							</div>
							<div style="overflow-x: auto">
							<div class="widget-content">
								<table class="table table-striped table-bordered table-hover table-checkable" style="width:100%;" id="myTable">
									<thead>
										<tr class="myHead">
											
										<th>Id</th>
											<th>Name</th>
											<th>Phone No.</th>
											<th class="hidden-xs">Role</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<?php 
											if($val!="all")
											{
												
												$db->select('login','*',NULL,"super='no' AND role='superadmin'",NULL,$val,$skip);
											}
											else
											{
												$db->select('login','*',NULL,"super='no' AND role='superadmin'",NULL);
											}
											
											
											$res=$db->getResult();
											$count=count($res);
											for($i=0;$i<$count; $i++) 
											{
    										// output data of each row
    										 	$id=$res[$i]['id'];
       											$name=$res[$i]['name'];
       										 	$role=$res[$i]['role'];
												$phone=$res[$i]['phone'];
												$super=$res[$i]['super'];
										?>
										<tr>
											<td><?php echo $id; ?></td>
											<td><?php echo $name; ?></td>
											<td><?php echo $phone; ?></td>
											<td> 
												<?php if($role=="superadmin")
												{?>
													Super Admin
												<?php }
												else 
												{?>
													Admin
												<?php } ?>
											 </td>
											
											<td class="align-center">
												<span class="btn-group">
													 <a data-toggle="modal" href="#myModaldd<?php echo $id; ?>"class="bs-tooltip" title="Click To Edit" class="btn btn-xs">Edit</a>&nbsp; &nbsp; &nbsp; 
													<a data-toggle="modal" href="#myModal<?php echo $id; ?>"class="bs-tooltip" title="Delete" class="btn btn-xs"><i style="font-size: 20px;" class="icon-trash"></i></a>
													
												</span>
											</td>
											
										</tr>
											<div class="modal fade" id="myModaldd<?php echo $id; ?>">
												<div class="modal-dialog">
													<div class="modal-content">
														<h2 style="text-align: center;">Edit Your Credentials</h2>
														<form action="update_admin" method="post">
															<input type="hidden" class="form-control" value="<?php echo $id; ?>" name="id">
															<div class="row" style="padding:10px;">
																<div class="col-md-3"> <label>Name</label></div>
																<div class="col-md-8"><input type="text" class="form-control" value="<?php echo $name; ?>" name="name"></div>
															</div>
															<div class="row" style="padding:10px;">
																<div class="col-md-3"> <label>Role</label></div>
																<div class="col-md-8">
																<select name="role" class="form-control">
																	<option value="admin">Super Admin</option>
																	<option value="subadmin">Admin</option>
																</select>
															<input type="hidden" value="<?php echo $index; ?>"  name="page">
															<input type="hidden" value="<?php echo $val; ?>"  name="val">
															</div>
															</div>
															
														<div class="modal-footer">
															<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
															<button type="submit"  class="btn btn-primary" >Yes</button>
														</div>
														</form>
													</div>
												</div>
											</div><!-- /.modal -->
										<!-- Modal dialog -->
										
											<div class="modal fade" id="myModal<?php echo $id; ?>">
												<div class="modal-dialog">
													<div class="modal-content">
														<div class="modal-header">
															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
															<h4 class="modal-title">Delete Confirmation</h4>
														</div>
														<div class="modal-body">
															Are You Sure You Want To Delete The Records
														</div>
														<form action="admin_delete.php" method="post">
															<input type="hidden" value="<?php echo $id; ?>"  name="id">

															<input type="hidden" value="<?php echo $index; ?>"  name="page">
															<input type="hidden" value="<?php echo $val; ?>"  name="val">
														<div class="modal-footer">
															<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
															<button type="submit" class="btn btn-primary" >Yes</button>
														</div>
														</form>
													</div><!-- /.modal-content -->
												</div><!-- /.modal-dialog -->
											</div><!-- /.modal -->
									<?php } ?>
									</tbody>
								</table>
											<?php
											
								if($val!="all")
								{
								 
									
								?>
								
						<div style="float:right">
								<?php
								$db->select('login','*',NULL,"super='no' AND role='superadmin'",NULL);
								$res1=$db->getResult();
								$count1=count($res1);
								$total_pages = ceil($count1 / $val); ?>
								<ul class="pagination">
								<?php 	if($val<=$count1)
									{
								if($page>1)
								{
									echo "<li><a href='?page=".($page-1)."&val=".($val)."'>PREVIOUS</a></li> "; // Goto 1st page  
								} 
								
								 for ($i=$index; $i<=($index+9); $i++) 
								 
								{
									if($i<=$total_pages)
									{
									 if($page==$i)
									{
										echo "<li "."class="."current"."><a href='?page=".$i."&val=".($val)."'>".$i."</a></li>"; 
									}
									 else
									 {
									 	 echo "<li><a href='?page=".$i."&val=".($val)."'>".$i."</a></li>"; 
									 }
									}
								};
								
								if($page!=$total_pages)
								{
									echo "<li><a href='?page=".($page+1)."&val=".($val)."'>NEXT</a></li> "; // Goto Last page  
								}
								}
								?>
								</ul>
						</div>
							<?php  } ?>
							</div>
							</div>
						</div>
					</div>
				</div>
				
			</div> <!-- /.row -->

				<!-- /Page Content -->
		</div>
			<!-- /.container -->

	</div>


</body>
<script>
	$('#search_field').on('keyup', function() {
  var value = $(this).val();
  var patt = new RegExp(value, "i");

  $('#myTable').find('tr').each(function() {
    if (!($(this).find('td').text().search(patt) >= 0)) {
      $(this).not('.myHead').hide();
    }
    if (($(this).find('td').text().search(patt) >= 0)) {
      $(this).show();
    }
  });

});
</script>
</html>
<?php } }
    else{

        # The User's IP Address Has Changed.
        # This Might Be A Session Hijacking Attempt

        # Destroy The Session
        $_SESSION = null;
        session_destroy(); # Destroy The Session
        session_unset(); # Unset The Session
        header('location: index');
		exit;

    }
}
else
{
    # The User's Browser Has Changed.
    # This Might Be A Session Hijacking Attempt

    # Destroy The Session
    $_SESSION = null;
    session_destroy(); # Destroy The Session
    session_unset(); # Unset The Session
            header('location: index');
		exit;
    
}?>