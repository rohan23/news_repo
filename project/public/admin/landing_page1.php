<?php 
include('conn.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<title>Near By News App</title>

		<!-- Bootstrap -->
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />


	<!-- Theme -->
	<link href="assets/css/main.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/plugins.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/icons.css" rel="stylesheet" type="text/css" />

	<link rel="stylesheet" href="assets/css/fontawesome/font-awesome.min.css">

	
	<!--=== JavaScript ===-->

	<script type="text/javascript" src="assets/js/libs/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>

	<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/libs/lodash.compat.min.js"></script>



	<!-- Smartphone Touch Events -->
	<script type="text/javascript" src="plugins/touchpunch/jquery.ui.touch-punch.min.js"></script>
	<script type="text/javascript" src="plugins/event.swipe/jquery.event.move.js"></script>
	<script type="text/javascript" src="plugins/event.swipe/jquery.event.swipe.js"></script>

	<!-- General -->
	<script type="text/javascript" src="assets/js/libs/breakpoints.js"></script>
	<script type="text/javascript" src="plugins/respond/respond.min.js"></script> <!-- Polyfill for min/max-width CSS3 Media Queries (only for IE8) -->
	<script type="text/javascript" src="plugins/cookie/jquery.cookie.min.js"></script>
	<script type="text/javascript" src="plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script type="text/javascript" src="plugins/slimscroll/jquery.slimscroll.horizontal.min.js"></script>

	<!-- Page specific plugins -->
	<!-- Charts -->
	<script type="text/javascript" src="plugins/sparkline/jquery.sparkline.min.js"></script>

	<script type="text/javascript" src="plugins/daterangepicker/moment.min.js"></script>
	<script type="text/javascript" src="plugins/daterangepicker/daterangepicker.js"></script>
	<script type="text/javascript" src="plugins/blockui/jquery.blockUI.min.js"></script>

	<!-- Forms -->
	<script type="text/javascript" src="plugins/uniform/jquery.uniform.min.js"></script> <!-- Styled radio and checkboxes -->
	<script type="text/javascript" src="plugins/select2/select2.min.js"></script> <!-- Styled select boxes -->

	<!-- DataTables -->
	<script type="text/javascript" src="plugins/datatables/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="plugins/datatables/tabletools/TableTools.min.js"></script> <!-- optional -->
	<script type="text/javascript" src="plugins/datatables/colvis/ColVis.min.js"></script> <!-- optional -->
	<script type="text/javascript" src="plugins/datatables/DT_bootstrap.js"></script>

	<!-- App -->
	<script type="text/javascript" src="assets/js/app.js"></script>
	<script type="text/javascript" src="assets/js/plugins.js"></script>
	<script type="text/javascript" src="assets/js/plugins.form-components.js"></script>
	<script type="text/javascript" src="plugins/bootstrap-multiselect/bootstrap-multiselect.min.js"></script>
	<script>
	$(document).ready(function(){
		"use strict";

		App.init(); // Init layout and core plugins
		Plugins.init(); // Init all plugins
		FormComponents.init(); // Init all form-specific plugins
	});
	</script>
	<!-- Demo JS -->
		<script type="text/javascript" src="assets/js/custom.js"></script>
		<script type="text/javascript" src="assets/js/demo/ui_general.js"></script>
	<script type="text/javascript" src="assets/js/demo/google_maps.js"></script>
</head>

<body >
	
	<!--top header page here-->
	<?php include('top_header.php');?>
	<!--end top header page here-->
	
	<div id="container">
		<div id="sidebar" class="sidebar-fixed">
		<!--side menu content here-->
		<?php include('side_menu.php');?>
		<!-- end side menu content here-->
			<div id="divider" class="resizeable"></div>
		</div>

		<div id="content">
			<div class="container">
				<div class="crumbs">
					<ul id="breadcrumbs" class="breadcrumb">
						<li>
							<i class="icon-home"></i>
							<a href="dashboard">Dashboard</a>
						</li>
						<li class="current">
							<a href="#" title="">Landing Page-1</a>
						</li>
					</ul>
				</div>
				<!-- Breadcrumbs line -->
			
				<!-- /Breadcrumbs line -->

				

				<!--=== Page Content ===-->
				<div class="row" style="padding-top: 50px;">
					<center><img src="page1.png" style="align: center;" height="400px" width="700px"/></center>
					<!--=== Basic ===-->
						<div class="row" style="padding-top: 60px;">
					<div class="col-md-1"></div>
					<div class="col-md-10">
						<div class="widget box">
							<div class="widget-header">
								<h3 style="text-align: center; font-weight: 900;"> Landing Page-1 </h3>
							</div>
							<div class="widget-content">
								<form class="form-horizontal row-border"  action="landing_page1_query.php" method="post" enctype="multipart/form-data">
									<?php
									include('main_class.php');
									$db = new Database();
									$db->connect();
									$db->select('landing1','*',NULL,"id='1'",NULL);
									$res=$db->getResult();
									$count=count($res);
									for($i=0;$i<$count; $i++) 
									{
    									$id=$res[$i]['id'];
										$heading=$res[$i]['heading'];
										$description=$res[$i]['description'];
										$image1=$res[$i]['sub_image1'];
										$image2=$res[$i]['sub_image2'];
									?>
									<input type="hidden" value="<?php echo $id; ?>" name="id" />
									<div class="form-group">
										<label class="col-md-3 control-label" id="c">Heading</label>
										<div class="col-md-9"><input type="text" name="heading" placeholder="Enter Heading" class="form-control" value="<?php echo $heading; ?>" ></div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label" id="c"> Description</label>
										<div class="col-md-9"><textarea name="description" rows="5" placeholder="Enter Promotional Text" class="form-control"><?php echo $description; ?></textarea></div>
									</div>
									
									<div class="form-group">
										<label class="col-md-3 control-label" id="c">Main Image</label>
										<div class="col-md-9"><input type="file" name="image1" class="form-control"></div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label" id="c">Title Image</label>
										<div class="col-md-9"><input type="file" name="image2" class="form-control"></div>
									</div>
									<?php } ?>
									<div style="align: center;" class="form-group">
										<div class="col-md-10"></div>
										<div class="col-md-2"><input type="submit" value="Submit" class="btn btn-primary pull-right"></div>
									</div>
								</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /Page Content -->
			</div>
			<!-- /.container -->

		</div>
	</div>

</body>
</html>