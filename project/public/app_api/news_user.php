
<?php
include('conn.php');
require_once __DIR__.'/server.php';
// Handle a request to a resource and authenticate the access token
if (!$server->verifyResourceRequest(OAuth2\Request::createFromGlobals()))
{
	$server->getResponse()->send();
	die;
}

$news_id=$_POST['news_id'];
$user_id=$_POST['user_id'];
	if($news_id!="")
	{
		$view_by=$_POST['view_by'];
		$latitude=$_POST['latitude'];
		$longitude=$_POST['longitude'];
		$category_id=$_POST['category_id'];
		$type="news";
		$date1=date("Y-m-d");
		$time1=date("H:i:s");
		if($view_by=="web")
		{
			$view=mysqli_query($conn,"INSERT INTO `views_data`(`id`, `type`,`category_id`, `news_id`, `user_id`, `user_lat`, `user_long`, `view_web`, `view_date`, `view_time`) VALUES (NULL,'$type','$category_id','$news_id','$user_id','$latitude','$longitude','1','$date1','$time1')");
		}
		if($view_by=="android")
		{
			$view=mysqli_query($conn,"INSERT INTO `views_data`(`id`, `type`,`category_id`, `news_id`, `user_id`, `user_lat`, `user_long`, `view_android`, `view_date`, `view_time`) VALUES (NULL,'$type','$category_id','$news_id','$user_id','$latitude','$longitude','1','$date1','$time1')");
		}
		if($view_by=="ios")
		{
			$view=mysqli_query($conn,"INSERT INTO `views_data`(`id`, `type`,`category_id`, `news_id`, `user_id`, `user_lat`, `user_long`, `view_web`, `view_date`, `view_time`) VALUES (NULL,'$type','$category_id','$news_id','$user_id','$latitude','$longitude','1','$date1','$time1')");
		}
	}

if(isset($_POST['cache_date_time']))
{
	$cache_date_time=$_POST['cache_date_time'];
	$select2=mysqli_query($conn,"SELECT news.*, images.image1,images.image2,images.image3,images.image4,images.image5,images.image6,images.image7,images.image8,images.image9,signup.name,signup.image FROM news INNER JOIN images INNER JOIN signup ON signup.id=news.user_id AND news.id=images.news_id AND signup.id=images.u_id WHERE news.id='$news_id' AND news_time>'$cache_date_time'");
}

else
{
	$select2=mysqli_query($conn,"SELECT news.*, images.image1,images.image2,images.image3,images.image4,images.image5,images.image6,images.image7,images.image8,images.image9,signup.name,signup.image FROM news INNER JOIN images INNER JOIN signup ON signup.id=news.user_id AND news.id=images.news_id AND signup.id=images.u_id WHERE news.id='$news_id'");
}
$num2=mysqli_num_rows($select2);
while($res2=mysqli_fetch_assoc($select2))
{
	//Calculate Time Difference	
	$now1 = new DateTime();
	$ts11 = strtotime($res2['news_time']);
	$ts22 = $now1->getTimestamp();
	$diff = $ts22 - $ts11;
	$hourdiff1 = round(( $now1->getTimestamp()-strtotime($res2['news_time']))/3600, 1);
	$res2['time_diff']=$hourdiff1;
	
	$news_distance= round(distance($latitude,$longitude,$res2['lat'],$res2['log'],'K'),1);

$res2['news_distance']=$news_distance;

	$id=$res2['language'];
    $cat_id=$res2['category'];
	$select1=mysqli_query($conn,"SELECT * FROM `language` WHERE `id`='$id' ");
	$num1=mysqli_num_rows($select1);
	while($res1=mysqli_fetch_assoc($select1))
	{
		$res2['language_name']=$res1['language'];
	}
    $arr= explode(",",$cat_id);
	$dm="";
	for ($x = 0; $x <sizeof($arr); $x++) 
	{
		$dm=$dm."'".$arr[$x]."',";
		
			
	}
	$dmm=rtrim($dm, ",");
	$category_name="";
	$select1=mysqli_query($conn,"SELECT * FROM `category` WHERE id IN ($dmm)");
	$num1=mysqli_num_rows($select1);
	while($res1=mysqli_fetch_assoc($select1))
	{
		$category_name=$category_name.$res1['category'].",";
	}
	$category_name=rtrim($category_name, ",");
	$res2['category_name']=$category_name;
 	$data['news'][]=$res2;
	
	
 }
 if($num2>0)
 {
 //	$data['status']=$message;
//	echo json_encode($data);
	$select=mysqli_query($conn,"SELECT status FROM `news_likes` WHERE `user_id`='$user_id' AND `news_id`='$news_id'");
	$num=mysqli_num_rows($select);
	while($res=mysqli_fetch_assoc($select))
	{
		$status=$res['status'];
	}
	if($num>0)
	{
		if($status!="yes")
		{
			$data['like']=array("Liked"=>"no");
			//echo json_encode($data);	
		}
		else
		{
			$data['like']=array("Liked"=>"yes");
			//echo json_encode($data);
		}
	}
	else
	{
		$data['like']=array("Liked"=>"no");
		//echo json_encode($data);
	}
	$select1=mysqli_query($conn,"SELECT status FROM `flag` WHERE `user_id`='$user_id' AND `news_id`='$news_id'");
	$num1=mysqli_num_rows($select1);
	while($res1=mysqli_fetch_assoc($select1))
	{
		$status1=$res1['status'];
	}
	if($num1>0)
	{
		if($status1!="yes")
		{
			$data['flag']=array("Flagged"=>"no");
			echo json_encode($data);	
		}
		else
		{
			$data['flag']=array("Flagged"=>"yes");
			echo json_encode($data);
		}
	}
	else
	{
		$data['flag']=array("Flagged"=>"no");
		echo json_encode($data);
	}
}
else
{
	$data['status']=array("message"=>"Unable To Get The Data");
	echo json_encode($data);
}

function distance($lat1, $lon1, $lat2, $lon2, $unit) {

  $theta = $lon1 - $lon2;
  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
  $dist = acos($dist);
  $dist = rad2deg($dist);
  $miles = $dist * 60 * 1.1515;
  $unit = strtoupper($unit);

  if ($unit == "K") {
    return ($miles * 1.609344);
  } else if ($unit == "N") {
      return ($miles * 0.8684);
    } else {
        return $miles;
      }
}
		
?>