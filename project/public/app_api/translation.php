<?php
include('conn.php');
$news_id=$_POST['news_id'];
$language=$_POST['language'];

if($language=="en")
{
	$lang="English";
}
if($language=="zh-CN")
{
	$lang="Chinese";
}
if($language=="ms")
{
	$lang="Malay";
}
if($language=="ta")
{
	$lang="Tamil";
}

$header=$_POST['header'];// news title
$data=$_POST['description']; // news description
$source=$_POST['source'];//existing language 



//mysqli_set_charset('utf8');
mysqli_query($conn,"SET NAMES utf8");
$select=mysqli_query($conn,"SELECT * FROM translated_news where news_id='$news_id' and language='$lang'");
$num1=mysqli_num_rows($select);
if($num1>0)
{
	while($res=mysqli_fetch_assoc($select))
	{
		$arr['news'][]=$res;
	}
	echo json_encode($arr);
}
else
{ 
	$apiKey = 'AIzaSyDkW0jctpjOU65qD7r8rRpSBwHsUtsfX2s';
	//Description Conversion
	$url = 'https://www.googleapis.com/language/translate/v2?key=' . $apiKey . '&q=' . rawurlencode($data) . '&source='.$source.'&target='.$language;
	$handle = curl_init($url);
	curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
	$response = curl_exec($handle);                 
	$responseDecoded = json_decode($response, true);
	curl_close($handle);
	$translated_news1=$responseDecoded['data']['translations'][0]['translatedText'];
	$translated_news=mysqli_escape_string($conn, $translated_news1);
	
	//Title Conversion
	$url = 'https://www.googleapis.com/language/translate/v2?key=' . $apiKey . '&q=' . rawurlencode($header) . '&source='.$source.'&target='.$language;
	$handle = curl_init($url);
	curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
	$response = curl_exec($handle);                 
	$responseDecoded = json_decode($response, true);
	curl_close($handle);
	$translated_header1= $responseDecoded['data']['translations'][0]['translatedText'];
	
	$translated_header=mysqli_escape_string($conn, $translated_header1);
	//mysqli_set_charset('utf8');
	mysqli_query($conn,"SET NAMES utf8");
	$update=mysqli_query($conn,"INSERT INTO `translated_news`(`id`, `news_id`, `header`, `text`,`language`) VALUES (NULL,'$news_id','$translated_header','$translated_news','$lang')");
	if($update>0)
	{
		$inserted_data=mysqli_query($conn,"SELECT * FROM translated_news where news_id='$news_id' and language='$lang'");
		$num=mysqli_num_rows($inserted_data);
		if($num>0)
		{
			while($result_data=mysqli_fetch_assoc($inserted_data))
			{
				$res_data['news'][]=$result_data; 
			} 
			echo json_encode($res_data);
		}
	}
}
?>