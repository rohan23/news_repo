<?php
include('conn.php');
require_once __DIR__.'/server.php';
// Handle a request to a resource and authenticate the access token
if (!$server->verifyResourceRequest(OAuth2\Request::createFromGlobals()))
{
	$server->getResponse()->send();
	die;
}
//date_default_timezone_set("Asia/Kolkata"); 
$distance=$_POST['distance'];
$index=$_POST['index'];

if (!empty($_POST["web"])) {
    $val="12";
}else{  
    $val="10";
}

$skip=$val*$index;
$time=$_POST['time'];
$latitude=$_POST['latitude'];
$longitude=$_POST['longitude'];

$current=date("Y-m-d");
if(isset($_POST['cache_date_time']))
{
	$cache_date_time=$_POST['cache_date_time'];
$query="SELECT promotion_news.*, promotion_images.image1,promotion_images.image2,promotion_images.image3,promotion_images.image4,promotion_images.image5,promotion_images.image6,promotion_images.image7,promotion_images.image8,promotion_images.image9 FROM promotion_news INNER JOIN promotion_images  ON promotion_news.id=promotion_images.p_id WHERE status='approve' AND time>'$cache_date_time' AND end_date > '$current' order by id desc LIMIT $skip,$val";   
}
else{
	$query="SELECT promotion_news.*, promotion_images.image1,promotion_images.image2,promotion_images.image3,promotion_images.image4,promotion_images.image5,promotion_images.image6,promotion_images.image7,promotion_images.image8,promotion_images.image9 FROM promotion_news INNER JOIN promotion_images  ON promotion_news.id=promotion_images.p_id WHERE status='approve' AND end_date > '$current' order by id desc LIMIT $skip,$val";
}

$data=array(); 
$select=mysqli_query($conn,$query);
$num1=mysqli_num_rows($select);

while($res=mysqli_fetch_assoc($select))
{
	$news_distance= round(distance($latitude,$longitude,$res['lat'],$res['log'],'K'),1);
	$now1 = new DateTime();
	$ts11 = strtotime($res['time']);
	$ts22 = $now1->getTimestamp();
	$diff = $ts22 - $ts11;
	$hourdiff1 = round(( $now1->getTimestamp()-strtotime($res['time']))/3600, 1);
	if($news_distance<$res['radius'])
	{
		$news_distance1= round(distance($latitude,$longitude,$res['lat'],$res['log'],'K'),1);
		$res['news_distance']=$news_distance1;
		$data[]=$res;
	}
	
}
$message=array("message"=>"Successful");
if($distance!="" AND $time!="")
{
	$i=0;
	foreach($data as $item)
	{
		$item['time'];
		$now = new DateTime();
		$ts1 = strtotime($item['time']);
		$ts2 = $now->getTimestamp();
		$diff = $ts2 - $ts1;
		$hourdiff = round(( $now->getTimestamp()-strtotime($item['time']))/3600, 1);
		$news_distance= round(distance($latitude,$longitude,$item['lat'],$item['log'],'K'),1);
		$data[$i]['news_distance']=$news_distance;
		if($hourdiff>$time && $news_distance>$item['radius'])
		{
			unset($data[$i]);
		}
		$i++;
	}
}

  //echo sizeof($data);
if(sizeof($data) >0)
{
	$records=array();
	$data=array_values($data);
	$records['news']=$data;
	$records['status']=$message;
	echo json_encode($records);
}
else
{
	$data['status']=array("message"=>"Unable To Get The Data");
	echo json_encode($data);	
}

function distance($lat1, $lon1, $lat2, $lon2, $unit) {

  $theta = $lon1 - $lon2;
  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
  $dist = acos($dist);
  $dist = rad2deg($dist);
  $miles = $dist * 60 * 1.1515;
  $unit = strtoupper($unit);

  if ($unit == "K") {
    return ($miles * 1.609344);
  } else if ($unit == "N") {
      return ($miles * 0.8684);
    } else {
        return $miles;
      }

}
?>