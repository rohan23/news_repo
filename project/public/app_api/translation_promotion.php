<?php
include('conn.php');

$promotion_id=$_POST['promotion_id'];
$language=$_POST['language'];

if($language=="en")
{
	$lang="English";
}
if($language=="zh-CN")
{
	$lang="Chinese";
}
if($language=="ms")
{
	$lang="Malay";
}
if($language=="ta")
{
	$lang="Tamil";
}

$header=$_POST['header'];
$data=$_POST['description'];
$source=$_POST['source'];



//mysqli_set_charset('utf8');
mysqli_query($conn,"SET NAMES utf8");
$select=mysqli_query($conn,"SELECT * FROM translate_promote where promotion_id='$promotion_id' and language='$lang'");
$num1=mysqli_num_rows($select);
if($num1>0)
{
	while($res=mysqli_fetch_assoc($select))
	{
		$arr['promotion'][]=$res;
	}
	echo json_encode($arr);
}
else
{ 
	$apiKey = 'AIzaSyDkW0jctpjOU65qD7r8rRpSBwHsUtsfX2s';
	//Description Conversion
	$url = 'https://www.googleapis.com/language/translate/v2?key=' . $apiKey . '&q=' . rawurlencode($data) . '&source='.$source.'&target='.$language;
	$handle = curl_init($url);
	curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
	$response = curl_exec($handle);                 
	$responseDecoded = json_decode($response, true);
	curl_close($handle);
	$translated_news=$responseDecoded['data']['translations'][0]['translatedText'];
	
	//Title Conversion
	$url = 'https://www.googleapis.com/language/translate/v2?key=' . $apiKey . '&q=' . rawurlencode($header) . '&source='.$source.'&target='.$language;
	$handle = curl_init($url);
	curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
	$response = curl_exec($handle);                 
	$responseDecoded = json_decode($response, true);
	curl_close($handle);
	$translated_header= $responseDecoded['data']['translations'][0]['translatedText'];
	//mysqli_set_charset('utf8');
	mysqli_query($conn,"SET NAMES utf8");
	$update=mysqli_query($conn,"INSERT INTO `translate_promote`(`id`, `promotion_id`, `header`, `text`,`language`) VALUES (NULL,'$promotion_id','$translated_header','$translated_news','$lang')");
	if($update>0)
	{
		$inserted_data=mysqli_query($conn,"SELECT * FROM translate_promote where promotion_id='$promotion_id' and language='$lang'");
		$num=mysqli_num_rows($inserted_data);
		if($num>0)
		{
			while($result_data=mysqli_fetch_assoc($inserted_data))
			{
				$res_data['promotion'][]=$result_data; 
			} 
			echo json_encode($res_data);
		}
	}
}
?>