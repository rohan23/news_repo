<?php
include('conn.php');
	
	require_once __DIR__.'/server.php';

// Handle a request to a resource and authenticate the access token
if (!$server->verifyResourceRequest(OAuth2\Request::createFromGlobals()))
 {
    $server->getResponse()->send();
	
	 
    die;
 }
else
{
	$user_id=$_POST['user_id'];
	$title=$_POST['promotion_title'];
	$promotion_title = str_replace("'", "\'", $title);
	$text=$_POST['promotion_text'];
	$promotion_text = str_replace("'", "\'", $text);
	$lat=$_POST['lat'];
	$log=$_POST['log'];
	$post_by=$_POST['post_by'];
	$start_date=$_POST['start_date'];
	$end_date=$_POST['end_date'];
	$radius=$_POST['radius'];
	$price=$_POST['price'];
	$phone=$_POST['phone'];
	$email=$_POST['email'];
	//date_default_timezone_set("Asia/Kolkata"); 
	$date = date('Y-m-d H:i:s');
	// Base 64 Image Upload Code
	define('UPLOAD_DIR','../admin/images/');
	$image1=$_POST['image1'];
	if($image1!="")
	{
	$image1=str_replace('data:image/png;base64,','',$image1);
	$image1=str_replace('','+',$image1);
	$data1=base64_decode($image1);
	$image_name1=uniqid().'.png';
	$file1=UPLOAD_DIR.uniqid().'.png';
	file_put_contents("../admin/images/".$image_name1,$data1);
	}
	else 
	{
	$image_name1="";	
	}
	
	// Base 64 Image Upload Code
	$image2=$_POST['image2'];
	if($image2!="")
	{
	$image2=str_replace('data:image/png;base64,','',$image2);
	$image2=str_replace('','+',$image2);
	$data2=base64_decode($image2);
	$image_name2=uniqid().'.png';
	$file2=UPLOAD_DIR.uniqid().'.png';
	file_put_contents("../admin/images/".$image_name2,$data2);
	}
	else 
	{
	$image_name2="";	
	}
	
	// Base 64 Image Upload Code
	$image3=$_POST['image3'];
	if($image3!="")
	{
	$image3=str_replace('data:image/png;base64,','',$image3);
	$image3=str_replace('','+',$image3);
	$data3=base64_decode($image3);
	$image_name3=uniqid().'.png';
	$file3=UPLOAD_DIR.uniqid().'.png';
	file_put_contents("../admin/images/".$image_name3,$data3);
	}
	else 
	{
	$image_name3="";	
	}
	
	// Base 64 Image Upload Code
	$image4=$_POST['image4'];
	if($image4!="")
	{
	$image4=str_replace('data:image/png;base64,','',$image4);
	$image4=str_replace('','+',$image4);
	$data4=base64_decode($image4);
	$image_name4=uniqid().'.png';
	$file4=UPLOAD_DIR.uniqid().'.png';
	file_put_contents("../admin/images/".$image_name4,$data4);
	}
	else 
	{
	$image_name4="";	
	}
	
	// Base 64 Image Upload Code
	$image5=$_POST['image5'];
	if($image5!="")
	{
	$image5=str_replace('data:image/png;base64,','',$image5);
	$image5=str_replace('','+',$image5);
	$data5=base64_decode($image5);
	$image_name5=uniqid().'.png';
	$file5=UPLOAD_DIR.uniqid().'.png';
	file_put_contents("../admin/images/".$image_name5,$data5);
	}
	else 
	{
	$image_name5="";	
	}
	
	
	// Base 64 Image Upload Code
	$image6=$_POST['image6'];
	if($image6!="")
	{
	$image6=str_replace('data:image/png;base64,','',$image6);
	$image6=str_replace('','+',$image6);
	$data6=base64_decode($image6);
	$image_name6=uniqid().'.png';
	$file6=UPLOAD_DIR.uniqid().'.png';
	file_put_contents("../admin/images/".$image_name6,$data6);
	}
	else 
	{
	$image_name6="";	
	}
	
	
	// Base 64 Image Upload Code.
	$image7=$_POST['image7'];
	if($image7!="")
	{
	$image7=str_replace('data:image/png;base64,','',$image7);
	$image7=str_replace('','+',$image7);
	$data7=base64_decode($image7);
	$image_name7=uniqid().'.png';
	$file7=UPLOAD_DIR.uniqid().'.png';
	file_put_contents("../admin/images/".$image_name7,$data7);
	}
	else 
	{
	$image_name7="";	
	}
	
	// Base 64 Image Upload Code
	$image8=$_POST['image8'];
	if($image8!="")
	{
	$image8=str_replace('data:image/png;base64,','',$image8);
	$image8=str_replace('','+',$image8);
	$data8=base64_decode($image8);
	$image_name8=uniqid().'.png';
	$file8=UPLOAD_DIR.uniqid().'.png';
	file_put_contents("../admin/images/".$image_name8,$data8);
	}
	else 
	{
	$image_name8="";	
	}
	
	
	// Base 64 Image Upload Code
	$image9=$_POST['image9'];
	if($image9!="")
	{
	
	$image9=str_replace('data:image/png;base64,','',$image9);
	$image9=str_replace('','+',$image9);
	$data9=base64_decode($image9);
	$image_name9=uniqid().'.png';
	
	$file9=UPLOAD_DIR.uniqid().'.png';
	file_put_contents("../admin/images/".$image_name9,$data9);
	}
	else 
	{
		$image_name9="";	
	}
	
	
		$url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($lat).','.trim($log).'&sensor=false';
		$json = @file_get_contents($url);
		$data1=json_decode($json);
		$v=$data1->results[0]->formatted_address;
		$country1=substr(strrchr($v, ','), 1);
		$country=ltrim($country1);
		
			if($post_by=="admin")
			{
				
				$msg="Thank You for Posting.";
				
				$status="approved";
				
						
						$co=false;
						
						$insert="INSERT INTO `promotion_news`(`id`, `user_id`,`email`,`phone`,`title`, `text`, `radius`, `price`, `time`, `lat`, `log`, `status`,`start_date`, `end_date`, `country`, `post_by`) VALUES (NULL,'$user_id','$email','$phone','$promotion_title','$promotion_text','$radius','$price','$date','$lat','$log','$status','$start_date','$end_date','$country','$post_by')";
						$con=mysqli_query($conn,$insert);
						
						if($con>0)
						{
							$fetch="SELECT id FROM `promotion_news`";
							$co=mysqli_query($conn,$fetch);
							while($result=mysqli_fetch_assoc($co))
							{
								$id=$result['id'];
							}
					
							$insert1="INSERT INTO `promotion_images`(`id`, `p_id`, `u_id`, `image1`, `image2`, `image3`, `image4`, `image5`, `image6`, `image7`, `image8`, `image9`) VALUES (NULL,'$id','$user_id','$image_name1','$image_name2','$image_name3','$image_name4','$image_name5','$image_name6','$image_name7','$image_name8','$image_name9')";
							$message=array("message"=>"Successful");
							$co=mysqli_query($conn,$insert1);
						}
					
					
		
			if($co)
			{
				$data['promotion Id']=$id;
				$data['status']=$message;
				$data['msg']=$msg;
				echo json_encode($data);
			}
			else
			{
				$data['status']=array("msg"=>"Unsuccessful");
				echo json_encode($data);
			}
		}
		else
		{
			$msg="Thank You for Posting our team contact you soon";
			$status="pending";
			$co=false;
			
						$insert="INSERT INTO `promotion_news`(`id`, `user_id`,`email`,`phone`, `title`, `text`, `radius`,  `price`, `time`, `lat`, `log`, `status`,`start_date`, `end_date`, `country`, `post_by`) VALUES (NULL,'$user_id','$email','$phone','$promotion_title','$promotion_text','$radius','$price','$date','$lat','$log','$status','$start_date','$end_date','$country','$post_by')";
						
						$con=mysqli_query($conn,$insert);
						
						if($con>0)
						{
							$fetch="SELECT id FROM `promotion_news`";
							$co=mysqli_query($conn,$fetch);
							while($result=mysqli_fetch_assoc($co))
							{
								$id=$result['id'];
							}
							$insert1="INSERT INTO `promotion_images`(`id`, `p_id`, `u_id`, `image1`, `image2`, `image3`, `image4`, `image5`, `image6`, `image7`, `image8`, `image9`) VALUES (NULL,'$id','$user_id','$image_name1','$image_name2','$image_name3','$image_name4','$image_name5','$image_name6','$image_name7','$image_name8','$image_name9')";
							$message=array("message"=>"Successful");
							$co=mysqli_query($conn,$insert1);
						}
					
					
		
			
			if($co)
			{
				$data['promotion id']=$id;
				$data['status']=$message;
				$data['msg']=$msg;
				echo json_encode($data);
			}
			else
			{
				$data['status']=array("msg"=>"Unsuccessful");
				echo json_encode($data);
			}	
		}
	

}

?>