<?php
include('conn.php');
error_reporting( error_reporting() & ~E_NOTICE );
require_once __DIR__.'/server.php';
// Handle a request to a resource and authenticate the access token
if (!$server->verifyResourceRequest(OAuth2\Request::createFromGlobals()))
{
	$server->getResponse()->send();
	die;
}

$user_id=$_POST['user_id'];

//Caclulate Distanc using Longitude and Lattitude
function distance($lat1, $lon1, $lat2, $lon2, $unit) {

  $theta = $lon1 - $lon2;
  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
  $dist = acos($dist);
  $dist = rad2deg($dist);
  $miles = $dist * 60 * 1.1515;
  $unit = strtoupper($unit);

  if ($unit == "K") {
    return ($miles * 1.609344);
  } else if ($unit == "N") {
      return ($miles * 0.8684);
    } else {
        return $miles;
      }
}

// If User is id is Blank OR User is not Login Get all news 
if($user_id=="")
{
	$user_lat=$_POST['lat'];
	$user_lng=$_POST['long'];
	$hour_diff=$_POST['hour_diff'];
	$distance=$_POST['distance'];
	$language_id=$_POST['language_id'];
	
	//Filter Accrording To Only Distance/Time 
	if(($user_lat!="" AND $user_lng!="" AND $hour_diff!="" AND $distance!="") AND $language_id=="")
	{
		$date = date('Y-m-d H:i:s');
		$start_date = new DateTime($date);
		$query="SELECT * from news WHERE status='approved'";
		$select=mysqli_query($conn,$query);
		while($row=mysqli_fetch_array($select))
		{
			$news_datetime=$row['news_time'];
			$since_start = $start_date->diff(new DateTime($news_datetime));
			$hour=$since_start->h;
			$news_lat=$row['lat'];
			$news_long=$row['log'];
			$news_id=$row['id'];
			echo $dist1= distance($user_lat, $user_lng, $news_lat, $news_long, "K");
			if($dist1<$distance && $hour<=$hour_diff)
			{
				$q="select news.* , images.image1,images.image2,images.image3,images.image4,images.image5,images.image6,images.image7,images.image8,images.image9 FROM news INNER JOIN images ON news.id=images.news_id where status='approved' AND news.id='$news_id'";
				$result=mysqli_query($conn, $q);
				while($r=mysqli_fetch_assoc($result))
				{
					$data['news'][]=$r;
				}	
			}
		}
		$c=count($data);
		if($c>0)
		{
			echo json_encode($data);		
		}
		else
		{
			$data['status']=array("message"=>"Not Any News");
			echo json_encode($data);
		}
	}
	//Filter Accrording To Only Language ID
	else if(($user_lat=="" AND $user_lng=="" AND $hour_diff=="" AND $distance=="") AND $language_id!="")
	{
		$data=array(); 
		$select=mysqli_query($conn,"SELECT news.*, images.image1,images.image2,images.image3,images.image4,images.image5,images.image6,images.image7,images.image8,images.image9,signup.name,signup.image FROM news INNER JOIN images INNER JOIN signup ON news.id=images.news_id AND signup.id=news.user_id WHERE status='approved' AND category LIKE '%$c_id%' AND language='$language_id' LIMIT 10");
		$num1=mysqli_num_rows($select);
		while($res=mysqli_fetch_assoc($select))
		{
			$data['news'][]=$res;
		}
		$message=array("message"=>"Successful");
		if($num1 >0)
		{
			$data['status']=$message;
			echo json_encode($data);
		}
		else
		{
			$data['status']=array("message"=>"Unable To Get The Data");
			echo json_encode($data);	
		}
	}
	
	//Filter According to Both
	else if(($user_lat!="" AND $user_lng!="" AND $hour_diff!="" AND $distance!="") AND $language_id!="")
	{
		$date = date('Y-m-d H:i:s');
		$start_date = new DateTime($date);
		$query="SELECT * from news WHERE status='approved'";
		$select=mysqli_query($conn,$query);
		while($row=mysqli_fetch_array($select))
		{
			$news_datetime=$row['news_time'];
			$since_start = $start_date->diff(new DateTime($news_datetime));
			$hour=$since_start->h;
			$news_lat=$row['lat'];
			$news_long=$row['log'];
			$news_id=$row['id'];
			$dist1= distance($user_lat, $user_lng, $news_lat, $news_long, "K");
			if($dist1<$distance && $hour<=$hour_diff)
			{
				$q="select news.* , images.image1,images.image2,images.image3,images.image4,images.image5,images.image6,images.image7,images.image8,images.image9 FROM news INNER JOIN images ON news.id=images.news_id where status='approved' AND news.id='$news_id' AND language='$language_id'";
				$result=mysqli_query($conn, $q);
				while($r=mysqli_fetch_assoc($result))
				{
					$data['news'][]=$r;
				}	
			}
		}
		$c=count($data);
		if($c>0)
		{
			echo json_encode($data);		
		}
		else
		{
			$data['status']=array("message"=>"Not Any News");
			echo json_encode($data);
		}
	}

	//Without Any Filter
	else
	{
		$c_id=$_POST['category_id'];
		$data=array(); 
		//echo "SELECT news.*, images.image1,images.image2,images.image3,images.image4,images.image5,images.image6,images.image7,images.image8,images.image9 FROM news INNER JOIN images  ON news.id=images.news_id  WHERE status='approved' AND category LIKE '%$c_id%' LIMIT 10";
		$select=mysqli_query($conn,"SELECT news.*, images.image1,images.image2,images.image3,images.image4,images.image5,images.image6,images.image7,images.image8,images.image9 FROM news INNER JOIN images  ON news.id=images.news_id  WHERE status='approved' AND category LIKE '%$c_id%' LIMIT 10");
		$num1=mysqli_num_rows($select);
		while($res=mysqli_fetch_assoc($select))
		{
			$data['news'][]=$res;
		}
		$message=array("message"=>"Successful");
		if($num1 >0)
		{
			$data['status']=$message;
			echo json_encode($data);
		}
		else
		{
			$data['status']=array("message"=>"Unable To Get The Data");
			echo json_encode($data);	
		}
	}	
}


// If User id is Not Blank OR User is Login Get news

else
{
	$user_lat=$_POST['lat'];
	$user_lng=$_POST['long'];
	$hour_diff=$_POST['hour_diff'];
	$distance=$_POST['distance'];
	$language_id=$_POST['language_id'];
	
	//Filter Accrording To Only Distance/Time 
	if(($user_lat!="" AND $user_lng!="" AND $hour_diff!="" AND $distance!="") AND $language_id=="")
	{
		$date = date('Y-m-d H:i:s');
		$start_date = new DateTime($date);
		$query="SELECT * from news WHERE status='approved'";
		$select=mysqli_query($conn,$query);
		while($row=mysqli_fetch_array($select))
		{
			$news_datetime=$row['news_time'];
			$since_start = $start_date->diff(new DateTime($news_datetime));
			$hour=$since_start->h;
			$news_lat=$row['lat'];
			$news_long=$row['log'];
			$news_id=$row['id'];
			echo $dist1= distance($user_lat, $user_lng, $news_lat, $news_long, "K");
			if($dist1<$distance && $hour<=$hour_diff)
			{
				$q="select news.* , images.image1,images.image2,images.image3,images.image4,images.image5,images.image6,images.image7,images.image8,images.image9 FROM news INNER JOIN images ON news.id=images.news_id where status='approved' AND news.id='$news_id'";
				$result=mysqli_query($conn, $q);
				while($r=mysqli_fetch_assoc($result))
				{
					$data['news'][]=$r;
				}	
			}
		}
		$c=count($data);
		if($c>0)
		{
			echo json_encode($data);		
		}
		else
		{
			$data['status']=array("message"=>"Not Any News");
			echo json_encode($data);
		}
	}
	//Filter Accrording To Only Language ID
	else if(($user_lat=="" AND $user_lng=="" AND $hour_diff=="" AND $distance=="") AND $language_id!="")
	{
		$data=array(); 
		$select=mysqli_query($conn,"SELECT news.*, images.image1,images.image2,images.image3,images.image4,images.image5,images.image6,images.image7,images.image8,images.image9,signup.name,signup.image FROM news INNER JOIN images INNER JOIN signup ON news.id=images.news_id AND signup.id=news.user_id WHERE status='approved' AND category LIKE '%$c_id%' AND language='$language_id' LIMIT 10");
		$num1=mysqli_num_rows($select);
		while($res=mysqli_fetch_assoc($select))
		{
			$data['news'][]=$res;
		}
		$message=array("message"=>"Successful");
		if($num1 >0)
		{
			$data['status']=$message;
			echo json_encode($data);
		}
		else
		{
			$data['status']=array("message"=>"Unable To Get The Data");
			echo json_encode($data);	
		}
	}
	
	//Filter According to Both
	else if(($user_lat!="" AND $user_lng!="" AND $hour_diff!="" AND $distance!="") AND $language_id!="")
	{
		$date = date('Y-m-d H:i:s');
		$start_date = new DateTime($date);
		$query="SELECT * from news WHERE status='approved'";
		$select=mysqli_query($conn,$query);
		while($row=mysqli_fetch_array($select))
		{
			$news_datetime=$row['news_time'];
			$since_start = $start_date->diff(new DateTime($news_datetime));
			$hour=$since_start->h;
			$news_lat=$row['lat'];
			$news_long=$row['log'];
			$news_id=$row['id'];
			echo $dist1= distance($user_lat, $user_lng, $news_lat, $news_long, "K");
			if($dist1<$distance && $hour<=$hour_diff)
			{
				$q="select news.* , images.image1,images.image2,images.image3,images.image4,images.image5,images.image6,images.image7,images.image8,images.image9 FROM news INNER JOIN images ON news.id=images.news_id where status='approved' AND news.id='$news_id' AND language='$language_id'";
				$result=mysqli_query($conn, $q);
				while($r=mysqli_fetch_assoc($result))
				{
					$data['news'][]=$r;
				}	
			}
		}
		$c=count($data);
		if($c>0)
		{
			echo json_encode($data);		
		}
		else
		{
			$data['status']=array("message"=>"Not Any News");
			echo json_encode($data);
		}
	}

	//Without Any Filter
	else
	{
		$c_id=$_POST['category_id'];
		$language_id=$_POST['language_id'];
		$data=array(); 
		$select=mysqli_query($conn,"SELECT news.*, images.image1,images.image2,images.image3,images.image4,images.image5,images.image6,images.image7,images.image8,images.image9 FROM news INNER JOIN images  ON news.id=images.news_id  WHERE status='approved' AND category LIKE '%$c_id%' LIMIT 10");
		$num1=mysqli_num_rows($select);
		while($res=mysqli_fetch_assoc($select))
		{
			$data['news'][]=$res;
		}
		$message=array("message"=>"Successful");
		if($num1 >0)
		{
			$data['status']=$message;
			echo json_encode($data);
		}
		else
		{
			$data['status']=array("message"=>"Unable To Get The Data");
			echo json_encode($data);	
		}
	}
}
	

?>