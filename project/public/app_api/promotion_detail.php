<?php
include('conn.php');
require_once __DIR__.'/server.php';
// Handle a request to a resource and authenticate the access token
if (!$server->verifyResourceRequest(OAuth2\Request::createFromGlobals()))
{
	$server->getResponse()->send();
	die;
}
	$p_id=$_POST['promotion_id'];
	$user_id=$_POST['user_id'];

	$latitude=$_POST['latitude'];
	$longitude=$_POST['longitude'];

$select2=mysqli_query($conn,"SELECT promotion_news.*, promotion_images.image1,promotion_images.image2,promotion_images.image3,promotion_images.image4,promotion_images.image5,promotion_images.image6,promotion_images.image7,promotion_images.image8,promotion_images.image9 FROM promotion_news INNER JOIN promotion_images ON promotion_news.id=promotion_images.p_id  WHERE promotion_news.id='$p_id'");

		  
		$num2=mysqli_num_rows($select2);
		
		 while($res2=mysqli_fetch_assoc($select2))
		 {
		 	 $now1 = new DateTime();
 			 $ts11 = strtotime($res2['time']);
 			 $ts22 = $now1->getTimestamp();
 			 $diff = $ts22 - $ts11;
 			 $hourdiff1 = round(( $now1->getTimestamp()-strtotime($res2['time']))/3600, 1);
 			 $res2['time_diff']=$hourdiff1;
			 
			 
			 $news_distance= round(distance($latitude,$longitude,$res2['lat'],$res2['log'],'K'),1);

			 $res2['news_distance']=$news_distance;

			 $data['news'][]=$res2;
		  }		 
			
		 if($num2>0)
		 {
		 //	$data['status']=$message;
		//	echo json_encode($data);
	
		$select=mysqli_query($conn,"SELECT status FROM `promotion_likes` WHERE `user_id`='$user_id' AND `p_id`='$p_id'");
		$num=mysqli_num_rows($select);
		while($res=mysqli_fetch_assoc($select))
		{
			$status=$res['status'];
		}
		if($num>0)
		{
			if($status!="yes")
			{
				$data['like']=array("Liked"=>"no");
				//echo json_encode($data);	
			}
			else
			{
				$data['like']=array("Liked"=>"yes");
				//echo json_encode($data);
			}
		}
		else
		{
			$data['like']=array("Liked"=>"no");
			//echo json_encode($data);
		}
		$select1=mysqli_query($conn,"SELECT status FROM `promotion_flag` WHERE `user_id`='$user_id' AND `p_id`='$p_id'");
		$num1=mysqli_num_rows($select1);
		while($res1=mysqli_fetch_assoc($select1))
		{
			$status1=$res1['status'];
		}
		if($num1>0)
		{
			if($status1!="yes")
			{
				$data['flag']=array("Flagged"=>"no");
				//echo json_encode($data);	
			}
			else
			{
				$data['flag']=array("Flagged"=>"yes");
				//echo json_encode($data);
			}
		}
		else
		{
			$data['flag']=array("Flagged"=>"no");
			//echo json_encode($data);
		}
		$select1=mysqli_query($conn,"SELECT status FROM `promotion_flag` WHERE `user_id`='$user_id' AND `p_id`='$p_id'");
		$num1=mysqli_num_rows($select1);
		while($res1=mysqli_fetch_assoc($select1))
		{
			$status1=$res1['status'];
		}
		if($num1>0)
		{
			if($status1!="yes")
			{
				$data['flag']=array("Flagged"=>"no");
				echo json_encode($data);	
			}
			else
			{
				$data['flag']=array("Flagged"=>"yes");
				echo json_encode($data);
			}
		}
		else
		{
			$data['flag']=array("Flagged"=>"no");
			echo json_encode($data);
		}
			 }
		else
		{
			$data['status']=array("message"=>"Unable To Get The Data");
			echo json_encode($data);
		}
function distance($lat1, $lon1, $lat2, $lon2, $unit) {

  $theta = $lon1 - $lon2;
  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
  $dist = acos($dist);
  $dist = rad2deg($dist);
  $miles = $dist * 60 * 1.1515;
  $unit = strtoupper($unit);

  if ($unit == "K") {
    return ($miles * 1.609344);
  } else if ($unit == "N") {
      return ($miles * 0.8684);
    } else {
        return $miles;
      }
}		
?>