<?php
include('conn.php');
require_once __DIR__.'/server.php';
// Handle a request to a resource and authenticate the access token
if (!$server->verifyResourceRequest(OAuth2\Request::createFromGlobals()))
{
	$server->getResponse()->send();
	die;
}
$data=array(); 
$select=mysqli_query($conn,"SELECT * FROM `faq`");
$num1=mysqli_num_rows($select);
while($res=mysqli_fetch_assoc($select))
{
	$data['faq'][]=$res;
}
$message=array("message"=>"Successful");
if($num1 >0)
{
	$data['status']=$message;
	echo json_encode($data);
}
else
{
	$data['status']=array("message"=>"Unable To Get The Data");
	echo json_encode($data);	
}
?>