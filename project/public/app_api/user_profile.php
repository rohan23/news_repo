<?php
include('conn.php');
require_once __DIR__.'/server.php';
// Handle a request to a resource and authenticate the access token

if (!$server->verifyResourceRequest(OAuth2\Request::createFromGlobals()))
{
	$server->getResponse()->send();
	die;
}

$data=array();
$data['general_news']=array();
$data['locational_news']=array();
$user_id=$_POST['user_id'];
$liu=$_POST['loged_in_user'];
$lat=$_POST['latitude'];
$log=$_POST['longitude'];

$select2=mysqli_query($conn,"SELECT  * from signup WHERE id='$user_id'");
// $message=array("message"=>"Successful");
 while($res2=mysqli_fetch_assoc($select2))
 {
 	$data['user_profile']=$res2;
 }

if($liu=="")
$data['is_follow']=false;

else

{

$select4=mysqli_query($conn,"SELECT `user_id` FROM `follow` WHERE `user_id`='$user_id' AND `follower_id`='$liu' AND status='yes'");
$num4=mysqli_num_rows($select4);

if($num4>0)
$data['is_follow']=true;
else
$data['is_follow']=false;
}
$d=0;
$select=mysqli_query($conn,"SELECT `like` FROM `news` WHERE `user_id`='$user_id'");
$num=mysqli_num_rows($select);
while($res=mysqli_fetch_assoc($select))
{
	 $d=$d+$res['like'];
	

}

$data['news likes']=$d;

$select3=mysqli_query($conn,"SELECT comment,news_id FROM `news_comments` WHERE `user_id`='$user_id'");
$num3=mysqli_num_rows($select3);
while($res3=mysqli_fetch_assoc($select3))
{

}

	$data['news comments']=$num3;
	

$select4=mysqli_query($conn,"SELECT * FROM `follow` WHERE `user_id`='$user_id'");
$num4=mysqli_num_rows($select4);




$data['follower_count']=$num4;
$check=mysqli_query($conn,"SELECT  * FROM `news` WHERE `user_id`='$user_id';");
$row=mysqli_num_rows($check);
if($row>0)
{
	if(isset($_POST['cache_date_time']))
{
	$cache_date_time=$_POST['cache_date_time'];
	$select2=mysqli_query($conn,"SELECT  signup.*, news.*, images.image1,images.image2,images.image3,images.image4,images.image5,images.image6,images.image7,images.image8,images.image9 FROM signup INNER JOIN news INNER JOIN images ON news.user_id=signup.id AND news.id=images.news_id where news.user_id='$user_id' AND news_time>'$cache_date_time' order by news.id desc");
}
else{
	$select2=mysqli_query($conn,"SELECT  signup.*, news.*, images.image1,images.image2,images.image3,images.image4,images.image5,images.image6,images.image7,images.image8,images.image9 FROM signup INNER JOIN news INNER JOIN images ON news.user_id=signup.id AND news.id=images.news_id where news.user_id='$user_id' order by news.id desc");
}	
 while($res2=mysqli_fetch_assoc($select2))
 {
 	

	$type=$res2['news_type'];
	
	
	$now1 = new DateTime();
	$ts11 = strtotime($res['news_time']);
	$ts22 = $now1->getTimestamp();
	$diff = $ts22 - $ts11;
	$hourdiff1 = round(( $now1->getTimestamp()-strtotime($res2['news_time']))/3600, 1);
	$res2['time_diff']=$hourdiff1;
	
		
		if($type=="0")
		{
			

			$res2['distance']="";
			
				$data['general_news'][]=$res2;
			
		}
		
		else
		{
			$news_distance= round(distance($lat,$log,$res2['lat'],$res2['log'],'K'),1);

			$res2['distance']=$news_distance;
				
				$data['locational_news'][]=$res2;
			
		}
		
	
 }
 

 //	$data['status']=$message;
//	echo json_encode($data);


	
}


$data['status']=array("message"=>"Successful");
	echo json_encode($data);

function distance($lat1, $lon1, $lat2, $lon2, $unit) {

  $theta = $lon1 - $lon2;
  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
  $dist = acos($dist);
  $dist = rad2deg($dist);
  $miles = $dist * 60 * 1.1515;
  $unit = strtoupper($unit);

  if ($unit == "K") {
    return ($miles * 1.609344);
  } else if ($unit == "N") {
      return ($miles * 0.8684);
    } else {
        return $miles;
      }
}


?>