<?php
//use Redirect;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'NewslistController@index');

 Route::get('/error', function() {
         return view('404error');
 });

Route::get('mobile-application', array( 'as' => 'landing_api',  'uses' => 'LandingController@landing_api'));

Route::get('/about-us', function() {
        return view('aboutus');
});

Route::get('/contact', function() { 
        return view('contact');
});

Route::get('/signup', function() { 
        return view('signup');
});

Route::get('/demo', function() { 
        return view('demo');
});

Route::get('/demo_load', function() { 
        return view('demo_load');
});


Route::get('/getlocation', function() { 
        return view('geolocation');
});

Route::get('/verify', function() { 
        return view('verify');
});

Route::get('/set_location', function() { 
        return view('set_location');
});

Route::get('/forgot_password', function() { 
        return view('forgot_password');
});


Route::get('/filter', function() { 
        return view('news_filter');
});

Route::get('/test', function() { 
        return view('test');
});

Route::get('/login', function() {
	if(Session::get('username')=="")
	{
		return view('login');
	} 
	else
	{
		return Redirect::route('/');
	}   
});


Route::get('/', array('as' => '/', 'uses' => 'IndexController@index'));
Route::get('/Category/{category_name}', array( 'as' => 'category_name',  'uses' => 'NewslistController@news_category'));

Route::get('/Local_News/{local_category}', array( 'as' => 'local_category',  'uses' => 'NewslistController@local_category'));
Route::get('/Latest_News/{latest_category}', array( 'as' => 'latest_category',  'uses' => 'NewslistController@local_category'));
Route::get('/Country_News/{country_category}', array( 'as' => 'country_category',  'uses' => 'NewslistController@local_category'));


Route::get('/news_detail/{news_id}', array( 'as' => 'news.route',  'uses' => 'NewslistController@news_detail'));
Route::get('/news_delete/{news_id}', array( 'as' => 'news.delete',  'uses' => 'NewslistController@news_delete'));
Route::get('likeunlike/{news_id}/{user_id}', array('as' => 'likeunlike.route', 'uses' => 'NewslistController@likeunlike'));
Route::get('demo1', array('as' => 'demo1', 'uses' => 'NewslistController@get_news_front'));
Route::post('report_news', 'NewslistController@report');
Route::post('/selected_location', ['as' => '/setlocation', 'uses' => 'IndexController@set_selected_location']);
Route::post('/current_location', ['as' => '/setlocation', 'uses' => 'IndexController@set_current_location']);
/*
Route::get('hide_news/{news_id}/{user_id}', array('as' => 'likeunlike.route', 'uses' => 'NewslistController@likeunlike'));
Route::get('hidden_news/{news_id}/{user_id}', array('as' => 'likeunlike.route', 'uses' => 'NewslistController@likeunlike'));
*/
Route::get('following_news/{user_id}', array('as' => 'following_news', 'uses' => 'FollowingnewsController@follow'));
Route::post('newscomments', 'CommentController@newscomment');
Route::get('/comment_delete/{news_id}/{comment_id}', array( 'as' => 'comment_delete.route',  'uses' => 'CommentController@deletecomment'));
Route::get('/comment_like/{user_id}/{comment_id}', array( 'as' => 'comment_like.route',  'uses' => 'CommentController@likecomment'));
Route::post('flag_comment', 'CommentController@flagcomment');

Route::post('loginuser', 'LoginController@do_login');
Route::post('signup', 'LoginController@insert');
Route::get('logout', 'LoginController@do_logout');
Route::post('verify_user', 'LoginController@verify_otp');

Route::get('follow/{u_id}/{f_id}', array( 'as' => 'follow',  'uses' => 'ProfileController@follow'));
Route::get('myaccount/{user_id}/{c_user}', array( 'as' => 'myaccount',  'uses' => 'ProfileController@profile'));

Route::get('mycomments/{user_id}', array( 'as' => 'mycomments',  'uses' => 'ProfileController@mycomment'));

Route::post('profile_edit', 'ProfileController@profiledata');
//Route::get('/myaccount/{user_id}', array( 'as' => 'myaccount.route',  'uses' => 'ProfileController@profile'));
Route::post('/ajaxurl', ['as' => '/ajaxurl', 'uses' => 'IndexController@index1']);

//Route::post('ajaxurl', 'SignupController@index');
//Route::get('/index1', array('as' => 'index1', 'uses' => 'IndexController@index'));

Route::get('news_edit/{news_id}', array( 'as' => 'news_edit',  'uses' => 'NewsController@edit'));

Route::get('news', 'NewsController@index');
Route::post('news_insert', 'NewsController@insert');

Route::get('promotions', array('as' => 'promotions', 'uses' => 'PromotionController@promotions'));

Route::post('translate', ['as' => 'translate', 'uses' => 'NewslistController@news_translate']);

Route::get('/promotion_detail/{p_id}', array( 'as' => 'promotion.route',  'uses' => 'PromotionController@news_detail'));

Route::get('likedislike/{p_id}/{user_id}', array('as' => 'likedislike.route', 'uses' => 'PromotionController@likedislike'));

Route::post('report_promotion', 'PromotionController@report');

Route::post('promotioncomments', 'PromotionController@comment');

Route::get('/delete_comment/{p_id}/{c_id}', array( 'as' => 'delete_comment.route',  'uses' => 'PromotionController@deletecomment'));

Route::get('/like_comment/{user_id}/{comment_id}', array( 'as' => 'like_comment.route',  'uses' => 'PromotionController@likecomment'));



//Route::post('/loadmore', array('NewslistController@loadmore'));

Route::post('/loadmore', ['as' => '/loadmore', 'uses' => 'NewslistController@loadmore']);

Route::post('/filters', ['as' => '/filters', 'uses' => 'IndexController@filters']);

Route::post('/index_locational', ['as' => '/index_locational', 'uses' => 'IndexController@loadmore']);

Route::post('/index_country', ['as' => '/index_country', 'uses' => 'IndexController@country_load']);

Route::post('/save_email', array( 'as' => '/save_email',  'uses' => 'LandingController@save_email'));


Route::post('/print_image', 'NewsController@print_screen');


Route::get('/map_news/{news_id}', array( 'as' => 'map.route',  'uses' => 'NewslistController@map_news'));
