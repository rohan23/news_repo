<?php

namespace App\Providers;
use App\Http\Controllers\Controller;
use Illuminate\Support\ServiceProvider;
use App\Category;
use App\Countrycodes;
use GuzzleHttp\Client;
use DB;
use Response;
use Session;

use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Promise;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
     
   
    public function boot()
    {
    	
		$tk=new Controller;
    	$access_token= $tk->token_get();
		$acc=json_decode($access_token, true);
		$tt=$acc['access_token'];
		
		$client = new Client();
		$promise = $client->requestAsync('POST', 'http://128.199.251.163/app_api/category.php?access_token='.$tt);
		$promise->then(
	    function (ResponseInterface $res) use(&$cat_list) {
	        $cat_list=$res->getBody() . "\n";
	    },
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
	    		echo $e->getRequest()->getMethod();
			}
		);
		$promise->wait();
		$catt=json_decode($cat_list, true);
		view()->share('catt', $catt);
		
		$result = DB::table('news')
           		->join('images', 'news.id', '=', 'images.news_id')
            	->select('news.*', 'images.*')
				->where('status', 'approved')
				->orderBy('news_time', 'ASC')
				->limit(6)
            	->get();
		 if(is_null($result))
		 {
            view()->share('result', $result);
	     }
		 else
		 {
		 	view()->share('result', $result);
		 }
		 
		 $result1 = DB::table('news')
           		->join('images', 'news.id', '=', 'images.news_id')
            	->select('news.*', 'images.*')
				->where('status', 'approved')
				->orderBy('news_time', 'ASC')
				->offset(10)
				->limit(6)
            	->get();
		 if(is_null($result1))
		 {
            view()->share('result1', $result1);
	     }
		 else
		 {
		 	view()->share('result1', $result1);
		 }
		 
		$country = DB::table('country')->get();
		view()->share('country', $country);
		
		
		
		
		
		
      	
		 
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
