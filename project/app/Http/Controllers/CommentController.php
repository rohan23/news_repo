<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Signup;
use App\News;
use App\Images;
use App\Category;
use App\Http\Requests;
use App\Countrycodes;
use View;
use DB;
use Redirect;
use App\Http\Controllers\Controller;
use Session;

use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Promise;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;

class CommentController extends Controller
{
	public function newscomment(Request $comment)
	{
		$tk=new Controller;
    	$access_token= $tk->token_get();
		$acc=json_decode($access_token, true);
		Session::put('token_no', $acc['access_token']);
		$user_id=$comment->get('user_id');
		$news_id=$comment->get('news_id');
		echo $get_comment=$comment->get('user_comment');
		$client = new Client();
		$promise = $client->requestAsync('POST', 'http://128.199.251.163/app_api/comment_api.php?access_token='.Session::get('token_no'),[ 
		'form_params' => [
		'news_id'=>$news_id,
		'user_id'=>$user_id,
		'comment'=>$get_comment
		]
		]);
		$promise->then(
	    function (ResponseInterface $res) use(&$comment_resp) {
	        echo $comment_resp= $res->getBody() . "\n";
	    },
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
	    		echo $e->getRequest()->getMethod();
			}
		);
		$promise->wait();
		
		$get_status=json_decode($comment_resp, true);
		if($get_status['status']['message']=="Comment Added Successfully")
		{
			Session::put('message', 'Comment Added Successfully');
			return Redirect::back();
			//return Redirect::route('/news_detail/'.$news_id);
		}
		else{
			Session::put('message', 'Not Added Successfully');
			return Redirect::back();
			//return Redirect::route('http://nearbynews.co/news_detail/'.$news_id);
		}
		
	}	
	public function deletecomment($news_id,$comment_id)
	{
		$tk=new Controller;
    	$access_token= $tk->token_get();
		$acc=json_decode($access_token, true);
		Session::put('token_no', $acc['access_token']);
		$client = new Client();
		$promise = $client->requestAsync('POST', 'http://128.199.251.163/app_api/delete_comment.php?access_token='.Session::get('token_no'),[ 
		'form_params' => [
		'news_id'=>$news_id,
		'comment_id'=>$comment_id
		]
		]);
		$promise->then(
	    function (ResponseInterface $res) use(&$comment_resp) {
	        echo $comment_resp= $res->getBody() . "\n";
	    },
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
	    		echo $e->getRequest()->getMethod();
			}
		);
		$promise->wait();
		
		$get_status=json_decode($comment_resp, true);
		print_r($get_status);
		if($get_status['scalar']=="Comment Deleted")
		{
			Session::put('message', 'Comment Deleted Successfully');
			return Redirect::back();
			//return Redirect::route('/news_detail/'.$news_id);
		}
		else{
			Session::put('message', 'Not Deleted Successfully');
			return Redirect::back();
			//return Redirect::route('http://nearbynews.co/news_detail/'.$news_id);
		}
		
	}	
	public function likecomment($user_id,$comment_id)
	{
		$tk=new Controller;
    	$access_token= $tk->token_get();
		$acc=json_decode($access_token, true);
		Session::put('token_no', $acc['access_token']);
		$client = new Client();
		$promise = $client->requestAsync('POST', 'http://128.199.251.163/app_api/comment_likes.php?access_token='.Session::get('token_no'),[ 
		'form_params' => [
		'user_id'=>$user_id,
		'comment_id'=>$comment_id
		]
		]);
		$promise->then(
	    function (ResponseInterface $res) use(&$commentlike_resp) {
	        echo $commentlike_resp= $res->getBody() . "\n";
	    },
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
	    		echo $e->getRequest()->getMethod();
			}
		);
		$promise->wait();
		
		$getlike_status=json_decode($commentlike_resp, true);
		if($getlike_status['status']['message']=="liked")
		{
			Session::put('message', 'liked');
			return Redirect::back();
		}
		else{
			Session::put('message', 'Unliked');
			return Redirect::back();
			//return Redirect::route('http://nearbynews.co/news_detail/'.$news_id);
		}
		
	}

	public function flagcomment(Request $flag_req)
	{
		$tk=new Controller;
    	$access_token= $tk->token_get();
		$acc=json_decode($access_token, true);
		Session::put('token_no', $acc['access_token']);
		$client = new Client();
		$user_id=$flag_req->get('u_id');
		$comment_id=$flag_req->get('comment_id');
		$promise = $client->requestAsync('POST', 'http://128.199.251.163/app_api/comment_flag.php?access_token='.Session::get('token_no'),[ 
		'form_params' => [
		'user_id'=>$user_id,
		'comment_id'=>$comment_id
		]
		]);
		$promise->then(
	    function (ResponseInterface $res) use(&$commentflag_resp) {
	        echo $commentflag_resp= $res->getBody() . "\n";
	    },
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
	    		echo $e->getRequest()->getMethod();
			}
		);
		$promise->wait();
		$getflag_status=json_decode($commentflag_resp, true);
		if($getflag_status['flag']['message']=="yes")
		{
			return Redirect::back();
		}
		else{
			return Redirect::back();
			//return Redirect::route('http://nearbynews.co/news_detail/'.$news_id);
		}
	}	
}
