<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;

use View;
use DB;
use Redirect;
use App\Http\Controllers\Controller;
use Session;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Promise;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;

class ProfileController extends Controller
{
    //start
	public function profile($user_id,$c_user)
    {
       	$lat=Session::get('user_lat');
		$long=Session::get('user_lon');
    	$tk=new Controller;
    	$access_token= $tk->token_get();
		$acc=json_decode($access_token, true);
		Session::put('token_no', $acc['access_token']);
		$client = new Client();
		$promise = $client->requestAsync('POST', 'http://128.199.251.163/app_api/user_profile.php?access_token='.Session::get('token_no'),[ 
		'form_params' => [
		'user_id'=>$user_id,
		'loged_in_user'=>$c_user,
		'latitude'=>$lat,
		'longitude'=>$long
		]
		]);
		$promise->then(
	    function (ResponseInterface $res) use(&$profile) {
	       $profile= $res->getBody() . "\n";
	    },
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
	    		echo $e->getRequest()->getMethod();
			}
		);
		
		
		$promise->wait();
		
	 	$profile_data=json_decode($profile, true);
		
		
		//Session::put('userimage',$profile_data['user_profile']['image']);
		return View::make('myaccount', compact('profile_data'));
		
    }
	
	public function mycomment($u_id)
    {
    	$lat=Session::get('user_lat');
		$long=Session::get('user_lon');
		$lid=Session::get('user_id');
    	$tk=new Controller;
    	$access_token= $tk->token_get();
		$acc=json_decode($access_token, true);
		Session::put('token_no', $acc['access_token']);
		$client = new Client();
		
		$promise1 = $client->requestAsync('POST', 'http://128.199.251.163/app_api/user_profile.php?access_token='.Session::get('token_no'),[ 
		'form_params' => [
		'user_id'=>$u_id,
		'loged_in_user'=>$lid,
		'latitude'=>$lat,
		'longitude'=>$long
		]
		]);
		$promise1->then(
	    function (ResponseInterface $res) use(&$profile) {
	       $profile= $res->getBody() . "\n";
	    },
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
	    		echo $e->getRequest()->getMethod();
			}
		);
		
		$promise = $client->requestAsync('POST', 'http://128.199.251.163/app_api/user_comments.php?access_token='.Session::get('token_no'),[ 
		'form_params' => [
		'user_id'=>$u_id
		]
		]);
		$promise->then(
	    function (ResponseInterface $res) use(&$mycomment) {
	       $mycomment= $res->getBody() . "\n";
	    },
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
	    		echo $e->getRequest()->getMethod();
			}
		);
		
		$promise->wait();
		$promise1->wait();
	 	$comment=json_decode($mycomment, true);
		$profile_data=json_decode($profile, true);
		return View::make('mycomments', compact('comment','profile_data'));
    }

	public function profiledata(Request $r)
    {
    	$lid=Session::get('user_id');
    	
		$name=$r->get('name');
		$id=$r->get('id');
		
		
		$image=$r->file('image');
		if($image!="")
		{
			$data = file_get_contents($image);
			$base64 = base64_encode($data);	
		}
		else{
			$base64="";
		}
		
		
		
		$tk=new Controller;
    	$access_token= $tk->token_get();
		$acc=json_decode($access_token, true);
		Session::put('token_no', $acc['access_token']);
		
		$client = new Client();
		$promise = $client->requestAsync('POST', 'http://128.199.251.163/app_api/profile.php?access_token='.Session::get('token_no'),[ 
		'form_params' => [
		'id'=>$id,
		'name'=>$name,
		'profile_pic'=>$base64,
		]
		]);
		$promise->then(
	    function (ResponseInterface $res) use(&$update) {
	        $update= $res->getBody() . "\n";
	    },
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
	    		echo $e->getRequest()->getMethod();
			}
		);
		$promise->wait();
		
		$get_resp=json_decode($update, true);
		
		if($get_resp['status']['message']=="Failed To Update")
		{
			$r->session()->flash('alert-danger', 'Error:Profile Not Updated');
			return Redirect::back();
		}
		else
		{
			//echo "success";
			$r->session()->flash('alert-success', 'Profile Updated Successfully');
			return Redirect::back();
			
		}
	
    }

	public function follow($u_id,$f_id)
    {
    	
		
    	$tk=new Controller;
    	$access_token= $tk->token_get();
		$acc=json_decode($access_token, true);
		Session::put('token_no', $acc['access_token']);
		$client = new Client();
		
		$promise = $client->requestAsync('POST', 'http://128.199.251.163/app_api/follow.php?access_token='.Session::get('token_no'),[ 
		'form_params' => [
		'user_id'=>$u_id,
		'follower_id'=>$f_id
		]
		]);
		$promise->then(
	    function (ResponseInterface $res) use(&$follow) {
	       $follow= $res->getBody() . "\n";
	    },
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
	    		echo $e->getRequest()->getMethod();
			}
		);
		
		
		
		$promise->wait();
		$follow=json_decode($follow, true);
		
		if($follow['status']['message']=="Unable To Follow")
		{
			Session::flash('alert-danger', 'Error:Unable To Follow');
			return Redirect::back();
		}
		if($follow['status']['message']=="Follow")
		{
			Session::flash('alert-success', 'Success:Followed Successfully');
			return Redirect::back();
		}
		if($follow['status']['message']=="UnFollow")
		{
			Session::flash('alert-success', 'Success:Successfully Unfollowed ');
			return Redirect::back();
		}
		if($follow['status']['message']=="Unable To UnFollow")
		{
			Session::flash('alert-danger', 'Error:Unable To Unfollow');
			return Redirect::back();
		}
    }
}
