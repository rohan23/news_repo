<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Signup;
use App\News;
use App\Images;
use App\Category;
use App\Http\Requests;
use App\Countrycodes;
use View;
use DB;
use Redirect;
use App\Http\Controllers\Controller;
use Session;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Promise;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use Spatie\Browsershot\Browsershot;
class NewsController extends Controller
{
   public function index()
   {
		$tk=new Controller;
		$access_token= $tk->token_get();
		$acc=json_decode($access_token, true);
		Session::put('token_no', $acc['access_token']);
		$client = new Client();
		$promise1 = $client->requestAsync('POST', 'http://128.199.251.163/app_api/category.php?access_token='.Session::get('token_no'),[ 
		]);
		$promise1->then(
		function (ResponseInterface $res) use(&$category) {
		   $category= $res->getBody() . "\n";
		},
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
				echo $e->getRequest()->getMethod();
			}
		);
		$promise2 = $client->requestAsync('POST', 'http://128.199.251.163/app_api/language.php?access_token='.Session::get('token_no'),[ 
		]);
		$promise2->then(
		function (ResponseInterface $res) use(&$language) {
		   $language= $res->getBody() . "\n";
		},
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
				echo $e->getRequest()->getMethod();
			}
		);
		$promise1->wait();
		$promise2->wait();
		$languages=json_decode($language, true);
		$categories=json_decode($category, true);
		//print_r($latest_data);
		return View::make('news', compact('languages','categories'));
	}
	public function insert(Request $i)
	{
		$tk=new Controller;
		$s=$i->all();
		$access_token= $tk->token_get();
		$acc=json_decode($access_token, true);
		Session::put('token_no', $acc['access_token']);
		$language=$i->get('language');
		$lat=$i->get('latitude');
		$log=$i->get('longitude');
		$country=$i->get('country');
		$category=$i->get('category');
		$cat = implode(', ', $category);
		$title=$i->get('title');
		$text=$i->get('text');
		$user_id=$i->get('user_id');
		$role=$i->get('role');
		$files=$i->file('images');
		if($lat!="0.0" AND $log!="0.0")
		{
			$news_type="1";
			$cc=$country;
		}
		else
		{
			$news_type="0";
			$cc="";
		}
	   	$image_arr=array();
	   	if ($files[0] != '') 
	  	{
		  foreach($files as $file) 
		  {
			$destinationPath = public_path().'/uploads';
			// Get the orginal filname or create the filename of your choice
			$img = $file->getPathName();
			$data = file_get_contents($img);
			$base64[] = base64_encode($data);
	        // Copy the file in our upload folder
	      }
	    }
		$image=array();
		for($x=0; $x<9; $x++)
		{
			if(isset($base64[$x]))
			{
				$image[]=$base64[$x];
			}
			else
			{
				$image[]="";
			}
		}
   		$client = new Client();
		$promise12 = $client->requestAsync('POST', 'http://128.199.251.163/app_api/news.php?access_token='.Session::get('token_no'),[ 
		'form_params' => [
		'category_id'=>$cat,
		'country'=>$cc,
		'language_id'=>$language,
		'user_id'=>$user_id,
		'news_title'=>$title,
		'news_text'=>$text,
		'lat'=>$lat,
		'log'=>$log,
		'post_by'=>$role,
		'news_type'=>$news_type,
		'image1'=>$image[0],
		'image2'=>$image[1],
		'image3'=>$image[2],
		'image4'=>$image[3],
		'image5'=>$image[4],
		'image6'=>$image[5],
		'image7'=>$image[6],
		'image8'=>$image[7],
		'image9'=>$image[8]
		]]);
		$promise12->then(
		function (ResponseInterface $res) use(&$news_insertion) {
			echo $news_insertion= $res->getBody() . "\n";
		},
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
			echo $e->getRequest()->getMethod();
		}
		);
		$promise12->wait();
		$news=json_decode($news_insertion, true);
		$s=$news['status'];
		echo "<pre>";
		print_r($s);
		echo "</pre>";
		if(array_key_exists("message", $s))
		{
			$mes=$news['msg'];
			$i->session()->flash('alert-success',$mes);
			return Redirect::route('/');
		}
		else if(array_key_exists("msg", $s))
		{
			$i->session()->flash('alert-danger', 'ERROR:IN POSTING NEWS');
			return Redirect::route('/');
		}
		else
		{
			$i->session()->flash('alert-danger', 'User Is Temporary Blocked For Add News');
			return Redirect::route('/');
		}
		
	}


public function edit($news_id)
    {
    	$cat_id="1cat_id";
		$user_id=Session::get('user_id');
    	$latitude=Session::get('user_lat');
		$longitude=Session::get('user_lon');
		$view_by="web";
    	if($user_id=='0')
		{
			$uid="";
		}
		else
		{
			$uid=$user_id;
		}
	
    	$tk=new Controller;
    	$access_token= $tk->token_get();
		$acc=json_decode($access_token, true);
		Session::put('token_no', $acc['access_token']);
		$client = new Client();
		$promise = $client->requestAsync('POST', 'http://128.199.251.163/app_api/news_user.php?access_token='.Session::get('token_no'),[ 
		'form_params' => [
		'news_id'=>$news_id,
		'latitude'=>$latitude,
		'longitude'=>$longitude,
		'view_by'=>$view_by,
		'category_id'=>$cat_id,
		'user_id'=>$uid
		]
		]);
		$promise->then(
	    function (ResponseInterface $res) use(&$news_user) {
	       $news_user= $res->getBody() . "\n";
	    },
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
	    		echo $e->getRequest()->getMethod();
			}
		);
	
		$promise1 = $client->requestAsync('POST', 'http://128.199.251.163/app_api/category.php?access_token='.Session::get('token_no'),[ 
		]);
		$promise1->then(
		function (ResponseInterface $res) use(&$category) {
		   $category= $res->getBody() . "\n";
		},
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
				echo $e->getRequest()->getMethod();
			}
		);
		$promise2 = $client->requestAsync('POST', 'http://128.199.251.163/app_api/language.php?access_token='.Session::get('token_no'),[ 
		]);
		$promise2->then(
		function (ResponseInterface $res) use(&$language) {
		   $language= $res->getBody() . "\n";
		},
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
				echo $e->getRequest()->getMethod();
			}
		);
		$promise->wait();
		$promise1->wait();
		$promise2->wait();
		$news=json_decode($news_user, true);
		$languages=json_decode($language, true);
		$categories=json_decode($category, true);
	
	
		return View::make('news_edit', compact('languages', 'categories', 'news'));
    }


	public function print_screen(Request $request)
	{
		
		$url =  $request->get('url');
		echo $location=public_path()."/screenshot/";
		$name_of_screenshot=uniqid();
		$browsershot= new Browsershot();
		$browsershot->setURL($url)->setWidth('1024')->setHeight('5000')->save($location.$name_of_screenshot.".jpg");
		echo $url;
		echo "<br>";
		echo "<img src='http://nearbynews.co/screenshot/$name_of_screenshot.jpg' alt='hello'/>";
		
	}
	
}
