<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Signup;
use App\News;
use App\Images;
use App\Category;
use App\Http\Requests;
use App\Countrycodes;
use View;
use DB;
use Redirect;
use App\Http\Controllers\Controller;
use Session;

use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Promise;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
class IndexController extends Controller
{
   public function index()
    {
		
    	$u=1;
		$index=0;
		$lat=Session::get('user_lat');
		$long=Session::get('user_lon');
		$get_radius=Session::get('radius');
		$get_days=Session::get('days');
		if($get_radius=="" AND $get_days=="")
		{
			$radius="";
			$days="";
		}
		else 
		{
			$radius=$get_radius;
			$days=$get_days;	
		}
    	$tk=new Controller;
    	$access_token= $tk->token_get();
		$acc=json_decode($access_token, true);
		Session::put('token_no', $acc['access_token']);
		$client = new Client();
		
		$promise1 = $client->requestAsync('POST', 'http://128.199.251.163/app_api/latest_news.php?access_token='.Session::get('token_no'),[ 
		'form_params' => [
		'language_id'=>"",
		'user_id'=>"",
		'distance'=>$radius,
		'index'=>$index,
		'time'=>$days,
		'latitude'=>$lat,
		'longitude'=>$long
		]
		]);
		$promise1->then(
	    function (ResponseInterface $res) use(&$latest) {
	       $latest= $res->getBody() . "\n";
	    },
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
	    		echo $e->getRequest()->getMethod();
			}
		);
		
		
		
		$promise2 = $client->requestAsync('POST', 'http://128.199.251.163/app_api/local_news.php?access_token='.Session::get('token_no'),[ 
		'form_params' => [
		'language_id'=>"",
		'user_id'=>$u,
		'distance'=>$radius,
		'index'=>$index,
		'time'=>$days,
		'web'=>"web",
		'latitude'=>$lat,
		'longitude'=>$long
		]
		]);
		$promise2->then(
	    function (ResponseInterface $res) use(&$local) {
	       $local= $res->getBody() . "\n";
	    },
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
	    		echo $e->getRequest()->getMethod();
			}
		);
		
		
		if($days=="")
		{
			$days="1800";
		}
		else 
		{
			$days=$days;	
		}
		
		
		$promise = $client->requestAsync('POST', 'http://128.199.251.163/app_api/country_news.php?access_token='.Session::get('token_no'),[ 
		'form_params' => [
		'language_id'=>"",
		'user_id'=>"",
		'index'=>$index,
		'sort_time'=>$days,
		]
		]);
		$promise->then(
	    function (ResponseInterface $res) use(&$country) {
	       $country= $res->getBody() . "\n";
	    },
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
	    		echo $e->getRequest()->getMethod();
			}
		);
		
		$promise->wait();
		$promise1->wait();
		$promise2->wait();
	 	$latest_data=json_decode($latest, true);
		$local_data=json_decode($local, true);
		$country_data=json_decode($country, true);
		//print_r($latest_data);
		
		return View::make('index', compact('latest_data','local_data','country_data','index'));
    }
	public function index1(Request $i)
	{
		$val1=$i->get('latt');
		$val2=$i->get('lngg');
		$verify=$i->get('verify');
		Session::put('user_lat', $val1);
		Session::put('user_lon', $val2);
		Session::put('user_verify', $verify);
		
	}
	public function set_selected_location(Request $i)
	{
		$val2=$i->get('latt');
		$val1=$i->get('lngg');
		Session::put('user_lat', $val1);
		Session::put('user_lon', $val2);
		return Redirect::back(); 
	}
	public function set_current_location(Request $i)
	{
		$val1=$i->get('latt');
		$val2=$i->get('lngg');
		Session::put('user_lat', $val1);
		Session::put('user_lon', $val2);
		return Redirect::back();
	}
	
	public function filters(Request $f)
	{
		$radius=$f->get('radius');
		$days=$f->get('days');
		$primery=$f->get('primery_filter');
		Session::put('radius', $radius);
		Session::put('days', $days);
		Session::put('primery_filter', $primery);
		return Redirect::route('/');
	}
	
	
	
	
	  public function loadmore(Request $load)
    {
		$index=$load->get('index');	
    	$u=1;
		
		$lat=Session::get('user_lat');
		$long=Session::get('user_lon');
		$get_radius=Session::get('radius');
		$get_days=Session::get('days');
		if($get_radius=="" AND $get_days=="")
		{
			$radius="";
			$days="";
		}
		else 
		{
			$radius=$get_radius;
			$days=$get_days;	
		}
    	$tk=new Controller;
    	$access_token= $tk->token_get();
		$acc=json_decode($access_token, true);
		Session::put('token_no', $acc['access_token']);
		$client = new Client();
		
		$promise1 = $client->requestAsync('POST', 'http://128.199.251.163/app_api/latest_news.php?access_token='.Session::get('token_no'),[ 
		'form_params' => [
		'language_id'=>"",
		'user_id'=>"",
		'distance'=>$radius,
		'index'=>$index,
		'time'=>$days,
		'latitude'=>$lat,
		'longitude'=>$long
		]
		]);
		$promise1->then(
	    function (ResponseInterface $res) use(&$latest) {
	       $latest= $res->getBody() . "\n";
	    },
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
	    		echo $e->getRequest()->getMethod();
			}
		);
		
		
		
		$promise2 = $client->requestAsync('POST', 'http://128.199.251.163/app_api/local_news.php?access_token='.Session::get('token_no'),[ 
		'form_params' => [
		'language_id'=>"",
		'user_id'=>$u,
		'distance'=>$radius,
		'index'=>$index,
		'time'=>$days,
		'web'=>"abc",
		'latitude'=>$lat,
		'longitude'=>$long
		]
		]);
		$promise2->then(
	    function (ResponseInterface $res) use(&$local) {
	       $local= $res->getBody() . "\n";
	    },
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
	    		echo $e->getRequest()->getMethod();
			}
		);
		
		if($days=="")
		{
			$days="1800";
		}
		else 
		{
			$days=$days;	
		}
		
		
		
		$promise = $client->requestAsync('POST', 'http://128.199.251.163/app_api/country_news.php?access_token='.Session::get('token_no'),[ 
		'form_params' => [
		'language_id'=>"",
		'user_id'=>"",
		'index'=>$index,
		'sort_time'=>$days
		]
		]);
		$promise->then(
	    function (ResponseInterface $res) use(&$country) {
	        $country= $res->getBody() . "\n";
	    },
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
	    		echo $e->getRequest()->getMethod();
			}
		);
		
		$promise->wait();
		$promise1->wait();
		$promise2->wait();
	 	$latest_data=json_decode($latest, true);
		$local_data=json_decode($local, true);
		$country_data=json_decode($country, true);
		//print_r($latest_data);
		
		return View::make('index_locational', compact('latest_data','local_data','country_data','index'));
    }
	
	
	  public function country_load(Request $load)
    {
		$index=$load->get('index');	
    	$u=1;
		
		$lat=Session::get('user_lat');
		$long=Session::get('user_lon');
		$get_radius=Session::get('radius');
		$get_days=Session::get('days');
		if($get_radius=="" AND $get_days=="")
		{
			$radius="";
			$days="";
		}
		else 
		{
			$radius=$get_radius;
			$days=$get_days;	
		}
    	$tk=new Controller;
    	$access_token= $tk->token_get();
		$acc=json_decode($access_token, true);
		Session::put('token_no', $acc['access_token']);
		$client = new Client();
		
		$promise1 = $client->requestAsync('POST', 'http://128.199.251.163/app_api/latest_news.php?access_token='.Session::get('token_no'),[ 
		'form_params' => [
		'language_id'=>"",
		'user_id'=>"",
		'distance'=>$radius,
		'index'=>$index,
		'time'=>$days,
		'latitude'=>$lat,
		'longitude'=>$long
		]
		]);
		$promise1->then(
	    function (ResponseInterface $res) use(&$latest) {
	       $latest= $res->getBody() . "\n";
	    },
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
	    		echo $e->getRequest()->getMethod();
			}
		);
		
		
		
		$promise2 = $client->requestAsync('POST', 'http://128.199.251.163/app_api/local_news.php?access_token='.Session::get('token_no'),[ 
		'form_params' => [
		'language_id'=>"",
		'user_id'=>$u,
		'distance'=>$radius,
		'index'=>$index,
		'time'=>$days,
		'web'=>"abc",
		'latitude'=>$lat,
		'longitude'=>$long
		]
		]);
		$promise2->then(
	    function (ResponseInterface $res) use(&$local) {
	       $local= $res->getBody() . "\n";
	    },
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
	    		echo $e->getRequest()->getMethod();
			}
		);
		
		if($days=="")
		{
			$days="1800";
		}
		else 
		{
			$days=$days;	
		}
		
		
		
		$promise = $client->requestAsync('POST', 'http://128.199.251.163/app_api/country_news.php?access_token='.Session::get('token_no'),[ 
		'form_params' => [
		'language_id'=>"",
		'user_id'=>"",
		'index'=>$index,
		'sort_time'=>$days
		]
		]);
		$promise->then(
	    function (ResponseInterface $res) use(&$country) {
	        $country= $res->getBody() . "\n";
	    },
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
	    		echo $e->getRequest()->getMethod();
			}
		);
		
		$promise->wait();
		$promise1->wait();
		$promise2->wait();
	 	$latest_data=json_decode($latest, true);
		$local_data=json_decode($local, true);
		$country_data=json_decode($country, true);
		//print_r($latest_data);
		
		return View::make('index_country', compact('latest_data','local_data','country_data','index'));
    }

}
