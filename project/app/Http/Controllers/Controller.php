<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client;
use Session;
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
	public function token_get () 
	{
		$client = new Client();
    	$promise = $client->requestAsync('POST', 'http://128.199.251.163/app_api/token.php',[ 
		'form_params' => [
		'client_id' => '!@#deepsandroid)(*',
        'client_secret' => 'MY$^%SQL&^%',
        'grant_type'=>'client_credentials'
        ]
        ]);
				
     	$promise->then(
	    function (ResponseInterface $res)  use(&$resp){
	     $resp=$res->getBody();
	    },
	    function (RequestException $e) {
	        echo $e->getMessage() . "\n";
	        echo $e->getRequest()->getMethod();
	    }
		);
		$promise->wait();
		return  $resp;
		Session::put('token_no', $resp);
	}
}
