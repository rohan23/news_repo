<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Redirect;
use App\Http\Controllers\Controller;
use View;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Promise;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use Session;
class LandingController extends Controller
{
	public function landing_api()
    {
		
		$client = new Client();
		
		$promise1 = $client->requestAsync('POST', 'http://128.199.251.163/app_api/landing.php');
		$promise1->then(
	    function (ResponseInterface $res) use(&$landing) {
	       $landing= $res->getBody() . "\n";
	    },
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
	    		echo $e->getRequest()->getMethod();
			}
		);
		
		
		$promise1->wait();
	 	$landing_data=json_decode($landing, true);
		return View::make('landing', compact('landing_data'));
    }
	
	public function save_email(Request $i)
    {
			$email=$i->get('email');
	$users=	DB::table('landing_users')->insert(
    ['email' => $email]
);
if($users>0)
{
	$i->session()->flash('alert-success', 'Thanx for Subscribing Nearbynews ');
	return Redirect::back();
}
		
    }
	
}
