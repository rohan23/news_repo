<?php
namespace App\Http\Controllers;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Promise;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;
use App\Signup;
use App\Http\Requests;
use App\Countrycodes;
use View;
use DB;
use Redirect;
use GuzzleHttp\Client;
use App\Http\Controllers\Controller;
use Session;

class LoginController extends Controller
{
	//Signup User
 	public function insert(Request $i)
    {
    	$tk=new Controller;
    	$access_token= $tk->token_get();
		$acc=json_decode($access_token, true);
		Session::put('token_no', $acc['access_token']);
		$date = date('Y-m-d H:i:s');
		$country_code=$i->get('country_name');
		$get_name=DB::table('country')->where('phonecode', $country_code)->get();
		foreach($get_name as $get_country)
		{
			$country=$get_country->country;
		}
		$name=$i->get('name');
		$image=$i->file('image');
		$data = file_get_contents($image);
		//$base6 = 'data:image/'.';base64,' . base64_encode($data);
		$base64 = base64_encode($data);
		
		$phone_code=$i->get('phone_code');
		$phone_no=$i->get('phone');
		$phone_number=$phone_code.$phone_no;
		$pass1=$i->get('password');
		$pass2=$i->get('password-again');
		//$salt='hr20aa7026';
		//$pass3=md5($salt.$pass1);
		$client = new Client();
		$promise = $client->requestAsync('POST', 'http://128.199.251.163/app_api/signup.php?access_token='.Session::get('token_no'),[ 
		'form_params' => [
		'name'=>$name,
		'country'=>$country,
		'phone'=> $phone_number,
		'password'=>$pass1,
		'profile_pic'=>$base64,
		'gcm_id'=>''
		]
		]);
		$promise->then(
	    function (ResponseInterface $res) use(&$login_resp) {
	        $login_resp= $res->getBody() . "\n";
	    },
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
	    		echo $e->getRequest()->getMethod();
			}
		);
		$promise->wait();
		
		$get_resp=json_decode($login_resp, true);
		
		if($get_resp['status']['message']=="Phone No. Already Registered")
		{
			$i->session()->flash('alert-danger', 'User is Allready Exist');
			return Redirect::back();
		}
		else
		{
			//echo "success";
			$i->session()->flash('alert-success', 'Registered Successfully');
			//$user_phonenumber=$get_resp['data']['phone'];
			$user_phonenumber=$phone_number;
			//return Redirect::route('/');
			return View::make('verify', compact('user_phonenumber'));
		}
	}

    /**
     Login User
     */ 
    public function do_login(Request $a)
    {
    	$tk=new Controller;
    	$access_token= $tk->token_get();
		$acc=json_decode($access_token, true);
		Session::put('token_no', $acc['access_token']);
		//$token="497a37486d9c5da97bc7991895f9d97496c4b35d";
    	$client = new Client();
		
		$phone_code=$a->get('phone_code');
    	$phone=$a->get('phone');
		$phone_no=$phone_code.$phone;
		$password=$a->get('password');
		$role=$a->get('role_type');
		//$salt='hr20aa7026';
		
		//$pass=md5($salt.$password);
		$promise = $client->requestAsync("POST", "http://128.199.251.163/app_api/login.php?access_token=".Session::get('token_no'),[ 
		'form_params' => [
		'phone' => $phone_no,
        'password' => $password,
        'role'=>$role,
        'gcm_id'=>''
        ]
        ]);
		$promise->then(
		    function (ResponseInterface $res) use(&$login_resp) {
		        $login_resp= $res->getBody() . "\n";
		    },
			function (RequestException $e) {
				echo $e->getMessage() . "\n";
		    		echo $e->getRequest()->getMethod();
				}
			);
			$promise->wait();
		$get_resp=json_decode($login_resp, true);
		
		if($get_resp['status']['message']=="Wrong Phone no. Or Password")
		{
			$a->session()->flash('alert-danger', 'Wrong Phone Number OR Password');
			//echo $pass;
			return Redirect::back();
		}
		else
		{
			//echo "success";
			$a->session()->flash('alert-success', 'Login Successfully');
			Session::put('username', $get_resp['data']['name']);
			Session::put('userimage', $get_resp['data']['image']);
			Session::put('user_id', $get_resp['data']['id']);
			Session::put('role', $get_resp['data']['role_type']);
			Session::put('country', $get_resp['data']['country']);
			return Redirect::route('/');
		}
	}
	
	
	//Verify User
	public function verify_otp(Request $i)
    {
    	$tk=new Controller;
    	$access_token= $tk->token_get();
		$acc=json_decode($access_token, true);
		Session::put('token_no', $acc['access_token']);
		$otp=$i->get('user_otp');
		$phone_number=$i->get('user_phoneno');
		$client = new Client();
		$promise = $client->requestAsync('POST', 'http://128.199.251.163/app_api/verify_user.php?access_token='.Session::get('token_no'),[ 
		'form_params' => [
		'otp'=>$otp,
		'phone'=> $phone_number
		]
		]);
		$promise->then(
	    function (ResponseInterface $res) use(&$get_verify) {
	        $get_verify= $res->getBody() . "\n";
	    },
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
	    		echo $e->getRequest()->getMethod();
			}
		);
		$promise->wait();
		$get_resp_verify=json_decode($get_verify, true);
		print_r($get_resp_verify);
		if($get_resp_verify['status']['message']=="Phone No. Already Registered")
		{
			$i->session()->flash('alert-danger', 'User is Allready Exist');
			return Redirect::route('/');
		}
		else
		{
			//echo "success";
			$i->session()->flash('alert-success', 'User Verified Successfully');
			Session::put('user_phone', $get_resp_verify['data']['phone']);
			Session::put('username', $get_resp_verify['data']['name']);
			Session::put('userimage', $get_resp_verify['data']['image']);
			Session::put('user_id', $get_resp_verify['data']['id']);
			Session::put('role', $get_resp_verify['data']['role_type']);
			Session::put('country', $get_resp_verify['data']['country']);
			return Redirect::route('/');
		}
		
	}
	public function do_logout(Request $a)
    {
    	Session:: forget('username');
		Session:: forget('userimage');
		Session:: forget('user_id');
		Session:: forget('user_lat');
		Session:: forget('user_lon');
		Session:: forget('role');
		Session:: forget('country');
		Session:: forget('radius');
		Session:: forget('days');
		Session:: forget('primery_filter');
    	return Redirect::route('/');
	}
	public function forgot_password()
	{
		echo "hello";
	} 
}
