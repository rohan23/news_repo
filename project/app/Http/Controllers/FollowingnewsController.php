<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Signup;
use App\News;
use App\Images;
use App\Category;
use App\Http\Requests;
use App\Countrycodes;
use View;
use DB;
use Redirect;
use App\Http\Controllers\Controller;
use Session;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Promise;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;

class FollowingnewsController extends Controller
{

	public function follow($user_id)
    {
		
		$latitude=Session::get('user_lat');
		$longitude=Session::get('user_lon');
		
		
	
    	$tk=new Controller;
    	$access_token= $tk->token_get();
		$acc=json_decode($access_token, true);
		Session::put('token_no', $acc['access_token']);
		$client = new Client();
		$promise = $client->requestAsync('POST', 'http://128.199.251.163/app_api/get_follower.php?access_token='.Session::get('token_no'),[ 
		'form_params' => [
		'user_id'=>$user_id,
		'distance'=>"",
		'time'=>"",
		'latitude'=>$latitude,
		'longitude'=>$longitude
		
		]
		]);
		$promise->then(
	    function (ResponseInterface $res) use(&$follow) {
	       $follow= $res->getBody() . "\n";
	    },
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
	    		echo $e->getRequest()->getMethod();
			}
		);
		
		$promise11 = $client->requestAsync('POST', 'http://128.199.251.163/app_api/get_promotion.php?access_token='.Session::get('token_no'),[ 
		'form_params' => [
		'index'=>0,
		'distance'=>"",
		'time'=>"",
		'latitude'=>$latitude,
		'longitude'=>$longitude
		
		]
		]);
		$promise11->then(
	    function (ResponseInterface $res) use(&$promotions) {
	       $promotions= $res->getBody() . "\n";
	    },
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
	    		echo $e->getRequest()->getMethod();
			}
		);
		
		$promise->wait();
		$promise11->wait();
		$promotion=json_decode($promotions, true);
	 	$following_data=json_decode($follow, true);
		return View::make('following_news', compact('following_data','promotion'));
    }
}
