<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Signup;
use App\News;
use App\Images;
use App\Category;
use App\Http\Requests;
use App\Countrycodes;
use View;
use DB;
use Redirect;
use App\Http\Controllers\Controller;
use Session;

use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Promise;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;



class NewslistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     
    public function get_news_front()
    {
    	$tk=new Controller;
    	$access_token= $tk->token_get();
		$acc=json_decode($access_token, true);
		Session::put('token_no', $acc['access_token']);
		$country_name="India";
		$client = new Client();
		$promise = $client->requestAsync('POST', 'http://128.199.251.163/app_api/get_news_web.php?access_token='.Session::get('token_no'),[ 
		'form_params' => [
		'country_name'=>'India'
		]
		]);
		$promise->then(
	    function (ResponseInterface $res) {
	        echo $res->getBody() . "\n";
	    },
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
	    		echo $e->getRequest()->getMethod();
			}
		);
		$promise->wait();
	}
     
     
     
    public function index()
    {
		return View::make('index');
    }
	
	//Get News List in Category List
	public function news_category($cat_name)
    {
		$search =  ' or ' ;
		$cat1=str_replace($search, "/", $cat_name);
    	$get_cat=DB::table('category')->where('category', $cat1)->get();
		foreach($get_cat as $cat_get)
		{
			$c_id=$cat_get->id;
		}
		$latitude=Session::get('user_lat');
		$longitude=Session::get('user_lon');
		$get_radius=Session::get('radius');
		$get_days=Session::get('days');
		if($get_radius=="" AND $get_days=="")
		{
			$radius="";
			$days="";
		}
		else 
		{
			$radius=$get_radius;
			$days=$get_days;	
		}
		//for count how many news for this category
		$getnews = DB::table('news')
           ->join('images', 'news.id', '=', 'images.news_id')
            ->select('news.*', 'images.*')
			->where('category', 'like', '%'.$c_id.'%', 'AND', 'status', 'approved')
            ->get();
		$newscount= count($getnews);
		$index=1;
		$in=$index-1;
		
		$user_id=Session::get('user_id');
    	if($user_id=='')
		{
			$uid="";
		}
		else
		{
			$uid=$user_id;
		}
    	$tk=new Controller;
    	$access_token= $tk->token_get();
		$acc=json_decode($access_token, true);
		Session::put('token_no', $acc['access_token']);
		$client = new Client();
		$promise = $client->requestAsync('POST', 'http://128.199.251.163/app_api/get_news.php?access_token='.Session::get('token_no'),[ 
		'form_params' => [
		'category_id'=>$c_id,
		'user_id'=>$uid,
		'index'=>$in,
		'language_id'=>'',
		'distance'=>$radius,
		'time'=>$days,
		'latitude'=>$latitude,
		'web'=>$c_id,
		'longitude'=>$longitude
		
		]
		]);
		$promise->then(
	    function (ResponseInterface $res) use(&$news_list) {
	       $news_list= $res->getBody() . "\n";
	    },
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
	    		echo $e->getRequest()->getMethod();
			}
		);
		
		$promise11 = $client->requestAsync('POST', 'http://128.199.251.163/app_api/get_promotion.php?access_token='.Session::get('token_no'),[ 
		'form_params' => [
		'index'=>0,
		'distance'=>$radius,
		'time'=>$days,
		'latitude'=>$latitude,
		'longitude'=>$longitude
		
		]
		]);
		$promise11->then(
	    function (ResponseInterface $res) use(&$promotions) {
	       $promotions= $res->getBody() . "\n";
	    },
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
	    		echo $e->getRequest()->getMethod();
			}
		);
		
		
		$promise11->wait();
	 	$promise->wait();
	 	$get_list=json_decode($news_list, true);
		$promotion=json_decode($promotions, true);
		return View::make('news_category', compact('get_list','cat_name','index','newscount','promotion'));
    }
	
	//Get News Detail
	
	public function news_detail($news_id)
    {
    	$cat_id="1cat_id";
    	$latitude=Session::get('user_lat');
		$longitude=Session::get('user_lon');
		$view_by="web";
		
		$user_id=Session::get('user_id');
    	if($user_id=='')
		{
			$uid="";
		}
		else
		{
			$uid=$user_id;
		}
		$get_comments=DB::table('news_comments')->where([
			    ['news_id', '=', $news_id],
			    ['user_id', '=', $uid],
			])->get();
    	$tk=new Controller;
    	$access_token= $tk->token_get();
		$acc=json_decode($access_token, true);
		Session::put('token_no', $acc['access_token']);
		$client = new Client();
		$promise = $client->requestAsync('POST', 'http://128.199.251.163/app_api/news_user.php?access_token='.Session::get('token_no'),[ 
		'form_params' => [
		'news_id'=>$news_id,
		'latitude'=>$latitude,
		'longitude'=>$longitude,
		'view_by'=>$view_by,
		'category_id'=>$cat_id,
		'user_id'=>$uid
		]
		]);
		$promise->then(
	    function (ResponseInterface $res) use(&$like_status) {
	       $like_status= $res->getBody() . "\n";
	    },
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
	    		echo $e->getRequest()->getMethod();
			}
		);
		
		$promise1 = $client->requestAsync('POST', 'http://128.199.251.163/app_api/get_comments.php?access_token='.Session::get('token_no'),[ 
		'form_params' => [
		'news_id'=>$news_id,
		'user_id'=>$uid
		]
		]);
		$promise1->then(
	    function (ResponseInterface $res) use(&$comment_status) {
	      $comment_status= $res->getBody() . "\n";
	    },
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
	    		echo $e->getRequest()->getMethod();
			}
		);
		
		
		$promise2 = $client->requestAsync('POST', 'http://128.199.251.163/app_api/language.php?access_token='.Session::get('token_no'),[ 
		]);
		$promise2->then(
		function (ResponseInterface $res) use(&$language) {
		   $language= $res->getBody() . "\n";
		},
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
				echo $e->getRequest()->getMethod();
			}
		);
		
		$promise3 = $client->requestAsync('POST', 'http://128.199.251.163/app_api/category.php?access_token='.Session::get('token_no'),[ 
		]);
		$promise3->then(
		function (ResponseInterface $res) use(&$categories) {
		   $categories= $res->getBody() . "\n";
		},
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
				echo $e->getRequest()->getMethod();
			}
		);
		
		$promise->wait();
		$promise1->wait();
		$promise2->wait();
		$promise3->wait();
		$languages=json_decode($language, true);
		$category=json_decode($categories, true);
	 	$get_comm=json_decode($comment_status, true);
		$getnews=json_decode($like_status, true);
		return View::make('news_detail', compact('getnews', 'get_comments', 'get_comm','languages','category'));
    }
	
	//Set News Like and Unlike
	public function likeunlike($news_id, $user_id)
	{
		$tk=new Controller;
    	$access_token= $tk->token_get();
		$acc=json_decode($access_token, true);
		Session::put('token_no', $acc['access_token']);
		$client = new Client();
		$promise = $client->requestAsync('POST', 'http://128.199.251.163/app_api/likes_api.php?access_token='.Session::get('token_no'),[ 
		'form_params' => [
		'news_id'=>$news_id,
		'user_id'=>$user_id
		]
		]);
		$promise->then(
	    function (ResponseInterface $res) use(&$like_status) {
	        $like_status= $res->getBody() . "\n";
	    },
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
	    		echo $e->getRequest()->getMethod();
			}
		);
		$promise->wait();
		
		$get_status=json_decode($like_status, true);
		if($get_status['status']['message']=="liked")
		{
			//$a->session()->flash('alert-danger', 'Wrong Phone Number OR Password');
			Session::put('message', 'liked');
			return Redirect::back();
			//return Redirect::route('/news_detail/'.$news_id);
		}
		else{
			Session::put('message', 'unliked');
			return Redirect::back();
			//return Redirect::route('http://nearbynews.co/news_detail/'.$news_id);
		}
	}
	
	//Give Flag On News
	public function report(Request $report)
	{
		$nid=$report->get('n_id');
		$uid=$report->get('u_id');
		$message=$report->get('message');
		$tk=new Controller;
    	$access_token= $tk->token_get();
		$acc=json_decode($access_token, true);
		Session::put('token_no', $acc['access_token']);
		$client = new Client();
		$promise = $client->requestAsync('POST', 'http://128.199.251.163/app_api/flag_api.php?access_token='.Session::get('token_no'),[ 
		'form_params' => [
		'news_id'=>$nid,
		'user_id'=>$uid,
		'comment'=>$message
		
		]
		]);
		$promise->then(
	    function (ResponseInterface $res) use(&$report_status) {
	        $report_status= $res->getBody() . "\n";
	    },
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
	    		echo $e->getRequest()->getMethod();
			}
		);
		$promise->wait();
		
		$get_report=json_decode($report_status, true);
		if($get_report['status']['message']=="Flagged")
		{
			//$a->session()->flash('alert-danger', 'Wrong Phone Number OR Password');
			Session::put('message', 'Flagged');
			
			return Redirect::route('/');
			//return Redirect::route('/news_detail/'.$news_id);
		}
		else{
			Session::put('message', 'Not Flagged');
			return Redirect::route('/');
			//return Redirect::route('http://nearbynews.co/news_detail/'.$news_id);
		}
	}
	//News Delete
	public function news_delete($id)
	{
		$tk=new Controller;
    	$access_token= $tk->token_get();
		$acc=json_decode($access_token, true);
		Session::put('token_no', $acc['access_token']);
		$client = new Client();
		$promise = $client->requestAsync('POST', 'http://128.199.251.163/app_api/news_delete.php?access_token='.Session::get('token_no'),[ 
		'form_params' => [
		'id'=>$id
		]
		]);
		$promise->then(
	    function (ResponseInterface $res) use(&$delete_news) {
	       $delete_news= $res->getBody() . "\n";
	    },
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
	    		echo $e->getRequest()->getMethod();
			}
		);
		$promise->wait();
		$delete_success=json_decode($delete_news, true);
		
		if($delete_success['status']['message']=="News Deleted Successfully")
		{
			$mes="News Deleted Successfully";
			session()->flash('alert-success',$mes);
			return Redirect::back();
		}
		else
		{
			$mes="News not deleted";
			session()->flash('alert-danger',$mes);
			return Redirect::back();
		}
		
	}


	public function news_translate(Request $request )
	{
		$translate_code=$request->get('translate_code');
		$news_id=$request->get('news_id');
		$news_title1=$request->get('news_title');
		$news_title2=ltrim($news_title1, '"');
		$news_title=rtrim($news_title2, '"');
		
		$news_text1=$request->get('news_text');
		$news_text2=ltrim($news_text1, '"');
		$news_text=rtrim($news_text2, '"');
		
		$existing_lang=$request->get('existing_lang');
		
		if($existing_lang=="English")
		{
			$existing_code="en";
		}
		if($existing_lang=="Malay")
		{
			$existing_code="ms";
		}
		if($existing_lang=="Tamil")
		{
			$existing_code="ta";
		}
		if($existing_lang=="Chinese")
		{
			$existing_code="zh-CN";
		}
		if($translate_code!=$existing_code)
		{
		$tk=new Controller;
    	$access_token= $tk->token_get();
		$acc=json_decode($access_token, true);
		Session::put('token_no', $acc['access_token']);
		$client = new Client();
		$promise = $client->requestAsync('POST', 'http://128.199.251.163/app_api/translation.php',[ 
		'form_params' => [
		'news_id'=>$news_id,
		'language'=>$translate_code,
		'header'=>$news_title,
		'description'=>$news_text,
		'source'=>$existing_code
		]
		]);
		$promise->then(
	    function (ResponseInterface $res) use(&$translate) {
	     $translate= $res->getBody() . "\n";
	    },
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
	    		echo $e->getRequest()->getMethod();
			}
		);
		$promise->wait();
		$translation=json_decode($translate, true);
		
		$t_text=$translation['news'][0]['text'];
		$t_lang=$translation['news'][0]['language'];
		$t_header=$translation['news'][0]['header'];
			if($t_header=="" OR $t_text=="" OR $t_lang=="")
			{
				$translation=array();
				$translation['news'][0]['header']=$news_title;
				$translation['news'][0]['text']=$news_text;
				$translation['news'][0]['language']=$existing_lang;
				return View::make('translate_news', compact('translation'));	
			}
			else
			{
				return View::make('translate_news', compact('translation'));
			}
			
		}
		else
		{
			$translation=array();
			$translation['news'][0]['header']=$news_title;
			$translation['news'][0]['text']=$news_text;
			$translation['news'][0]['language']=$existing_lang;
		}
		return View::make('translate_news', compact('translation'));
		
	}
	
	
	
	
	
	//Get News List in Category List
	public function loadmore(Request $request)
    {
    	$get_radius=Session::get('radius');
		$get_days=Session::get('days');
		if($get_radius=="" AND $get_days=="")
		{
			$radius="";
			$days="";
		}
		else 
		{
			$radius=$get_radius;
			$days=$get_days;	
		}
    	$cat_name=$request->get('category_name');
		$user_id=$request->get('user_id');
		$index=$request->get('index');
    
		$search =  ' or ' ;
		$cat1=str_replace($search, "/", $cat_name);
		
	
    	$get_cat=DB::table('category')->where('category', $cat1)->get();
		foreach($get_cat as $cat_get)
		{
			$c_id=$cat_get->id;
		}
		$latitude=Session::get('user_lat');
		$longitude=Session::get('user_lon');
		
	
		
		$in=$index-1;
    	if($user_id=='0')
		{
			$uid="";
		}
		else
		{
			$uid=$user_id;
		}
    	$tk=new Controller;
    	$access_token= $tk->token_get();
		$acc=json_decode($access_token, true);
		Session::put('token_no', $acc['access_token']);
		$client = new Client();
		$promise = $client->requestAsync('POST', 'http://128.199.251.163/app_api/get_news.php?access_token='.Session::get('token_no'),[ 
		'form_params' => [
		'category_id'=>$c_id,
		'user_id'=>$uid,
		'index'=>$in,
		'language_id'=>'',
		'distance'=>$radius,
		'time'=>$days,
		'latitude'=>$latitude,
		'web'=>$c_id,
		'longitude'=>$longitude
		
		]
		]);
		$promise->then(
	    function (ResponseInterface $res) use(&$news_list) {
	        $news_list= $res->getBody() . "\n";
	    },
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
	    		echo $e->getRequest()->getMethod();
			}
		);
	
		
		
	 	$promise->wait();
	 	$get_list=json_decode($news_list, true);
		return View::make('category_load', compact('get_list','cat_name','index'));
    }
	//local Latest Country Category News
	public function local_category($cat_name)
    {
    	
		$index=0;
		$user_id=Session::get('user_id');
		if($user_id=="")
		{
			$user="";
		}
		else 
		{
			$user=$user_id;	
		}
		$lat=Session::get('user_lat');
		$long=Session::get('user_lon');
		$get_radius=Session::get('radius');
		$get_days=Session::get('days');
		if($get_radius=="" AND $get_days=="")
		{
			$radius="";
			$days="";
		}
		else 
		{
			$radius=$get_radius;
			$days=$get_days;	
		}
    	$tk=new Controller;
    	$access_token= $tk->token_get();
		$acc=json_decode($access_token, true);
		Session::put('token_no', $acc['access_token']);
		$client = new Client();
		
		if($cat_name=="Latest")
		{
				$promise = $client->requestAsync('POST', 'http://128.199.251.163/app_api/latest_news.php?access_token='.Session::get('token_no'),[ 
			'form_params' => [
			'language_id'=>"",
			'user_id'=>$user,
			'distance'=>$radius,
			'index'=>$index,
			'time'=>$days,
			'latitude'=>$lat,
			'longitude'=>$long
			]
			]);
		}
		if($cat_name=="Local")
		{
				$promise = $client->requestAsync('POST', 'http://128.199.251.163/app_api/local_news.php?access_token='.Session::get('token_no'),[ 
			'form_params' => [
			'language_id'=>"",
			'user_id'=>$user,
			'distance'=>$radius,
			'index'=>$index,
			'time'=>$days,
			'web'=>"web",
			'latitude'=>$lat,
			'longitude'=>$long
			]
			]);	
		}
		if($cat_name=="Country")
		{
			if($days=="")
			{
				$days="1800";
			}
			else 
			{
				$days=$days;	
			}
				$promise = $client->requestAsync('POST', 'http://128.199.251.163/app_api/country_news.php?access_token='.Session::get('token_no'),[ 
			'form_params' => [
			'language_id'=>"",
			'user_id'=>$user,
			'index'=>$index,
			'sort_time'=>$days,
			]
			]);	
		}
		
		
		
		
		$promise->then(
	    function (ResponseInterface $res) use(&$latest) {
	       $latest= $res->getBody() . "\n";
	    },
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
	    		echo $e->getRequest()->getMethod();
			}
		);
		
		$promise->wait();
		
		$latest_data=json_decode($latest, true);
		if($cat_name=="Country")
		{
			return View::make('country_news', compact('latest_data'));	
		}
		if($cat_name=="Latest")
		{
			return View::make('latest_news', compact('latest_data'));	
		}
		if($cat_name=="Local")
		{
			return View::make('local_news', compact('latest_data'));	
		}
    }
	public function map_news($news_id)
	{
		$select=DB::select("select lat, log from news where id='$news_id'");
		foreach($select as $data)
		{
			$latt=$data->lat;
			$lngg=$data->log;
		}
		return View::make('location_map', compact('latt', 'lngg'));
	}
}
