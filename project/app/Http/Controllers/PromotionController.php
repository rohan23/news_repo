<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Signup;
use App\News;
use App\Images;
use App\Category;
use App\Http\Requests;
use App\Countrycodes;
use View;
use DB;
use Redirect;
use App\Http\Controllers\Controller;
use Session;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Promise;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;


class PromotionController extends Controller
{
    public function promotions()
    {
		
		$latitude=Session::get('user_lat');
		$longitude=Session::get('user_lon');
		$get_radius=Session::get('radius');
		$get_days=Session::get('days');
		if($get_radius=="" AND $get_days=="")
		{
			$radius="";
			$days="";
		}
		else 
		{
			$radius=$get_radius;
			$days=$get_days;	
		}
		
		
		$index=0;
    	$tk=new Controller;
    	$access_token= $tk->token_get();
		$acc=json_decode($access_token, true);
		Session::put('token_no', $acc['access_token']);
		$client = new Client();
		$promise = $client->requestAsync('POST', 'http://128.199.251.163/app_api/get_promotion.php?access_token='.Session::get('token_no'),[ 
		'form_params' => [
		'index'=>$index,
		'distance'=>$radius,
		'time'=>$days,
		'latitude'=>$latitude,
		'longitude'=>$longitude
		
		]
		]);
		$promise->then(
	    function (ResponseInterface $res) use(&$promotions) {
	       $promotions= $res->getBody() . "\n";
	    },
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
	    		echo $e->getRequest()->getMethod();
			}
		);
		
		
		$promise->wait();
	 	$promotion=json_decode($promotions, true);
		return View::make('promotions', compact('promotion','index'));
    }
	
	
		public function news_detail($p_id)
    {
    	$latitude=Session::get('user_lat');
		$longitude=Session::get('user_lon');
		$view_by="web";
		$user_id=Session::get('user_id');
    	if($user_id=='')
		{
			$uid="";
		}
		else
		{
			$uid=$user_id;
		}
		
    	$tk=new Controller;
    	$access_token= $tk->token_get();
		$acc=json_decode($access_token, true);
		Session::put('token_no', $acc['access_token']);
		$client = new Client();
		$promise = $client->requestAsync('POST', 'http://128.199.251.163/app_api/promotion_detail.php?access_token='.Session::get('token_no'),[ 
		'form_params' => [
		'promotion_id'=>$p_id,
		'latitude'=>$latitude,
		'longitude'=>$longitude,
		'user_id'=>$uid
		]
		]);
		$promise->then(
	    function (ResponseInterface $res) use(&$promotion) {
	       $promotion= $res->getBody() . "\n";
	    },
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
	    		echo $e->getRequest()->getMethod();
			}
		);
		
		$promise1 = $client->requestAsync('POST', 'http://128.199.251.163/app_api/get_promotion_comments.php?access_token='.Session::get('token_no'),[ 
		'form_params' => [
		'promotion_id'=>$p_id,
		'user_id'=>$uid
		]
		]);
		$promise1->then(
	    function (ResponseInterface $res) use(&$comment) {
	      $comment= $res->getBody() . "\n";
	    },
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
	    		echo $e->getRequest()->getMethod();
			}
		);
		
		
		
		
		$promise->wait();
		$promise1->wait();
	
	 	$comments=json_decode($comment, true);
		$promotions=json_decode($promotion, true);
		return View::make('promotion_detail', compact('comments', 'promotions'));
    }


		public function likedislike($p_id, $user_id)
	{
		$tk=new Controller;
    	$access_token= $tk->token_get();
		$acc=json_decode($access_token, true);
		Session::put('token_no', $acc['access_token']);
		$client = new Client();
		$promise = $client->requestAsync('POST', 'http://128.199.251.163/app_api/promotion_likes.php?access_token='.Session::get('token_no'),[ 
		'form_params' => [
		'promotion_id'=>$p_id,
		'user_id'=>$user_id
		]
		]);
		$promise->then(
	    function (ResponseInterface $res) use(&$like_status) {
	        $like_status= $res->getBody() . "\n";
	    },
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
	    		echo $e->getRequest()->getMethod();
			}
		);
		$promise->wait();
		
		$get_status=json_decode($like_status, true);
		if($get_status['status']['message']=="liked")
		{
			//$a->session()->flash('alert-danger', 'Wrong Phone Number OR Password');
			Session::put('message', 'liked');
			return Redirect::back();
			//return Redirect::route('/news_detail/'.$news_id);
		}
		else{
			Session::put('message', 'unliked');
			return Redirect::back();
			//return Redirect::route('http://nearbynews.co/news_detail/'.$news_id);
		}
	}
	
	
	public function report(Request $report)
	{
		$pid=$report->get('p_id');
		$uid=$report->get('u_id');
		$message=$report->get('message');
		$tk=new Controller;
    	$access_token= $tk->token_get();
		$acc=json_decode($access_token, true);
		Session::put('token_no', $acc['access_token']);
		$client = new Client();
		$promise = $client->requestAsync('POST', 'http://128.199.251.163/app_api/promotion_flag.php?access_token='.Session::get('token_no'),[ 
		'form_params' => [
		'promotion_id'=>$pid,
		'user_id'=>$uid,
		'comment'=>$message
		
		]
		]);
		$promise->then(
	    function (ResponseInterface $res) use(&$report_status) {
	        $report_status= $res->getBody() . "\n";
	    },
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
	    		echo $e->getRequest()->getMethod();
			}
		);
		$promise->wait();
		
		$get_report=json_decode($report_status, true);
		if($get_report['status']['message']=="Flagged")
		{
			$report->session()->flash('alert-success', 'Sucess: Successfully Report The Promotion');
			Session::put('message', 'Flagged');
			
			return Redirect::route('/');
			//return Redirect::route('/news_detail/'.$news_id);
		}
		else{
			$report->session()->flash('alert-danger', 'Error: in reporting news');
			Session::put('message', 'Not Flagged');
			return Redirect::back();
			//return Redirect::route('http://nearbynews.co/news_detail/'.$news_id);
		}
	}


	public function comment(Request $comment)
	{
		$tk=new Controller;
    	$access_token= $tk->token_get();
		$acc=json_decode($access_token, true);
		Session::put('token_no', $acc['access_token']);
		$p_id=$comment->get('promotion_id');
		$user_id=$comment->get('user_id');
		$get_comment=$comment->get('comment');
		$client = new Client();
		$promise = $client->requestAsync('POST', 'http://128.199.251.163/app_api/promotion_comment.php?access_token='.Session::get('token_no'),[ 
		'form_params' => [
		'promotion_id'=>$p_id,
		'user_id'=>$user_id,
		'comment'=>$get_comment
		]
		]);
		$promise->then(
	    function (ResponseInterface $res) use(&$comment_resp) {
	        echo $comment_resp= $res->getBody() . "\n";
	    },
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
	    		echo $e->getRequest()->getMethod();
			}
		);
		$promise->wait();
		
		$get_status=json_decode($comment_resp, true);
		if($get_status['status']['message']=="Comment Added Successfully")
		{
			$comment->session()->flash('alert-success', 'Success:Comment Added Successfully');
			Session::put('message', 'Comment Added Successfully');
			return Redirect::back();
			
		}
		else{
			$comment->session()->flash('alert-danger', 'Error');
			Session::put('message', 'Not Added Successfully');
			return Redirect::back();
			
		}
		
	}



		public function deletecomment($p_id,$c_id)
	{
		$tk=new Controller;
    	$access_token= $tk->token_get();
		$acc=json_decode($access_token, true);
		Session::put('token_no', $acc['access_token']);
		$client = new Client();
		$promise = $client->requestAsync('POST', 'http://128.199.251.163/app_api/promotion_comment_delete.php?access_token='.Session::get('token_no'),[ 
		'form_params' => [
		'promotion_id'=>$p_id,
		'comment_id'=>$c_id
		]
		]);
		$promise->then(
	    function (ResponseInterface $res) use(&$comment_resp) {
	        echo $comment_resp= $res->getBody() . "\n";
	    },
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
	    		echo $e->getRequest()->getMethod();
			}
		);
		$promise->wait();
		
		$get_status=json_decode($comment_resp, true);
		if($get_status['scalar']=="Comment Deleted")
		{
			//$comment->session()->flash('alert-success', 'Success:Comment Deleted Successfully');
			Session::put('alert-success', 'Success:Comment Deleted Successfully');
			return Redirect::back();
			//return Redirect::route('/news_detail/'.$news_id);
		}
		else{
			//$comment->session()->flash('alert-danger', 'Error ');
			Session::put('alert-danger', 'Error');
			return Redirect::back();
			//return Redirect::route('http://nearbynews.co/news_detail/'.$news_id);
		}
		
	}

	public function likecomment($user_id,$comment_id)
	{
		$tk=new Controller;
    	$access_token= $tk->token_get();
		$acc=json_decode($access_token, true);
		Session::put('token_no', $acc['access_token']);
		$client = new Client();
		$promise = $client->requestAsync('POST', 'http://128.199.251.163/app_api/promotion_comment_likes.php?access_token='.Session::get('token_no'),[ 
		'form_params' => [
		'user_id'=>$user_id,
		'comment_id'=>$comment_id
		]
		]);
		$promise->then(
	    function (ResponseInterface $res) use(&$commentlike_resp) {
	        echo $commentlike_resp= $res->getBody() . "\n";
	    },
		function (RequestException $e) {
			echo $e->getMessage() . "\n";
	    		echo $e->getRequest()->getMethod();
			}
		);
		$promise->wait();
		
			return Redirect::back();
		
		
	}

}
